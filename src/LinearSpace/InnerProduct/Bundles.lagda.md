---
title: Inner Product Bundles
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

module LinearSpace.InnerProduct.Bundles where

-- stdlib
open import Algebra.Bundles
open import Algebra.Module.Bundles
open import Algebra.Module.Ordered.Partial.Bundles
open import Algebra.Star.Bundles using (StarCommutativeSemiring)
open import Algebra.Star.Ordered.Partial.Bundles
open import Level

-- carya
import Maps.Linear.Construct.Functional as LF
open import LinearSpace.InnerProduct.Structures

private
    variable
      a ℓa ℓa₁ ℓa₂ r ℓr ℓr₁ ℓr₂ : Level
```

</details>

```agda
record ProtoInnerProduct
  (𝓡 : StarCommutativeSemiring r ℓr)
  (a ℓa : Level)
  : Set (r ⊔ ℓr ⊔ suc (a ⊔ ℓa))
  where

  constructor mk

  field
    Carrier : LeftSemimodule (StarCommutativeSemiring.semiring 𝓡) a ℓa

  open LeftSemimodule Carrier renaming (Carrierᴹ to A)
  open LF (StarCommutativeSemiring.commutativeSemiring 𝓡)

  field
    f : A → LinearFunctional Carrier
    isProtoInnerProduct : IsProtoInnerProduct {𝓡 = 𝓡} f

  open IsProtoInnerProduct isProtoInnerProduct public

  module leftSemimodule = LeftSemimodule Carrier

  open LeftSemimodule Carrier public
```

```agda
record InnerProduct
  (𝓡 : StarCommutativeSemiring r ℓr)
  (a ℓa : Level)
  : Set (r ⊔ ℓr ⊔ suc (a ⊔ ℓa))
  where

  constructor mk

  field
    Carrier : LeftSemimodule (StarCommutativeSemiring.semiring 𝓡) a ℓa

  open LeftSemimodule Carrier renaming (Carrierᴹ to A)
  open LF (StarCommutativeSemiring.commutativeSemiring 𝓡)

  field
    f : A → LinearFunctional Carrier
    isInnerProduct : IsInnerProduct {𝓡 = 𝓡} f

  open IsInnerProduct isInnerProduct public

  protoInnerProduct : ProtoInnerProduct 𝓡 _ _
  protoInnerProduct = record {isProtoInnerProduct = isProtoInnerProduct}

  open ProtoInnerProduct protoInnerProduct public
    using (module leftSemimodule)

  open leftSemimodule public
```

```agda
record SymmetricInnerProduct
  (𝓡 : StarCommutativeSemiring r ℓr)
  (a ℓa : Level)
  : Set (r ⊔ ℓr ⊔ suc (a ⊔ ℓa))
  where
  constructor mk

  field
    Carrier : Semimodule (StarCommutativeSemiring.commutativeSemiring 𝓡) a ℓa

  open Semimodule Carrier renaming (Carrierᴹ to A)
  open LF (StarCommutativeSemiring.commutativeSemiring 𝓡)

  field
    f : A → LinearFunctional leftSemimodule
    isSymmetricInnerProduct : IsSymmetricInnerProduct {𝓡 = 𝓡} {𝓜ᴬ = Carrier} f

  open IsSymmetricInnerProduct isSymmetricInnerProduct public

  innerProduct : InnerProduct _ _ _
  innerProduct = record {isInnerProduct = isInnerProduct}

  open InnerProduct innerProduct public
    using (module leftSemimodule)

  open leftSemimodule public
```

```agda
record SemiDefiniteInnerProduct
  (𝓡 : StarCommutativeSemiring r ℓr)
  (a ℓa : Level)
  : Set (r ⊔ ℓr ⊔ suc (a ⊔ ℓa))
    where
  constructor mk

  field
    Carrier : LeftSemimodule (StarCommutativeSemiring.semiring 𝓡) a ℓa

  open LeftSemimodule Carrier renaming (Carrierᴹ to A)
  open LF (StarCommutativeSemiring.commutativeSemiring 𝓡)

  field
    f : A → LinearFunctional Carrier
    isSemiDefiniteInnerProduct : IsSemiDefiniteInnerProduct {𝓡 = 𝓡} f

  open IsSemiDefiniteInnerProduct isSemiDefiniteInnerProduct public

  innerProduct : InnerProduct _ _ _
  innerProduct = record {isInnerProduct = isInnerProduct}

  open InnerProduct innerProduct public
    using (module leftSemimodule)

  open leftSemimodule public
```

```agda
record DefiniteInnerProduct
  (𝓡 : StarCommutativeSemiring r ℓr)
  (a ℓa : Level)
  : Set (r ⊔ ℓr ⊔ suc (a ⊔ ℓa))
    where

  constructor mk

  field
    Carrier : LeftSemimodule (StarCommutativeSemiring.semiring 𝓡) a ℓa

  open LeftSemimodule Carrier renaming (Carrierᴹ to A)
  open LF (StarCommutativeSemiring.commutativeSemiring 𝓡)

  field
    f : A → LinearFunctional Carrier
    isDefiniteInnerProduct : IsDefiniteInnerProduct {𝓡 = 𝓡} f

  open IsDefiniteInnerProduct isDefiniteInnerProduct public

  innerProduct : InnerProduct _ _ _
  innerProduct = record {isInnerProduct = isInnerProduct}

  open InnerProduct innerProduct public
    using (module leftSemimodule)

  open leftSemimodule public
```

```agda
record NonNegativeSemiDefiniteInnerProduct
  (𝓡 : PoStarCommutativeSemiring r ℓr₁ ℓr₂)
  (a ℓa₁ ℓa₂ : Level)
  : Set (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ suc (a ⊔ ℓa₁ ⊔ ℓa₂))
    where

  constructor mk

  field
    Carrier : PoLeftSemimodule (PoStarCommutativeSemiring.poSemiring 𝓡) a ℓa₁ ℓa₂

  open PoLeftSemimodule Carrier renaming (Carrierᴹ to A)
  open LF (PoStarCommutativeSemiring.commutativeSemiring 𝓡)

  field
    f : A → LinearFunctional leftSemimodule
    isNonNegativeSemiDefiniteInnerProduct :
        IsNonNegativeSemiDefiniteInnerProduct {𝓡 = 𝓡} {𝓜ᴬ = Carrier} f

  open IsNonNegativeSemiDefiniteInnerProduct
     isNonNegativeSemiDefiniteInnerProduct public

  innerProduct : InnerProduct _ _ _
  innerProduct = record {isInnerProduct = isInnerProduct}

  open InnerProduct innerProduct public
    using (module leftSemimodule)

  open leftSemimodule public
```

```agda
record NonNegativeDefiniteInnerProduct
  (𝓡 : PoStarCommutativeSemiring r ℓr₁ ℓr₂)
  (a ℓa₁ ℓa₂ : Level)
  : Set (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ suc (a ⊔ ℓa₁ ⊔ ℓa₂))
    where

  constructor mk

  field
    Carrier : PoLeftSemimodule (PoStarCommutativeSemiring.poSemiring 𝓡) a ℓa₁ ℓa₂

  open PoLeftSemimodule Carrier renaming (Carrierᴹ to A)
  open LF (PoStarCommutativeSemiring.commutativeSemiring 𝓡)

  field
    f : A → LinearFunctional leftSemimodule
    isNonNegativeDefiniteInnerProduct :
        IsNonNegativeDefiniteInnerProduct {𝓡 = 𝓡} {𝓜ᴬ = Carrier} f

  open IsNonNegativeDefiniteInnerProduct
     isNonNegativeDefiniteInnerProduct public

  innerProduct : InnerProduct _ _ _
  innerProduct = record {isInnerProduct = isInnerProduct}

  open InnerProduct innerProduct public
    using (module leftSemimodule)

  open leftSemimodule public
```
