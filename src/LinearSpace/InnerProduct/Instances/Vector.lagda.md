---
title: Inner Product on Vectors by dot product
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Algebra.Star.Bundles using (ConjugateStarCommutativeSemiring)

module LinearSpace.InnerProduct.Instances.Vector
  {c ℓ}
  (𝓡 : ConjugateStarCommutativeSemiring c ℓ)
  where

open ConjugateStarCommutativeSemiring 𝓡

-- stdlib
open import Algebra.Module.Construct.Vector
  renaming (finLeftSemimodule to vector)
open import LinearSpace.DotProduct.Core commutativeSemiring
open import LinearSpace.DotProduct.Properties.ConjugateStarCommutativeSemiring 𝓡
open import Data.Nat as ℕ using (ℕ)

-- carya
open import Maps.Prelinear.Core semiring using (mk)
open import Maps.Linear.Core semiring using (mk)
open import LinearSpace.InnerProduct.Bundles
```

</details>

```agda
protoInnerProduct : (n : ℕ) → ProtoInnerProduct starCommutativeSemiring _ _
protoInnerProduct n =
  mk
    (vector n)
    (λ u →
      mk
        (mk (record { to = (u ·_) ; cong = (·-congˡ u) }))
        (mk (·-distribˡ-+ u) (*-scale u)))
    record
      { isCongruent = λ x≈y {z} → ·-congʳ z x≈y
      ; conjugateSymmetric = ·-preserves-⋆ {m = n}
      }
```
