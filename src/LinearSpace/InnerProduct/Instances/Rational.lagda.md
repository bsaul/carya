---
title: Inner Products on (scalar) Rational Numbers
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

module LinearSpace.InnerProduct.Instances.Rational where

open import Data.Rational.Star.Algebra
open import Algebra.Module.Construct.TensorUnit
open import LinearSpace.InnerProduct.Bundles
open import LinearSpace.InnerProduct.Construct.TensorUnit
```

</details>

```agda
protoInnerProduct : ProtoInnerProduct ℚStarCommutativeSemiring _ _
protoInnerProduct = mkProtoInnerProduct {𝓡 = ℚConjugateStarCommutativeSemiring}
```
