---
title: Properties of Inner Product
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

module LinearSpace.InnerProduct.Properties  where

open import Algebra.Star.Bundles
open import Algebra.Star.Ordered.Partial.Bundles
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Data.Product as × using (_×_; _,_)
open import Data.Sum using (inj₁ ; inj₂)
open import Level using (Level)
open import LinearSpace.InnerProduct.Bundles

import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

private
  variable
    a ℓa ℓa₁ ℓa₂ r ℓr ℓr₁ ℓr₂ : Level
```

</details>

### Orthogonal implies Pythagorean Theorem

```agda
module _
  {𝓡 : StarCommutativeSemiring r ℓr}
  {𝓘 : SymmetricInnerProduct 𝓡 a ℓa}
  where

  open StarCommutativeSemiring 𝓡 renaming (_≈_ to _≈ᴿ_)
  open SymmetricInnerProduct 𝓘 renaming (Carrierᴹ to A)

  orthogonal⇒pythagorean : ∀ {x y : A}
    → x ⊥ y
    → ∥ x ∥² + ∥ y ∥² ≈ᴿ ∥ x +ᴹ y ∥²
  orthogonal⇒pythagorean {x} {y} orthogonal =
    begin
      ∥ x ∥² + ∥ y ∥²
    ≈⟨ +-cong (sym (+-identityʳ ∥ x ∥²)) (sym (+-identityˡ ∥ y ∥²)) ⟩
     ∥ x ∥² + 0# + (0# + ∥ y ∥²)
    ≈⟨ +-cong (+-congˡ (sym orthogonal)) (+-congʳ (sym orthogonal)) ⟩
     ∥ x ∥² + ⟨ x ∣ y ⟩ + (⟨ x ∣ y ⟩ + ∥ y ∥²)
    ≈⟨ +-congʳ (+-congˡ symmetric)  ⟩
     ∥ x ∥² + ⟨ y ∣ x ⟩ + (⟨ x ∣ y ⟩ + ∥ y ∥²)
    ≈˘⟨ +-assoc (∥ x ∥² + ⟨ y ∣ x ⟩) ⟨ x ∣ y ⟩ ∥ y ∥²  ⟩
     ∥ x ∥² + ⟨ y ∣ x ⟩ + ⟨ x ∣ y ⟩ + ∥ y ∥²
    ≈˘⟨ expandQuadratic ⟩
      ∥ x +ᴹ y ∥²
    ∎
    where open ≈-Reasoning setoid
```

## Cauchy-Schwartz

TODO: the below are simply parts of the Cauchy-Schwartz inequality.

```agda
module _
  {𝓡 : PoStarCommutativeSemiring r ℓr₁ ℓr₂}
  {𝓘 : ProtoInnerProduct (PoStarCommutativeSemiring.starCommutativeSemiring 𝓡) a ℓa}
  where

  open import Maps.Linear.Core (PoStarCommutativeSemiring.semiring 𝓡) hiding (setoid)
  open PoStarCommutativeSemiring 𝓡
    renaming (_≈_ to _≈ᴿ_; zeroʳ to 0ʳ; zeroˡ to 0ˡ)
  open ProtoInnerProduct 𝓘 renaming (Carrierᴹ to A; Carrier to 𝓜ᴬ)
  open import Algebra.Module.Properties.LinearDependence 𝓜ᴬ

  CauchySchwartzEquality0 : ∀ {x : A}
    → ⟨ x ∣ 0ᴹ ⟩ * ⟨ x ∣ 0ᴹ ⟩ ≈ᴿ ∥ x ∥² * ∥ 0ᴹ ∥²
  CauchySchwartzEquality0 {x} =
    begin
     ⟨ x ∣ 0ᴹ ⟩ * ⟨ x ∣ 0ᴹ ⟩
    ≈⟨ *-congʳ zeroʳ ⟩
     0# * ⟨ x ∣ 0ᴹ ⟩
    ≈⟨ 0ˡ ⟨ x ∣ 0ᴹ ⟩ ⟩
     0#
    ≈˘⟨ 0ʳ ∥ x ∥² ⟩
     ∥ x ∥² * 0#
    ≈⟨ *-congˡ (Eq.sym zeroʳ) ⟩
     ∥ x ∥² * ∥ 0ᴹ ∥²
    ∎
    where open ≈-Reasoning setoid

  CauchySchwartzEquality : ∀ {x y : A}
    → LinearlyDependent x y -- linearly dependent
    → (∀ {x} → x ⋆ ≈ᴿ x)    -- trivial star operation
    → ⟨ x ∣ y ⟩ * ⟨ x ∣ y ⟩ ≈ᴿ ∥ x ∥² * ∥ y ∥²
  CauchySchwartzEquality {x} {y} (c , inj₁ prf) ⋆-trivial =
    begin
     ⟨ x ∣ y ⟩ * ⟨ x ∣ y ⟩
    ≈⟨ *-cong (congʳ (≈ᴹ-sym prf)) (congʳ (≈ᴹ-sym prf)) ⟩
     ⟨ x ∣ c *ₗ x ⟩ * ⟨ x ∣ c *ₗ x ⟩
    ≈⟨ *-cong scalableʳ scalableʳ ⟩
      ∥ x ∥² * c * ( ⟨ x ∣ x ⟩ * c)
    ≈⟨ *-assoc ⟨ x ∣ x ⟩ c (⟨ x ∣ x ⟩ * c) ⟩
      ∥ x ∥² * (c * (⟨ x ∣ x ⟩ * c))
    ≈⟨ *-congˡ (*-cong (Eq.sym ⋆-trivial) (Eq.sym scalableʳ)) ⟩
      ∥ x ∥² * (c ⋆ * ⟨ x ∣ c *ₗ x ⟩)
    ≈⟨ *-congˡ (Eq.sym scalableˡ) ⟩
      ∥ x ∥² * ⟨ c *ₗ x ∣ c *ₗ x ⟩
    ≈⟨ *-congˡ (cong prf prf) ⟩
     ∥ x ∥² * ∥ y ∥²
    ∎
    where open ≈-Reasoning setoid
  CauchySchwartzEquality {x} {y} (c , inj₂ prf) ⋆-trivial =
    begin
     ⟨ x ∣ y ⟩ * ⟨ x ∣ y ⟩
    ≈⟨ *-cong (congˡ (≈ᴹ-sym prf) ) (congˡ (≈ᴹ-sym prf)) ⟩
     ⟨ c *ₗ y ∣ y ⟩ * ⟨ c *ₗ y ∣ y ⟩
    ≈⟨ *-cong scalableˡ scalableˡ ⟩
     c ⋆ * ⟨ y ∣ y ⟩ * (c ⋆ * ⟨ y ∣ y ⟩)
    ≈˘⟨ *-assoc (c ⋆ * ⟨ y ∣ y ⟩) (c ⋆) ⟨ y ∣ y ⟩ ⟩
     c ⋆ * ⟨ y ∣ y ⟩ * c ⋆ * ∥ y ∥²
    ≈⟨ *-congʳ (*-cong (Eq.sym scalableˡ) ⋆-trivial) ⟩
     (⟨ c *ₗ y ∣ y ⟩ * c ) * ∥ y ∥²
    ≈⟨ *-congʳ (Eq.sym scalableʳ) ⟩
     (⟨ c *ₗ y ∣ c *ₗ y ⟩ ) * ∥ y ∥²
    ≈⟨ *-congʳ (cong prf prf) ⟩
     ∥ x ∥² * ∥ y ∥²
    ∎
    where open ≈-Reasoning setoid
```
