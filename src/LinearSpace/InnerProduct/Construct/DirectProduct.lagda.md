---
title: Create Inner Product from existing
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

module LinearSpace.InnerProduct.Construct.DirectProduct where

import Algebra.Properties.CommutativeSemigroup
open import Algebra.Star.Bundles
open import Algebra.Module.Bundles
open import Algebra.Module.Construct.DirectProduct
open import Data.Product using (_,_)
open import Level using (Level ; _⊔_)

open import LinearSpace.InnerProduct.Bundles
open import Maps.Linear.Core hiding (setoid)
open import Maps.Prelinear.Core using (mk)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

private
  variable
    r ℓr ℓr₁ ℓr₂ a ℓa ℓa₁ ℓa₂ b ℓb ℓb₁ ℓb₂ : Level
```

</details>

```agda
mkProtoInnerProduct : {𝓡 : StarCommutativeSemiring r ℓr}
    → ProtoInnerProduct 𝓡 a ℓa
    → ProtoInnerProduct 𝓡 b ℓb
    → ProtoInnerProduct 𝓡 (a ⊔ b) (ℓa ⊔ ℓb)
mkProtoInnerProduct {𝓡 = 𝓡} 𝓘ᴬ 𝓘ᴮ =
    mk
      (leftSemimodule 𝓘ᴬ.Carrier 𝓘ᴮ.Carrier)
      (λ (a₁ , b₁) →
        mk
          (mk
            (record
             { to = (λ (a₂ , b₂) → 𝓘ᴬ.⟨ a₁ ∣ a₂ ⟩ + 𝓘ᴮ.⟨ b₁ ∣ b₂ ⟩)
             ; cong = λ (x , y) → +-cong (𝓘ᴬ.congʳ x) (𝓘ᴮ.congʳ y)
            }))
          (mk
            (λ {(a₂ , b₂)} {(a₃ , b₃)} →
            begin
              𝓘ᴬ.⟨ a₁ ∣ a₂ 𝓘ᴬ.+ᴹ a₃ ⟩ + 𝓘ᴮ.⟨ b₁ ∣ b₂ 𝓘ᴮ.+ᴹ b₃ ⟩
            ≈⟨ +-cong 𝓘ᴬ.additiveʳ 𝓘ᴮ.additiveʳ ⟩
               (𝓘ᴬ.⟨ a₁ ∣ a₂ ⟩ + 𝓘ᴬ.⟨ a₁ ∣ a₃ ⟩)
             + (𝓘ᴮ.⟨ b₁ ∣ b₂ ⟩ + 𝓘ᴮ.⟨ b₁ ∣ b₃ ⟩)
            ≈⟨ interchange 𝓘ᴬ.⟨ a₁ ∣ a₂ ⟩ _ _ _ ⟩
               (𝓘ᴬ.⟨ a₁ ∣ a₂ ⟩ + 𝓘ᴮ.⟨ b₁ ∣ b₂ ⟩)
             + (𝓘ᴬ.⟨ a₁ ∣ a₃ ⟩ + 𝓘ᴮ.⟨ b₁ ∣ b₃ ⟩)
            ∎)
            λ {r} {(a₂ , b₂)} →
              begin
                r * (𝓘ᴬ.⟨ a₁ ∣ a₂ ⟩ + 𝓘ᴮ.⟨ b₁ ∣ b₂ ⟩)
              ≈⟨ distribˡ r _ _  ⟩
                r * 𝓘ᴬ.⟨ a₁ ∣ a₂ ⟩ + r * 𝓘ᴮ.⟨ b₁ ∣ b₂ ⟩
              ≈⟨ +-cong (*-comm _ _) (*-comm _ _)  ⟩
                𝓘ᴬ.⟨ a₁ ∣ a₂ ⟩ * r + 𝓘ᴮ.⟨ b₁ ∣ b₂ ⟩ * r
              ≈⟨ +-cong (sym 𝓘ᴬ.scalableʳ) (sym 𝓘ᴮ.scalableʳ)  ⟩
                𝓘ᴬ.⟨ a₁ ∣ r 𝓘ᴬ.*ₗ a₂ ⟩ + 𝓘ᴮ.⟨ b₁ ∣ r 𝓘ᴮ.*ₗ b₂ ⟩
              ∎
          )
       )
      (record
        { isCongruent = λ (x , y) → +-cong (𝓘ᴬ.isCongruent x) (𝓘ᴮ.isCongruent y)
        ; conjugateSymmetric = λ {(a₁ , b₁)} {(a₂ , b₂)} →
          begin
            (𝓘ᴬ.⟨ a₁ ∣ a₂ ⟩ + 𝓘ᴮ.⟨ b₁ ∣ b₂ ⟩)
          ≈⟨ +-cong 𝓘ᴬ.conjugateSymmetric 𝓘ᴮ.conjugateSymmetric ⟩
            (𝓘ᴬ.⟨ a₂ ∣ a₁ ⟩ ⋆) + (𝓘ᴮ.⟨ b₂ ∣ b₁ ⟩ ⋆)
          ≈˘⟨ ⋆-homo-+ _ _ ⟩
            (𝓘ᴬ.⟨ a₂ ∣ a₁ ⟩ + 𝓘ᴮ.⟨ b₂ ∣ b₁ ⟩) ⋆
          ∎
        }
      )
    where module 𝓘ᴬ = ProtoInnerProduct 𝓘ᴬ
          module 𝓘ᴮ = ProtoInnerProduct 𝓘ᴮ
          open StarCommutativeSemiring 𝓡
          open ≈-Reasoning setoid
          open Algebra.Properties.CommutativeSemigroup +-commutativeSemigroup
```
