---
title: Create Inner Product from existing
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

module LinearSpace.InnerProduct.Construct.Pointwise where

open import Algebra.Star.Bundles
open import Algebra.Star.Construct.Pointwise
open import Algebra.Construct.Pointwise
open import Algebra.Module.Bundles
open import Algebra.Module.Construct.Pointwise
open import LinearSpace.InnerProduct.Bundles
open import Maps.Linear.Core
```

</details>

```agda
mkProtoInnerProduct :
    ∀ {a ℓa r ℓr}
    → {𝓡 : StarCommutativeSemiring r ℓr}
    → (A : Set a)
    → ProtoInnerProduct 𝓡 a ℓa
    → ProtoInnerProduct (mkStarCommutativeSemiring A 𝓡) _ _
mkProtoInnerProduct {𝓡 = 𝓡} A 𝓘 =
  mk
    (mkLeftSemimoduleLiftSemiring A 𝓘.Carrier)
    (λ g →
      mk⊸
        _
        (λ h a → 𝓘.⟨ g a ∣ h a ⟩)
        (λ x {a} → 𝓘.congʳ (x {a}))
        (λ {i} {j} {a} → 𝓘.additiveʳ {x = i a} {y = j a} {z = g a})
        λ {r} {i} {a} →
          trans
            (*-comm (r a) 𝓘.⟨ g a ∣ i a ⟩)
            (sym (𝓘.scalableʳ {r = r a } {x = g a} {y = i a}))
      )
    (record
        { isCongruent = λ x {f} {a} → 𝓘.isCongruent x {f a}
        ; conjugateSymmetric = λ {x} {y} {a}
           → 𝓘.conjugateSymmetric {x = x a} {y = y a}
        }
    )
    where module 𝓘 = ProtoInnerProduct 𝓘
          open StarCommutativeSemiring 𝓡
```
