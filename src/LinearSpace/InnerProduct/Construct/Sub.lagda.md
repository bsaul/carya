---
title: Create Inner Product from existing
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

module LinearSpace.InnerProduct.Construct.Sub where

import Algebra.Properties.CommutativeSemigroup
open import Algebra.Star.Bundles
open import Algebra.Module.Bundles
open import Algebra.Module.Construct.Sub
open import Data.Product using (_,_)
open import Level using (_⊔_)
open import LinearSpace.InnerProduct.Bundles
open import Maps.Linear.Core
open import Relation.DependentOps
open Inj
open Inj'
```

</details>

```agda
module _
  {a ℓa r ℓr ℓ}
  {𝓡 : StarCommutativeSemiring r ℓr}
  {𝓘ᴬ : ProtoInnerProduct 𝓡 a ℓa}
  (P : ProtoInnerProduct.Carrierᴹ 𝓘ᴬ → Set ℓ)
  where

  open StarCommutativeSemiring 𝓡
  open ProtoInnerProduct 𝓘ᴬ renaming (Carrier to 𝓜)
  open ∃op P
  open ∃op' {A = Carrierᴹ} {B = Carrier} P

  mkProtoInnerProduct :
      (D₂ _+ᴹ_)
    → (D₂' _*ₗ_)
    → (D₀ 0ᴹ)
    → ProtoInnerProduct 𝓡 (a ⊔ ℓ) ℓa
  mkProtoInnerProduct _+'_  _*ₗ'_ 0' =
      mk
        (leftSemimodule 𝓜 P _+'_ _*ₗ'_ 0')
        (λ (a₁ , _) →
          mk⊸ semiring
            (λ (a₂ , _) → ⟨ a₁ ∣ a₂ ⟩)
            congʳ
            additiveʳ
            (trans (*-comm _ _) (sym scalableʳ))
          )
        (record
          { isCongruent = λ x → congˡ x
          ; conjugateSymmetric = conjugateSymmetric
          }
        )
```
