---
title: Create Inner Product from existing
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

module LinearSpace.InnerProduct.Construct.TensorUnit where

import Algebra.Properties.CommutativeSemigroup
open import Algebra.Star.Bundles
open import Algebra.Module.Construct.TensorUnit
open import LinearSpace.InnerProduct.Bundles
open import Maps.Linear.Core
open import Maps.Prelinear.Core using (mk)
```

</details>

```agda
mkProtoInnerProduct :
    ∀ {r ℓr}
    → {𝓡 : ConjugateStarCommutativeSemiring r ℓr}
    → ProtoInnerProduct
        (ConjugateStarCommutativeSemiring.starCommutativeSemiring 𝓡) r ℓr
mkProtoInnerProduct {𝓡 = 𝓡} =
    mk
      leftSemimodule
      (λ r₁ →
        mk
          (mk
            (record { to =  r₁ *_ ; cong = *-congˡ }))
            (mk (distribˡ r₁ _ _) (x∙yz≈y∙xz _ r₁ _))
        )
      (record
        { isCongruent = λ x → *-congʳ x
        ; conjugateSymmetric = *-conjugateSymmetric
        })
    where open ConjugateStarCommutativeSemiring 𝓡
          open Algebra.Properties.CommutativeSemigroup *-commutativeSemigroup
```
