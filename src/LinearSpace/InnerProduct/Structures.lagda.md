---
title: Inner Product Structures
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

module LinearSpace.InnerProduct.Structures where

-- stdlib
open import Algebra.Bundles
open import Algebra.Module.Bundles
  using (LeftSemimodule; Semimodule)
open import Algebra.Module.Ordered.Partial.Bundles
open import Algebra.Star.Bundles using (StarCommutativeSemiring)
open import Algebra.Star.Ordered.Partial.Bundles
open import Data.Product using (_,_)
open import Data.Sum using (inj₁)
open import Function using (Congruent)
open import Level using (Level ; _⊔_)
open import Relation.Binary using (IsEquivalence)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning
open import Relation.Nullary.Negation using (contraposition; _¬-⊎_)

-- carya
import Maps.Linear.Core as LC
import Maps.Linear.Construct.Functional as LF

private
    variable
      a r ℓa ℓa₁ ℓa₂ ℓr ℓr₁ ℓr₂ : Level
```

</details>

## Inner Product Space(s)

The structures herein use the definition of inner product
from [nlab](https://ncatlab.org/nlab/show/inner+product+space),
which is both a bit more general and easier to work with
than the definition in
[wikipedia](https://en.wikipedia.org/wiki/Inner_product_space)
or in many linear algebra textbooks.

An inner product space is vector space $V$
(over a ring $R$ equipped with an involutive operation)
with the contraint that the vector space
has a function `⟨_,_⟩ : V × V → R`.
This function must satisfy the following:

* The function is linear in one argument [^1].
For example, if linear in the second argument this means:
  * `⟨ x ∣ y + z ⟩ = ⟨ x ∣ y ⟩ + ⟨ x ∣ z ⟩`
  * `⟨ x ∣ cy ⟩ = c ⟨ x ∣ y ⟩`
Seen this way, the `⟨_,_⟩` function
is determined by a map from `V` to its `Dual`.
* The function must be conjugate symmetric
with respect to the involutive operator `⟨ x ∣ y ⟩ = ⟨ y ∣ x ⟩ ᵒ`.

The above expresses the prototypical inner product.
Inner product may have also additional properties;
some of which required additional structure on the ring space.
For example,
the wikipedia definition requires positive-definiteness,
(`x ≠ 0 ⇒ ⟨ x ∣ x ⟩ > 0`),
which implies that the ring has a strict ordering.

[^1]: Which argument depends on convention.
Here, we use the
["physicist's standard"](https://en.wikipedia.org/wiki/Inner_product_space#Convention_variant).

## "Raw" inner product

The `RawInnerProduct` module is used to define
the shape of the inner product function defined above.
This alone defines a function linear in the second argument
without the requirement of conjugate symmetry.

```agda
module RawInnerProduct
  (𝓡 : CommutativeSemiring r ℓr)
  (𝓜ᴬ : LeftSemimodule (CommutativeSemiring.semiring 𝓡) a ℓa)
  where

  private module 𝓡 = CommutativeSemiring 𝓡
  open LF 𝓡

  RawInnerProduct = LeftSemimodule.Carrierᴹ 𝓜ᴬ → LinearFunctional 𝓜ᴬ
```

## Prototypical inner product

```agda
module _
  {𝓡 : StarCommutativeSemiring r ℓr}
  {𝓜ᴬ : LeftSemimodule (StarCommutativeSemiring.semiring 𝓡) a ℓa}
  where

  open StarCommutativeSemiring 𝓡
    renaming
      ( Carrier to R
      ; _≈_ to _≈ᴿ_ )
    hiding (zeroʳ ; zeroˡ)

  open RawInnerProduct commutativeSemiring 𝓜ᴬ
  open LC semiring hiding (setoid)

  record IsProtoInnerProduct (g : RawInnerProduct) : Set (a ⊔ r ⊔ ℓa ⊔ ℓr) where

      open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A)

      -- The "Hermitian" form
      ⟨_∣_⟩ : A → A → R
      ⟨ x ∣ y ⟩ = _⊸_.to (g x) y

      field
        isCongruent : Congruent _≈ᴹ_ _≈_ g
        conjugateSymmetric : ∀ {x y} → ⟨ x ∣ y ⟩ ≈ᴿ ⟨ y ∣ x ⟩ ⋆

      -- norm
      ∥_∥² : A → R
      ∥ x ∥² = ⟨ x ∣ x ⟩

      -- With an inner product,
      -- one can define the relation of Orthogonality
      _⊥_ : A → A → Set ℓr
      x ⊥ y = ⟨ x ∣ y ⟩ ≈ᴿ 0#

      cong : ∀ {x y u v} → x ≈ᴹ y → u ≈ᴹ v → ⟨ x ∣ u ⟩ ≈ᴿ ⟨ y ∣ v ⟩
      cong {x} {y} {u} {v} x≈y u≈v =
          begin
            ⟨ x ∣  u ⟩
          ≈⟨ isCongruent x≈y {u} ⟩
            ⟨ y ∣ u ⟩
          ≈⟨ _⊸_.cong (g y) {u} {v} u≈v  ⟩
            ⟨ y ∣ v ⟩
          ∎
          where open ≈-Reasoning setoid

      congʳ : ∀ {x u v } →  u ≈ᴹ v → ⟨ x ∣ u ⟩ ≈ᴿ ⟨ x ∣ v ⟩
      congʳ u≈v = cong ≈ᴹ-refl u≈v

      congˡ : ∀ {x y u } → x ≈ᴹ y → ⟨ x ∣ u ⟩ ≈ᴿ ⟨ y ∣ u ⟩
      congˡ x≈y = cong x≈y ≈ᴹ-refl

      zeroʳ : ∀ {x} → ⟨ x ∣ 0ᴹ ⟩ ≈ᴿ 0#
      zeroʳ {x} = _⊸_.f0≈0 (g x)

      additiveʳ : ∀ {x y z} → ⟨ z ∣ x +ᴹ y ⟩ ≈ᴿ ⟨ z ∣ x ⟩ + ⟨ z ∣ y ⟩
      additiveʳ {z = z} = _⊸_.+-homo (g z)

      scalableʳ : ∀ {r} → ∀ {x y} → ⟨ x ∣ r *ₗ y ⟩ ≈ᴿ ⟨ x ∣ y ⟩ * r
      scalableʳ {r} {x} {y} =
        trans (sym (_⊸_.*ₗ-homo (g x))) (*-comm r (_⊸_.to (g x) y))

      scalableˡ : ∀ {r} → ∀ {x y} → ⟨ r *ₗ x ∣  y ⟩ ≈ᴿ r ⋆ * ⟨ x ∣ y ⟩
      scalableˡ {r} {x} {y} =
          begin
            ⟨ r *ₗ x ∣  y ⟩
          ≈⟨ conjugateSymmetric ⟩
            ⟨  y ∣ r *ₗ x ⟩ ⋆
          ≈⟨ ⋆-cong scalableʳ ⟩
            (⟨  y ∣ x ⟩ * r) ⋆
          ≈⟨ ⋆-anti-homo-* ⟨ y ∣ x ⟩ r ⟩
            r ⋆ *  ⟨ y ∣ x ⟩ ⋆
          ≈⟨ *-congˡ (sym conjugateSymmetric) ⟩
            r ⋆ * ⟨ x ∣ y ⟩
          ∎
          where open ≈-Reasoning setoid

      symmetricNorm : ∀ {x} → ∥ x ∥² ⋆ ≈ᴿ ∥ x ∥²
      symmetricNorm = sym conjugateSymmetric

      sandwich : ∀ {r} {x} → ∥ r *ₗ x ∥² ≈ᴿ r ⋆ * ∥ x ∥² * r
      sandwich {r} {x} =
          begin
            ∥ r *ₗ x ∥²
          ≈⟨ scalableʳ ⟩
            ⟨ r *ₗ x ∣ x ⟩ * r
          ≈⟨ *-congʳ scalableˡ ⟩
            r ⋆ * ∥ x ∥² * r
          ∎
        where open ≈-Reasoning setoid

      expandQuadratic : ∀ {x y} →
        ∥ x +ᴹ y ∥² ≈ᴿ ∥ x ∥² + ⟨ y ∣ x ⟩ + ⟨ x ∣ y ⟩ + ∥ y ∥²
      expandQuadratic {x} {y} =
          begin
            ∥ x +ᴹ y ∥²
          ≈⟨ additiveʳ ⟩
            ⟨ x +ᴹ y ∣ x ⟩ + ⟨ x +ᴹ y ∣ y ⟩
          ≈⟨ +-cong conjugateSymmetric conjugateSymmetric ⟩
            ⟨ x ∣ x +ᴹ y ⟩ ⋆ + ⟨ y ∣ x +ᴹ y ⟩ ⋆
          ≈⟨ +-cong (⋆-cong additiveʳ) (⋆-cong additiveʳ) ⟩
            (∥ x ∥² + ⟨ x ∣ y ⟩) ⋆ + (⟨ y ∣ x ⟩ + ∥ y ∥²) ⋆
          ≈⟨ +-cong (⋆-homo-+ ∥ x ∥² ⟨ x ∣ y ⟩ ) (⋆-homo-+ ⟨ y ∣ x ⟩ ∥ y ∥²) ⟩
            (∥ x ∥² ⋆ + ⟨ x ∣ y ⟩ ⋆) + (⟨ y ∣ x ⟩ ⋆ + ∥ y ∥² ⋆)
          ≈⟨ sym (+-assoc (∥ x ∥² ⋆ + ⟨ x ∣ y ⟩ ⋆) (⟨ y ∣ x ⟩ ⋆) (∥ y ∥² ⋆)) ⟩
            ∥ x ∥² ⋆ + ⟨ x ∣ y ⟩ ⋆ + ⟨ y ∣ x ⟩ ⋆ + ∥ y ∥² ⋆
          ≈⟨ +-cong
                (+-cong
                    (+-cong symmetricNorm
                      (sym conjugateSymmetric))
                      (sym conjugateSymmetric))
                symmetricNorm
            ⟩
            ∥ x ∥² + ⟨ y ∣ x ⟩ + ⟨ x ∣ y ⟩ + ∥ y ∥²
          ∎
        where open ≈-Reasoning setoid

      -- TODO Rename me
      expandQuadratic₂ : ∀ {r} {x y} →
        ∥ x +ᴹ r *ₗ y ∥² ≈ᴿ ∥ x ∥² + r ⋆ * ⟨ y ∣ x ⟩ + ⟨ x ∣ y ⟩ * r + r ⋆ * ∥ y ∥² * r
      expandQuadratic₂ {r} {x} {y} =
            begin
            ∥ x +ᴹ r *ₗ y ∥²
          ≈⟨ expandQuadratic ⟩
            ∥ x ∥² + ⟨ r *ₗ y ∣ x ⟩ + ⟨ x ∣ r *ₗ y ⟩ + ∥ r *ₗ y ∥²
          ≈⟨ +-cong (+-cong (+-congˡ scalableˡ) scalableʳ) sandwich ⟩
            ∥ x ∥² + r ⋆ * ⟨ y ∣ x ⟩ + ⟨ x ∣  y ⟩ * r + r ⋆ * ∥ y ∥² * r
          ∎
        where open ≈-Reasoning setoid

      -- If the star involution operation is *trivial* (the identity),
      -- (as is the case with (e.g.) ℝ)
      -- then the map can be flipped around
      -- and ⟨_,_⟩ is symmetric (see below).
      g* : (∀ {x} → x ⋆ ≈ᴿ x) → RawInnerProduct
      g* trivial a =
         mk⊸
          ⟨_∣ a ⟩
          (λ x → isCongruent x {a})
          (λ {a₁} {a₂} →
              begin
                ⟨ a₁ +ᴹ a₂ ∣ a ⟩
              ≈⟨ conjugateSymmetric ⟩
                ⟨ a ∣ a₁ +ᴹ a₂ ⟩ ⋆
              ≈⟨ ⋆-cong additiveʳ ⟩
                ( ⟨ a ∣ a₁ ⟩ + ⟨  a ∣ a₂ ⟩ ) ⋆
              ≈⟨ trivial ⟩
                ⟨ a ∣ a₁ ⟩ + ⟨  a ∣ a₂ ⟩
              ≈⟨ +-cong (sym trivial) (sym trivial) ⟩
                ⟨ a ∣ a₁ ⟩ ⋆ + ⟨ a ∣ a₂ ⟩ ⋆
              ≈⟨ +-cong (sym conjugateSymmetric) (sym conjugateSymmetric) ⟩
                ⟨ a₁ ∣ a ⟩ + ⟨ a₂ ∣ a ⟩
              ∎)
          λ {r} {a₁} →
              begin
                r * ⟨ a₁ ∣ a ⟩
              ≈⟨ *-comm r ⟨ a₁ ∣ a ⟩ ⟩
                ⟨ a₁ ∣ a ⟩ * r
              ≈⟨ *-congʳ conjugateSymmetric ⟩
                ⟨ a ∣ a₁ ⟩ ⋆ * r
              ≈⟨ *-congʳ trivial ⟩
                ⟨ a ∣ a₁ ⟩  * r
              ≈⟨ sym scalableʳ ⟩
                ⟨ a ∣ r *ₗ a₁ ⟩
              ≈⟨ conjugateSymmetric ⟩
                ⟨ r *ₗ a₁ ∣ a ⟩ ⋆
              ≈⟨ trivial ⟩
                ⟨ r *ₗ a₁ ∣ a ⟩
              ∎
        where open ≈-Reasoning setoid

      trivial⇒symmetric : (∀ {x} → x ⋆ ≈ᴿ x) → ∀ x y → ⟨ x ∣ y ⟩ ≈ᴿ ⟨ y ∣ x ⟩
      trivial⇒symmetric trivial x y =
          begin
            ⟨ x ∣ y ⟩
          ≈⟨ conjugateSymmetric ⟩
            ⟨ y ∣ x ⟩ ⋆
          ≈⟨ trivial ⟩
            ⟨ y ∣ x ⟩
          ∎
        where open ≈-Reasoning setoid
```

## NonDegenerate inner product

A `NonDegenerate` inner product is one with the condition
that if the application of the inner product function `⟨_∣_⟩`
to any two elements in the vector space is zero,
then the first argument must be zero.
With this condition,
one can show that orthogonal vectors are also linearly independent.
Note that the non-degenerate assumption is implicit in many informal proofs.

A `NonDegenerate` inner product is commonly required in applications,
hence we give this inner product the vaulted name of `IsInnerProduct`
and subsequent inner products defined below contain this inner product.

```agda
module _
  {𝓡 : StarCommutativeSemiring r ℓr}
  {𝓜ᴬ : LeftSemimodule (StarCommutativeSemiring.semiring 𝓡) a ℓa}
  where

  open StarCommutativeSemiring 𝓡
    renaming
      ( Carrier to R
      ; _≈_ to _≈ᴿ_ )
  open RawInnerProduct commutativeSemiring 𝓜ᴬ
  open import Algebra.Module.Properties.LinearDependence 𝓜ᴬ

  record IsInnerProduct (g : RawInnerProduct) : Set (a ⊔ r ⊔ ℓa ⊔ ℓr) where

      open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A)

      field
        isProtoInnerProduct : IsProtoInnerProduct {𝓡 = 𝓡} g

      open IsProtoInnerProduct isProtoInnerProduct public

      field
        nonDegenerate : ∀ {x y} → ⟨ x ∣ y ⟩ ≈ᴿ 0# → x ≈ᴹ 0ᴹ

      orthogonal⇒independent : ∀ {x y}
        → x ≉ᴹ 0ᴹ
        → y ≉ᴹ 0ᴹ
        → x ⊥ y
        → LinearlyIndependent x y
      orthogonal⇒independent x≉ᴹ0ᴹ y≉ᴹ0ᴹ orthogonal =
        contraposition
          (λ _ → inj₁ (nonDegenerate orthogonal))
          (x≉ᴹ0ᴹ ¬-⊎ y≉ᴹ0ᴹ)
```

## Symmetric inner product

A symmetric inner product function is one which the arguments can be flipped.
This condition implies the inner product has a `Bilinear` form.

```agda
module _
  {𝓡 : StarCommutativeSemiring r ℓr}
  {𝓜ᴬ : Semimodule (StarCommutativeSemiring.commutativeSemiring 𝓡) a ℓa}
  where

  open StarCommutativeSemiring 𝓡 renaming (_≈_ to _≈ᴿ_ )
  open Semimodule 𝓜ᴬ
  open RawInnerProduct commutativeSemiring leftSemimodule
  open LC semiring hiding (setoid)
  open import Maps.Bilinear.Construct.Functional commutativeSemiring
    using (BilinearFunctional ; mkBilinear)

  record IsSymmetricInnerProduct
      (g : RawInnerProduct) : Set (a ⊔ r ⊔ ℓa ⊔ ℓr) where

      field
        isInnerProduct : IsInnerProduct {𝓡 = 𝓡} g

      open IsInnerProduct isInnerProduct public

      field
        symmetric : ∀ {x y} → ⟨ x ∣ y ⟩ ≈ᴿ ⟨ y ∣ x ⟩

      bilinear : BilinearFunctional 𝓜ᴬ 𝓜ᴬ
      bilinear = mkBilinear
          (λ (a₁ , a₂) → (g a₁) ⟨$⟩ a₂)
          (λ (x , y) → cong x y )
          (λ {a} {b} {c} {d} →
              additiveʳ
            , (begin
                ⟨ a +ᴹ b ∣ c ⟩
              ≈⟨ symmetric ⟩
                ⟨ c ∣ a +ᴹ b ⟩
              ≈⟨ _⊸_.+-homo (g c) ⟩
                ⟨ c ∣ a ⟩ + ⟨ c ∣ b ⟩
              ≈⟨ +-cong symmetric symmetric ⟩
                ⟨ a ∣ c ⟩ + ⟨ b ∣ c ⟩
              ∎)
            )
          λ {r} {a₁} {a₂} →
              (trans (scalableʳ {r}) (*-comm ⟨ a₁ ∣ a₂ ⟩ r))
            , (begin
                ⟨ r *ₗ a₁ ∣ a₂ ⟩
              ≈⟨ symmetric ⟩
                ⟨ a₂  ∣ r *ₗ a₁ ⟩
              ≈⟨ scalableʳ ⟩
                ⟨ a₂ ∣ a₁ ⟩ * r
              ≈⟨ *-congʳ symmetric ⟩
                ⟨ a₁ ∣ a₂ ⟩ * r
              ∎)
        where open ≈-Reasoning setoid
```

## SemiDefinite inner product

```agda
module _
  {𝓡 : StarCommutativeSemiring r ℓr}
  {𝓜ᴬ : LeftSemimodule (StarCommutativeSemiring.semiring 𝓡) a ℓa}
  where

  open StarCommutativeSemiring 𝓡 renaming (_≈_ to _≈ᴿ_)
  open RawInnerProduct commutativeSemiring 𝓜ᴬ

  record IsSemiDefiniteInnerProduct
      (g : RawInnerProduct) : Set (a ⊔ r ⊔ ℓa ⊔ ℓr) where

      open LeftSemimodule 𝓜ᴬ

      field
        isInnerProduct : IsInnerProduct {𝓡 = 𝓡} g

      open IsInnerProduct isInnerProduct public

      field
        semiDefinite : ∀ {x y} → ∥ x ∥² ≈ᴿ 0# → ∥ y ∥² ≈ᴿ 0# → ∥ x +ᴹ y ∥² ≈ᴿ 0#
```

## Definite inner product

```agda
module _
  {𝓡 : StarCommutativeSemiring r ℓr}
  {𝓜ᴬ : LeftSemimodule (StarCommutativeSemiring.semiring 𝓡) a ℓa}
  where

  open StarCommutativeSemiring 𝓡 renaming (_≈_ to _≈ᴿ_)
  open RawInnerProduct commutativeSemiring 𝓜ᴬ

  record IsDefiniteInnerProduct
      (g : RawInnerProduct) : Set (a ⊔ r ⊔ ℓa ⊔ ℓr) where

      open LeftSemimodule 𝓜ᴬ

      field
        isInnerProduct : IsInnerProduct {𝓡 = 𝓡} g

      open IsInnerProduct isInnerProduct public

      field
        definite : ∀ {x} → ∥ x ∥² ≈ᴿ 0# → x ≈ᴹ 0ᴹ
```

## NonNegative inner product

```agda
module _
  {𝓡 : PoStarCommutativeSemiring r ℓr₁ ℓr₂}
  {𝓜ᴬ : PoLeftSemimodule (PoStarCommutativeSemiring.poSemiring 𝓡) a ℓa₁ ℓa₂}
  where

  open PoStarCommutativeSemiring 𝓡 renaming (_≈_ to _≈ᴿ_)
  open PoLeftSemimodule 𝓜ᴬ
  open RawInnerProduct commutativeSemiring leftSemimodule

  record IsNonNegativeInnerProduct
      (g : RawInnerProduct) : Set (a ⊔ ℓa₁ ⊔ ℓa₂ ⊔ r ⊔ ℓr₁ ⊔ ℓr₂) where

      field
        isInnerProduct : IsInnerProduct {𝓡 = starCommutativeSemiring} g

      open IsInnerProduct isInnerProduct public

      field
        nonNegative : ∀ {x} → 0# ≤ ∥ x ∥²
```

## NonNegative SemiDefinite inner product

```agda
module _
  {𝓡 : PoStarCommutativeSemiring r ℓr₁ ℓr₂}
  {𝓜ᴬ : PoLeftSemimodule (PoStarCommutativeSemiring.poSemiring 𝓡) a ℓa₁ ℓa₂}
  where

  open PoStarCommutativeSemiring 𝓡 renaming (_≈_ to _≈ᴿ_)
  open PoLeftSemimodule 𝓜ᴬ
  open RawInnerProduct commutativeSemiring leftSemimodule

  record IsNonNegativeSemiDefiniteInnerProduct
      (g : RawInnerProduct) : Set (a ⊔ ℓa₁ ⊔ ℓa₂ ⊔ r ⊔ ℓr₁ ⊔ ℓr₂) where

      field
        isInnerProduct : IsInnerProduct {𝓡 = starCommutativeSemiring} g

      open IsInnerProduct isInnerProduct public

      field
        nonNegative : ∀ {x} → 0# ≤ ∥ x ∥²
        semiDefinite : ∀ {x y} → ∥ x ∥² ≈ᴿ 0# → ∥ y ∥² ≈ᴿ 0# → ∥ x +ᴹ y ∥² ≈ᴿ 0#

      isNonNegativeInnerProduct : IsNonNegativeInnerProduct {𝓡 = 𝓡} {𝓜ᴬ} g
      isNonNegativeInnerProduct =
          record {isInnerProduct = isInnerProduct ; nonNegative = nonNegative}

      isSemiDefiniteInnerProduct : IsSemiDefiniteInnerProduct
        {𝓡 = starCommutativeSemiring} {leftSemimodule} g
      isSemiDefiniteInnerProduct =
          record {isInnerProduct = isInnerProduct ; semiDefinite = semiDefinite }
```

## NonNegative Definite inner product

```agda
module _
  {𝓡 : PoStarCommutativeSemiring r ℓr₁ ℓr₂}
  {𝓜ᴬ : PoLeftSemimodule (PoStarCommutativeSemiring.poSemiring 𝓡) a ℓa₁ ℓa₂}
  where

  open PoStarCommutativeSemiring 𝓡 renaming (_≈_ to _≈ᴿ_)
  open PoLeftSemimodule 𝓜ᴬ
  open RawInnerProduct commutativeSemiring leftSemimodule

  record IsNonNegativeDefiniteInnerProduct
      (g : RawInnerProduct) : Set (a ⊔ ℓa₁ ⊔ ℓa₂ ⊔ r ⊔ ℓr₁ ⊔ ℓr₂) where

      field
        isInnerProduct : IsInnerProduct {𝓡 = starCommutativeSemiring} g

      open IsInnerProduct isInnerProduct public

      field
        nonNegative : ∀ {x} → 0# ≤ ∥ x ∥²
        definite : ∀ {x} → ∥ x ∥² ≈ᴿ 0# → x ≈ᴹ 0ᴹ

      isNonNegativeInnerProduct : IsNonNegativeInnerProduct {𝓡 = 𝓡} {𝓜ᴬ} g
      isNonNegativeInnerProduct =
          record {isInnerProduct = isInnerProduct ; nonNegative = nonNegative}

      isDefiniteInnerProduct : IsDefiniteInnerProduct
        {𝓡 = starCommutativeSemiring} {leftSemimodule} g
      isDefiniteInnerProduct =
          record {isInnerProduct = isInnerProduct ; definite = definite }
```
