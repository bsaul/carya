---
title: Properties of Modules over a CommutativeRing
---

<details>
<summary>Options/Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles
open import Algebra.Module.Bundles
```

</details>

```agda
module LinearSpace.Submodule.Properties.CommutativeRing
  {r ℓʳ} {𝓡 : CommutativeRing r ℓʳ} where
```

<details>
<summary>Imports</summary>

```agda
open module 𝓡 = CommutativeRing 𝓡

-- stdlib
open import Level using (Level; _⊔_)
open import Algebra.Properties.AbelianGroup
open import Data.Product as × using (Σ ; ∃ ; ∃₂; _,_; _×_)
open import Data.Unit.Polymorphic using (tt)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

-- carya
open import Maps.Linear.Core semiring using (_⊸_)
open import Maps.Linear.Base semiring using (¡)

open import Maps.Linear.Properties semiring using (Injective ; Surjective)

private
  variable
    a b ℓa ℓb : Level
```

</details>

```agda
open import LinearSpace.Submodule.Properties.Semiring {𝓡 = semiring} public
```

### Trival Kernel ⇒ Injective

```agda
module _
  {𝓜ᴬ : Module 𝓡 a ℓa}
  {𝓜ᴮ : Module 𝓡 b ℓb}
  {T : Module.leftSemimodule 𝓜ᴬ ⊸ Module.leftSemimodule 𝓜ᴮ}
  where

  private
    open module Mᴬ = Module 𝓜ᴬ using (_+ᴹ_; -ᴹ_; 0ᴹ)
    open module Mᴮ = Module 𝓜ᴮ using (≈ᴹ-trans)
  open _⊸_ T

  trivialker⇒injective : TrivialKernel T → Injective T
  trivialker⇒injective (Tx≈0ᵇ→0ᵃ≈x , 0ᵃ≈x→Tx≈0ᵇ) {x = a₁} {y = a₂} Ta₁≈Ta₂ = begin
    a₁                  ≈⟨ xyx⁻¹≈y Mᴬ.+ᴹ-abelianGroup a₂ a₁ ⟨
    a₂ +ᴹ a₁ +ᴹ -ᴹ a₂   ≈⟨ Mᴬ.+ᴹ-assoc _ _ _ ⟩
    a₂ +ᴹ (a₁ +ᴹ -ᴹ a₂) ≈⟨ Mᴬ.+ᴹ-congˡ lemma ⟩
    a₂ +ᴹ 0ᴹ            ≈⟨ Mᴬ.+ᴹ-identityʳ _ ⟩
    a₂                  ∎
    where open ≈-Reasoning Mᴬ.≈ᴹ-setoid
          lemma = Tx≈0ᵇ→0ᵃ≈x
              ((to (a₂ +ᴹ -ᴹ a₂)
                , ≈ᴹ-trans (cong (Group.inverseʳ Mᴬ.+ᴹ-group a₂)) f0≈0)
              , ≈ᴹ-trans
                +-homo
                (≈ᴹ-trans (Mᴮ.+ᴹ-congʳ Ta₁≈Ta₂) (Mᴮ.≈ᴹ-sym +-homo)))
```

### Exact ¡ T ⇒ Injective T

```agda
module _
  {𝓜ᴬ : Module 𝓡 a ℓa}
  {𝓜ᴮ : Module 𝓡 b ℓb}
  {T : Module.leftSemimodule 𝓜ᴬ ⊸ Module.leftSemimodule 𝓜ᴮ}
  where

  private
    open module Mᴬ = Module 𝓜ᴬ using (_+ᴹ_; -ᴹ_; 0ᴹ)
    open module Mᴮ = Module 𝓜ᴮ using (≈ᴹ-trans)
  open _⊸_ T

  exact⇒injective : Exact (¡ {a} {ℓa}) T → Injective T
  exact⇒injective (im⇒ker , ker⇒im) {a₁} {a₂} =
    trivialker⇒injective {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ = 𝓜ᴮ} {T}
      ((λ x → Mᴬ.≈ᴹ-sym (×.proj₂ (ker⇒im x)))
      , λ x → im⇒ker ((tt , tt) , Mᴬ.≈ᴹ-sym x))
```
