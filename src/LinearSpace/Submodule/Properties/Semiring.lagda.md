---
title: Properties of Subsemimodules over a Semiring
---

<details>
<summary>Options/Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (Semiring)
open import Algebra.Module.Bundles using (LeftSemimodule)
```

</details>

```agda
module LinearSpace.Submodule.Properties.Semiring {r ℓʳ} {𝓡 : Semiring r ℓʳ} where
```

<details>
<summary>Imports</summary>

```agda
open import Level using (Level; _⊔_; 0ℓ ; lift) renaming (suc to ℓsuc)
open import Algebra.Module.Construct.DirectProduct
  renaming (leftSemimodule to _⊕ₗ_) using ()
open import Algebra.Module.Construct.TensorUnit
  renaming (leftSemimodule to 𝓡*) using ()
open import Data.Fin using (Fin ; splitAt) ; open Fin
open import Data.Fin.Properties renaming (≡-setoid to fin)
open import Data.Nat as ℕ using (_+_)
open import Data.Product as × using (Σ ; ∃ ; ∃₂; _,_; _×_)
open import Data.Product.Relation.Binary.Pointwise.NonDependent using (_×ₛ_)
open import Data.Sum using ([_,_])
open import Data.Unit using (tt)
open import Function renaming (_∘_ to _∘ᶠ_ ; Surjection to Surjectionᶠ) using ()
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning
open import Relation.Unary
  using (Pred; _≐_ ; _∈_; U)
  renaming
    (_∩_ to _∩′_
    ; _∪_ to _∪′_
    ; _⊆_ to _⊆′_
    ; Satisfiable to Satisfiable′)
open import Relation.Binary using (Setoid)
open import Relation.Binary.PropositionalEquality using (setoid)

open import Maps.Linear.Core 𝓡 using (_⊸_; mk) renaming (_≈_ to _⊸≈_)
open import Maps.Linear.Base 𝓡 using (_∘_; ¡ ; ! ; id)
open import Maps.Linear.Properties 𝓡 using (Injective ; Surjective)
open import LinearSpace.Submodule.Core 𝓡

open SubLeftSemimodule

open Semiring 𝓡
  renaming
    ( refl to reflᴿ
    ; Carrier to R
    ; setoid to setoidᴿ
    ; _≈_ to _≈ᴿ_
    ; _*_ to _*ᴿ_
    ; _+_ to _+ᴿ_
    )
  using (0# ; 1#)

private
  variable
    a b c i ℓa ℓb ℓc ℓ : Level
```

</details>

## Image(s)/Preimage(s)

Given a semimodule homomorphism,
a subsemimodule on the domain (and codomain) induces
a predicate and correspending subsemimodule
on the codomain (resp, domain).

References:

* Theorem 3.1 in @Blyth2018

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  (𝓜ᴬ⊸𝓜ᴮ : 𝓜ᴬ ⊸ 𝓜ᴮ) where

  open LeftSemimodule 𝓜ᴬ
    renaming
      ( Carrierᴹ to A
      ; _+ᴹ_ to _+ᴬ_
      ; _*ₗ_ to _*ᴬ_
      ; *ₗ-congˡ to *ᴬ-congˡ
      ; 0ᴹ to 0ᴬ
      )
  open LeftSemimodule 𝓜ᴮ
    renaming
      ( Carrierᴹ to B
      ; _+ᴹ_ to _+ᴮ_
      ; _*ₗ_ to _*ᴮ_
      ; _≈ᴹ_ to _≈ᴮ_
      ; ≈ᴹ-sym to ≈ᴮ-sym
      ; ≈ᴹ-trans to ≈ᴮ-trans
      ; +ᴹ-cong to +ᴮ-cong
      ; *ₗ-congˡ to *ᴮ-congˡ
      ; 0ᴹ to 0ᴮ
      )

  image : SubLeftSemimodule 𝓜ᴬ ℓ → Pred B (a ⊔ ℓb ⊔ ℓ)
  image 𝓜ᴬ∈P b = ∃ λ Pa → to Pa ≈ᴮ b
      where open _⊸_ (𝓜ᴬ⊸𝓜ᴮ ∘ inclusion⊸ 𝓜ᴬ∈P)

  imageIsSub : (S : SubLeftSemimodule 𝓜ᴬ ℓ) → IsSubLeftSemimodule 𝓜ᴮ (image S)
  imageIsSub S =
        mk
          (λ { {b₁} {b₂} ((a₁ , Pa₁) , fa₁≈b₁) ((a₂ , Pa₂), fa₂≈b₂)
                → ((a₁ +ᴬ a₂) , +ᴹ-closed S Pa₁ Pa₂)
                  , ≈ᴮ-trans +-homo (+ᴮ-cong fa₁≈b₁ fa₂≈b₂)} )
          (λ {r} ((a , Pa) , fa≈b)
                → (r *ᴬ a , *ₗ-closed S Pa)
                  , ≈ᴮ-trans (≈ᴮ-sym *ₗ-homo) (*ᴮ-congˡ fa≈b) )
          ((0ᴬ , 0ᴹ∈P S) , f0≈0)
        where open _⊸_ 𝓜ᴬ⊸𝓜ᴮ

  imageSub : SubLeftSemimodule 𝓜ᴬ ℓ → SubLeftSemimodule 𝓜ᴮ (a ⊔ ℓb ⊔ ℓ)
  imageSub S = mk (image S) (imageIsSub S)

  preimage : SubLeftSemimodule 𝓜ᴮ ℓ → Pred A (b ⊔ ℓb ⊔ ℓ)
  preimage 𝓜ᴮ∈P a = ∃ λ b → to a ≈ᴮ g b
      where open _⊸_ 𝓜ᴬ⊸𝓜ᴮ
            open _⊸_ (inclusion⊸ 𝓜ᴮ∈P) renaming (to to g)

  preimageIsSub :
      (S : SubLeftSemimodule 𝓜ᴮ ℓ)
    → IsSubLeftSemimodule 𝓜ᴬ (preimage S)
  preimageIsSub S =
    mk
      (λ ((b₁ , Pb₁) , fb₁≈a₁) ((b₂ , Pb₂), fb₂≈a₂)
            → (b₁ +ᴮ b₂ , +ᴹ-closed S Pb₁ Pb₂)
            , ≈ᴮ-trans +-homo (+ᴮ-cong fb₁≈a₁ fb₂≈a₂))
      (λ {r} ((b , Pb), fb≈a)
              → (r *ᴮ b , *ₗ-closed S Pb)
                , ≈ᴮ-trans (≈ᴮ-sym *ₗ-homo) (*ᴮ-congˡ fb≈a))
      ((0ᴮ , 0ᴹ∈P S) , f0≈0)
      where open _⊸_ 𝓜ᴬ⊸𝓜ᴮ

  preimageSub :  SubLeftSemimodule 𝓜ᴮ ℓ →  SubLeftSemimodule 𝓜ᴬ (b ⊔ ℓb ⊔ ℓ)
  preimageSub S = mk (preimage S) (preimageIsSub S)
```

## Examples

### Image

The `Image` of a linear map is a subsemimodule.
The `Image` is the set of all $b : B$
such that there exists an $a : A$
for which a linear map $T$ maps $a$ to $b$.

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
         (𝓜ᴬ⊸𝓜ᴮ : 𝓜ᴬ ⊸ 𝓜ᴮ) where

  open LeftSemimodule 𝓜ᴮ renaming (Carrierᴹ to B)

  Image : Pred B _
  Image = image 𝓜ᴬ⊸𝓜ᴮ (trivial 𝓜ᴬ)

  ImageSub : SubLeftSemimodule 𝓜ᴮ (a ⊔ ℓa ⊔ ℓb)
  ImageSub = mk Image (imageIsSub 𝓜ᴬ⊸𝓜ᴮ (trivial 𝓜ᴬ))
```

### Kernel

The `Kernel` of a linear map is a subsemimodule.
The `Kernel` is set of all $a : A$ that a linear map $T$
maps to the $0$ element (in $B$).
That is, the set of solutions to an equation of linear maps.

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
         {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
         (𝓜ᴬ⊸𝓜ᴮ : 𝓜ᴬ ⊸ 𝓜ᴮ) where
  open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A)
  open LeftSemimodule 𝓜ᴮ
    renaming
      ( Carrierᴹ to B
      ; _≈ᴹ_ to _≈ᴮ_
      ; 0ᴹ to 0ᴮ
      ; ≈ᴹ-trans to ≈ᴮ-trans
      ; +ᴹ-identityˡ to +ᴮ-identityˡ
      ; +ᴹ-cong to +ᴮ-cong
      ; *ₗ-zeroʳ to *ᴮ-zeroʳ
      ; *ₗ-congˡ to *ᴮ-congˡ
      ; ≈ᴹ-refl to ≈ᴮ-refl
      )

  -- The submodule of elements equivalent to then singleton 0ᴮ
  0* : SubLeftSemimodule 𝓜ᴮ ℓb
  0* = mk
        (_≈ᴮ 0ᴮ)
        (mk
          (λ a₁≈0 a₂≈0 → ≈ᴮ-trans (+ᴮ-cong a₁≈0 a₂≈0) (+ᴮ-identityˡ 0ᴮ))
          (λ {r} a≈0 → ≈ᴮ-trans (*ᴮ-congˡ a≈0) (*ᴮ-zeroʳ r) )
          ≈ᴮ-refl)

  -- The set of all elements in A that map *to* 0ᴮ
  Kernel : Pred A _
  Kernel = preimage 𝓜ᴬ⊸𝓜ᴮ 0*

  KernelSub : SubLeftSemimodule 𝓜ᴬ (b ⊔ ℓb)
  KernelSub = mk Kernel (preimageIsSub 𝓜ᴬ⊸𝓜ᴮ 0*)
```

#### Trivial Kernel

A trivial `Kernel` is one that this equivalent to the
singleton identity element in the domain.

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb} where
  open LeftSemimodule 𝓜ᴬ renaming (0ᴹ to 0ᴬ; _≈ᴹ_ to _≈ª_)

  TrivialKernel : 𝓜ᴬ ⊸ 𝓜ᴮ → Set (a ⊔ ℓa ⊔ b ⊔ ℓb)
  TrivialKernel T = Kernel T ≐ (_≈ª 0ᴬ)
```

#### All linear maps have a Kernel

First point of @Aluffi2021 Proposition 6.2:
for all linear maps, a kernel exists.

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb} where
  open LeftSemimodule 𝓜ᴬ renaming ( 0ᴹ to 0ᴬ )
  open LeftSemimodule 𝓜ᴮ renaming ( 0ᴹ to 0ᴮ ; ≈ᴹ-refl to reflᴮ )

  ∃Kernel : ∀ (T : 𝓜ᴬ ⊸ 𝓜ᴮ) → ∃ (Kernel T)
  ∃Kernel T = 0ᴬ , (0ᴮ , reflᴮ) , _⊸_.f0≈0 T
```

### Injective ⇒ Trival Kernel

See `trivialker⇒injective` in `CommutativeRing` for other direction.

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {T : 𝓜ᴬ ⊸ 𝓜ᴮ}
  where

  open LeftSemimodule 𝓜ᴮ renaming
    ( 0ᴹ to 0ᴮ
    ; ≈ᴹ-refl to reflᴮ
    ; ≈ᴹ-sym to symᴮ
    ; ≈ᴹ-trans to transᴮ
    )

  open _⊸_ T

  inj⇒trivialker : Injective T → TrivialKernel T
  inj⇒trivialker inj =
    (λ ((b , b≈0ᴮ), fx≈b) → inj (transᴮ (transᴮ fx≈b b≈0ᴮ) (symᴮ f0≈0)))
    , λ a≈0ᴬ → (0ᴮ , reflᴮ) , transᴮ (cong a≈0ᴬ) f0≈0
```

## Submodule generated by predicate

```agda
module _ {ℓ} {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} where

  open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A)
  open import LinearSpace.Submodule.Algebra using (_⊆_; ⋂)

  -- The indexed collection of submodules
  -- that contain the given submodule `S`.
  HasCollection : {i ℓi : Level} → SubLeftSemimodule 𝓜ᴬ ℓ → Set _
  HasCollection {i} {ℓi} S =
    ∃₂ λ (I : Setoid i ℓi) (f : Setoid.Carrier I → SubLeftSemimodule 𝓜ᴬ ℓ)
      → ∀ {i : Setoid.Carrier I} → S ⊆ f i

  -- `Collection` is a triple:
  -- * a submodule `S`
  -- * a type `I` of an index set
  -- * an function indexing submodules of `𝓜ᴬ` from `I`
  -- * a proof that each of the submodules indexed contains `S`
  Collection : {i ℓi : Level} → Set _
  Collection {i} {ℓi} = ∃ (HasCollection {i} {ℓi})

  -- The submodule generated by
  -- an indexed collection of the submodules which contain `S`;
  -- i.e., the smallest submodule of 𝓜ᴬ containing `S`.
  ⟨_⟩ : ∀ {i} {ℓi} → Collection {i} {ℓi} → SubLeftSemimodule 𝓜ᴬ _
  ⟨ (X , I , f , is) ⟩ = ⋂ f

  -- The `Generates` relation
  -- states that the submodule generated by given Collection X
  -- is equivalent to a given submodule.
  _Generates_ : ∀ {i} {ℓi} → Collection {i} {ℓi} → SubLeftSemimodule 𝓜ᴬ (i ⊔ ℓ) → Set _
  X Generates S = S ≈ ⟨ X ⟩
```

## Finitely Generated Submodules/Modules

TODO: This section needs to be revisited.

```agda
-- module _
--   {ℓ}
--   {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
--   {S : SubLeftSemimodule 𝓜ᴬ ℓ}
--   where
--   open import LinearSpace.Submodule.Algebra {𝓡 = 𝓡} {𝓜ᴬ}

--   open import Finite.Core

--   IsSubFinitelyGenerated : HasCollection {ℓ = ℓ} S → Set _
--   IsSubFinitelyGenerated ( I , _ , _) = StrictlyFinite I
```

```agda
-- module _  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} where
--   open import LinearSpace.Submodule.Algebra {𝓡 = 𝓡} {𝓜ᴬ}

--   IsSubFinitelyGenerated : ∀ {ℓ} → SubLeftSemimodule 𝓜ᴬ ℓ → Set _
--   IsSubFinitelyGenerated {ℓ} S =
--     ∃ λ n →
--       Σ (Surjectionᶠ (fin n) (setoid (SubLeftSemimodule 𝓜ᴬ ℓ)))
--         λ f → S ≈ ⋂ (Surjectionᶠ.to f)

```

```agda
-- FinitelyGenerated : LeftSemimodule 𝓡 a ℓa → Set (r ⊔ a ⊔ ℓsuc ℓa)
-- FinitelyGenerated 𝓜ᴬ = IsSubFinitelyGenerated (trivial 𝓜ᴬ)
```

## Sequences of Submodules

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  where

  Exact : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴮ ⊸ 𝓜ᶜ → Set (a ⊔ ℓa ⊔ b ⊔ ℓb ⊔ c ⊔ ℓc)
  Exact T₁ T₂ = Image T₁ ≐ Kernel T₂

  SemiExact : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴮ ⊸ 𝓜ᶜ → Set (a ⊔ ℓa ⊔ b ⊔ ℓb ⊔ c ⊔ ℓc)
  SemiExact T₁ T₂ = Image T₁ ⊆′ Kernel T₂
```

### Injective T ⇒ Exact ¡ T

* one direction of Theorem 3.6 in @Blyth2018 (1)

See `exact⇒injective` in `CommutativeRing` for other direction.

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {T : 𝓜ᴬ ⊸ 𝓜ᴮ} where

  open LeftSemimodule 𝓜ᴮ
    renaming (
        ≈ᴹ-refl to reflᴮ
      ; ≈ᴹ-trans to transᴮ
      ; ≈ᴹ-sym to symᴮ)
  open _⊸_ T

  injective⇒exact : Injective T → Exact {a} {ℓa} ¡ T
  injective⇒exact inj =
     (λ {a} (_ , 0≈a) → (to a , transᴮ (symᴮ (cong 0≈a)) f0≈0) , reflᴮ)
    , λ ((_ , b≈0) , fa≈b)
      → (lift tt , lift tt) , inj (transᴮ f0≈0 (transᴮ (symᴮ b≈0) (symᴮ fa≈b)))
```

* Theorem 3.6 in @Blyth2018 (2)

```agda
module _  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
          {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
          {T : 𝓜ᴬ ⊸ 𝓜ᴮ} where

  open LeftSemimodule 𝓜ᴬ renaming ( ≈ᴹ-refl to reflᴬ )
  open LeftSemimodule 𝓜ᴮ renaming (≈ᴹ-trans to transᴮ)
  open _⊸_ T

  exact⇒surjective : Exact T ! → Surjective T
  exact⇒surjective (im⇒ker , ker⇒im) b =
     let v = ker⇒im {b} ((lift tt , lift tt) , (lift tt))
     in
       ×.proj₁ (×.proj₁ v) , λ x → transᴮ (cong x) (×.proj₂ v)

  surjective⇒exact : Surjective T → Exact T !
  surjective⇒exact sur =
      (λ _ → (lift tt , lift tt) , (lift tt))
    , λ {b} _ → ( ×.proj₁ (sur b) , lift tt) , ×.proj₂ (sur b) reflᴬ
```

### Short Exact Sequence

```agda
module _  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
          {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
          {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
          where

  ShortExact : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴮ ⊸ 𝓜ᶜ → Set (a ⊔ ℓa ⊔ b ⊔ ℓb ⊔ c ⊔ ℓc)
  ShortExact T₁ T₂ = Exact {a} {ℓa} ¡ T₁ × Exact T₁ T₂ × Exact T₂ !
```
