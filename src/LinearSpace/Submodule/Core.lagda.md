---
title: Submodules
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (Semiring)

module LinearSpace.Submodule.Core {r ℓʳ} (𝓡 : Semiring r ℓʳ) where

open import Level using (Level; _⊔_; suc; 0ℓ ; lift)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.Sub
  renaming (leftSemimodule to mkSubLeftSemimodule)
open import Algebra.Module.Construct.Coarsen
  renaming (leftSemimodule to mkQuotientLeftSemimodule)
open import Data.Product as × using (∃ ; Σ; _,_; _×_)
open import Data.Unit using (tt)
open import Function renaming (_∘′_ to _∘ᶠ_) using (_on_)
open import Relation.Unary
  using (Pred; _∈_; Satisfiable ; Empty; _≐_ )
open import Relation.Unary.Relation.Binary.Equality
open import Relation.Unary.Polymorphic
  using (U)
open import Relation.Binary
  using (Setoid; IsEquivalence; Substitutive)
open import Relation.Binary.Construct.On as On using ()
open import Relation.Nullary.Negation using (contradiction)

open import Maps.Linear.Core 𝓡
  renaming (_≈_ to _⊸≈_)
  hiding (⟦_⟧; isEquivalence; setoid)
open import Maps.Linear.Properties 𝓡 using (Injective; Surjective)
open import Maps.Linear.Base 𝓡 using (_∘_)
open import Maps.Prelinear.Core 𝓡 using (mk)

private
  variable
    a b ℓᵃ ℓᵇ : Level

open Semiring 𝓡
  renaming
    ( Carrier to R
    ; refl to reflᴿ
    )
  using
    (1# ; 0#)
```

</details>

## SubLeftSemimodule

The most common definition of a Submodule (herein: Sub(Left)Semimodule)
as found in @Blyth2018 and @Tan2014basesInS is:
a `SubSemimodule` is a subset of the `Carrierᴹ` of a `Semimodule`
that is
closed under addition and
the (left) action of the semimodule.

```agda
record IsSubLeftSemimodule {ℓ}
    (𝓜 : LeftSemimodule 𝓡 a ℓᵃ)
    (P : Pred (LeftSemimodule.Carrierᴹ 𝓜) ℓ) : Set (a ⊔ ℓᵃ ⊔ r ⊔ suc ℓ) where
  constructor mk

  open LeftSemimodule 𝓜

  field
    +ᴹ-closed : ∀ {a₁ a₂} → a₁ ∈ P → a₂ ∈ P → a₁ +ᴹ a₂ ∈ P
    *ₗ-closed : ∀ {r} {a} → a ∈ P → (r *ₗ a ∈ P)
    -- TODO: I could not include the following field,
    -- but then (as best I can figure),
    -- I would need to _≈ᴹ_ to be substitutive and P to be satisfiable
    -- (see 0ᴹ∈P')
    0ᴹ∈P : 0ᴹ ∈ P

  -- Sometimes having +ᴹ-closed and *ₗ-closed in one statement
  -- is easier to work with.
  linear-closed : ∀ {r₁ r₂ : R} {a₁ a₂ : Carrierᴹ}
    → a₁ ∈ P → a₂ ∈ P → r₁ *ₗ a₁ +ᴹ r₂ *ₗ a₂ ∈ P
  linear-closed Pa₁ Pa₂ = +ᴹ-closed (*ₗ-closed Pa₁) (*ₗ-closed Pa₂)
```

```agda
record SubLeftSemimodule
    (𝓜 : LeftSemimodule 𝓡 a ℓᵃ)
    (ℓ : Level) : Set (a ⊔ ℓᵃ ⊔ suc ℓ ⊔ r) where
  constructor mk

  open LeftSemimodule 𝓜 public

  field
    P : Pred Carrierᴹ ℓ
    isSubLeftSemimodule : IsSubLeftSemimodule 𝓜 P

  open IsSubLeftSemimodule isSubLeftSemimodule public

  subLeftSemimodule : LeftSemimodule 𝓡 (a ⊔ ℓ) ℓᵃ
  subLeftSemimodule = mkSubLeftSemimodule 𝓜 P +ᴹ-closed *ₗ-closed 0ᴹ∈P

  -- The Sub construct creates a LeftSemimodule
  -- for the subLeftSemimodule
  -- from which a homomorphism can be easily defined,
  -- namely, the inclusion map:
  inclusion⊸ : subLeftSemimodule ⊸ 𝓜
  inclusion⊸ =
      mk
        (mk (record { to = ×.proj₁ ; cong = λ x → x })) (mk ≈ᴹ-refl ≈ᴹ-refl)

  inclusion⊸Injective : Injective inclusion⊸
  inclusion⊸Injective x≈y = x≈y

  -- If the equivalence on A is Substitutive and P is Satisfiable
  -- then we can prove 0 is in P
  -- (and dispense with the 0ᴹ∈P field above).
  -- TODO: see comment above.
  -- Is there a simpler way to prove 0ᴹ ∈ P?
  -- Feels like I'm missing something obvious.
  0ᴹ∈P' : Substitutive _≈ᴹ_ ℓ → Satisfiable P → 0ᴹ ∈ P
  0ᴹ∈P' subst (x , Px) = subst P (*ₗ-zeroˡ x) (*ₗ-closed Px)

  -- If P is Empty then the only element of the subsemimodule is 0ᴹ.
  Empty⇒S≡｛0ᴹ｝ : Empty P → ∀ (a : Carrierᴹ) → P a → a ≈ᴹ 0ᴹ
  Empty⇒S≡｛0ᴹ｝ empty x Px = contradiction Px (empty x)
```

## Equivalence

Equivalence of `SubLeftSemimodule`s is equivalence of the predicates.

```agda
module _  {ℓ} {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} where

  open LeftSemimodule 𝓜ᴬ using (Carrierᴹ)

  ⟦_⟧ : (S : SubLeftSemimodule 𝓜ᴬ ℓ) → Pred Carrierᴹ ℓ
  ⟦_⟧ = SubLeftSemimodule.P

  infix 4 _≈_
  _≈_ : SubLeftSemimodule 𝓜ᴬ ℓ → SubLeftSemimodule 𝓜ᴬ ℓ → Set (a ⊔ ℓ)
  _≈_ = _≐_ on ⟦_⟧

  isEquivalence : IsEquivalence _≈_
  isEquivalence = On.isEquivalence ⟦_⟧ ≐-isEquivalence

  setoid : Setoid (r ⊔ a ⊔ ℓᵃ ⊔ suc ℓ) (a ⊔ ℓ)
  setoid = record {isEquivalence = isEquivalence}
```

## Trivial Submodule

Every `LeftSemimodule` is trivially a `SubLeftSemimodule`
by the universal predicate.

TODO: Not sure "trivial" is the right terminology here.

```agda
trivial : (𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ) → SubLeftSemimodule 𝓜ᴬ ℓᵃ
trivial 𝓜ᴬ = mk U (mk (λ _ _ → lift tt) (λ _ → lift tt) (lift tt))
```

## NonEmpty subLeftSemimodule

```agda
NonEmpty : ∀ {ℓ} {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ}
  → SubLeftSemimodule 𝓜ᴬ ℓ → Set (a ⊔ ℓ)
NonEmpty = Satisfiable ∘ᶠ ⟦_⟧
```
