---
title: Algebra of Submodules
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (Semiring)
open import Algebra.Module.Bundles using (LeftSemimodule)

module LinearSpace.Submodule.Algebra
   {a r ℓᵃ ℓʳ}
   {𝓡 : Semiring r ℓʳ}
   {𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ} where

open import Level using (Level; _⊔_; suc)
open import Data.Product as × using (Σ ; ∃ ; _,_; _×_)
open import Data.Sum as ⊎ using (inj₁ ; inj₂; _⊎_)
open import Function renaming (_∘_ to _∘ᶠ_)
open import Relation.Unary
  using (Pred ; _≐_ )
  renaming (_∩_ to _∩′_; _∪_ to _∪′_ ; _⊆_ to _⊆′_)

open import LinearSpace.Submodule.Core 𝓡
open SubLeftSemimodule hiding (Carrierᴹ)
open LeftSemimodule 𝓜ᴬ using (Carrierᴹ)

private
  variable
    i ℓ : Level
```

</details>

## Operations on SubLeftSemimodules

```agda
_⊆_ : SubLeftSemimodule 𝓜ᴬ ℓ → SubLeftSemimodule 𝓜ᴬ ℓ → Set (a ⊔ ℓ)
S₁ ⊆ S₂ = P S₁ ⊆′ P S₂

_⊇_ : SubLeftSemimodule 𝓜ᴬ ℓ → SubLeftSemimodule 𝓜ᴬ ℓ → Set (a ⊔ ℓ)
S₁ ⊇ S₂ = S₂ ⊆ S₁

_∩ₛ_ : {P₁ P₂ : Pred Carrierᴹ ℓ}
    → IsSubLeftSemimodule 𝓜ᴬ P₁
    → IsSubLeftSemimodule 𝓜ᴬ P₂
    → IsSubLeftSemimodule 𝓜ᴬ (P₁ ∩′ P₂)
(mk add₁ scale₁ 0₁) ∩ₛ (mk add₂ scale₂ 0₂) =
    mk (×.zip′ add₁ add₂) (×.map scale₁ scale₂) (0₁ , 0₂)

_∩_ : SubLeftSemimodule 𝓜ᴬ ℓ → SubLeftSemimodule 𝓜ᴬ ℓ
    → SubLeftSemimodule 𝓜ᴬ ℓ
(mk _ is₁) ∩ (mk _ is₂) = mk _ (is₁ ∩ₛ is₂)

_+ₛ_ : ∀ {P₁ P₂ : Pred Carrierᴹ ℓ} {inclusion : P₁ ⊆′ P₂}
    → IsSubLeftSemimodule 𝓜ᴬ P₁
    → IsSubLeftSemimodule 𝓜ᴬ P₂
    → IsSubLeftSemimodule 𝓜ᴬ (P₁ ∪′ P₂)
_+ₛ_ {inclusion = P₁⊆P₂} (mk add₁ scale₁ 0₁) (mk add₂ scale₂ 0₂) =
    mk
      (λ { (inj₁ P₁a₁) (inj₁ P₁a₂) → inj₁ (add₁ P₁a₁ P₁a₂)
        ; (inj₁ P₁a₁) (inj₂ P₂a₂) → inj₂ (add₂ (P₁⊆P₂ P₁a₁) P₂a₂ )
        ; (inj₂ P₂a₁) (inj₁ P₁a₂) → inj₂ (add₂ P₂a₁ (P₁⊆P₂ P₁a₂) )
        ; (inj₂ P₂a₁) (inj₂ P₂a₂) → inj₂ (add₂ P₂a₁ P₂a₂)
        })
      (⊎.map scale₁ scale₂)
      (inj₁ 0₁)

_+_[_] : (S₁ S₂ : SubLeftSemimodule 𝓜ᴬ ℓ)
    → S₁ ⊆ S₂
    → SubLeftSemimodule 𝓜ᴬ ℓ
(mk _ is₁) + (mk _ is₂) [ S₁⊆S₂ ] = mk _ (_+ₛ_ {inclusion = S₁⊆S₂} is₁ is₂)
```

## Intersection of an indexed set

The intersection of an indexed set of submodules is a submodule.

* Theorem 2.1 in @Blyth2018

```agda
⋂ : ∀ {I : Set i} → (I → SubLeftSemimodule 𝓜ᴬ ℓ) → SubLeftSemimodule 𝓜ᴬ (i ⊔ ℓ)
⋂ {I = I} f =
  mk
    (λ a → ∀ (i : I) → P (f i) a)
    (mk
        (λ g₁ g₂ i → +ᴹ-closed (f i) (g₁ i) (g₂ i))
        (λ g i → *ₗ-closed (f i) (g i))
        λ i → 0ᴹ∈P (f i))
```

## Properties

...a start...

```agda
S₁⊆S₂⇒S₁+S₂≈S₂ : {S₁ S₂ : SubLeftSemimodule 𝓜ᴬ ℓ}
    → (⊆ : S₁ ⊆ S₂) → S₁ + S₂ [ ⊆ ] ≈ S₂
S₁⊆S₂⇒S₁+S₂≈S₂ S₁⊆S₂ =
    (λ { (inj₁ P₁) → S₁⊆S₂ P₁; (inj₂ P₂) → P₂} ) , λ P₂ → inj₂ P₂

-- see https://en.wikipedia.org/wiki/Modular_lattice
PredIsModular : {P₁ P₂ P₃ : Pred Carrierᴹ ℓ}
  → P₁ ⊆′ P₃
  → P₁ ∪′ (P₂ ∩′ P₃) ≐ (P₁ ∪′ P₂) ∩′ P₃
PredIsModular P₁⊆P₃  =
      (λ { (inj₁ P₁a) → inj₁ P₁a , P₁⊆P₃ P₁a
          ; (inj₂ (P₂a , P₃a)) → inj₂ P₂a , P₃a })
    , λ { (inj₁ P₁a , P₃a) → inj₁ P₁a
        ; (inj₂ P₂a , P₃a) → inj₂ (P₂a , P₃a)
        }

-- TODO: complete this proof
-- -- Theorem 2.4 in @Blyth2018 (Modular Law of Submodules)
-- SubIsModular : {S₁ S₂ S₃ : SubLeftSemimodule 𝓜ᴬ ℓ}
--   → S₁ ⊆ S₃
--   → S₁ + (S₂ ∩ S₃) [ {!   !} ] ≈ (S₁ + S₂ [ {!   !} ]) ∩ S₃
--   -- → S₃ ⊆ S₁
--   -- → S₁ ∩ (S₂ + S₃) ≈ (S₁ ∩ S₂) + S₃
--   --   ^^ is how the theorem is stated in Blyth
-- SubIsModular S₁⊆S₃ = PredIsModular S₁⊆S₃
```
