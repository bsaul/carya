---
title: Dual of a linear space
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (CommutativeSemiring)

module LinearSpace.Dual.Core {r ℓr} (𝓡 : CommutativeSemiring r ℓr) where

private module 𝓡 = CommutativeSemiring 𝓡

open import Level using (Level; _⊔_)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.TensorUnit
  renaming (leftSemimodule to 𝓡*)

private
   variable
      a ℓa : Level

open import Maps.Linear.Construct.Functional 𝓡
open import Maps.Linear.Module 𝓡
```

</details>

## Dual space of a LeftSemimodule

`Dual` defines the [dual space](https://en.wikipedia.org/wiki/Dual_space)
the set of all linear functionals for a `LeftSemimodule`.
The `_*` function creates the `LeftSemimodule` in a Dual space.
The `Carrierᴹ` of the `LeftSemimodule` resulting from `_*`
has type `Dual M`.

```agda
_* : LeftSemimodule 𝓡.semiring a ℓa
   → LeftSemimodule 𝓡.semiring (r ⊔ ℓr ⊔ a ⊔ ℓa) (ℓr ⊔ a)
𝓜 * = record
  { Carrierᴹ = LinearFunctional 𝓜
  ; isLeftSemimodule = LF.isLeftSemimodule
  }
  where module LF = LeftSemimodule (leftSemimodule 𝓜 𝓡*)
  -- _* is more verbose than needed
  -- in order to clearly show what the Carrierᴹ is.
```

## Double Dual space of a LeftSemimodule

```agda
Dual² : LeftSemimodule 𝓡.semiring a ℓa → Set (r ⊔ ℓr ⊔ a ⊔ ℓa)
Dual² M = LinearFunctional (M *)
```

The `_**` function creates the `LeftSemimodule` in a Double Dual space.

```agda
_** : LeftSemimodule 𝓡.semiring a ℓa
    → LeftSemimodule 𝓡.semiring (r ⊔ ℓr ⊔ a ⊔ ℓa) (r ⊔ ℓr ⊔ a ⊔ ℓa)
M ** = (M *) *
```
