---
title: Operations on Linear Maps of Dual Spaces
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --without-K #-}

open import Algebra.Bundles using (CommutativeSemiring)

module LinearSpace.Dual.Base {r ℓr} (𝓡 : CommutativeSemiring r ℓr) where

private
  module 𝓡 = CommutativeSemiring 𝓡

-- stdlib
open import Level using (Level; _⊔_)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.Vector
  renaming (mkLeftSemimodule to [_])
open import Algebra.Properties.CommutativeSemigroup 𝓡.*-commutativeSemigroup
  using (x∙yz≈yx∙z)
open import Data.Fin using (Fin)
open import Data.Nat using (ℕ)
open import Function as F using ()
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

-- carya
open import Fold.Properties.CommutativeSemiring ⦃ 𝓡 ⦄
open import Fold.Properties.Indicator ⦃ 𝓡 ⦄
open import Maps.Linear.Core 𝓡.semiring
open import Maps.Linear.Base 𝓡.semiring as L using ()
open import Maps.Linear.Module 𝓡
open import Maps.Linear.Construct.Functional 𝓡 using (LinearFunctional)
open import LinearSpace.Dual.Core 𝓡
open import LinearSpace.DotProduct.Core 𝓡
open import LinearSpace.DotProduct.Properties.CommutativeSemiring 𝓡


private
   variable
      a b ℓa ℓb : Level
      m : ℕ
```

</details>

## Transpose

The `transpose` of a linear map
contravariantly takes the domain and codomain to their respective `Dual`.

```agda
module _
   {𝓜ᴬ : LeftSemimodule 𝓡.semiring a ℓa}
   {𝓜ᴮ : LeftSemimodule 𝓡.semiring b ℓb}
   where

   transpose : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴮ * ⊸ 𝓜ᴬ *
   transpose T = mk⊸ (L._∘ T) (λ x → x) 𝓡.refl 𝓡.refl
```

## Relationship(s) between a Finite linear space and its `Dual`

```agda
vec⇒Dual : (Fin m → 𝓡.Carrier) → LinearFunctional [ Fin m ]
vec⇒Dual f = mk⊸ (f ·_) (·-congˡ f) (·-distribˡ-+ f) (*-scale f)

m⊸m* : [ Fin m ] ⊸ [ Fin m ] *
m⊸m* = mk⊸
   vec⇒Dual
   (λ x → ∑-cong λ i → 𝓡.*-congʳ (x {i}))
   (λ {a = a} → ·-distribʳ-+ a)
   λ {r} {x} {y} → begin
      r 𝓡.* (x · y)                  ≈⟨ *-scale x {r} {y} ⟩
      ∑ (λ i → x i 𝓡.* (r 𝓡.* y i))  ≈⟨ ∑-cong (λ i → x∙yz≈yx∙z (x i) r (y i)) ⟩
      ∑ (λ i → r 𝓡.* x i 𝓡.* y i )   ∎
  where open ≈-Reasoning 𝓡.setoid

m*⊸m : [ Fin m ] * ⊸ [ Fin m ]
m*⊸m = mk⊸ (λ T i → T ⟨$⟩ 𝕀 i) (λ x → x) 𝓡.refl 𝓡.refl
```

## Relationship(s) between a linear space and it double `Dual`

```agda
module _ {𝓜 : LeftSemimodule 𝓡.semiring a ℓa} where

   private open module 𝓜 = LeftSemimodule 𝓜 renaming (Carrierᴹ to A)

   ev : A → Dual² 𝓜
   ev a = mk⊸ (_⟨$⟩ a) (λ x → x) 𝓡.refl 𝓡.refl

   M⊸M** : 𝓜 ⊸ 𝓜 **
   M⊸M** = mk⊸
      ev
      (λ {f} fx≈gx {T} → _⊸_.cong T {f} fx≈gx)
      (λ {a₁} {a₂} {T} → _⊸_.+-homo T {a₁} {a₂})
       λ {r} {a} {T}   → _⊸_.*ₗ-homo T {r} {a}
```

### Double `Dual` relationships specialized to `Vector`s

```agda
m⊸m** : [ Fin m ] ⊸ [ Fin m ] **
m⊸m** {m} = M⊸M** {𝓜 = [ Fin m ]}

toDual : Fin m → LinearFunctional ([ Fin m ])
toDual i = mk⊸ (λ f → f i) (λ x → x) 𝓡.refl 𝓡.refl

Dual²⇒vec : Dual² [ Fin m ] → Fin m → 𝓡.Carrier
Dual²⇒vec T = (T ⟨$⟩_) F.∘ toDual

m**⊸m : [ Fin m ] ** ⊸ [ Fin m ]
m**⊸m = mk⊸ Dual²⇒vec (λ x → x) 𝓡.refl 𝓡.refl
```
