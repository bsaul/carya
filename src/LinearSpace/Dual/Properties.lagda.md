---
title: Properties of Dual spaces and Linear Functionals
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --without-K #-}

open import Algebra.Bundles using (CommutativeSemiring ; Semiring)

module LinearSpace.Dual.Properties
   {r ℓr}
   (𝓡 : CommutativeSemiring r ℓr)
   where

open import Level using (Level; _⊔_)

private
   open module 𝓡 = CommutativeSemiring 𝓡
                        renaming
                           ( refl to reflᴿ
                           ; Carrier to R
                           ; _≈_ to _≈ᴿ_
                           ; _+_ to _+ᴿ_
                           ; _*_ to _*ᴿ_)
   variable
      a b ℓa ℓb : Level

-- stdlib
open import Level using (Level; _⊔_)
open import Algebra.Definitions using (Involutive)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.TensorUnit
   renaming (
     leftSemimodule to TU-leftSemimodule
   )
open import Algebra.Module.Construct.Vector
  renaming (mkLeftSemimodule to [_])
open import Data.Fin using (Fin)
open import Data.Product using (_,_)
open import Function renaming (_∘_ to _∘ᶠ_)
   using (flip; Injective; Surjective)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

-- carya
open import Fold.Properties.CommutativeSemiring ⦃ 𝓡 ⦄
open import Fold.Properties.Indicator ⦃ 𝓡 ⦄
open import LinearSpace.Dual.Core 𝓡
open import LinearSpace.Dual.Base 𝓡
open import Maps.Linear semiring
```

</details>

## Bilinear Properties of the "canonical" dual map

```agda
module CanonicalIsBilinear {𝓜ᴬ : LeftSemimodule semiring a ℓa} where
   open LeftSemimodule 𝓜ᴬ
      renaming (
        Carrierᴹ to A
      ; _+ᴹ_ to _+ᴬ_
      ; _*ₗ_ to _*ᴬ_
      )
      using ()

   open LeftSemimodule (𝓜ᴬ *)
      renaming (
         Carrierᴹ to Aᴰ
      ; _*ₗ_ to _*ᵈ_
      ) using ()

   ⟨_,_⟩ : A → Aᴰ → R
   ⟨ a , aᴰ ⟩ = aᴰ ⟨$⟩ a

   .linearₗ : ∀ {a₁ a₂ : A} {aᴰ : Aᴰ}
       → ⟨ a₁ +ᴬ a₂ , aᴰ ⟩ ≈ᴿ ⟨ a₁ , aᴰ ⟩ +ᴿ ⟨ a₂ , aᴰ ⟩
   linearₗ {aᴰ = aᴰ} = _⊸_.+-homo aᴰ

   linearᵣ : ∀ {a : A} {a₁ᴰ a₂ᴰ : Aᴰ}
       → ⟨ a , a₁ᴰ + a₂ᴰ ⟩ ≈ᴿ ⟨ a , a₁ᴰ ⟩ +ᴿ ⟨ a , a₂ᴰ ⟩
   linearᵣ {a₁ᴰ = a₁ᴰ} = reflᴿ

   .scaleₗ :  ∀ {r : R} {a : A} {aᴰ : Aᴰ}
       → r *ᴿ ⟨ a , aᴰ ⟩ ≈ᴿ ⟨ r *ᴬ a , aᴰ ⟩
   scaleₗ {aᴰ = aᴰ} = _⊸_.*ₗ-homo aᴰ

   scaleᵣ :  ∀ {r : R} {a : A} {aᴰ : Aᴰ}
       → ⟨ a , r *ᵈ aᴰ ⟩ ≈ᴿ ⟨ a , aᴰ ⟩ *ᴿ r
   scaleᵣ {r = r} {a = a} {aᴰ = aᴰ} = CommutativeSemiring.*-comm 𝓡 r (aᴰ ⟨$⟩ a)
```

## Properties of `transpose`

```agda
module _ {𝓜ᴬ : LeftSemimodule semiring a ℓa}  where

   private
      _ᵀ = transpose {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ}

   transpose-identity : id ᵀ ≈ id
   transpose-identity = reflᴿ
```

```agda
module _ {𝓜ᴬ 𝓜ᴮ 𝓜ᶜ : LeftSemimodule semiring a ℓa}  where

   private
      _ᵀ = transpose

   transpose-∘-contravariant : ∀ {T₁ : 𝓜ᴮ ⊸ 𝓜ᶜ} {T₂ : 𝓜ᴬ ⊸ 𝓜ᴮ}
       → (T₁ ∘ T₂) ᵀ ≈ T₂ ᵀ ∘ T₁ ᵀ
   transpose-∘-contravariant = reflᴿ
```

```agda
module _
   {𝓜ᴬ : LeftSemimodule semiring a ℓa}
   {𝓜ᴮ : LeftSemimodule semiring b ℓb}
   where

   private
     _ᵀ = transpose

   transpose-+-covariant : ∀ {T₁ T₂ : 𝓜ᴬ ⊸ 𝓜ᴮ}
       → ( T₁ + T₂ ) ᵀ ≈ (T₁ ᵀ) + (T₂ ᵀ)
   transpose-+-covariant {a = x} = _⊸_.+-homo x
```

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡.semiring a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡.semiring b ℓb}
  {∪ᴮ : 𝓜ᴮ ** ⊸ 𝓜ᴮ}
  where

   private
      module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
      module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ

      _ᵀᵀ : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᴮ
      T ᵀᵀ = ∪ᴮ ∘ (transpose (transpose T)) ∘ M⊸M**

  -- TODO: prove transpose involutive in general case
   -- transpose-involutive : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} → T ≈ T ᵀᵀ
   -- transpose-involutive {T} {x} = {!   !}
   --   where open _⊸_ T
   --         module ∪ᴮ = _⊸_ ∪ᴮ
```

```agda
module _ {m n} where

   private
      _ᵀᵀ : [ Fin m ] ⊸ [ Fin n ] → [ Fin m ] ⊸ [ Fin n ]
      T ᵀᵀ = (m**⊸m {n}) ∘ (transpose (transpose T)) ∘ m⊸m** {m}

   transpose-involutive : ∀ {T : [ Fin m ] ⊸ [ Fin n ]} → T ≈ T ᵀᵀ
   transpose-involutive {T} {x} = 𝓡.refl
```


## Relationships between a Finite vector space and its Double `Dual`

```agda
module _ {m} where

   private
      module Finm = LeftSemimodule {semiring = semiring} [ Fin m ]
      module Finm** = LeftSemimodule {semiring = semiring} ([ Fin m ] ** )

   m⊸m**-injective : Injective Finm._≈ᴹ_ Finm**._≈ᴹ_ (_⊸_.to (m⊸m** {m}))
   m⊸m**-injective T {i} = T {a = toDual i}

   m**⊸m-surjective : Surjective Finm**._≈ᴹ_ Finm._≈ᴹ_ (_⊸_.to (m**⊸m {m}))
   m**⊸m-surjective f = ev f , λ x {i} → x {a = toDual i}
```

## Relationships between a Finite vector space and its `Dual`

```agda
module _ {m} where

   private
      module Finm = LeftSemimodule {semiring = semiring} [ Fin m ]
      module Finm* = LeftSemimodule {semiring = semiring} ([ Fin m ] *)

   m⊸m*-injective : Injective Finm._≈ᴹ_ Finm*._≈ᴹ_ (_⊸_.to m⊸m*)
   m⊸m*-injective {f} {g} x {i} = begin
      f i                     ≈⟨ ∑𝕀*f≈fi i f ⟨
      ∑ (λ j →  𝕀 i j *ᴿ f j) ≈⟨ ∑-cong {m} (λ j → 𝓡.*-comm (𝕀 i j) (f j)) ⟩
      ∑ (λ j → f j *ᴿ 𝕀 i j)  ≈⟨ x {a = 𝕀 i} ⟩
      ∑ (λ j →  g j *ᴿ 𝕀 i j) ≈⟨ ∑-cong {m} (λ j → 𝓡.*-comm (g j) (𝕀 i j)) ⟩
      ∑ (λ j →  𝕀 i j *ᴿ g j) ≈⟨ ∑𝕀*f≈fi i g ⟩
      g i ∎
      where open ≈-Reasoning 𝓡.setoid

   m*⊸m-surjective : Surjective Finm*._≈ᴹ_ Finm._≈ᴹ_ (_⊸_.to m*⊸m)
   m*⊸m-surjective f = vec⇒Dual f , λ {T} x {i} →
      let
         module T = _⊸_ T
      in begin
      T.to (λ j → 𝕀 i j)       ≈⟨ x {a = 𝕀 i} ⟩
      ∑ (λ j → f j *ᴿ 𝕀 i j)   ≈⟨ ∑-cong {m} (λ j → 𝓡.*-comm (f j) (𝕀 i j)) ⟩
      ∑ (λ j →  𝕀 i j *ᴿ f j)  ≈⟨ ∑𝕀*f≈fi i f ⟩
      f i                      ∎
      where open ≈-Reasoning 𝓡.setoid
```
