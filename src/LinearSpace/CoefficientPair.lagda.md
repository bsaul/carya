---
title: LeftSemimodule of pairs of "Coefficients" with a `Carrierᴹ`
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (Semiring)
open import Algebra.Module.Bundles using (LeftSemimodule)

module LinearSpace.CoefficientPair
  {a r ℓ ℓʳ} {𝓡 : Semiring r ℓʳ}
  (𝓜ᴬ : LeftSemimodule 𝓡 a ℓ)
  where

open Semiring 𝓡 renaming (Carrier to R) hiding (zero)
open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A)

open import Algebra.Module.Construct.DirectProduct
  renaming (leftSemimodule to _⊕_) using ()
open import Algebra.Module.Construct.TensorUnit
  renaming (leftSemimodule to 𝓡*) using ()
open import Data.Product using (_,_; map₁ ; proj₁ ; proj₂)
open import Data.Fin using (Fin); open Fin
open import Data.Nat using (ℕ)
open import Data.Vec.Functional renaming (map to mapᵥ)
open import Data.Vec.Relation.Unary.Unique.Setoid ≈ᴹ-setoid using (Unique)
open import Data.Vec.Functional.Relation.Unary.All
open import Function using (_∘_)
open import Level using (_⊔_)
open import Relation.Unary using (Pred ; Satisfiable)

import Relation.Binary.Reasoning.Setoid as ≈-Reasoning
```

</details>

## LeftSemimodule of "Coefficients" with a `Carrierᴹ`

The `Carrierᴹ` of this LeftSemimodule are pairs of
"coefficients" (element of the base `Semiring`) and
"vectors" (elements of the given `LeftSemimodule`).

```agda
mkCoefficients : LeftSemimodule 𝓡 (r ⊔ a) (ℓʳ ⊔ ℓ)
mkCoefficients = 𝓡* ⊕ 𝓜ᴬ
```

## Computational Formula for linear combinations

A linear combination is a (finite) sum of products
of scalars (elements of the semiring)
and elements *of a given SubLeftSemimodule*
(i.e. elements of LeftSemimodule satisfying some predicate).
Here, we define the computational formula for a *finite* linear combination.

```agda
open LeftSemimodule (mkCoefficients) public
  renaming (Carrierᴹ to Term ; ≈ᴹ-setoid to Term-setoid)
  using ()

open import Fold.Setoid.Core Term-setoid

term : Term → A
term (r , a) = r *ₗ a

∑ : ∀ {n} → (Fin n → Term) → A
∑ xs = fold ⦃ +ᴹ-monoid ⦄ xs term
```

## Utility functions

```agda
coefficients : ∀ {n} → (Fin n → Term) → Fin n → R
coefficients = mapᵥ proj₁

vectors : ∀ {n} → (Fin n → Term) → Fin n → A
vectors = mapᵥ proj₂
```

## Properties

### `∑` preserves the left action (scalar multiplication)

```agda
∑-preserves-⋆ₗ : ∀ {n} → (f : Fin n → Term)
  → ∀ (r : R) → r *ₗ ∑ f ≈ᴹ ∑ (map₁ (r *_) ∘ f)
∑-preserves-⋆ₗ {n = ℕ.zero} _ r = *ₗ-zeroʳ r
∑-preserves-⋆ₗ {n = ℕ.suc n} f r =
    begin
      r *ₗ ∑ f
    ≡⟨⟩
      r *ₗ (term f0 +ᴹ ∑ {n = n} (f ∘ suc))
    ≈⟨ *ₗ-distribˡ r (term (f zero)) (∑ {n = n} (f ∘ suc) ) ⟩
      r *ₗ (term f0) +ᴹ r *ₗ ∑ {n = n} (f ∘ suc)
    ≈⟨ +ᴹ-congˡ (∑-preserves-⋆ₗ {n = n} (f ∘ suc) r) ⟩
      r *ₗ (term f0) +ᴹ ∑ (map₁ (_*_ r) ∘ (f ∘ suc))
    ≈⟨  +ᴹ-congʳ (≈ᴹ-sym (*ₗ-assoc r (proj₁ f0) (proj₂ f0))) ⟩
      ∑ ( map₁ (r *_) ∘ f)
    ∎
    where open ≈-Reasoning ≈ᴹ-setoid
          f0 = f zero
```

### Linearly Independent

A set of elements of `𝓜ᴬ` that satisfy some `Satisfiable` predicate `P`
(i.e. a nonempty subset)
is __linearly independent__ (or "free")
if all vectors of *distinct* elements that
both satisfy `P` and
sum to 0
implies that the coefficients of those vectors are all 0.

```agda
LinearlyIndepedent : ∀ {ℓ} (P : Pred A ℓ) → {_ : Satisfiable P} → Set _
LinearlyIndepedent P =
  ∀ {n}
  → ∀ (x : Fin n → Term)
  → All P (vectors x)              -- Satisfy P
  → Unique (toVec (vectors x))     -- Distinct elements of 𝓜
  → ∑ x ≈ᴹ 0ᴹ                      -- Sum to 0
  → All (_≈ 0#) (coefficients x)   -- All coefficients are 0
```
