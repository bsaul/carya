---
title: Leftsemimodule of Predicates
---

<details>
<summary>Options</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}
```

</details>

```agda
module LinearSpace.Spaces.Predicates where
```

<details>
<summary>Imports</summary>

```agda
open import Algebra.Bundles
open import Algebra.Module.Bundles
open import Algebra.Module.Construct.Pointwise as F using ()
open import Algebra.Module.Construct.TensorUnit as T using ()
open import Level using (Level; _⊔_; suc)
open import Data.Product.Algebra

private
  variable
    a ℓ : Level

×-⊎-semiring : (ℓ : Level) → Semiring (suc ℓ) ℓ
×-⊎-semiring ℓ = CommutativeSemiring.semiring (×-⊎-commutativeSemiring ℓ)
```

</details>

## Predicates form a `LeftSemimodule` over the semiring of types

```agda
leftSemimodule : Set a → LeftSemimodule (×-⊎-semiring ℓ) (a ⊔ suc ℓ) (a ⊔ ℓ)
leftSemimodule A = F.mkLeftSemimodule A T.leftSemimodule
```

## Predicates form a `RightSemimodule` over the semiring of types

```agda
rightSemimodule : Set a → RightSemimodule (×-⊎-semiring ℓ) (a ⊔ suc ℓ) (a ⊔ ℓ)
rightSemimodule A = F.mkRightSemimodule A T.rightSemimodule
```

## Predicates form a `BiSemimodule` over the semiring of types

```agda
biSemimodule : Set a → Bisemimodule (×-⊎-semiring ℓ) (×-⊎-semiring ℓ) (a ⊔ suc ℓ) (a ⊔ ℓ)
biSemimodule A = F.mkBisemimodule A T.bisemimodule
```

## Predicates form a `Semimodule` over the commutative semiring of types

```agda
semimodule : Set a → Semimodule (×-⊎-commutativeSemiring ℓ) (a ⊔ suc ℓ) (a ⊔ ℓ)
semimodule A = F.mkSemimodule A T.semimodule
```
