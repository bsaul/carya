---
title: Positive Dual spaces and Linear Functionals
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Partial.Bundles

module LinearSpace.PositiveDual.Core
   {r ℓr₁ ℓr₂}
   (𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂)
   where

open import Level using (Level; _⊔_)
open import Algebra.Bundles
open import Algebra.Module.Ordered.Partial.Bundles using (PoLeftSemimodule)
open import Algebra.Module.Ordered.Partial.Construct.TensorUnit
   renaming (mkPoLeftSemimodule to 𝓡!)

open PoCommutativeSemiring 𝓡
   renaming
      (poSemiring to 𝓡*
      ; Carrier to R
      ; _≤_ to _≤ᴿ_
      ; 0# to 0ᴿ
      )

open import Maps.PositiveLinear.Construct.Functional 𝓡

private
   variable
      a ℓa₁ ℓa₂ : Level
```

</details>

## Dual space of an `PoLeftSemimodule`

See [Maps.Linear.Dual.Core](src/LinearMap/Dual/Core.lagda.md)
for details and description of a Dual space.
The `Dual⁺` defined here is over ordered spaces
and the linear map is a postive linear map.

```agda
module _ (rPos : ∀ r → 0ᴿ ≤ᴿ r) where

   _* : PoLeftSemimodule 𝓡* a ℓa₁ ℓa₂
      → PoLeftSemimodule 𝓡* (a ⊔ r ⊔ ℓa₁ ⊔ ℓa₂ ⊔ ℓr₂ ⊔ ℓr₁) (a ⊔ ℓr₁) (a ⊔ ℓr₂)
   𝓜 * = record
    { Carrierᴹ = PositiveLinearFunctional 𝓜
    ; isPoLeftSemimodule = PLF.isPoLeftSemimodule
    }
    where open import Maps.PositiveLinear.Module 𝓡 𝓜 𝓡!
          module PLF = PoLeftSemimodule (poLeftSemimodule {rPos})

   _** : PoLeftSemimodule 𝓡* a ℓa₁ ℓa₂
       → PoLeftSemimodule 𝓡*
            (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ a ⊔ ℓa₁ ⊔ ℓa₂)
            (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ a ⊔ ℓa₁ ⊔ ℓa₂)
            (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ a ⊔ ℓa₁ ⊔ ℓa₂)
   M ** = (M *) *
```
