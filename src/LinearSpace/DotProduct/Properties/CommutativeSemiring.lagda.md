---
title: Properties of Vector Dot Product over a Commutative Semiring
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Algebra.Bundles using (CommutativeSemiring)

module LinearSpace.DotProduct.Properties.CommutativeSemiring
  {c ℓ} (𝓡 : CommutativeSemiring c ℓ) where

private
  open module 𝓡 = CommutativeSemiring 𝓡
                    renaming (Carrier to R)
                    hiding (zero)

open import Algebra.Module.Construct.Vector
  renaming (finLeftSemimodule to vector)
open import Algebra.Properties.CommutativeSemigroup +-commutativeSemigroup
  using (interchange)
open import Algebra.Properties.CommutativeSemigroup *-commutativeSemigroup
  using (x∙yz≈y∙xz)
open import Algebra.Properties.Monoid.Sum +-monoid
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Data.Fin using (Fin); open Fin
open import Data.Nat as ℕ using (ℕ)
open import Data.Vec.Functional using (Vector ; zipWith)
open import Function using (id ; _∘_)

import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

-- carya
open import LinearSpace.DotProduct.Core 𝓡

private
  variable
    m : ℕ
```

</details>

## Properties

```agda
·-congˡ : ∀ {m} (x : Vector R m) {y z : Vector R m}
    → (∀ {i} → y i ≈ z i) → (x · y) ≈ (x · z)
·-congˡ {m = ℕ.zero} _ _ = refl
·-congˡ {m = ℕ.suc m} x {y} {z} eq = +-cong
      (*-congˡ (eq {zero}))
      (·-congˡ (x ∘ suc) λ {i} → eq {suc i})

·-congʳ : ∀ {m} (x : Vector R m) {y z : Vector R m}
    → (∀ {i} → y i ≈ z i) → (y · x) ≈ (z · x)
·-congʳ {m = ℕ.zero} _ _ = refl
·-congʳ  {m = ℕ.suc m} x {y} {z} eq = +-cong
    (*-congʳ (eq {zero}))
    (·-congʳ (x ∘ suc) λ {i} → eq {suc i})
```

```agda
·-distribˡ-+ : ∀ {m} (x : Vector R m) {y z : Vector R m}
    → (x · (LeftSemimodule._+ᴹ_ (vector {𝓡 = semiring} m) y z)) ≈ (x · y) + (x · z)
·-distribˡ-+ {m = ℕ.zero} _ = sym (+-identityˡ 0#)
·-distribˡ-+ {m = ℕ.suc m} x {y} {z} =
  begin
    x · LeftSemimodule._+ᴹ_ (vector {𝓡 = semiring} (ℕ.suc m)) y z
  ≈⟨ +-cong
      (distribˡ (x zero) (y zero) (z zero))
      (·-distribˡ-+ (x ∘ suc))
    ⟩
       x zero * y zero + x zero * z zero
    + ((x ∘ suc · y ∘ suc) + (x ∘ suc · z ∘ suc))
  ≈⟨ interchange
        (x zero * y zero)
        (x zero * z zero)
        (x ∘ suc · y ∘ suc)
        (x ∘ suc · z ∘ suc)
    ⟩
    (x · y) + (x · z)
  ∎
  where open ≈-Reasoning setoid

·-distribʳ-+ : ∀ {m} (x : Vector R m) {y z : Vector R m}
    → (LeftSemimodule._+ᴹ_ (vector {𝓡 = semiring} m) y z) · x ≈ (y · x) + (z · x)
·-distribʳ-+ {m = ℕ.zero} _ = sym (+-identityˡ 0#)
·-distribʳ-+ {m = ℕ.suc m} x {y} {z} =
  begin
    (LeftSemimodule._+ᴹ_ (vector {𝓡 = semiring} (ℕ.suc m)) y z) · x
  ≈⟨ +-cong (distribʳ (x zero) (y zero) (z zero)) (·-distribʳ-+ (x ∘ suc)) ⟩
      y zero * x zero + z zero * x zero
    + ((y ∘ suc · x ∘ suc) + ((z ∘ suc) · (x ∘ suc)))
  ≈⟨ interchange _ _ _ _ ⟩
    (y · x) + (z · x)
  ∎
  where open ≈-Reasoning setoid

*-scale : ∀ {m} (x : Vector R m) {r : R} {y : Vector R m}
    → r * (x · y) ≈ x · LeftSemimodule._*ₗ_ (vector {𝓡 = semiring} m) r y
*-scale {m = ℕ.zero} _ {r} = zeroʳ r
*-scale {m = ℕ.suc m} x {r} {y} =
  begin
    r * (x · y)
  ≈⟨ distribˡ r
        (id (zipWith _*_ x y zero))
        (x ∘ suc · y ∘ suc)
    ⟩
      r * id (zipWith _*_ x y zero)
    + r * (x ∘ suc · y ∘ suc)
  ≈⟨ +-cong
        (sym (x∙yz≈y∙xz  (x zero) r (y zero)))
        (*-scale (x ∘ suc) {r} {y = y ∘ suc})
    ⟩
   x · (vector {𝓡 = semiring} (ℕ.suc m) LeftSemimodule.*ₗ r) y
  ∎
  where open ≈-Reasoning setoid
```
