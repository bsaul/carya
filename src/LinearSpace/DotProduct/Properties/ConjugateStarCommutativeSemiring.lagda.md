---
title: Properties of Vector Dot Product
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}


open import Algebra.Star.Bundles using (ConjugateStarCommutativeSemiring)

module LinearSpace.DotProduct.Properties.ConjugateStarCommutativeSemiring
  {c ℓ} (𝓡 : ConjugateStarCommutativeSemiring c ℓ) where

private
  open module 𝓡 = ConjugateStarCommutativeSemiring 𝓡
                    renaming (Carrier to R)
                    hiding (zero)

open import Data.Fin using (Fin); open Fin
open import Data.Nat as ℕ using (ℕ)
open import Data.Vec.Functional using (Vector; zipWith)
open import Function using (id ; _∘_)

import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

-- carya
open import LinearSpace.DotProduct.Core 𝓡.commutativeSemiring
open import Fold.Properties.CommutativeSemiring ⦃ 𝓡.commutativeSemiring ⦄

private
  variable
    m : ℕ
```

</details>

## Properties

```agda
open import LinearSpace.DotProduct.Properties.CommutativeSemiring 𝓡.commutativeSemiring
  public
```

```agda
·-preserves-⋆ : ∀ {m} → ∀ {x y : Vector R m} → (x · y) ≈ (y · x) ⋆
·-preserves-⋆ {m = ℕ.zero} = sym 0⋆≈0
·-preserves-⋆ {m = ℕ.suc m} {x} {y} = begin
    x · y ≈⟨ ∑-cong (λ i → *-conjugateSymmetric {x i} {y i}) ⟩
    ∑ (zipWith (λ x₁ x₂ → (x₂ * x₁) ⋆) x y) ≈⟨ +-congˡ (∑-cong
                    λ i → trans
                      (sym *-conjugateSymmetric)
                      (*-comm (x (suc i)) (y (suc i)))) ⟩
      id ((y ⟨*⟩ x) zero) ⋆ +  ∑ ((y ⟨*⟩ x) ∘ suc)
    ≈⟨ +-congˡ (∑-cong λ i → *-comm (y (suc i)) (x (suc i))) ⟩
      id ((y ⟨*⟩ x) zero) ⋆ + (∑ ((x ⟨*⟩ y) ∘ suc))
    ≈⟨ +-congˡ (·-preserves-⋆ {x = x ∘ suc} {y ∘ suc}) ⟩
      id ((y ⟨*⟩ x) zero) ⋆ + (∑ ((y ⟨*⟩ x) ∘ suc) ) ⋆
    ≈⟨ ⋆-homo-+ _ _ ⟨
      (y · x) ⋆
    ∎
    where open ≈-Reasoning setoid
```
