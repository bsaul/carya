---
title: Vector Dot Product
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Algebra.Bundles using (CommutativeSemiring)

module LinearSpace.DotProduct.Core {c ℓ} (𝓡 : CommutativeSemiring c ℓ) where

private
  open module 𝓡 = CommutativeSemiring 𝓡
                    renaming (Carrier to R)
                    hiding (zero)

-- stdlid
open import Data.Nat using (ℕ)
open import Data.Vec.Functional using (Vector ; zipWith)

-- carya
open import Fold.Properties.CommutativeSemiring ⦃ 𝓡 ⦄

private
  variable
    m : ℕ
```

</details>

## Vector Dot Product

```agda
infixr 6 _⟨*⟩_
_⟨*⟩_ : Vector R m → Vector R m → Vector R m
_⟨*⟩_ = zipWith _*_

infixr 6 _·_
_·_ : Vector R m → Vector R m → R
u · v = ∑ (u ⟨*⟩ v)
```
