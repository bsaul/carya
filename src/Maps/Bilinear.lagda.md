---
title: Biinear Maps with Bundles as Objects
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (CommutativeSemiring)

module Maps.Bilinear {r ℓʳ : Level} (𝓡 : CommutativeSemiring r ℓʳ) where
```

</details>

```agda
open import Maps.Bilinear.Core 𝓡 public
open import Maps.Bilinear.Base 𝓡 public
```
