---
title: Functions Maps with Module Bundles as Objects
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Ordered.Partial.Bundles using (PoSemiring)

module Maps.PositiveLinear {r ℓr₁ ℓr₂ : Level} (𝓡 : PoSemiring r ℓr₁ ℓr₂) where
```

</details>

```agda
open import Maps.PositiveLinear.Core 𝓡 public
open import Maps.PositiveLinear.Base 𝓡 public
```
