---
title: Operations on Prelinear maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_; 0ℓ)
open import Algebra.Bundles using (Semiring)

module Maps.Prelinear.Base {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.Zero
  renaming (leftSemimodule to 𝟘) using ()
open import Algebra.Module.Construct.DirectProduct
  renaming (leftSemimodule to _⊕ₗ_) using ()
open import Data.Product as × using (_,_)
open import Data.Product.Function.NonDependent.Setoid
open import Data.Unit.Polymorphic using (tt)
open import Function as F using ()
open import Function.Construct.Composition

open import Maps.Prelinear.Core 𝓡

open Semiring 𝓡 renaming (Carrier to R) using ()

private
  variable
    a b c d ℓa ℓb ℓc ℓd : Level
```

</details>

## Operations with 1 module

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} where

  private module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ

  id :  Prelinear 𝓜ᴬ 𝓜ᴬ
  id = mkPre F.id F.id

  diag : R → Prelinear 𝓜ᴬ 𝓜ᴬ
  diag r = mkPre (r 𝓜ᴬ.*ₗ_) 𝓜ᴬ.*ₗ-congˡ
```

## Operations with 2 modules

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ

  const : 𝓜ᴮ.Carrierᴹ → Prelinear 𝓜ᴬ 𝓜ᴮ
  const b = mkPre (F.const b) (F.const 𝓜ᴮ.≈ᴹ-refl)

  zero : Prelinear 𝓜ᴬ 𝓜ᴮ
  zero = const 𝓜ᴮ.0ᴹ

  _+_ : Prelinear 𝓜ᴬ 𝓜ᴮ → Prelinear 𝓜ᴬ 𝓜ᴮ → Prelinear 𝓜ᴬ 𝓜ᴮ
  f + g =
    mkPre
      (λ a → f ⟨$⟩ a 𝓜ᴮ.+ᴹ g ⟨$⟩ a)
      λ x≈y → 𝓜ᴮ.+ᴹ-cong (f.cong x≈y) (g.cong x≈y)
    where module f = Prelinear f ; module g = Prelinear g
```

### Maps to/from `Zero`

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ

  ! : Prelinear 𝓜ᴬ (𝟘 {0ℓ} {0ℓ})
  ! = const tt

  ¡ : Prelinear (𝟘 {0ℓ} {0ℓ}) 𝓜ᴬ
  ¡ = zero
```

## Operations with 3 bundles

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  where

  infixr 9 _∘_
  _∘_ : Prelinear 𝓜ᴮ 𝓜ᶜ → Prelinear 𝓜ᴬ 𝓜ᴮ → Prelinear 𝓜ᴬ 𝓜ᶜ
  (mk f) ∘ (mk g) = mk (function g f)

  -- Synonym for composition that reverses the order
  infixr -1 _·_
  _·_ = F.flip _∘_
```

## Operations with 4 bundles

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  {𝓜ᴰ : LeftSemimodule 𝓡 d ℓd}
  where

  infixr 6 _⊕_
  _⊕_ : Prelinear 𝓜ᴬ 𝓜ᶜ → Prelinear 𝓜ᴮ 𝓜ᴰ → Prelinear (𝓜ᴬ ⊕ₗ 𝓜ᴮ) (𝓜ᶜ ⊕ₗ 𝓜ᴰ)
  (mk f) ⊕ (mk g) = mk (f ×-function g)
```

## Etc

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  _*ₗ_ : R → Prelinear 𝓜ᴬ 𝓜ᴮ → Prelinear 𝓜ᴬ 𝓜ᴮ
  r *ₗ T = diag r ∘ T
```

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} where

  private module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ

  reduce : Prelinear (𝓜ᴬ ⊕ₗ 𝓜ᴬ) 𝓜ᴬ
  reduce = mkPre (×.uncurry 𝓜ᴬ._+ᴹ_) (×.uncurry 𝓜ᴬ.+ᴹ-cong)

  copy : Prelinear 𝓜ᴬ (𝓜ᴬ ⊕ₗ 𝓜ᴬ)
  copy = mkPre ×.< F.id , F.id > ×.< F.id , F.id >
```

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  where

  infixr 6 _▵_
  _▵_ : Prelinear 𝓜ᴬ 𝓜ᴮ → Prelinear 𝓜ᴬ 𝓜ᶜ → Prelinear 𝓜ᴬ (𝓜ᴮ ⊕ₗ 𝓜ᶜ)
  f ▵ g = (f ⊕ g) ∘ copy

  infixr 6 _▿_
  _▿_ : Prelinear 𝓜ᴬ 𝓜ᶜ → Prelinear 𝓜ᴮ 𝓜ᶜ → Prelinear (𝓜ᴬ ⊕ₗ 𝓜ᴮ) 𝓜ᶜ
  f ▿ g = reduce ∘ (f ⊕ g)
```

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  proj₁ : Prelinear (𝓜ᴬ ⊕ₗ 𝓜ᴮ) 𝓜ᴬ
  proj₁ = id ▿ zero

  proj₂ : Prelinear (𝓜ᴬ ⊕ₗ 𝓜ᴮ) 𝓜ᴮ
  proj₂ = zero ▿ id

  inj₁ : Prelinear 𝓜ᴬ (𝓜ᴬ ⊕ₗ 𝓜ᴮ)
  inj₁ = id ▵ zero

  inj₂ : Prelinear 𝓜ᴮ (𝓜ᴬ ⊕ₗ 𝓜ᴮ)
  inj₂ = zero ▵ id

  swap : Prelinear (𝓜ᴬ ⊕ₗ 𝓜ᴮ) (𝓜ᴮ ⊕ₗ 𝓜ᴬ)
  swap = mk swapₛ
```

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  where

  assocʳ : Prelinear ((𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊕ₗ 𝓜ᶜ) (𝓜ᴬ ⊕ₗ (𝓜ᴮ ⊕ₗ 𝓜ᶜ))
  assocʳ = mkPre ×.assocʳ′ ×.assocʳ′

  assocˡ : Prelinear (𝓜ᴬ ⊕ₗ (𝓜ᴮ ⊕ₗ 𝓜ᶜ)) ((𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊕ₗ 𝓜ᶜ)
  assocˡ = mkPre ×.assocˡ′ ×.assocˡ′
```
