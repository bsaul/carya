---
title: Properties of Prelinear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (Semiring)

module Maps.Prelinear.Properties {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

import Algebra.Definitions as Defs
import Algebra.Properties.CommutativeSemigroup as CS
open import Algebra.Bundles using (CommutativeMonoid)
open import Algebra.Structures using (IsCommutativeSemiring)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.DirectProduct
  renaming (leftSemimodule to _⊕ₗ_) using ()
open import Data.Product as × using (_,_)
open import Function as F using ()
open import Function.Construct.Composition as FC using ()

open import Maps.Prelinear.Core 𝓡
open import Maps.Prelinear.Base 𝓡

open Semiring 𝓡 renaming (Carrier to R) using ()

private
  variable
    a b c ℓa ℓb ℓc : Level
```

</details>

## Properties of `_+_`

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ

  open Defs (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ})

  +-cong : Congruent₂ _+_
  +-cong T U = 𝓜ᴮ.+ᴹ-cong T U

  +-assoc : Associative  _+_
  +-assoc T U V {a} = 𝓜ᴮ.+ᴹ-assoc (T ⟨$⟩ a) (U ⟨$⟩ a) (V ⟨$⟩ a)

  +-comm : Commutative _+_
  +-comm T U {a} = 𝓜ᴮ.+ᴹ-comm (T ⟨$⟩ a) (U ⟨$⟩  a)

  +-identityˡ : LeftIdentity zero _+_
  +-identityˡ T {a} = 𝓜ᴮ.+ᴹ-identityˡ  (T ⟨$⟩ a)

  +-identityʳ : RightIdentity zero _+_
  +-identityʳ T {a} = 𝓜ᴮ.+ᴹ-identityʳ (T ⟨$⟩ a)

  +-identity : Identity zero _+_
  +-identity = +-identityˡ , +-identityʳ
```

## Properties of `_∘_`

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ
    module 𝓜ᶜ = LeftSemimodule 𝓜ᶜ

  congruent : ∀ {f : Prelinear 𝓜ᴬ 𝓜ᴮ} {g : Prelinear  𝓜ᴮ 𝓜ᶜ}
    → F.Congruent 𝓜ᴬ._≈ᴹ_ 𝓜ᴮ._≈ᴹ_ (f ⟨$⟩_)
    → F.Congruent 𝓜ᴮ._≈ᴹ_ 𝓜ᶜ._≈ᴹ_ (g ⟨$⟩_)
    → F.Congruent 𝓜ᴬ._≈ᴹ_ 𝓜ᶜ._≈ᴹ_ ((g ∘ f) ⟨$⟩_)
  congruent  = FC.congruent 𝓜ᴬ._≈ᴹ_ 𝓜ᴮ._≈ᴹ_ 𝓜ᶜ._≈ᴹ_
```

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ

  open Defs  (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ})

  ∘-cong : Congruent₂  _∘_
  ∘-cong {_} {g} f≈g h≈i = 𝓜ᴬ.≈ᴹ-trans f≈g (g.cong h≈i)
    where module g = Prelinear g

  ∘-assoc : Associative _∘_
  ∘-assoc _ _ _ = 𝓜ᴬ.≈ᴹ-refl

  ∘-identityˡ : LeftIdentity id  _∘_
  ∘-identityˡ _ = 𝓜ᴬ.≈ᴹ-refl

  ∘-identityʳ : RightIdentity id  _∘_
  ∘-identityʳ _ = 𝓜ᴬ.≈ᴹ-refl

  ∘-identity : Identity id  _∘_
  ∘-identity = ∘-identityˡ , ∘-identityʳ

  ∘-zeroˡ : LeftZero zero  _∘_
  ∘-zeroˡ _ = 𝓜ᴬ.≈ᴹ-refl {x = 𝓜ᴬ.0ᴹ}
```

## Disributivity of Composition over Addition

With `Prelinear` maps,
composition alone distributes from the right;
distribution from the left requires
addition to be preserved (i.e. linearity).

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} where

  open Defs  (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ})

  distribʳ : _∘_ DistributesOverʳ _+_
  distribʳ _ _ _ = ≈ᴹ-refl
      where open LeftSemimodule 𝓜ᴬ
```

## Properties of `proj`/`inj`, fork and split

Many of these properties are described in @macedo2013
for linear maps,
but those proven here are true for prelinear maps too.

```agda
module _ {𝓜ᴬ 𝓜ᴮ : LeftSemimodule 𝓡 a ℓa} where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ

    π₁ : Prelinear (𝓜ᴬ ⊕ₗ 𝓜ᴮ) 𝓜ᴬ
    π₁ = proj₁

    π₂ : Prelinear (𝓜ᴬ ⊕ₗ 𝓜ᴮ) 𝓜ᴮ
    π₂ = proj₂

    ι₁ : Prelinear 𝓜ᴬ (𝓜ᴬ ⊕ₗ 𝓜ᴮ)
    ι₁ = inj₁

    ι₂ : Prelinear 𝓜ᴮ (𝓜ᴬ ⊕ₗ 𝓜ᴮ)
    ι₂ = inj₂

  -- IsProduct
  project₁ : ∀ {h : Prelinear 𝓜ᴬ 𝓜ᴬ} {i} → π₁ ∘ (h ▵ i) ≈ h
  project₁ = 𝓜ᴬ.+ᴹ-identityʳ _

  project₂ : ∀ {i} {h : Prelinear 𝓜ᴬ 𝓜ᴬ} → π₂ ∘ (h ▵ i) ≈ i
  project₂ = 𝓜ᴮ.+ᴹ-identityˡ _

  unique : ∀ {h : Prelinear 𝓜ᴬ (𝓜ᴬ ⊕ₗ 𝓜ᴮ)} {i} {j} →
       π₁ ∘ h ≈ i → π₂ ∘ h ≈ j → (i ▵ j) ≈ h
  unique π₁∘h≈i π₂∘h≈j =
       𝓜ᴬ.≈ᴹ-trans (𝓜ᴬ.≈ᴹ-sym π₁∘h≈i) (𝓜ᴬ.+ᴹ-identityʳ _)
     , 𝓜ᴮ.≈ᴹ-trans (𝓜ᴮ.≈ᴹ-sym π₂∘h≈j) (𝓜ᴮ.+ᴹ-identityˡ _)

  -- IsBiproduct (almost)
  -- of course,
  -- to be a BiProduct would require PrelinearMap to be IsCoproduct too.
  -- See:
  -- https://agda.github.io/agda-categories/Categories.Object.Biproduct.html#598

  -- @macedo2013 eq 11
  π₁∘ι₁≈id : π₁ ∘ ι₁ ≈ id
  π₁∘ι₁≈id = 𝓜ᴬ.+ᴹ-identityʳ _

  -- @macedo2013 eq 12
  π₂∘ι₂≈id : π₂ ∘ ι₂ ≈ id
  π₂∘ι₂≈id = 𝓜ᴮ.+ᴹ-identityˡ _

  -- @macedo2013  eq 13
  ι₁∘π₁+ι₂∘π₂≈id : (ι₁ ∘ π₁) + (ι₂ ∘ π₂) ≈ id
  ι₁∘π₁+ι₂∘π₂≈id =
        (𝓜ᴬ.≈ᴹ-trans (𝓜ᴬ.+ᴹ-identityʳ (_ 𝓜ᴬ.+ᴹ 𝓜ᴬ.0ᴹ)) π₁∘ι₁≈id)
      , 𝓜ᴮ.≈ᴹ-trans (𝓜ᴮ.+ᴹ-identityˡ (𝓜ᴮ.0ᴹ 𝓜ᴮ.+ᴹ _)) π₂∘ι₂≈id

  permute : ι₁ ∘ π₁ ∘ ι₂ ∘ π₂ ≈ ι₂ ∘ π₂ ∘ ι₁ ∘ π₁
  permute = π₁∘ι₁≈id , (𝓜ᴮ.≈ᴹ-sym π₂∘ι₂≈id)

  -- Orthogonality
  -- @macedo2013 eq 14
  π₁∘ι₂≈zero : π₁ ∘ ι₂ ≈ zero
  π₁∘ι₂≈zero = 𝓜ᴬ.+ᴹ-identityʳ 𝓜ᴬ.0ᴹ

  -- @macedo2013 eq 15
  π₂∘ι₁≈zero : π₂ ∘ ι₁ ≈ zero
  π₂∘ι₁≈zero = 𝓜ᴮ.+ᴹ-identityʳ 𝓜ᴮ.0ᴹ

  -- @macedo2013 eq 18
  ι₁▿ι₂≈id : ι₁ ▿ ι₂ ≈ id
  ι₁▿ι₂≈id = 𝓜ᴬ.+ᴹ-identityʳ _ , 𝓜ᴮ.+ᴹ-identityˡ _

  -- @macedo2013 eq 19
  π₁▵π₂≈id : π₁ ▵ π₂ ≈ id
  π₁▵π₂≈id  = 𝓜ᴬ.+ᴹ-identityʳ _ , 𝓜ᴮ.+ᴹ-identityˡ _

module _ {𝓜ᴬ 𝓜ᴮ 𝓜ᶜ : LeftSemimodule 𝓡 a ℓa} where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ
    module 𝓜ᶜ = LeftSemimodule 𝓜ᶜ

  -- @macedo2013 eq 16
  [T|U]≈T∘π₁+U∘π₂ : ∀ {f : Prelinear 𝓜ᴬ 𝓜ᶜ} {g : Prelinear 𝓜ᴮ 𝓜ᶜ}
      → f ▿ g ≈ (f ∘ proj₁) + (g ∘ proj₂)
  [T|U]≈T∘π₁+U∘π₂ {f = f} {g = g} =
     𝓜ᶜ.+ᴹ-cong
       (f.cong (𝓜ᴬ.≈ᴹ-sym (𝓜ᴬ.+ᴹ-identityʳ _)))
       (g.cong (𝓜ᴮ.≈ᴹ-sym (𝓜ᴮ.+ᴹ-identityˡ _)))
     where module f = Prelinear f ; module g = Prelinear g

  -- @macedo2013 eq 17
  [T-U]≈ι₁∘T+ι₂∘U : ∀ {T : Prelinear 𝓜ᶜ 𝓜ᴬ} {U : Prelinear 𝓜ᶜ 𝓜ᴮ}
      → T ▵ U  ≈ (inj₁ ∘ T) + (inj₂ ∘ U)
  [T-U]≈ι₁∘T+ι₂∘U =
          𝓜ᴬ.≈ᴹ-sym (𝓜ᴬ.+ᴹ-identityʳ _)
        , 𝓜ᴮ.≈ᴹ-sym (𝓜ᴮ.+ᴹ-identityˡ _)
```

## Fusion Laws

```agda
module _ {𝓜ᴬ 𝓜ᴮ 𝓜ᶜ 𝓜ᴰ : LeftSemimodule 𝓡 a ℓa} where

  open LeftSemimodule 𝓜ᴮ renaming (≈ᴹ-refl to reflᴮ) using ()
  open LeftSemimodule 𝓜ᶜ renaming (≈ᴹ-refl to reflᶜ) using ()

  -- @macedo2013 eq 26
  ▵-fusion :
    ∀ {T₁ : Prelinear 𝓜ᴬ 𝓜ᴮ} {T₂ : Prelinear 𝓜ᴬ 𝓜ᶜ} {T₃ : Prelinear 𝓜ᴰ 𝓜ᴬ}
    → (T₃ · (T₁ ▵ T₂)) ≈ (T₃ · T₁) ▵ (T₃ · T₂)
  ▵-fusion = reflᴮ , reflᶜ
```

## Cancellation Laws

```agda
module _ {𝓜ᴬ 𝓜ᴮ 𝓜ᶜ : LeftSemimodule 𝓡 a ℓa} where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ
    module 𝓜ᶜ = LeftSemimodule 𝓜ᶜ

  -- NOTE: @macedo2013 states these differently. (proj and inj are flipped)
  -- What's up with that?
  -- @macedo2013 eq. 28
  proj₁-cancels-▵ : ∀ {T₁ : Prelinear 𝓜ᴬ 𝓜ᴮ} {T₂ : Prelinear 𝓜ᴬ 𝓜ᶜ}
    → ((T₁ ▵ T₂) · proj₁) ≈ T₁
  proj₁-cancels-▵ {T₁ = T₁} =  𝓜ᴮ.+ᴹ-identityʳ (T₁ ⟨$⟩ _)

  -- @macedo2013 eq. 28
  proj₂-cancels-▵ : ∀ {T₁ : Prelinear 𝓜ᴬ 𝓜ᴮ} {T₂ : Prelinear 𝓜ᴬ 𝓜ᶜ}
    → ((T₁ ▵ T₂) · proj₂) ≈ T₂
  proj₂-cancels-▵ {T₂ = T₂} = 𝓜ᶜ.+ᴹ-identityˡ (T₂ ⟨$⟩ _)
```

## "Abide" (junc/split exchange) laws

```agda
module _ {𝓜ᴬ 𝓜ᴮ 𝓜ᶜ 𝓜ᴰ : LeftSemimodule 𝓡 a ℓa} where

  private
    module 𝓜ᶜ = LeftSemimodule 𝓜ᶜ
    module 𝓜ᴰ = LeftSemimodule 𝓜ᴰ

  -- @macedo2013 eq 30
  abide₁ : ∀ {T₁ : Prelinear 𝓜ᴬ 𝓜ᶜ} {T₂ : Prelinear 𝓜ᴬ 𝓜ᴰ}
       {T₃ : Prelinear 𝓜ᴮ 𝓜ᶜ} {T₄ : Prelinear 𝓜ᴮ 𝓜ᴰ}
    → ((T₁ ▵ T₂) ▿ (T₃ ▵ T₄)) ≈ (T₁ ▿ T₃) ▵ (T₂ ▿ T₄)
  abide₁ = 𝓜ᶜ.≈ᴹ-refl , 𝓜ᴰ.≈ᴹ-refl

--   -- TODO: what's the other eqn?
--   -- abide₂ : ∀ {T₁ : m ⊸ o} {T₂ : m ⊸ p} {T₃ : n ⊸ o} {T₄ : n ⊸ p}
--   --   → (T₁ — T₃) ∣ (T₂ — T₄) ≈ ?
--   -- abide₂ = {!   !}
```

```agda
module _ {𝓜ᴬ 𝓜ᴮ 𝓜ᶜ : LeftSemimodule 𝓡 a ℓa} where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ
    module 𝓜ᶜ = LeftSemimodule 𝓜ᶜ

  -- @macedo2013 eq 31
  split-add : ∀ {T₁ T₃ : Prelinear 𝓜ᴬ 𝓜ᴮ} {T₂ T₄ : Prelinear 𝓜ᴬ 𝓜ᶜ }
    → ((T₁ ▵ T₂) + (T₃ ▵ T₄)) ≈ (T₁ + T₃) ▵ (T₂ + T₄)
  split-add = 𝓜ᴮ.≈ᴹ-refl , 𝓜ᶜ.≈ᴹ-refl

  -- @macedo2013 eq 32
  junc-add : ∀ {T₁ T₃ : Prelinear 𝓜ᴮ 𝓜ᴬ} {T₂ T₄ : Prelinear 𝓜ᶜ 𝓜ᴬ}
    → ((T₁ ▿ T₂) + (T₃ ▿ T₄)) ≈ (T₁ + T₃) ▿ (T₂ + T₄)
  junc-add = interchange _ _ _ _
     where open CS ((CommutativeMonoid.commutativeSemigroup 𝓜ᴬ.+ᴹ-commutativeMonoid))
```

## Divide and Conquer

```agda
module _ {𝓜ᴬ 𝓜ᴮ 𝓜ᶜ 𝓜ᴰ : LeftSemimodule 𝓡 a ℓa} where

  private
    module 𝓜ᴰ = LeftSemimodule 𝓜ᴰ

  -- @macedo2013 eq 30
  div&con :
    ∀ {T₁ : Prelinear 𝓜ᴬ 𝓜ᴮ} {T₂ : Prelinear 𝓜ᴬ 𝓜ᶜ}
      {T₃ : Prelinear 𝓜ᴮ 𝓜ᴰ} {T₄ : Prelinear 𝓜ᶜ 𝓜ᴰ}
    → ((T₁ ▵ T₂) · (T₃ ▿ T₄)) ≈ (T₁ · T₃) + (T₂ · T₄)
  div&con = 𝓜ᴰ.≈ᴹ-refl
```
