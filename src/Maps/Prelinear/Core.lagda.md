---
title: Functions Maps with Module Bundles as Objects
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (Semiring)

module Maps.Prelinear.Core {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

open import Algebra.Module.Bundles using (LeftSemimodule)
open import Function as F using (Func; _on_; Congruent)
open import Function.Setoid as FuncEquiv using ()
open import Relation.Binary
 using (Rel; REL; Setoid; IsEquivalence)
open import Relation.Binary.Construct.On as On using ()

private
  variable
    a b ℓa ℓb : Level
```

</details>

## Prelinear Map

A "prelinear" map is simply a function (`Func`) between the `Carrierᴹ`
of two modules
(in this case a `LeftSemimodule`).
In other words, a prelinear map is simply a `Func`
where the `To`/`From` setoids are constrained to the setoids of module spaces.

```agda
record Prelinear
  (𝓜ᴬ : LeftSemimodule 𝓡 a ℓa)
  (𝓜ᴮ : LeftSemimodule 𝓡 b ℓb)
  : Set (a ⊔ b ⊔ ℓa ⊔ ℓb) where

  constructor mk

  module Domain = LeftSemimodule 𝓜ᴬ
  module Codomain = LeftSemimodule 𝓜ᴮ

  field
    map : Func Domain.≈ᴹ-setoid Codomain.≈ᴹ-setoid

  open Func map public
```

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ

  mkPre : (f : 𝓜ᴬ.Carrierᴹ → 𝓜ᴮ.Carrierᴹ)
          → (cong : Congruent 𝓜ᴬ._≈ᴹ_ 𝓜ᴮ._≈ᴹ_ f)
          → Prelinear 𝓜ᴬ 𝓜ᴮ
  mkPre to cong = mk record
    { to        = to
    ; cong      = cong
    }
```

## Denotation and Equivalence

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ
    𝒮 = FuncEquiv.→-setoid {A = 𝓜ᴬ.≈ᴹ-setoid} {B = 𝓜ᴮ.≈ᴹ-setoid}

  ⟦_⟧ : Prelinear 𝓜ᴬ 𝓜ᴮ → Func 𝓜ᴬ.≈ᴹ-setoid 𝓜ᴮ.≈ᴹ-setoid
  ⟦_⟧ = Prelinear.map

  infix 4 _≈_
  _≈_ : Rel (Prelinear 𝓜ᴬ 𝓜ᴮ) (a ⊔ ℓb)
  _≈_ = Setoid._≈_ 𝒮 on ⟦_⟧

  isEquivalence : IsEquivalence _≈_
  isEquivalence = On.isEquivalence ⟦_⟧ (Setoid.isEquivalence 𝒮)

  setoid : Setoid (a ⊔ ℓa ⊔ b ⊔ ℓb) (a ⊔ ℓb)
  setoid = record {isEquivalence = isEquivalence}
```

## Application

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ

  _⟨$⟩_ : Prelinear 𝓜ᴬ 𝓜ᴮ → 𝓜ᴬ.Carrierᴹ → 𝓜ᴮ.Carrierᴹ
  f ⟨$⟩ a = ⟦ f ⟧ F.⟨$⟩ a
```
