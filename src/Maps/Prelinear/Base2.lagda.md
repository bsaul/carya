---
title: Additonal operations on Prelinear maps
---

NOTE: the operations defined herein are not in `Base` in order to avoid
cycle dependencies among the modules.
TODO: consider a module structure that avoids this situation.

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (Semiring)

module Maps.Prelinear.Base2 {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

open Semiring 𝓡 renaming (Carrier to R) using ()

open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.DirectProduct
  renaming (leftSemimodule to _⊕ₗ_) using ()
open import Data.Product as × using (_,_)
open import Function using (_∘_)

open import Maps.Prelinear.Core 𝓡
open import Maps.Prelinear.Module renaming (mkLeftSemimodule to _⟶_)

private
  variable
    a b c d ℓa ℓb ℓc ℓd : Level
```

</details>

## uncurry/curry

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ
    module 𝓜ᶜ = LeftSemimodule 𝓜ᶜ

  uncurry : Prelinear (𝓜ᴬ ⊕ₗ 𝓜ᴮ) 𝓜ᶜ → Prelinear 𝓜ᴬ (𝓜ᴮ ⟶ 𝓜ᶜ)
  uncurry f =
    mkPre
      (λ a → mkPre (to ∘ (a ,_)) (cong ∘ (𝓜ᴬ.≈ᴹ-refl ,_)))
      λ x → cong (x , 𝓜ᴮ.≈ᴹ-refl)
    where open Prelinear f

  curry : Prelinear 𝓜ᴬ (𝓜ᴮ ⟶ 𝓜ᶜ) → Prelinear (𝓜ᴬ ⊕ₗ 𝓜ᴮ) 𝓜ᶜ
  curry f =
    mkPre
      (λ (a , b) → to a ⟨$⟩ b)
      λ {(a , _)} (x , y) → 𝓜ᶜ.≈ᴹ-trans (Prelinear.cong (to a) y) (cong x)
    where open Prelinear f
```
