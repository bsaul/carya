---
title: Algebraic Module Constructs on Prelinear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (Semiring ; CommutativeMonoid)
open import Algebra.Module.Bundles using (LeftSemimodule)

module Maps.Prelinear.Module
    {r ℓʳ : Level}
    {𝓡 : Semiring r ℓʳ}
    where

open import Maps.Prelinear.Core 𝓡
open import Maps.Prelinear.Base 𝓡 using (_*ₗ_)
open import Maps.Prelinear.Algebra 𝓡

private
  variable
    a b ℓa ℓb : Level
```

</details>

## `LeftSemimodule` of Prelinear maps

```agda
mkLeftSemimodule : LeftSemimodule 𝓡 a ℓa → LeftSemimodule 𝓡 b ℓb
  → LeftSemimodule 𝓡 (a ⊔ ℓa ⊔ b ⊔ ℓb) (a ⊔ ℓb)
mkLeftSemimodule 𝓜ᴬ 𝓜ᴮ = record
  { Carrierᴹ = Prelinear 𝓜ᴬ 𝓜ᴮ
  ;  _*ₗ_ = _*ₗ_
  ; isLeftSemimodule = record
    { +ᴹ-isCommutativeMonoid = isCommutativeMonoid
    ; isPreleftSemimodule = record
        { *ₗ-cong = λ x y → *ₗ-cong x y
        ; *ₗ-zeroˡ = λ f → *ₗ-zeroˡ (f ⟨$⟩ _)
        ; *ₗ-distribʳ = λ f r₁ r₂ → *ₗ-distribʳ (f ⟨$⟩ _) r₁ r₂
        ; *ₗ-identityˡ = λ f → *ₗ-identityˡ (f ⟨$⟩ _)
        ; *ₗ-assoc = λ r₁ r₂ f → *ₗ-assoc r₁ r₂ (f ⟨$⟩ _)
        ; *ₗ-zeroʳ = λ r → *ₗ-zeroʳ r
        ; *ₗ-distribˡ = λ r f g → *ₗ-distribˡ r (f ⟨$⟩ _) (g ⟨$⟩ _)
        }
    }
  }
  where open CommutativeMonoid (+-0-commutativeMonoid)
        open LeftSemimodule 𝓜ᴮ hiding (_*ₗ_)
```

