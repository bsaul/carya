---
title: Pointwise Prelinear maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_; 0ℓ)
open import Algebra.Bundles using (Semiring)

module Maps.Prelinear.Construct.Pointwise
   {r ℓʳ : Level}
   (𝓡 : Semiring r ℓʳ) where

open Semiring 𝓡
  renaming (Carrier to R)
  using ()

-- lib
open import Algebra.Module.Construct.Vector
  renaming (mkLeftSemimodule to [_])
open import Data.Sum as ⊎ using (_⊎_)
open import Data.Unit.Polymorphic using (⊤)

-- carya
open import Maps.Prelinear.Core 𝓡
  using (
      Prelinear
    ; ⟦_⟧
    ; _≈_
    ; isEquivalence
    ; setoid
    ; mk
  ) public
open import Maps.Prelinear.Base 𝓡 as P using ()
open import Maps.Prelinear.Related 𝓡

private
  variable
    a b c d : Level
    A : Set a
    B : Set b
    C : Set c
    D : Set d
```

</details>

An pointwise prelinear map is a function between generalized vectors;
i.e., a function of the type:

```code
_ : (A → R) → (B → R)
```

where `R` is the carrier of the vector space's associated semiring
and `A` and `B` are  arbitrary types.

```agda
IndexPrelinear : Set a → Set b → Set (r ⊔ ℓʳ ⊔ a ⊔ b)
IndexPrelinear A B = Prelinear [ A ] [ B ]
```

```agda
diag : R → IndexPrelinear A A
diag = P.diag

const : (B → R) → IndexPrelinear A B
const = P.const

_*ₗ_ : R → IndexPrelinear A B → IndexPrelinear A B
_*ₗ_ = P._*ₗ_

zero : IndexPrelinear A B
zero = P.zero

infixl 6 _+_
_+_ : IndexPrelinear A B → IndexPrelinear A B → IndexPrelinear A B
_+_ = P._+_

id : IndexPrelinear A A
id = P.id

infixr 9 _∘_
_∘_ : IndexPrelinear B C → IndexPrelinear A B → IndexPrelinear A C
_∘_ = P._∘_

! : IndexPrelinear A ⊤
! = 𝟘⇒⊤ P.∘ P.!

¡ : IndexPrelinear ⊤ A
¡ = P.¡ P.∘ ⊤⇒𝟘

infixr 6 _⊕_
_⊕_ : IndexPrelinear A C → IndexPrelinear B D → IndexPrelinear (A ⊎ B) (C ⊎ D)
f ⊕ g = ⊕⇒⊎ P.∘ (f P.⊕ g) P.∘ ⊎⇒⊕

reduce : IndexPrelinear (A ⊎ A) A
reduce = P.reduce P.∘ ⊎⇒⊕

copy : IndexPrelinear A (A ⊎ A)
copy = ⊕⇒⊎ P.∘ P.copy

swap : IndexPrelinear (A ⊎ B) (B ⊎ A)
swap = ⊕⇒⊎ P.∘ P.swap P.∘ ⊎⇒⊕

infixr 6 _▵_
_▵_ : IndexPrelinear A B → IndexPrelinear A C → IndexPrelinear A (B ⊎ C)
f ▵ g = (f ⊕ g) ∘ copy

proj₁ : IndexPrelinear A (A ⊎ B)
proj₁ = id ▵ zero

proj₂ :  IndexPrelinear B (A ⊎ B)
proj₂ = zero ▵ id

infixr 6 _▿_
_▿_ : IndexPrelinear A C → IndexPrelinear B C → IndexPrelinear (A ⊎ B) C
f ▿ g = reduce ∘ (f ⊕ g)

inj₁ : IndexPrelinear (A ⊎ B) A
inj₁ = id ▿ zero

inj₂ : IndexPrelinear (A ⊎ B) B
inj₂ = zero ▿ id

assocʳ : IndexPrelinear ((A ⊎ B) ⊎ C) (A ⊎ (B ⊎ C))
assocʳ = ⊕assoc⊎ʳ P.∘ P.assocʳ P.∘ ⊎assoc⊕ˡ

assocˡ : IndexPrelinear (A ⊎ (B ⊎ C)) ((A ⊎ B) ⊎ C)
assocˡ = ⊕assoc⊎ˡ P.∘ P.assocˡ P.∘ ⊎assoc⊕ʳ
```
