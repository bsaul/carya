---
title: Relations between Prelinear Spaces
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_; 0ℓ; lower)
open import Algebra.Bundles using (Semiring)

module Maps.Prelinear.Related {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

open Semiring 𝓡 using (0# ; refl)

open import Algebra.Module.Construct.Vector
  renaming (mkLeftSemimodule to [_])
open import Algebra.Module.Construct.DirectProduct
  renaming (leftSemimodule to _⊕ₗ_) using ()
open import Algebra.Module.Construct.Zero
  renaming (leftSemimodule to 𝟘) using ()
open import Data.Fin using (Fin); open Fin
open import Data.Fin.Properties using (+↔⊎)
open import Data.Fin.Properties.Extra
open import Data.Nat using (ℕ; _+_)
open import Data.Sum as ⊎ using (_⊎_)
open import Data.Product as × using (_,_)
open import Data.Unit.Polymorphic using (⊤ ; tt)
open import Function as F using (Inverse)

-- carya
open import Maps.Prelinear.Core 𝓡

private
  variable
    a b c ℓ : Level
    A : Set a
    B : Set b
    C : Set c
    m n o : ℕ
```

</details>

## Maps relating `_⊎_` and `_⊕ₗ_`

```agda
⊎⇒⊕ : Prelinear [ A ⊎ B ] ([ A ] ⊕ₗ [ B ])
⊎⇒⊕ = mk record
  { to = λ f → (λ a → f (⊎.inj₁ a)) , λ b → f (⊎.inj₂ b)
  ; cong = λ fx≈gx  → (λ {a} → fx≈gx {⊎.inj₁ a}) , λ {b} → fx≈gx {⊎.inj₂ b}
  }

⊕⇒⊎ : Prelinear ([ A ] ⊕ₗ [ B ]) [ A ⊎ B ]
⊕⇒⊎ = mk record
  { to = λ (f , g) → ⊎.[ f , g ]
  ; cong = λ {( f , _ ) {⊎.inj₁ a} → f {a}; ( _ , g) {⊎.inj₂ b} → g {b}}
  }

⊎assoc⊕ˡ : Prelinear [ (A ⊎ B) ⊎ C ] (([ A ] ⊕ₗ [ B ]) ⊕ₗ [ C ])
⊎assoc⊕ˡ = mk record
  { to = λ f → (f F.∘ ⊎.inj₁ F.∘ ⊎.inj₁ , f F.∘ ⊎.inj₁ F.∘ ⊎.inj₂), f F.∘ ⊎.inj₂
  ; cong = λ f →
    ((λ {x} → f { ⊎.inj₁ (⊎.inj₁ x)})
    , λ {x} → f { ⊎.inj₁ (⊎.inj₂ x) })
    , λ {x} → f { ⊎.inj₂ x }
  }

⊎assoc⊕ʳ : Prelinear [ A ⊎ B ⊎ C ] ([ A ] ⊕ₗ ([ B ] ⊕ₗ [ C ]))
⊎assoc⊕ʳ = mk record
  { to = λ f → f F.∘ ⊎.inj₁ , f F.∘ ⊎.inj₂ F.∘ ⊎.inj₁ , f F.∘ ⊎.inj₂ F.∘ ⊎.inj₂
  ; cong = λ f →
      (λ {x} → f {⊎.inj₁ x})
    , (λ {x} → f { ⊎.inj₂ (⊎.inj₁ x) })
    , λ {x} → f { ⊎.inj₂ (⊎.inj₂ x) }
  }

⊕assoc⊎ʳ : Prelinear ([ A ] ⊕ₗ ([ B ] ⊕ₗ [ C ])) [ A ⊎ B ⊎ C ]
⊕assoc⊎ʳ  = mk record
  { to = λ ( f , g  , h) → ⊎.[ f , ⊎.[ g , h ] ]
  ; cong =
      λ { (f , _ , _)  {⊎.inj₁ a} → f {a}
        ; ( _ , g , _) {⊎.inj₂ (⊎.inj₁ b)} → g {b}
        ; ( _ , _ , h) {⊎.inj₂ (⊎.inj₂ c)} → h {c}
        }
  }

⊕assoc⊎ˡ : Prelinear (([ A ] ⊕ₗ [ B ]) ⊕ₗ [ C ]) [ (A ⊎ B) ⊎ C ]
⊕assoc⊎ˡ  = mk record
  { to = λ ( (f , g)  , h) → ⊎.[ ⊎.[ f , g ] , h ] -- ⊎.[ f , ⊎.[ g , h ] ]
  ; cong =
      λ { ((f , _) , _)  {⊎.inj₁ (⊎.inj₁ a)} → f {a}
        ; (( _ , g) , _) {⊎.inj₁ (⊎.inj₂ b)} → g {b}
        ; ( _ , h) {⊎.inj₂ c} → h {c}
        }
  }

𝟘⇒⊤ : Prelinear {a = a} {ℓa = ℓ} 𝟘 [ ⊤ { a } ]
𝟘⇒⊤ = mk record
  { to = λ f x →  0#
  ; cong = λ _ → refl
  }

⊤⇒𝟘 : ∀ {b} → Prelinear {b = b} {ℓb = ℓ} [ ⊤ { b } ] 𝟘
⊤⇒𝟘 = mk record { to = λ _ → tt ; cong = λ _ → tt }
```

## Maps relating Vectors

```agda
1⇒⊤ : ∀ {ℓ} → Prelinear [ Fin 1 ] [ (⊤ { ℓ }) ]
1⇒⊤ = mk
  record
    { to = λ f → f F.∘ Inverse.from 1↔⊤ F.∘ (λ _ → tt) F.∘ lower
    ; cong = λ f {_} → f { Inverse.from 1↔⊤ tt }
    }

⊤⇒1 : ∀ {ℓ} → Prelinear [ ⊤ { ℓ } ] [ Fin 1 ]
⊤⇒1 = mk
    record
      { to = λ f → f F.∘ (λ _ → tt) F.∘ Inverse.to 1↔⊤
      ; cong = λ f {_} → f { tt }
      }

m⊎n⇒m+n : Prelinear [ (Fin m) ⊎ (Fin n) ] [ Fin (m + n) ]
m⊎n⇒m+n = mk
     record
      { to = λ f → f F.∘ Inverse.to +↔⊎
      ; cong = λ f {x} → f { Inverse.to +↔⊎ x } }

m+n⇒m⊎n : Prelinear [ Fin (m + n)] [ (Fin m) ⊎ (Fin n) ]
m+n⇒m⊎n = mk
   record
      { to = λ f → f F.∘ Inverse.from +↔⊎
      ; cong = λ f {x} → f { Inverse.from +↔⊎ x } }

⊎assoc+ˡ : Prelinear [ (Fin m ⊎ Fin n) ⊎ Fin o ] [ Fin ((m + n) + o) ]
⊎assoc+ˡ = mk
    record
      { to = λ f → f F.∘ ⊎.map₁ (Inverse.to +↔⊎) F.∘ (Inverse.to +↔⊎)
      ; cong = λ f {x} → f { ⊎.map₁ (Inverse.to +↔⊎) (Inverse.to +↔⊎ x) }
      }

⊎assoc+ʳ : Prelinear [ Fin m ⊎ (Fin n ⊎ Fin o) ] [ Fin (m + (n + o)) ]
⊎assoc+ʳ = mk
    record
      { to = λ f → f F.∘ ⊎.map₂ (Inverse.to +↔⊎) F.∘ (Inverse.to +↔⊎)
      ; cong = λ f {x} → f { ⊎.map₂ (Inverse.to +↔⊎) (Inverse.to +↔⊎ x) }
      }

+assoc⊎ˡ : Prelinear [ Fin ((m + n) + o) ] [ (Fin m ⊎ Fin n) ⊎ Fin o ]
+assoc⊎ˡ = mk
    record
      { to = λ f → f F.∘ Inverse.from +↔⊎ F.∘ ⊎.map₁ (Inverse.from +↔⊎)
      ; cong = λ f {x} → f { Inverse.from +↔⊎ (⊎.map₁ (Inverse.from +↔⊎) x) }
      }

+assoc⊎ʳ : Prelinear [ Fin (m + (n + o)) ] [ Fin m ⊎ (Fin n ⊎ Fin o) ]
+assoc⊎ʳ = mk
    record
      { to = λ f → f F.∘ Inverse.from +↔⊎ F.∘ ⊎.map₂ (Inverse.from +↔⊎)
      ; cong = λ f {x} → f { Inverse.from +↔⊎ (⊎.map₂ (Inverse.from +↔⊎) x) }
      }
```

```agda
open import Maps.Prelinear.Base 𝓡

invl : ∀ {A : Set a} {B : Set b} → (⊎⇒⊕ {A = A} {B = B}) ∘ ⊕⇒⊎ ≈ id
invl = refl , refl

invr : ∀ {A : Set a} {B : Set b} → ⊕⇒⊎ ∘ (⊎⇒⊕ {A = A} {B = B}) ≈ id
invr {x = ⊎.inj₁ _} = refl
invr {x = ⊎.inj₂ _} = refl

-- _ : 𝟘⇒⊤ ∘ ⊤⇒𝟘 ≈ id
-- _ = {!   !}

-- _ : ⊤⇒𝟘 ∘ 𝟘⇒⊤ ≈ id
-- _ = tt
```
