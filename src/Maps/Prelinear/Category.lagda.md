MĈ---
title: Category of Prelinear maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --without-K #-}

open import Level using (Level; _⊔_; suc)
open import Algebra.Bundles using (Semiring)

module Maps.Prelinear.Category {m ℓm} {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

-- stdlib
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.DirectProduct
  renaming (leftSemimodule to _⊕ₗ_) using ()
open import Algebra.Module.Construct.Zero {m} {ℓm}
  renaming (leftSemimodule to 𝟘) using ()
open import Data.Product using (_,_)
open import Data.Unit.Polymorphic using (tt)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open LeftSemimodule

--- agda-categories
open import Categories.Category
open import Categories.Category.BinaryProducts
open import Categories.Category.Cartesian
open import Categories.Object.Product
open import Categories.Object.Terminal
open import Categories.Functor.Bifunctor
open import Categories.Category.Monoidal

-- carya
open import Maps.Prelinear.Core 𝓡
open import Maps.Prelinear.Base 𝓡
open import Maps.Prelinear.Properties 𝓡
```

</details>

## Category

```agda
prelinear : Category (suc m ⊔ suc ℓm ⊔ r ⊔ ℓʳ) (m ⊔ ℓm) (m ⊔ ℓm)
prelinear = record
  { Obj = LeftSemimodule 𝓡 m ℓm
  ; _⇒_ = Prelinear
  ; _≈_ = _≈_
  ; id = id
  ; _∘_ = _∘_
  ; assoc = λ {_} {_} {_} {D} → ≈ᴹ-refl D
  ; sym-assoc =  λ {_} {_} {_} {D} → ≈ᴹ-refl D
  ; identityˡ = λ {_} {B} → ≈ᴹ-refl B
  ; identityʳ = λ {_} {B} → ≈ᴹ-refl B
  ; identity² = λ {A} → ≈ᴹ-refl A
  ; equiv = λ {_} {B} → record
    { refl = ≈ᴹ-refl B
    ; sym = λ x → ≈ᴹ-sym B x
    ; trans = λ x y → ≈ᴹ-trans B x y
    }
  ; ∘-resp-≈ = λ {_} {_} {C = C} {f} {h} {g} {i} f≈h g≈i {x} →
        ≈ᴹ-trans C f≈h (Prelinear.cong h g≈i)
  }
```

## Product

```agda
module _ {𝓜ᴬ 𝓜ᴮ : LeftSemimodule 𝓡 m ℓm} where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ

  product : Product prelinear 𝓜ᴬ 𝓜ᴮ
  product = record
   { A×B =  𝓜ᴬ ⊕ₗ 𝓜ᴮ
   ; π₁ = proj₁
   ; π₂ = proj₂
   ; ⟨_,_⟩ = _▵_
   ; project₁ = 𝓜ᴬ.+ᴹ-identityʳ _
   ; project₂ = 𝓜ᴮ.+ᴹ-identityˡ _
   ; unique = λ π₁∘h≈i π₂∘h≈j →
       𝓜ᴬ.≈ᴹ-trans (𝓜ᴬ.≈ᴹ-sym π₁∘h≈i) (𝓜ᴬ.+ᴹ-identityʳ _)
     , 𝓜ᴮ.≈ᴹ-trans (𝓜ᴮ.≈ᴹ-sym π₂∘h≈j) (𝓜ᴮ.+ᴹ-identityˡ _)
   }
```

## Binary Products

```agda
binProd : BinaryProducts prelinear
binProd = record { product = product }
```

## Terminal

```agda
terminal : Terminal prelinear
terminal = record
  { ⊤ = 𝟘
  ; ⊤-is-terminal = record
    { ! = !
    ; !-unique = λ _ → tt
    }
  }
```

## Cartesian

```agda
cartesian : Cartesian prelinear
cartesian = record
 { terminal = terminal
 ; products = binProd
 }
```

## Bifunctor

```agda
bifunctor : Bifunctor prelinear prelinear prelinear
bifunctor = record
   { F₀ = λ (A , B) → A ⊕ₗ B
   ; F₁ = λ (f , g) → f ⊕ g
   ; identity =
      λ {(A , B)} → ≈ᴹ-refl A , ≈ᴹ-refl B
   ; homomorphism =
      λ { {Z = (E , F)} → ≈ᴹ-refl E , ≈ᴹ-refl F}
   ; F-resp-≈ = λ (a , b) → a , b
   }
```

-- ## Monoidal

-- ```agda
-- -- Wait, I think this is just the induced `monoidal` from `Cartesian`
-- monoidal : Monoidal prelinear
-- monoidal = record
--  { ⊗ = bifunctor
--  ; unit = 𝟘
--  ; unitorˡ = λ {A} → record
--     { from = proj₂
--     ; to = inj₂
--     ; iso = record
--         { isoˡ = tt , +ᴹ-identityˡ A _
--         ; isoʳ = +ᴹ-identityˡ A _
--         }
--     }
--  ; unitorʳ = λ {A} →
--     record
--       { from = proj₁
--       ; to = inj₁
--       ; iso = record
--          { isoˡ = +ᴹ-identityʳ A _ , tt
--          ; isoʳ = +ᴹ-identityʳ A _
--          }
--       }
--  ; associator = λ {A} {B} {C} → record
--     { from = assocʳ
--     ; to = assocˡ
--     ; iso =
--        record
--          { isoˡ = (≈ᴹ-refl A , ≈ᴹ-refl B) , ≈ᴹ-refl C
--          ; isoʳ = ≈ᴹ-refl A , ≈ᴹ-refl B , ≈ᴹ-refl C
--          }
--     }
--  ; unitorˡ-commute-from =
--    λ {A} {B} {f} {(x , a)} →
--        ≈ᴹ-trans B
--          (+ᴹ-identityˡ B _)
--          (≈ᴹ-sym B (Prelinear.cong f (+ᴹ-identityˡ A a)))
--  ; unitorˡ-commute-to =
--     λ {_} {B} → tt , (≈ᴹ-refl B)
--  ; unitorʳ-commute-from =
--     λ {A} {B} {f} {(a , x)} →
--        ≈ᴹ-trans B
--          (+ᴹ-identityʳ B _)
--          (≈ᴹ-sym B (Prelinear.cong f (+ᴹ-identityʳ A a)))
--  ; unitorʳ-commute-to =
--     λ {A} {B} → ≈ᴹ-refl B , tt
--  ; assoc-commute-from =
--     λ { {A} {B} {C} {D} {E} {F} {G} {H} → ≈ᴹ-refl B , ≈ᴹ-refl E , ≈ᴹ-refl H }
--  ; assoc-commute-to =
--     λ { {A} {B} {C} {D} {E} {F} {G} {H} → (≈ᴹ-refl B , ≈ᴹ-refl E) , (≈ᴹ-refl H) }
--  ; triangle =
--     λ { {A} {B} { ((a , x) , b) } →
--        ≈ᴹ-sym A (+ᴹ-identityʳ A a) , (+ᴹ-identityˡ B b) }
--  ; pentagon = λ {A} {B} {C} {D} { (((a , b) , c) , d) } →
--      ≈ᴹ-refl A , ≈ᴹ-refl B , ≈ᴹ-refl C , ≈ᴹ-refl D
--  }

-- ```
