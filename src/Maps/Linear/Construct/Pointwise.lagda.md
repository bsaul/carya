---
title: Construct a linear map pointwise
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (CommutativeSemiring)

module Maps.Linear.Construct.Pointwise
  {r ℓr : Level}
  (𝓡 : CommutativeSemiring r ℓr) where

open CommutativeSemiring 𝓡
  renaming (Carrier to R)
  using (semiring; isCommutativeSemiring)

-- stdlib
open import Algebra.Module.Construct.Vector
  renaming (mkLeftSemimodule to [_])
open import Data.Sum as ⊎ using (_⊎_)
open import Data.Unit.Polymorphic using (⊤)

-- carya
open import Maps.Linear.Core semiring as Core using ()
open import Maps.Linear.Core semiring
  using (
      ⟦_⟧
    ; _≈_
    ; isEquivalence
    ; setoid
    ; mk
    ; mk⊸
    ; _⟨$⟩_
  ) public
open import Maps.Linear.Related semiring
open import Maps.Linear.Base semiring as L using ()

private
  variable
    a b c d : Level
    A : Set a
    B : Set b
    C : Set c
    D : Set d
```

</details>

```agda
_⊸_ : Set a → Set b → Set (r ⊔ ℓr ⊔ a ⊔ b)
A ⊸ B = [ A ] Core.⊸ [ B ]
```

```agda
diag : R → A ⊸ A
diag r = L.diag {C𝓡 = isCommutativeSemiring} r

_*ₗ_ : R → A ⊸ B → A ⊸ B
_*ₗ_ = L._*ₗ_ {C𝓡 = isCommutativeSemiring}

zero : A ⊸ B
zero = L.zero

infixl 6 _+_
_+_ : A ⊸ B → A ⊸ B → A ⊸ B
_+_ = L._+_

id : A ⊸ A
id = L.id

infixr 9 _∘_
_∘_ : B ⊸ C → A ⊸ B → A ⊸ C
_∘_ = L._∘_

! : A ⊸ ⊤
! = 𝟘⊸⊤ L.∘ L.!

¡ : ∀ {b ℓb : Level} → ⊤ ⊸ A
¡ {b = b} {ℓb} = L.¡ L.∘ (⊤⊸𝟘 {b} {ℓb})

infixr 6 _⊕_
_⊕_ : A ⊸ C → B ⊸ D → (A ⊎ B) ⊸ (C ⊎ D)
f ⊕ g = ⊕⊸⊎ L.∘ (f L.⊕ g) L.∘ ⊎⊸⊕

reduce :  (A ⊎ A) ⊸ A
reduce = L.reduce L.∘ ⊎⊸⊕

copy : A ⊸ (A ⊎ A)
copy = ⊕⊸⊎ L.∘ L.copy

swap :  (A ⊎ B) ⊸ (B ⊎ A)
swap = ⊕⊸⊎ L.∘ L.swap L.∘ ⊎⊸⊕

infixr 6 _▵_
_▵_ : A ⊸ B → A ⊸ C → A ⊸ (B ⊎ C)
f ▵ g = (f ⊕ g) ∘ copy

proj₁ : A ⊸ (A ⊎ B)
proj₁ = id ▵ zero

proj₂ : B ⊸ (A ⊎ B)
proj₂ = zero ▵ id

infixr 6 _▿_
_▿_ : A ⊸ C → B ⊸ C → (A ⊎ B) ⊸ C
f ▿ g = reduce ∘ (f ⊕ g)

inj₁ : (A ⊎ B) ⊸ A
inj₁ = id ▿ zero

inj₂ : (A ⊎ B) ⊸ B
inj₂ = zero ▿ id

assocʳ : ((A ⊎ B) ⊎ C) ⊸ (A ⊎ (B ⊎ C))
assocʳ = ⊕assoc⊎ʳ L.∘ L.assocʳ L.∘ ⊎assoc⊕ˡ

assocˡ : (A ⊎ (B ⊎ C)) ⊸ ((A ⊎ B) ⊎ C)
assocˡ = ⊕assoc⊎ˡ L.∘ L.assocˡ L.∘ ⊎assoc⊕ʳ
```
