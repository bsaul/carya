---
title: Finite Dimensional Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --without-K #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (CommutativeSemiring)

module Maps.Linear.Construct.FiniteDimension
  {r ℓr : Level}
  (𝓡 : CommutativeSemiring r ℓr)
  where

private open module 𝓡 = CommutativeSemiring 𝓡
            renaming (Carrier to R)
            using (semiring)

-- stdlib
open import Algebra.Module.Construct.Vector
  renaming (mkLeftSemimodule to [_])
open import Function renaming (_∘_ to _∘ᶠ_) using ()
open import Data.Nat as ℕ using (ℕ)
open import Data.Fin using (Fin)
open import Data.Fin.Patterns
open import Data.Sum as ⊎ using ()
open import Data.Product using (_×_ ; _,_)
open import Data.Vec.Functional using (_∷_)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

-- carya
open import LinearSpace.Dual.Base 𝓡
open import Maps.Linear.Core semiring as Core using ()
open import Maps.Linear.Base semiring as C using ()
open import Maps.Linear.Related semiring
open import Maps.Linear.Construct.Pointwise 𝓡 as L
  using (
      ⟦_⟧
    ; _≈_
    ; isEquivalence
    ; setoid
    ; mk
    ; mk⊸
    ; _⟨$⟩_
  ) public

private
  variable
    m n o p : ℕ
```

</details>

## Type

```agda
_⊸_ : ℕ → ℕ → Set (r ⊔ ℓr)
m ⊸ n = (Fin m) L.⊸ (Fin n)
```

## Operations

```agda
diag : R → m ⊸ m
diag = L.diag

_*ₗ_ : R → m ⊸ n → m ⊸ n
_*ₗ_ = L._*ₗ_

zero : m ⊸ n
zero = L.zero

infixl 6 _+_
_+_ : m ⊸ n → m ⊸ n → m ⊸ n
_+_ = L._+_

id : m ⊸ m
id = L.id

infixr 9 _∘_
_∘_ : n ⊸ o → m ⊸ n → m ⊸ o
_∘_ = L._∘_

! : m ⊸ 1
! = ⊤⊸1 L.∘ L.!

¡ : 1 ⊸ m
¡ = (L.¡ {b = r} { ℓr }) L.∘ 1⊸⊤

infixr 6 _⊕_
_⊕_ : m ⊸ o → n ⊸ p → (m ℕ.+ n) ⊸ (o ℕ.+ p)
f ⊕ g = m⊎n⊸m+n L.∘ (f L.⊕ g) L.∘ m+n⊸m⊎n

reduce : (m ℕ.+ m) ⊸ m
reduce = L.reduce L.∘ m+n⊸m⊎n

copy : m ⊸ (m ℕ.+ m)
copy = m⊎n⊸m+n L.∘ L.copy

infixr 6 _▵_
_▵_ : m ⊸ n → m ⊸ o → m ⊸ (n ℕ.+ o)
f ▵ g = (f ⊕ g) ∘ copy

proj₁ : m ⊸ (m ℕ.+ n)
proj₁ = id ▵ zero

proj₂ : n ⊸ (m ℕ.+ n)
proj₂ = zero ▵ id

infixr 6 _▿_
_▿_ : m ⊸ o → n ⊸ o → (m ℕ.+ n) ⊸ o
f ▿ g = reduce ∘ (f ⊕ g)

inj₁ : (m ℕ.+ n) ⊸ m
inj₁ = id ▿ zero

inj₂ : (m ℕ.+ n) ⊸ n
inj₂ = zero ▿ id

swap : ∀ {m n} → (n ℕ.+ m) ⊸ (m ℕ.+ n)
swap {m} {n} = (m⊎n⊸m+n {m} {n}) L.∘ L.swap L.∘ m+n⊸m⊎n

assocˡ : ∀ {m n o} → (m ℕ.+ (n ℕ.+ o)) ⊸ ((m ℕ.+ n) ℕ.+ o)
assocˡ {m} {n} {o} = (⊎assoc+ˡ {m} {n} {o}) L.∘ L.assocˡ L.∘ +assoc⊎ʳ

assocʳ : ∀ {m n o} → ((m ℕ.+ n) ℕ.+ o) ⊸ (m ℕ.+ (n ℕ.+ o))
assocʳ {m} {n} {o} = (⊎assoc+ʳ {m} {n} {o}) L.∘ L.assocʳ L.∘ +assoc⊎ˡ
```

## Additional operations/utilites

### Transpose

```agda
_ᵀ : m ⊸ n → n ⊸ m
T ᵀ = m*⊸m C.∘ transpose T C.∘ m⊸m*
```

### Constructors

```agda
fromScalar : R → 1 ⊸ 1
fromScalar = _*ₗ id

toScalar : 1 ⊸ 1 → R
toScalar x = (x ⟨$⟩ λ _ → 𝓡.1#) 0F

private
  scalar-tofrom : ∀ x → toScalar (fromScalar x) 𝓡.≈ x
  scalar-tofrom = 𝓡.*-identityʳ

  scalar-fromto : ∀ x → fromScalar (toScalar x) ≈ x
  scalar-fromto x {f} {0F} = begin
    to (λ _ → 𝓡.1#) 0F 𝓡.* f 0F ≈⟨ 𝓡.*-comm _ _ ⟩
    f 0F 𝓡.* to (λ _ → 𝓡.1#) 0F ≈⟨ *ₗ-homo ⟩
    to (λ _ → f 0F 𝓡.* 𝓡.1#) 0F ≈⟨ cong (λ { {0F} → 𝓡.*-identityʳ _ }) ⟩
    to f 0F ∎
    where open ≈-Reasoning 𝓡.setoid
          open Core._⊸_ x

vecToRow : (Fin m → R) → 1 ⊸ m
vecToRow {ℕ.zero} _  = zero
vecToRow {ℕ.suc m} x = fromScalar (x 0F) ▵ vecToRow (x ∘ᶠ Fin.suc)

rowToVec : 1 ⊸ m → Fin m → R
rowToVec {ℕ.suc m} x = toScalar (inj₁ ∘ x) ∷ rowToVec {m} (inj₂ ∘ x)

-- private
  -- TODO: complete
  -- row-tofrom : ∀ x → (vecToRow {m} (rowToVec x)) ≈ x
  -- row-tofrom {ℕ.suc m} x {f} {0F} = {!   !}
  --     where open ≈-Reasoning 𝓡.setoid
  --           open Core._⊸_ x
  -- row-tofrom {ℕ.suc m} x {f} {Fin.suc i} = {!   !}
  --     where open ≈-Reasoning 𝓡.setoid
  --           open Core._⊸_ x

vecToCol : (Fin m → R) → m ⊸ 1
vecToCol {ℕ.zero} _  = zero
vecToCol {ℕ.suc m} x = fromScalar (x 0F) ▿ vecToCol (x ∘ᶠ Fin.suc)

colToVec : m ⊸ 1 → Fin m → R
colToVec {ℕ.suc m} x = toScalar (x ∘ proj₁) ∷ colToVec {m} (x ∘ proj₂)
```

### Other

```agda
split : m ⊸ (n ℕ.+ o) → m ⊸ n × m ⊸ o
split x = inj₁ ∘ x , inj₂ ∘ x
```
