---
title: Linear Functionals
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (CommutativeSemiring)

module Maps.Linear.Construct.Functional
   {r ℓr}
   (𝓡 : CommutativeSemiring r ℓr)
   where

open CommutativeSemiring 𝓡
  renaming (Carrier to R)
  using (semiring; isCommutativeSemiring)

-- stdlib
open import Level using (Level; _⊔_)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.TensorUnit
 renaming (leftSemimodule to 𝓡*)
open import Algebra.Module.Construct.DirectProduct
  renaming (leftSemimodule to _⊕_) using ()
open import Algebra.Module.Construct.Zero
  renaming (leftSemimodule to 𝟘) using ()

-- carya
open import Maps.Linear.Core semiring using (_⊸_)
open import Maps.Linear.Core semiring
  using (
      ⟦_⟧
    ; _≈_
    ; isEquivalence
    ; setoid
    ; mk
    ; mk⊸
    ; _⟨$⟩_
  ) public
open import Maps.Linear.Base semiring as L using ()


private
  variable
    a ℓa b ℓb : Level
    𝓜ᴬ : LeftSemimodule semiring a ℓa
    𝓜ᴮ : LeftSemimodule semiring b ℓb
```

</details>

A [linear functional](https://en.wikipedia.org/wiki/Linear_form)
is a linear map from a `LeftSemimodule` (or module or vector)
to its scalar space.
These are also referred to as linear forms or covectors.

```agda
LinearFunctional : LeftSemimodule semiring a ℓa → Set (r ⊔ ℓr ⊔ a ⊔ ℓa)
LinearFunctional = _⊸ 𝓡*
```

```agda
_*ₗ_ : R → LinearFunctional 𝓜ᴬ → LinearFunctional 𝓜ᴬ
_*ₗ_ = L._*ₗ_ {C𝓡 = isCommutativeSemiring}

0# : LinearFunctional 𝓜ᴬ
0# = L.zero

infixl 6 _+_
_+_ : LinearFunctional 𝓜ᴬ → LinearFunctional 𝓜ᴬ → LinearFunctional 𝓜ᴬ
_+_ = L._+_
```

```agda
¡ : LinearFunctional (𝟘 {a} {ℓa})
¡ = L.¡

infixr 6 _▿_
_▿_ : LinearFunctional 𝓜ᴬ → LinearFunctional 𝓜ᴮ → LinearFunctional (𝓜ᴬ ⊕ 𝓜ᴮ)
_▿_ = L._▿_
```
