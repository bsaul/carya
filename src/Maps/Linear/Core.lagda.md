---
title: Linear Maps with Bundles as Objects
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (Semiring; CommutativeMonoid)

module Maps.Linear.Core {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

open import Algebra.Module.Bundles using (LeftSemimodule)
open import Function using (Congruent; _on_)
open import Relation.Binary using (Setoid ; IsEquivalence)
open import Relation.Binary.Construct.On as On using ()

import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open Semiring 𝓡
    renaming
    ( Carrier to R
    ; _≈_ to _≈ᴿ_
    ; _+_ to _+ᴿ_
    ; refl to reflᴿ
    )
    using (
      1#
    ; 0#
    )

open import Maps.Prelinear.Core 𝓡 as P using (Prelinear)
import Maps.Linear.Definitions as Defs

private
  variable
    a b ℓa ℓb : Level
```

</details>

## Linear Predicate

```agda
record IsLinear
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  (map : Prelinear 𝓜ᴬ 𝓜ᴮ) : Set (a ⊔ b ⊔ r ⊔ ℓa ⊔ ℓb ⊔ ℓʳ) where

  constructor mk
  open Prelinear map public
  open Defs 𝓜ᴬ 𝓜ᴮ

  private
    f = to

  field
    +-homo  : +-Homomorphic f
    *ₗ-homo : *ₗ-Homomorphic f

  open Domain renaming
      ( _*ₗ_ to _*ᴬ_
      ; 0ᴹ to 0ᴬ
      ; ≈ᴹ-sym to symᴬ
      ; *ₗ-zeroˡ to *ᴬ-zeroˡ
      )
      using ()

  open Codomain renaming
      ( _*ₗ_ to _*ᴮ_
      ; _≈ᴹ_ to _≈ᴮ_
      ; 0ᴹ to 0ᴮ
      )

  f0≈0 : f 0ᴬ ≈ᴮ 0ᴮ
  f0≈0 =
    begin
      f 0ᴬ
    ≈⟨ cong (symᴬ (*ᴬ-zeroˡ _)) ⟩
      f (0#  *ᴬ 0ᴬ)
    ≈˘⟨ *ₗ-homo ⟩
      (0# *ᴮ (f 0ᴬ))
    ≈⟨ *ₗ-zeroˡ (f 0ᴬ) ⟩
      0ᴮ
    ∎
    where open ≈-Reasoning ≈ᴹ-setoid
```

## Linear Map

```agda
infix 3 _⊸_
record _⊸_
  (𝓜ᴬ : LeftSemimodule 𝓡 a ℓa)
  (𝓜ᴮ : LeftSemimodule 𝓡 b ℓb)
  : Set (a ⊔ b ⊔ r ⊔ ℓa ⊔ ℓb ⊔ ℓʳ) where

  constructor mk

  field
    map : Prelinear 𝓜ᴬ 𝓜ᴮ
    isLinear : IsLinear map

  open IsLinear isLinear public
```

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ

  open Defs 𝓜ᴬ 𝓜ᴮ

  mk⊸ : (f : 𝓜ᴬ.Carrierᴹ → 𝓜ᴮ.Carrierᴹ)
      → (cong : Congruent 𝓜ᴬ._≈ᴹ_ 𝓜ᴮ._≈ᴹ_ f)
      → +-Homomorphic f
      → *ₗ-Homomorphic f
      → 𝓜ᴬ ⊸ 𝓜ᴮ
  mk⊸ to cong +-homo *ₗ-homo = mk (P.mkPre to cong) (mk +-homo *ₗ-homo)
```

## Equivalence and Denotation

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  ⟦_⟧ : 𝓜ᴬ ⊸ 𝓜ᴮ → Prelinear 𝓜ᴬ 𝓜ᴮ
  ⟦_⟧ = _⊸_.map

  infix 4 _≈_
  _≈_ : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᴮ → Set _
  _≈_ = P._≈_ on ⟦_⟧

  isEquivalence : IsEquivalence _≈_
  isEquivalence = On.isEquivalence ⟦_⟧ P.isEquivalence

  setoid : Setoid _ _
  setoid = record {isEquivalence = isEquivalence}
```

## Application

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A) using ()
  open LeftSemimodule 𝓜ᴮ renaming (Carrierᴹ to B) using ()

  infixr -1 _⟨$⟩_
  _⟨$⟩_ : 𝓜ᴬ ⊸ 𝓜ᴮ → A → B
  T ⟨$⟩ a = ⟦ T ⟧ P.⟨$⟩ a
```
