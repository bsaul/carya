---
title: Algebra Constructs on Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles

module Maps.Linear.Algebra {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

import Algebra.Structures as Struct
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Maps.Linear.Core 𝓡
open import Maps.Linear.Base 𝓡
open import Maps.Linear.Properties 𝓡
open import Data.Product as × using (_,_)

private
  variable
    a b ℓa ℓb : Level
```

</details>

## Algebraic Structures/Bundles with `_+_`

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  open LeftSemimodule 𝓜ᴮ

  open Struct (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ})

  +-isMagma : IsMagma _+_
  +-isMagma = record
    { isEquivalence = isEquivalence
    ; ∙-cong = λ {x} {y} {u} {v} → +-cong {x = x} {y} {u} {v}
    }

  +-isSemigroup : IsSemigroup _+_
  +-isSemigroup = record
    { isMagma = +-isMagma
    ; assoc = +-assoc
    }

  +-0-isMonoid : IsMonoid _+_ zero
  +-0-isMonoid  = record
    { isSemigroup = +-isSemigroup
    ; identity = +-identity
    }

  +-0-isCommutativeMonoid : IsCommutativeMonoid _+_ zero
  +-0-isCommutativeMonoid  = record
    { isMonoid = +-0-isMonoid
    ; comm = +-comm
    }

  +-magma : Magma (r ⊔ ℓʳ ⊔ a ⊔ ℓa ⊔ b ⊔ ℓb) (a ⊔ ℓb)
  +-magma = record { isMagma = +-isMagma }

  +-semigroup : Semigroup (r ⊔ ℓʳ ⊔ a ⊔ ℓa ⊔ b ⊔ ℓb) (a ⊔ ℓb)
  +-semigroup = record { isSemigroup = +-isSemigroup }

  +-0-monoid : Monoid (r ⊔ ℓʳ ⊔ a ⊔ ℓa ⊔ b ⊔ ℓb) (a ⊔ ℓb)
  +-0-monoid = record { isMonoid = +-0-isMonoid }

  +-0-commutativeMonoid : CommutativeMonoid (r ⊔ ℓʳ ⊔ a ⊔ ℓa ⊔ b ⊔ ℓb) (a ⊔ ℓb)
  +-0-commutativeMonoid =
    record { isCommutativeMonoid = +-0-isCommutativeMonoid }
```

## Algebraic Structures/Bundles with `_∘_`

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} where

  open LeftSemimodule 𝓜ᴬ

  open Struct (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ})

  ∘-isMagma : IsMagma _∘_
  ∘-isMagma = record
    { isEquivalence = isEquivalence
    ; ∙-cong = λ {x} {y} {u} {v} → ∘-cong {𝓜ᴬ = 𝓜ᴬ} {x} {y} {u} {v}
    }

  ∘-isSemigroup : IsSemigroup _∘_
  ∘-isSemigroup = record { isMagma = ∘-isMagma ; assoc = ∘-assoc }

  ∘-id-isMonoid : IsMonoid _∘_ id
  ∘-id-isMonoid = record
    { isSemigroup = ∘-isSemigroup
    ; identity = ∘-identity
    }

  ∘-magma : Magma (r ⊔ ℓʳ ⊔ a ⊔ ℓa) (a ⊔ ℓa)
  ∘-magma = record { isMagma = ∘-isMagma }

  ∘-semigroup : Semigroup (r ⊔ ℓʳ ⊔ a ⊔ ℓa) (a ⊔ ℓa)
  ∘-semigroup = record { isSemigroup = ∘-isSemigroup }

  ∘-id-monoid : Monoid (r ⊔ ℓʳ ⊔ a ⊔ ℓa) (a ⊔ ℓa)
  ∘-id-monoid = record { isMonoid = ∘-id-isMonoid }
```

## Algebraic Structures/Bundles with `_∘_` and `_+_`

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} where

  open LeftSemimodule 𝓜ᴬ

  open Struct (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ})

  +-∘-isNearSemiring : IsNearSemiring _+_ _∘_ zero
  +-∘-isNearSemiring = record
    { +-isMonoid = +-0-isMonoid
    ; *-cong = λ {f} {g} {h} {i} → ∘-cong {x = f} {g} {h} {i}
    ; *-assoc = ∘-assoc
    ; distribʳ = distribʳ
    ; zeroˡ = ∘-zeroˡ
    }

  +-∘-isSemiringWithoutAnnihilatingZero :
     IsSemiringWithoutAnnihilatingZero _+_ _∘_ zero id
  +-∘-isSemiringWithoutAnnihilatingZero = record
    { +-isCommutativeMonoid = +-0-isCommutativeMonoid
    ; *-cong = λ {f} {g} {h} {i} → ∘-cong {x = f} {g} {h} {i}
    ; *-assoc = ∘-assoc
    ; *-identity = ∘-identity
    ; distrib = distrib
    }

  +-∘-isSemiring : IsSemiring _+_ _∘_ zero id
  +-∘-isSemiring = record
    { isSemiringWithoutAnnihilatingZero = +-∘-isSemiringWithoutAnnihilatingZero
    ; zero = ∘-zero
    }

  +-∘-NearSemiring : NearSemiring (r ⊔ ℓʳ ⊔ a ⊔ ℓa) (a ⊔ ℓa)
  +-∘-NearSemiring = record { isNearSemiring = +-∘-isNearSemiring }

  +-∘-semiring : Semiring (r ⊔ ℓʳ ⊔ a ⊔ ℓa) (a ⊔ ℓa)
  +-∘-semiring = record { isSemiring = +-∘-isSemiring }
```
