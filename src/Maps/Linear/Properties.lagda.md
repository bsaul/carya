---
title: Properties of Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_; suc)
open import Algebra.Bundles using (Semiring)

module Maps.Linear.Properties {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

-- stdlib
import Algebra.Definitions as Defs
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.DirectProduct
  renaming (leftSemimodule to _⊕ₗₛ_) using ()
open import Data.Product as × using (_,_ ; _×_ ; ∃; Σ)
open import Function as F using () renaming (_∘_ to _∘ᶠ_)
open import Function.Definitions as F using ()
open import Function.Construct.Composition as FC using ()
open import Relation.Binary using (Rel; REL)
open import Relation.Unary using (_≐_)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

-- carya
open import Maps.Prelinear.Properties 𝓡 as P using ()
open import Maps.Linear.Core 𝓡
open import Maps.Linear.Base 𝓡
-- open import Maps.Linear.Definitions 𝓡

private
  variable
    a b c m ℓa ℓb ℓc ℓm : Level

open Semiring 𝓡 renaming
  ( Carrier to Cᴿ
  ; _≈_ to _≈ᴿ_
  ; refl to reflᴿ
  ; zeroʳ to zeroʳᴿ
  )
  using (
    0#
  )
```

</details>

```agda
-- TODO: is this the standard name?
--       should this go here since it's about the Semimodule not a map?
module _  (𝓜 : LeftSemimodule 𝓡 a ℓa) where
  open LeftSemimodule 𝓜

  IsUnit : Carrierᴹ → Set (a ⊔ ℓa)
  IsUnit m = m ≉ᴹ 0ᴹ → ∀ (m′ : Carrierᴹ) → m ≉ᴹ m′ → m′ ≈ᴹ 0ᴹ
```

## Injection/Surjection/Bijection

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  open LeftSemimodule 𝓜ᴬ renaming (_≈ᴹ_ to _≈ᴬ_)
  open LeftSemimodule 𝓜ᴮ renaming (_≈ᴹ_ to _≈ᴮ_)

  Injective : 𝓜ᴬ ⊸ 𝓜ᴮ → Set (a ⊔ ℓa ⊔ ℓb)
  Injective T = F.Injective _≈ᴬ_ _≈ᴮ_ (T ⟨$⟩_)

  Surjective : 𝓜ᴬ ⊸ 𝓜ᴮ → Set (a ⊔ ℓa ⊔ b ⊔ ℓb)
  Surjective T = F.Surjective _≈ᴬ_ _≈ᴮ_ (T ⟨$⟩_)

  Bijective : 𝓜ᴬ ⊸ 𝓜ᴮ → Set (a ⊔ ℓa ⊔ b ⊔ ℓb)
  Bijective T = Injective T × Surjective T

  Inverseᵇ : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴮ ⊸ 𝓜ᴬ → Set (a ⊔ ℓa ⊔ b ⊔ ℓb)
  Inverseᵇ T₁ T₂ = F.Inverseᵇ _≈ᴬ_ _≈ᴮ_ (T₁ ⟨$⟩_) (T₂ ⟨$⟩_)
```

## Monomorphic/Epimorphic/Isomorphic Linear maps

```agda
module _
  {𝓜ᴬ 𝓜ᴮ : LeftSemimodule 𝓡 a ℓa}
  where

  Monomorphic : 𝓜ᴬ ⊸ 𝓜ᴮ → Set (r ⊔ ℓʳ ⊔ suc a ⊔ suc ℓa)
  Monomorphic T =
    ∀ {𝓜ᶜ : LeftSemimodule 𝓡 a ℓa} {S₁ S₂ : 𝓜ᶜ ⊸ 𝓜ᴬ}
    → T ∘ S₁ ≈ T ∘ S₂
    → S₁ ≈ S₂

  Epimorphic : 𝓜ᴬ ⊸ 𝓜ᴮ → Set (r ⊔ ℓʳ ⊔ suc a ⊔ suc ℓa)
  Epimorphic T =
    ∀ {𝓜ᶜ : LeftSemimodule 𝓡 a ℓa} {S₁ S₂ : 𝓜ᴮ ⊸ 𝓜ᶜ}
    → S₁ ∘ T ≈ S₂ ∘ T
    → S₁ ≈ S₂

  Isomorphic : 𝓜ᴬ ⊸ 𝓜ᴮ → Set (r ⊔ ℓʳ ⊔ suc a ⊔ suc ℓa)
  Isomorphic  T = Monomorphic  T × Epimorphic  T
```

### Equivalence of Injective/Monomorphic

```agda
module _ {𝓜ᴬ 𝓜ᴮ : LeftSemimodule 𝓡 m ℓm} where

  open LeftSemimodule 𝓜ᴬ -- renaming (_≈ᴹ_ to _≈ᴮ_)

  Inj⇒Mono : {T : 𝓜ᴬ ⊸ 𝓜ᴮ} → Injective T → Monomorphic T
  Inj⇒Mono inj x = inj x

  -- TODO: stuck on the following, but close
  -- Mono⇒Inj : ∀ {T : 𝓜ᴬ ⊸ 𝓜ᴮ} → {𝓜ᶜ : LeftSemimodule 𝓡 m ℓm} → Monomorphic T → Injective T
  -- Mono⇒Inj {T = T} {𝓜ᶜ} mono {x = x} {y = y} Tx≈Ty = mono {𝓜ᶜ = 𝓜ᶜ} Tx≈Ty
```

### Equivalence of Surjective/Epimorphic

```agda
module _ {𝓜ᴬ 𝓜ᴮ : LeftSemimodule 𝓡 m ℓm} where

  open LeftSemimodule 𝓜ᴬ renaming (≈ᴹ-refl to ≈ᵃ-refl) using ()

  Sur⇒Epi : ∀{T : 𝓜ᴬ ⊸ 𝓜ᴮ} → Surjective T → Epimorphic T
  Sur⇒Epi surjection {𝓜ᶜ = 𝓜ᶜ} {S₁ = S₁} {S₂ = S₂} s₁fx≈s₂fx  =
   let (a , surject) = surjection _
       Ta≈x = surject {z = a} ≈ᵃ-refl
   in ≈ᴹ-trans
        (≈ᴹ-trans (≈ᴹ-sym (s₁-cong Ta≈x)) s₁fx≈s₂fx)
        (s₂-cong Ta≈x)
    where open _⊸_ S₁ renaming (to to s₁; cong to s₁-cong) using ()
          open _⊸_ S₂ renaming (to to s₂; cong to s₂-cong) using ()
          open LeftSemimodule 𝓜ᶜ
```

## Invertible

```agda
module _ {𝓜ᴬ 𝓜ᴮ : LeftSemimodule 𝓡 m ℓm} where

  _IsRightInverseOf_ : REL (𝓜ᴮ ⊸ 𝓜ᴬ) (𝓜ᴬ ⊸ 𝓜ᴮ) _
  T⁻¹ IsRightInverseOf T = T ∘ T⁻¹ ≈ id

  RightInvertible : 𝓜ᴬ ⊸ 𝓜ᴮ → Set _
  RightInvertible T = ∃ (_IsRightInverseOf T)

  _IsLeftInverseOf_ : REL (𝓜ᴮ ⊸ 𝓜ᴬ) (𝓜ᴬ ⊸ 𝓜ᴮ) _
  T⁻¹ IsLeftInverseOf T = T⁻¹ ∘ T ≈ id

  LeftInvertible : 𝓜ᴬ ⊸ 𝓜ᴮ → Set _
  LeftInvertible T = ∃ (_IsLeftInverseOf T)
```

## Properties of `_+_`

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  open Defs (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ})

  +-cong : Congruent₂ _+_
  +-cong {x} {y} {u} {v} = P.+-cong {x = ⟦ x ⟧} {⟦ y ⟧} {⟦ u ⟧} {⟦ v ⟧}

  +-assoc : Associative _+_
  +-assoc f g h = P.+-assoc ⟦ f ⟧ ⟦ g ⟧ ⟦ h ⟧

  +-comm : Commutative _+_
  +-comm f g = P.+-comm ⟦ f ⟧ ⟦ g ⟧

  -- @macedo2013 eq 7
  +-identityˡ : LeftIdentity zero _+_
  +-identityˡ = P.+-identityˡ ∘ᶠ ⟦_⟧

  +-identityʳ : RightIdentity zero _+_
  +-identityʳ = P.+-identityʳ ∘ᶠ ⟦_⟧

  +-identity : Identity zero _+_
  +-identity = +-identityˡ , +-identityʳ
```

## Properties of `_∘_`

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ
    module 𝓜ᶜ = LeftSemimodule 𝓜ᶜ

  congruent : ∀ {f : 𝓜ᴬ ⊸ 𝓜ᴮ} {g : 𝓜ᴮ ⊸ 𝓜ᶜ}
    → F.Congruent 𝓜ᴬ._≈ᴹ_ 𝓜ᴮ._≈ᴹ_ (f ⟨$⟩_)
    → F.Congruent 𝓜ᴮ._≈ᴹ_ 𝓜ᶜ._≈ᴹ_ (g ⟨$⟩_)
    → F.Congruent 𝓜ᴬ._≈ᴹ_ 𝓜ᶜ._≈ᴹ_ ((g ∘ f) ⟨$⟩_)
  congruent  = FC.congruent 𝓜ᴬ._≈ᴹ_ 𝓜ᴮ._≈ᴹ_ 𝓜ᶜ._≈ᴹ_
```

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 m ℓm} where

  open LeftSemimodule 𝓜ᴬ
  open Defs (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ})

  ∘-cong : Congruent₂  _∘_
  ∘-cong {x} {y} {u} {v} = P.∘-cong {x = ⟦ x ⟧} {⟦ y ⟧} {⟦ u ⟧} {⟦ v ⟧}

  ∘-assoc : Associative  _∘_
  ∘-assoc T U V = P.∘-assoc ⟦ T ⟧ ⟦ U ⟧ ⟦ V ⟧

  -- @macedo2013 eq 4
  ∘-identityˡ : LeftIdentity id _∘_
  ∘-identityˡ = P.∘-identityˡ ∘ᶠ ⟦_⟧

  ∘-identityʳ : RightIdentity id _∘_
  ∘-identityʳ = P.∘-identityʳ ∘ᶠ ⟦_⟧

  ∘-identity : Identity id _∘_
  ∘-identity = ∘-identityˡ , ∘-identityˡ

  -- @macedo2013 eq 8
  ∘-zeroˡ : LeftZero zero _∘_
  ∘-zeroˡ f {x} = P.∘-zeroˡ ⟦ f ⟧ {x}

  -- @macedo2013 eq 8
  -- NOTE: linearity makes this property possible
  ∘-zeroʳ : RightZero zero _∘_
  ∘-zeroʳ T = _⊸_.f0≈0 T

  ∘-zero : Zero zero _∘_
  ∘-zero = ∘-zeroˡ , ∘-zeroʳ
```

## Disributivity of Composition over Addition

```agda
  -- @macedo2013 eq 9
  -- NOTE: linearity makes this property possible
  distribˡ :  _∘_ DistributesOverˡ _+_
  distribˡ f g h = _⊸_.+-homo f

  -- @macedo2013 eq 10
  distribʳ :  _∘_ DistributesOverʳ _+_
  distribʳ f g h = P.distribʳ ⟦ f ⟧ ⟦ g ⟧ ⟦ h ⟧

  distrib :  _∘_ DistributesOver _+_
  distrib = distribˡ , distribʳ
```

# Properties of `proj`/`inj`

See properties of Prelinear maps.

# Blocked Linear Algebra

The following laws are shown in Section 5 of @macedo2013.

## Fusion Laws

See properties of Prelinear maps for `▵-fusion`.


```agda
module _ {𝓜ᴬ 𝓜ᴮ 𝓜ᶜ 𝓜ᴰ : LeftSemimodule 𝓡 m ℓm} where

  open LeftSemimodule 𝓜ᴮ renaming (≈ᴹ-refl to reflᴮ) using ()
  open LeftSemimodule 𝓜ᶜ renaming (≈ᴹ-refl to reflᶜ) using ()

  -- @macedo2013 eq 27
  ▿-fusion : ∀ {T₁ : 𝓜ᴮ ⊸ 𝓜ᴬ} {T₂ : 𝓜ᶜ ⊸ 𝓜ᴬ} {T₃ : 𝓜ᴬ ⊸ 𝓜ᴰ}
    → ((T₁ ▿ T₂) · T₃) ≈ (T₁ · T₃) ▿ (T₂ · T₃)
  ▿-fusion {T₁ = T₁} {T₂ = T₂} {T₃ = T₃} = _⊸_.+-homo T₃
```

## Cancellation Laws

See properties of Prelinear maps for properties on `proj`/fork.

```agda
module _ {𝓜ᴬ 𝓜ᴮ 𝓜ᶜ : LeftSemimodule 𝓡 m ℓm} where

  private
    module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ
    module 𝓜ᶜ = LeftSemimodule 𝓜ᶜ

  -- @macedo2013 eq. 29
  inj₁-cancels-▿ : ∀ {T₁ : 𝓜ᴮ ⊸ 𝓜ᴬ} {T₂ : 𝓜ᶜ ⊸ 𝓜ᴬ}
    → (inj₁ · (T₁ — T₂)) ≈ T₁
  inj₁-cancels-▿ {T₁ = T₁} {T₂ = T₂}  =
    begin
     ((inj₁ · (T₁ — T₂)) ⟨$⟩ _)
    ≡⟨⟩
     ((T₁ ⟨$⟩ _) 𝓜ᴬ.+ᴹ (T₂ ⟨$⟩ _))
    ≈⟨ 𝓜ᴬ.+ᴹ-congˡ (_⊸_.f0≈0 T₂) ⟩
     ((T₁ ⟨$⟩ _) 𝓜ᴬ.+ᴹ 𝓜ᴬ.0ᴹ)
    ≈⟨ 𝓜ᴬ.+ᴹ-identityʳ (T₁ ⟨$⟩ _) ⟩
     (T₁ ⟨$⟩ _)
    ∎
    where open ≈-Reasoning 𝓜ᴬ.≈ᴹ-setoid

  inj₂-cancels-▿ :  ∀ {T₁ : 𝓜ᴮ ⊸ 𝓜ᴬ} {T₂ : 𝓜ᶜ ⊸ 𝓜ᴬ}
    → (inj₂ · (T₁ — T₂))  ≈  T₂
  inj₂-cancels-▿ {T₁ = T₁} {T₂ = T₂} =
    begin
     ((inj₂ · (T₁ — T₂)) ⟨$⟩ _)
    ≡⟨⟩
     ((T₁ ⟨$⟩ _) 𝓜ᴬ.+ᴹ (T₂ ⟨$⟩ _))
    ≈⟨ 𝓜ᴬ.+ᴹ-congʳ (_⊸_.f0≈0 T₁)  ⟩
      (𝓜ᴬ.0ᴹ 𝓜ᴬ.+ᴹ (T₂ ⟨$⟩ _))
    ≈⟨ 𝓜ᴬ.+ᴹ-identityˡ (T₂ ⟨$⟩ _) ⟩
     (T₂ ⟨$⟩ _)
    ∎
    where open ≈-Reasoning 𝓜ᴬ.≈ᴹ-setoid
```

## "Abide" (junc/split exchange) laws

See properties of Prelinear maps.

## Divide and Conquer

See properties of Prelinear maps.

