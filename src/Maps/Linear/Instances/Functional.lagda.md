---
title: Instances of Linear Functionals
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (CommutativeSemiring)

module Maps.Linear.Instances.Functional
  {r ℓr}
  (𝓡 : CommutativeSemiring r ℓr)
  where

private
  open module 𝓡 = CommutativeSemiring 𝓡

-- stdlib
open import Algebra.Module.Construct.Vector
  renaming (mkLeftSemimodule to [_]; finLeftSemimodule to vector)
open import Data.Product using (_,_)
open import Function using (Inverse)
open import Relation.Binary using (Setoid)

-- carya
open import Maps.Linear.Construct.Functional 𝓡
import Fold.Setoid.Properties.Semiring as Foldₛ
import Fold.Properties.Semiring as Fold

-- zizia
open import Finite.Core using (StrictlyFinite)
```

</details>


```agda
∑VectorForm : ∀ n → LinearFunctional (vector n)
∑VectorForm n =
   mk⊸
     ∑_
     (λ x → ∑-cong λ i → x {i})
     (λ {f} {g} → 𝓡.sym  (∑-preserves-+ f g))
     λ {r} {f} → c*∑≈∑c* f r
   where open Fold ⦃ 𝓡.semiring ⦄
```

```agda
∑FiniteForm : ∀ {a ℓa} {𝓐 : Setoid a ℓa}
  → (StrictlyFinite 𝓐)
  → LinearFunctional [ Setoid.Carrier 𝓐 ]
∑FiniteForm {𝓐 = 𝓐} (n , A↔n)  =
  mk⊸
    (∑[ Inverse.to A↔n ]_)
    (λ {f} {g} x≈y → ∑-cong {n = n} {f = f} {g} {x≈y})
    (λ {f} {g} → 𝓡.sym (∑-preserves-+ (Inverse.to A↔n) f g))
    λ {r} {f} → c*∑≈∑c* (Inverse.to A↔n) f r
    where open Foldₛ 𝓐 ⦃ 𝓡.semiring ⦄
```
