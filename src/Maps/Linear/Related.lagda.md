---
title: Relations between Linear Spaces
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_; 0ℓ)
open import Algebra.Bundles using (Semiring)

module Maps.Linear.Related {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

open Semiring 𝓡  using (sym ; refl; +-identityˡ; zeroʳ)

open import Algebra.Module.Construct.Vector
  renaming (mkLeftSemimodule to [_])
open import Algebra.Module.Construct.DirectProduct
  renaming (leftSemimodule to _⊕ₗ_) using ()
open import Algebra.Module.Construct.Zero
  renaming (leftSemimodule to 𝟘) using ()
open import Data.Fin using (Fin)
open import Data.Nat using (ℕ; _+_)
open import Data.Sum as ⊎ using (_⊎_)
open import Data.Product as × using (_,_)
open import Data.Unit.Polymorphic using (⊤ ; tt)
open import Function as F using ()

-- carya
open import Maps.Linear.Core 𝓡
open import Maps.Prelinear.Related 𝓡 as P using ()

private
  variable
    a b c ℓa ℓb : Level
    A : Set a
    B : Set b
    C : Set c
    m n o : ℕ
```

</details>

## Maps relating `_⊎_` and `_⊕ₗ_`

```agda
𝟘⊸⊤ : (𝟘 {a} {ℓa}) ⊸ [ ⊤ ]
𝟘⊸⊤ = mk P.𝟘⇒⊤ (mk (sym (+-identityˡ _)) (zeroʳ _))

⊤⊸𝟘 : [ ⊤ ] ⊸ (𝟘 {b} {ℓb})
⊤⊸𝟘 = mk P.⊤⇒𝟘 (mk tt tt)

⊎⊸⊕ : [ A ⊎ B ] ⊸ ([ A ] ⊕ₗ [ B ])
⊎⊸⊕ = mk P.⊎⇒⊕ (mk (refl , refl) (refl , refl))

⊕⊸⊎ : ([ A ] ⊕ₗ [ B ]) ⊸ [ A ⊎ B ]
⊕⊸⊎ = mk P.⊕⇒⊎
  (mk
    (λ { {x = ⊎.inj₁ _ } → refl
       ; {x = ⊎.inj₂ _ } → refl
       })
    (λ { {x = ⊎.inj₁ _ } → refl
       ; {x = ⊎.inj₂ _ } → refl
       }))

⊎assoc⊕ˡ : [ (A ⊎ B) ⊎ C ] ⊸ (([ A ] ⊕ₗ [ B ]) ⊕ₗ [ C ])
⊎assoc⊕ˡ = mk P.⊎assoc⊕ˡ
  (mk
     ((refl , refl) , refl)
     ((refl , refl) , refl))

⊎assoc⊕ʳ : [ A ⊎ B ⊎ C ] ⊸ ([ A ] ⊕ₗ ([ B ] ⊕ₗ [ C ]))
⊎assoc⊕ʳ = mk P.⊎assoc⊕ʳ
  (mk
    (refl , refl , refl)
    (refl , refl , refl))

⊕assoc⊎ʳ :  ([ A ] ⊕ₗ ([ B ] ⊕ₗ [ C ])) ⊸ [ A ⊎ B ⊎ C ]
⊕assoc⊎ʳ = mk P.⊕assoc⊎ʳ
  (mk
    (λ { {x = ⊎.inj₁ _} → refl
       ; {x = ⊎.inj₂ (⊎.inj₁ _)} → refl
       ; {x = ⊎.inj₂ (⊎.inj₂ _)} → refl })
    (λ { {x = ⊎.inj₁ _} → refl
       ; {x = ⊎.inj₂ (⊎.inj₁ _)} → refl
       ; {x = ⊎.inj₂ (⊎.inj₂ _)} → refl }))

⊕assoc⊎ˡ : (([ A ] ⊕ₗ [ B ]) ⊕ₗ [ C ]) ⊸ [ (A ⊎ B) ⊎ C ]
⊕assoc⊎ˡ = mk P.⊕assoc⊎ˡ
  (mk
    (λ { {x = ⊎.inj₁ (⊎.inj₁ _)} → refl
       ; {x = ⊎.inj₁ (⊎.inj₂ _)} → refl
       ; {x = ⊎.inj₂ _} → refl })
    (λ { {x = ⊎.inj₁ (⊎.inj₁ _)} → refl
       ; {x = ⊎.inj₁ (⊎.inj₂ _)} → refl
       ; {x = ⊎.inj₂ _} → refl }))
```

## Linear maps relating `Vector`s

```agda
1⊸⊤ : ∀ {ℓ} → [ Fin 1 ] ⊸ [ ⊤ {ℓ} ]
1⊸⊤ = mk P.1⇒⊤ (mk refl refl)

⊤⊸1 : ∀ {ℓ} → [ ⊤ {ℓ} ] ⊸ [ Fin 1 ]
⊤⊸1 = mk P.⊤⇒1 (mk refl refl)

m⊎n⊸m+n : [ (Fin m) ⊎ (Fin n) ] ⊸ [ Fin (m + n) ]
m⊎n⊸m+n = mk P.m⊎n⇒m+n (mk refl refl)

m+n⊸m⊎n : [ Fin (m + n) ] ⊸ [ (Fin m) ⊎ (Fin n) ]
m+n⊸m⊎n = mk P.m+n⇒m⊎n (mk refl refl)

⊎assoc+ˡ : [ (Fin m ⊎ Fin n) ⊎ Fin o ] ⊸ [ Fin ((m + n) + o) ]
⊎assoc+ˡ = mk P.⊎assoc+ˡ (mk refl refl)

⊎assoc+ʳ : [ Fin m ⊎ (Fin n ⊎ Fin o) ] ⊸ [ Fin (m + (n + o)) ]
⊎assoc+ʳ = mk P.⊎assoc+ʳ (mk refl refl)

+assoc⊎ˡ : [ Fin ((m + n) + o) ] ⊸ [ (Fin m ⊎ Fin n) ⊎ Fin o ]
+assoc⊎ˡ = mk P.+assoc⊎ˡ (mk refl refl)

+assoc⊎ʳ : [ Fin (m + (n + o)) ] ⊸ [ Fin m ⊎ (Fin n ⊎ Fin o) ]
+assoc⊎ʳ = mk P.+assoc⊎ʳ (mk refl refl)
```
