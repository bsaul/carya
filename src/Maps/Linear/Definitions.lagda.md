---
title: Definitions for Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (Semiring)
open import Algebra.Module.Bundles using (LeftSemimodule)

module Maps.Linear.Definitions
  {a b r ℓa ℓb ℓr}
  {𝓡 : Semiring r ℓr}
  (𝓜ᴬ : LeftSemimodule 𝓡 a ℓa)
  (𝓜ᴮ : LeftSemimodule 𝓡 b ℓb)
  where

open import Level using (Level; _⊔_; suc)
open import Algebra.Module.Bundles using (LeftSemimodule)

open LeftSemimodule 𝓜ᴬ renaming
    ( Carrierᴹ to A
    ; _+ᴹ_ to _+ᴬ_
    ; _*ₗ_ to _*ᴬ_
    )
    using ()
open LeftSemimodule 𝓜ᴮ renaming
    ( Carrierᴹ to B
    ; _+ᴹ_ to _+ᴮ_
    ; _*ₗ_ to _*ᴮ_
    ; _≈ᴹ_ to _≈ᴮ_
    )
    using ()
```

</details>

```agda
+-Homomorphic : (A → B) → Set (a ⊔ ℓb)
+-Homomorphic f = ∀ {a₁ a₂ : A} → f (a₁ +ᴬ a₂) ≈ᴮ f a₁ +ᴮ f a₂

*ₗ-Homomorphic : (A → B) → Set (a ⊔ r ⊔ ℓb)
*ₗ-Homomorphic f = ∀ {r} {a : A} → r *ᴮ f a ≈ᴮ f (r *ᴬ a)
```

