---
title: Operations on Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level)
open import Algebra.Bundles using (Semiring)

module Maps.Linear.Base {r ℓ : Level} (𝓡 : Semiring r ℓ) where

-- stdlib
import Algebra.Properties.CommutativeSemigroup as CS
open import Algebra.Structures using (IsCommutativeSemiring)
open import Algebra.Bundles using (CommutativeMonoid)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Construct.DirectProduct
  renaming (leftSemimodule to _⊕ₗ_) using ()
open import Algebra.Module.Construct.Zero
  renaming (leftSemimodule to 𝟘) using ()
open import Data.Product as × using (_,_; ∃)
open import Data.Unit.Polymorphic using (tt)
open import Function using (flip)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

-- carya
open import Maps.Prelinear.Base 𝓡 as P using ()
open import Maps.Prelinear.Core 𝓡 using (_≈_; Prelinear)
open import Maps.Linear.Core 𝓡 hiding (_≈_)

private
  variable
    a b c d ℓa ℓb ℓc ℓd : Level
    𝓜ᴬ : LeftSemimodule 𝓡 a ℓa
    𝓜ᴮ : LeftSemimodule 𝓡 b ℓb

open Semiring 𝓡
  renaming
  ( refl to reflᴿ
  ; Carrier to R
  ; _≈_ to _≈ᴿ_
  ; _+_ to _+ᴿ_
  ; _*_ to _*ᴿ_
  )
  using
  (0# ; 1#)
```

</details>


## Special Maps

### Zero

```agda
module _ {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb} where

  open LeftSemimodule 𝓜ᴮ
  zero : 𝓜ᴬ ⊸ 𝓜ᴮ
  zero = mk P.zero (mk (≈ᴹ-sym (+ᴹ-identityˡ 0ᴹ)) λ {r} → *ₗ-zeroʳ r)

  ⟦zero⟧ : ⟦ zero ⟧ ≈ (P.zero {𝓜ᴬ  = 𝓜ᴬ})
  ⟦zero⟧ = ≈ᴹ-refl
```

### Identity

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa} where

  open LeftSemimodule 𝓜ᴬ

  id : 𝓜ᴬ ⊸ 𝓜ᴬ
  id = mk P.id (mk (+ᴹ-cong ≈ᴹ-refl ≈ᴹ-refl)  ≈ᴹ-refl)

  ⟦id⟧ : ⟦ id ⟧ ≈ P.id
  ⟦id⟧ = ≈ᴹ-refl
```

### Point

```agda
! : 𝓜ᴬ ⊸ 𝟘
! = mk P.! (record { +-homo = tt ; *ₗ-homo = tt })

⟦!⟧ : ⟦ ! ⟧ ≈ (P.! {𝓜ᴬ = 𝓜ᴬ})
⟦!⟧ = tt
```

### Co-Point (TODO: what is name of this?)

```agda
¡ : (𝟘 {a} {ℓa}) ⊸ 𝓜ᴬ
¡ = zero
```

### Diagonal

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {C𝓡 : IsCommutativeSemiring _≈ᴿ_ _+ᴿ_ _*ᴿ_ 0# 1#} where

  open LeftSemimodule 𝓜ᴬ

  diag : R → 𝓜ᴬ ⊸ 𝓜ᴬ
  diag r =
    mk
      (P.diag r)
      (mk
        (λ {a₁} {a₂} → *ₗ-distribˡ r a₁ a₂)
        (λ {r₁} {a} →
          begin
            r₁ *ₗ r *ₗ a
          ≈˘⟨ *ₗ-assoc r₁ r a ⟩
            (r₁ *ᴿ r) *ₗ a
          ≈⟨ *ₗ-congʳ (IsCommutativeSemiring.*-comm C𝓡 r₁ r) ⟩
            (r *ᴿ r₁) *ₗ a
          ≈⟨ *ₗ-assoc r r₁ a ⟩
            r *ₗ r₁ *ₗ a
          ∎))
      where open ≈-Reasoning ≈ᴹ-setoid

  ⟦diag⟧ : ∀ {r} → ⟦ diag r ⟧ ≈ P.diag r
  ⟦diag⟧ = ≈ᴹ-refl
```

## Addition

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  open LeftSemimodule 𝓜ᴬ
    renaming ( _+ᴹ_ to _+ᴬ_ ; _*ₗ_ to _*ᴬ_)
    using ()
  private
    open module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ
        renaming ( _+ᴹ_ to _+ᴮ_ ; _*ₗ_ to _*ᴮ_)

  infixl 6 _+_
  _+_ : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᴮ
  (mk pref (mk +₁ *₁)) + (mk preg (mk  +₂ *₂)) =
    mk
      (pref P.+ preg)
      (mk
        (λ {a₁} {a₂} →
            begin
              f (a₁ +ᴬ a₂) +ᴮ g (a₁ +ᴬ a₂)
            ≈⟨ 𝓜ᴮ.+ᴹ-cong +₁ +₂ ⟩
              f a₁ +ᴮ f a₂ +ᴮ (g a₁ +ᴮ g a₂)
            ≈⟨ interchange (f a₁) (f a₂) (g a₁) (g a₂) ⟩
              f a₁ +ᴮ g a₁ +ᴮ (f a₂ +ᴮ g a₂)
            ∎)
        λ {r} {a} →
            begin
              r *ᴮ (f a +ᴮ g a)
            ≈⟨ 𝓜ᴮ.*ₗ-distribˡ r (f a) (g a) ⟩
              r *ᴮ f a +ᴮ r *ᴮ g a
            ≈⟨ 𝓜ᴮ.+ᴹ-congˡ *₂ ⟩
              r *ᴮ f a +ᴮ g (r *ᴬ a)
            ≈⟨ 𝓜ᴮ.+ᴹ-congʳ *₁ ⟩
              f (r *ᴬ a) +ᴮ g (r *ᴬ a)
            ∎
      )
      where open ≈-Reasoning 𝓜ᴮ.≈ᴹ-setoid
            open Prelinear pref renaming (to to f)
            open Prelinear preg renaming (to to g)
            open CS (CommutativeMonoid.commutativeSemigroup 𝓜ᴮ.+ᴹ-commutativeMonoid)

  ⟦+⟧ : ∀ {x y} → ⟦ x + y ⟧ ≈ ⟦ x ⟧ P.+ ⟦ y ⟧
  ⟦+⟧ = ≈ᴹ-refl
```

## Composition

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  where

  open LeftSemimodule 𝓜ᴬ renaming
      (Carrierᴹ to A; _+ᴹ_ to _+ᴬ_; _*ₗ_ to _*ᴬ_)
       using ()
  open LeftSemimodule 𝓜ᴮ renaming
      (Carrierᴹ to B; _+ᴹ_ to _+ᴮ_; _*ₗ_ to _*ᴮ_ )
      using ()
  private
    open module 𝓜ᶜ = LeftSemimodule 𝓜ᶜ renaming
        (Carrierᴹ to C; _+ᴹ_ to _+ᶜ_; _*ₗ_ to _*ᶜ_)
        using ()

  infixr 9 _∘_
  _∘_ : 𝓜ᴮ ⊸ 𝓜ᶜ → 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᶜ
  (mk pref (mk +₁ *₁)) ∘ (mk preg (mk  +₂ *₂)) =
    mk
      (pref P.∘ preg)
      (mk
          (λ {a₁} {a₂}  →
            begin
              f (g (a₁ +ᴬ a₂))
            ≈⟨ congᶠ +₂ ⟩
              f (g a₁ +ᴮ g a₂)
            ≈⟨ +₁ ⟩
              (f (g a₁) +ᶜ f (g a₂))
            ∎)
          λ {r} {a} →
            (begin
              (r *ᶜ f (g a))
            ≈⟨ *₁ ⟩
              f (r *ᴮ g a)
            ≈⟨ congᶠ *₂ ⟩
              f (g (r *ᴬ a))
            ∎)
        )
      where open ≈-Reasoning 𝓜ᶜ.≈ᴹ-setoid
            open Prelinear pref renaming (to to f ; cong to congᶠ)
            open Prelinear preg renaming (to to g)

  ⟦∘⟧ : ∀ {x y} → ⟦ x ∘ y ⟧ ≈ ⟦ x ⟧ P.∘ ⟦ y ⟧
  ⟦∘⟧ = 𝓜ᶜ.≈ᴹ-refl

  -- Synonym for composition that reverses the order
  infixr -1 _·_
  _·_ = flip _∘_
```

## Scaling

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {C𝓡 : IsCommutativeSemiring _≈ᴿ_ _+ᴿ_ _*ᴿ_ 0# 1#} where

  _*ₗ_ : R → 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᴮ
  r *ₗ T = diag {C𝓡 = C𝓡} r ∘ T
```

## Direct Sum

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  {𝓜ᴰ : LeftSemimodule 𝓡 d ℓd}
  where

  infixr 6 _⊕_
  _⊕_ : 𝓜ᴬ ⊸ 𝓜ᶜ → 𝓜ᴮ ⊸ 𝓜ᴰ → (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸ (𝓜ᶜ ⊕ₗ 𝓜ᴰ)
  (mk pref (mk +₁ *₁)) ⊕ (mk preg (mk +₂ *₂)) =
    mk (pref P.⊕ preg) (mk ( +₁ , +₂) (*₁ , *₂))

  ⟦⊕⟧ : ∀ {x y} → ⟦ x ⊕ y ⟧ ≈ ⟦ x ⟧ P.⊕ ⟦ y ⟧
  ⟦⊕⟧ = LeftSemimodule.≈ᴹ-refl (𝓜ᶜ ⊕ₗ 𝓜ᴰ)
```

## Copy

```agda
module _ {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}  where

  open LeftSemimodule 𝓜ᴬ
  copy : 𝓜ᴬ ⊸ (𝓜ᴬ ⊕ₗ 𝓜ᴬ)
  copy = mk P.copy (mk (≈ᴹ-refl , ≈ᴹ-refl) (≈ᴹ-refl , ≈ᴹ-refl))

  ⟦copy⟧ : ⟦ copy ⟧ ≈ P.copy
  ⟦copy⟧ = ≈ᴹ-refl , ≈ᴹ-refl
```

## Reduce ("jam")

```agda
  reduce : (𝓜ᴬ ⊕ₗ 𝓜ᴬ) ⊸ 𝓜ᴬ
  reduce =
      mk
        P.reduce
        (mk
          (interchange _ _ _ _)
          (*ₗ-distribˡ _ _ _))
       where open ≈-Reasoning ≈ᴹ-setoid
             open CS (CommutativeMonoid.commutativeSemigroup +ᴹ-commutativeMonoid)


  ⟦reduce⟧ : ⟦ reduce ⟧ ≈ P.reduce
  ⟦reduce⟧ = ≈ᴹ-refl
```

## Fork (split)

Horizontal stacking:

```md
  A | B
```

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  where

  open LeftSemimodule 𝓜ᴮ renaming (≈ᴹ-refl to reflᴮ)
  open LeftSemimodule 𝓜ᶜ renaming (≈ᴹ-refl to reflᶜ)

  infixr 6 _▵_ _∣_
  _▵_ : 𝓜ᴬ ⊸ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᶜ → 𝓜ᴬ ⊸ (𝓜ᴮ ⊕ₗ 𝓜ᶜ)
  f ▵ g = (f ⊕ g) ∘ copy

  _∣_ = _▵_

  ⟦▵⟧ : ∀ {x y} → ⟦ x ▵ y ⟧ ≈ ⟦ x ⟧ P.▵ ⟦ y ⟧
  ⟦▵⟧ = reflᴮ , reflᶜ
```

## Join (junc)

Vertical stacking:

```md
  A
 ---
  B
```

```agda
  infixr 6 _▿_ _—_
  _▿_ : 𝓜ᴬ ⊸ 𝓜ᶜ → 𝓜ᴮ ⊸ 𝓜ᶜ → (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸ 𝓜ᶜ
  f ▿ g = reduce ∘ (f ⊕ g)

  _—_ = _▿_

  ⟦▿⟧ : ∀ {x y} → ⟦ x ▿ y ⟧ ≈ ⟦ x ⟧ P.▿ ⟦ y ⟧
  ⟦▿⟧ = reflᶜ
```


## Projection/Injection

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  open LeftSemimodule 𝓜ᴬ renaming (≈ᴹ-refl to reflᴬ)
  open LeftSemimodule 𝓜ᴮ renaming (≈ᴹ-refl to reflᴮ)

  proj₁ : (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸ 𝓜ᴬ
  proj₁ = id ▿ zero

  ⟦proj₁⟧ : ⟦ proj₁ ⟧ ≈ P.proj₁
  ⟦proj₁⟧ = reflᴬ

  proj₂ : (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸ 𝓜ᴮ
  proj₂ = zero ▿ id

  ⟦proj₂⟧ : ⟦ proj₂ ⟧ ≈ P.proj₂
  ⟦proj₂⟧ = reflᴮ

  inj₁ : 𝓜ᴬ ⊸ (𝓜ᴬ ⊕ₗ 𝓜ᴮ)
  inj₁ = id ▵ zero

  ⟦inj₁⟧ : ⟦ inj₁ ⟧ ≈ P.inj₁
  ⟦inj₁⟧ = reflᴬ , reflᴮ

  inj₂ : 𝓜ᴮ ⊸ (𝓜ᴬ ⊕ₗ 𝓜ᴮ)
  inj₂ = zero ▵ id

  ⟦inj₂⟧ : ⟦ inj₂ ⟧ ≈ P.inj₂
  ⟦inj₂⟧ = reflᴬ , reflᴮ
```

## Swap

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  where

  private
    open module 𝓜ᴬ = LeftSemimodule 𝓜ᴬ renaming (≈ᴹ-refl to reflᴬ)
    open module 𝓜ᴮ = LeftSemimodule 𝓜ᴮ renaming (≈ᴹ-refl to reflᴮ)

  swap : (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸ (𝓜ᴮ ⊕ₗ 𝓜ᴬ)
  swap = inj₂ ▿ inj₁

  ⟦swap⟧ : ⟦ swap ⟧ ≈ P.swap
  ⟦swap⟧ = 𝓜ᴮ.+ᴹ-identityˡ _ , 𝓜ᴬ.+ᴹ-identityʳ _
```

## Associators

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  where

  open LeftSemimodule 𝓜ᴬ renaming (≈ᴹ-refl to reflᴬ)
  open LeftSemimodule 𝓜ᴮ renaming (≈ᴹ-refl to reflᴮ)
  open LeftSemimodule 𝓜ᶜ renaming (≈ᴹ-refl to reflᶜ)

  assocʳ : ((𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊕ₗ 𝓜ᶜ) ⊸ (𝓜ᴬ ⊕ₗ (𝓜ᴮ ⊕ₗ 𝓜ᶜ))
  assocʳ = mk P.assocʳ (mk (reflᴬ , reflᴮ , reflᶜ) (reflᴬ , reflᴮ , reflᶜ))

  assocˡ : (𝓜ᴬ ⊕ₗ (𝓜ᴮ ⊕ₗ 𝓜ᶜ)) ⊸ ((𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊕ₗ 𝓜ᶜ)
  assocˡ = mk P.assocˡ (mk ((reflᴬ , reflᴮ), reflᶜ) ((reflᴬ , reflᴮ), reflᶜ))
```

## Etc

```agda
module _
  {𝓜ᴬ : LeftSemimodule 𝓡 a ℓa}
  {𝓜ᴮ : LeftSemimodule 𝓡 b ℓb}
  {𝓜ᶜ : LeftSemimodule 𝓡 c ℓc}
  where

  first : 𝓜ᴬ ⊸ 𝓜ᴮ → (𝓜ᴬ ⊕ₗ 𝓜ᶜ) ⊸ (𝓜ᴮ ⊕ₗ 𝓜ᶜ)
  first T = T ⊕ id

  second : 𝓜ᴬ ⊸ 𝓜ᴮ → (𝓜ᶜ ⊕ₗ 𝓜ᴬ) ⊸ (𝓜ᶜ ⊕ₗ 𝓜ᴮ)
  second T = id ⊕ T
```
