---
title: Instances of Postive Linear Functionals
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Partial.Bundles using (PoCommutativeSemiring)

module Maps.PositiveLinear.Instances.Functional
  {r ℓr₁ ℓr₂}
  (𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂)
  where

private
  open module 𝓡 = PoCommutativeSemiring 𝓡

-- stdlib
open import Algebra.Module.Ordered.Partial.Construct.Vector
  renaming (mkPoLeftSemimodule to [_]; mkFinPoLeftSemimodule to vector)
open import Data.Product using (_,_)
open import Function using (Inverse)
open import Relation.Binary using (Setoid)
import Relation.Binary.Reasoning.PartialOrder as <-Reasoning

-- carya
open import Maps.PositiveLinear.Construct.Functional 𝓡
open import Maps.Linear.Instances.Functional 𝓡.commutativeSemiring as L using ()
import Fold.Setoid.Properties.PoSemiring as Foldₛ
import Fold.Properties.PoSemiring as Fold

-- zizia
open import Finite.Core using (StrictlyFinite)
```

</details>


```agda
∑VectorForm : ∀ n → PositiveLinearFunctional (vector n)
∑VectorForm n =
  mk
   (L.∑VectorForm n)
   λ {f} x →
     begin
       𝓡.0#
      ≈˘⟨ ∑-preserves-0 {n = n} ⟩
        (∑_ {n = n} (λ _ → 𝓡.0#))
      ≤⟨ ∑-preserves-≤ {n} x ⟩
        ∑ f
      ∎
   where open Fold ⦃ 𝓡.poSemiring ⦄
         open <-Reasoning 𝓡.poset
```

```agda
∑FiniteForm : ∀ {a ℓa} {𝓐 : Setoid a ℓa}
  → (StrictlyFinite 𝓐)
  → PositiveLinearFunctional [ Setoid.Carrier 𝓐 ]
∑FiniteForm {𝓐 = 𝓐} FinA@(n , A↔n) =
  mk
   (L.∑FiniteForm FinA)
   λ {f} x →
     begin
       𝓡.0#
      ≈˘⟨ ∑-preserves-0 {n} (Inverse.to A↔n) ⟩
        ∑[ Inverse.to A↔n ] (λ _ → 𝓡.0#)
      ≤⟨ ∑-preserves-≤ {n} (λ {a} → x {a}) ⟩
        ∑[ Inverse.to A↔n ] f
      ∎
   where open Foldₛ 𝓐 ⦃ 𝓡.poSemiring ⦄
         open <-Reasoning 𝓡.poset
```
