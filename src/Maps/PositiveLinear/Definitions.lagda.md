---
title: Definitions for Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Partial.Bundles using (PoSemiring)

module Maps.PositiveLinear.Definitions {r ℓr₁ ℓr₂} (𝓡 : PoSemiring r ℓr₁ ℓr₂) where

open PoSemiring 𝓡
open import Level using (Level; _⊔_; suc)
open import Algebra.Module.Ordered.Partial.Bundles using (PoLeftSemimodule)
open import Maps.Linear.Core semiring

private
   variable
      a b ℓa₁ ℓb₁ ℓa₂ ℓb₂ : Level
```

## Positive

```agda
module _
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓa₁ ℓa₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓb₁ ℓb₂}
  where

  open PoLeftSemimodule 𝓜ᴬ
    renaming (Carrierᴹ to A; leftSemimodule to lA ; 0ᴹ to 0ᴬ; _≤ᴹ_ to _≤ᴬ_)
  open PoLeftSemimodule 𝓜ᴮ
    renaming (Carrierᴹ to B; leftSemimodule to lB; 0ᴹ to 0ᴮ  ; _≤ᴹ_ to _≤ᴮ_)

  Positive : lA ⊸ lB → Set (a ⊔ ℓa₂ ⊔ ℓb₂)
  Positive f = ∀ {a : A} → 0ᴬ ≤ᴬ a → 0ᴮ ≤ᴮ (f ⟨$⟩ a)
```

