---
title: Positive Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_; 0ℓ)
open import Algebra.Ordered.Partial.Bundles using (PoSemiring)

module Maps.PositiveLinear.Base
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : PoSemiring r ℓr₁ ℓr₂)
  where

open PoSemiring 𝓡
  renaming
  ( refl to reflᴿ
  ; Carrier to R
  ; _≈_ to _≈ᴿ_
  ; _≤_ to _≤ᴿ_
  ; _+_ to _+ᴿ_
  ; _*_ to _*ᴿ_
  )
  using
  (0# ; 1# ; semiring)

open import Algebra.Structures using (IsCommutativeSemiring)
open import Algebra.Module.Ordered.Partial.Bundles
open import Algebra.Module.Ordered.Partial.Construct.DirectProduct
  renaming (mkPoLeftSemimodule to _⊕ₗ_) using ()
open import Algebra.Module.Ordered.Partial.Construct.Zero
  renaming (mkPoLeftSemimodule to 𝟘) using ()
open import Data.Product as × using (_,_)
open import Data.Unit.Polymorphic using (tt)
open import Function
  renaming (id to idᶠ; _∘_ to _∘ᶠ_)
  using ()

open import Maps.Linear.Base semiring as L using ()
open import Maps.PositiveLinear.Core 𝓡

```

</details>

## Special Maps

### Zero

```agda
module _
  {a b ℓᵃ₁ ℓᵃ₂ ℓᵇ₁ ℓᵇ₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓᵃ₁ ℓᵃ₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓᵇ₁ ℓᵇ₂}
  where

  open PoLeftSemimodule 𝓜ᴮ

  zero : 𝓜ᴬ ⊸⁺ 𝓜ᴮ
  zero = mk L.zero λ _ → refl
```

### Diagonal

```agda
module _
  {a ℓᵃ₁ ℓᵃ₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓᵃ₁ ℓᵃ₂}
  {C𝓡 : IsCommutativeSemiring _≈ᴿ_ _+ᴿ_ _*ᴿ_ 0# 1#}
  where

  open PoLeftSemimodule 𝓜ᴬ

  diag : (r : R) → ⦃ 0# ≤ᴿ r ⦄ → 𝓜ᴬ ⊸⁺ 𝓜ᴬ
  diag r ⦃ 0#≤r ⦄ =
      mk
        (L.diag {C𝓡 = C𝓡} r)
        λ 0≤a → trans (reflexive (≈ᴹ-sym (*ₗ-zeroʳ r))) (*ₗ-compatₗ 0#≤r 0≤a)
```

### Identity

```agda
module _
  {a ℓᵃ₁ ℓᵃ₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓᵃ₁ ℓᵃ₂}
  where

  id : 𝓜ᴬ ⊸⁺ 𝓜ᴬ
  id = mk L.id idᶠ
```

## Addition

```agda
module _
  {a b ℓᵃ₁ ℓᵃ₂ ℓᵇ₁ ℓᵇ₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓᵃ₁ ℓᵃ₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓᵇ₁ ℓᵇ₂}
  where

  open PoLeftSemimodule 𝓜ᴮ

  _+_ : 𝓜ᴬ ⊸⁺ 𝓜ᴮ → 𝓜ᴬ ⊸⁺ 𝓜ᴮ → 𝓜ᴬ ⊸⁺ 𝓜ᴮ
  (mk f fpos) + (mk g gpos) =
    mk
      (f L.+ g)
      λ 0≤a →
        trans
          (reflexive (≈ᴹ-sym (+ᴹ-identityʳ 0ᴹ)))
          (+ᴹ-mono₂ (fpos 0≤a) (gpos 0≤a))
```

## Composition

```agda
module _
  {a b c ℓᵃ₁ ℓᵃ₂ ℓᵇ₁ ℓᵇ₂ ℓᶜ₁ ℓᶜ₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓᵃ₁ ℓᵃ₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓᵇ₁ ℓᵇ₂}
  {𝓜ᶜ : PoLeftSemimodule 𝓡 c ℓᶜ₁ ℓᶜ₂}
  where

  infixr 9 _∘_
  _∘_ : 𝓜ᴮ ⊸⁺ 𝓜ᶜ → 𝓜ᴬ ⊸⁺ 𝓜ᴮ → 𝓜ᴬ ⊸⁺ 𝓜ᶜ
  (mk f fpos) ∘ (mk g gpos) =
    mk
      (f L.∘ g)
      (fpos ∘ᶠ gpos)
```

## Scaling

```agda
module _
  {a b ℓᵃ₁ ℓᵃ₂ ℓᵇ₁ ℓᵇ₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓᵃ₁ ℓᵃ₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓᵇ₁ ℓᵇ₂}
  {C𝓡 : IsCommutativeSemiring _≈ᴿ_ _+ᴿ_ _*ᴿ_ 0# 1#}
  where

  _*ₗ_ : (r : R) → ⦃ 0# ≤ᴿ r ⦄ → 𝓜ᴬ ⊸⁺ 𝓜ᴮ → 𝓜ᴬ ⊸⁺ 𝓜ᴮ
  r *ₗ T =  diag {C𝓡 = C𝓡} r ∘ T
```

### Point

```agda
module _
  {a ℓa₁ ℓa₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓa₁ ℓa₂}
  where

  ! : 𝓜ᴬ ⊸⁺ 𝟘
  ! = mk L.! λ x → tt
```

### Co-Point (TODO: what is name of this?)

```agda
  ¡ : (𝟘 {a} {ℓa₁}) ⊸⁺ 𝓜ᴬ
  ¡ = mk L.¡ λ x → refl
    where open PoLeftSemimodule 𝓜ᴬ
```

## Direct Sum

```agda
module _
  {a b c d ℓᵃ₁ ℓᵃ₂ ℓᵇ₁ ℓᵇ₂ ℓᶜ₁ ℓᶜ₂ ℓᵈ₁ ℓᵈ₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓᵃ₁ ℓᵃ₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓᵇ₁ ℓᵇ₂}
  {𝓜ᶜ : PoLeftSemimodule 𝓡 c ℓᶜ₁ ℓᶜ₂}
  {𝓜ᴰ : PoLeftSemimodule 𝓡 d ℓᵈ₁ ℓᵈ₂}
  where

  infixr 6 _⊕_
  _⊕_ : 𝓜ᴬ ⊸⁺ 𝓜ᶜ → 𝓜ᴮ ⊸⁺ 𝓜ᴰ → (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸⁺ (𝓜ᶜ ⊕ₗ 𝓜ᴰ)
  (mk f fpos) ⊕ (mk g gpos) =
      mk
        (f L.⊕ g)
        (×.map fpos gpos)
```

## Copy

```agda
module _
  {a ℓᵃ₁ ℓᵃ₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓᵃ₁ ℓᵃ₂}
  where

  copy : 𝓜ᴬ ⊸⁺ (𝓜ᴬ ⊕ₗ 𝓜ᴬ)
  copy = mk L.copy λ x → x , x
```

## Reduce ("jam")

```agda
  reduce : (𝓜ᴬ ⊕ₗ 𝓜ᴬ) ⊸⁺ 𝓜ᴬ
  reduce = mk L.reduce
    λ (0≤a₁ , 0≤a₂) →
      trans (reflexive (≈ᴹ-sym (+ᴹ-identityˡ _))) (+ᴹ-mono₂ 0≤a₁ 0≤a₂)
    where open PoLeftSemimodule 𝓜ᴬ
```

## Fork (split)

Horizontal stacking:

```md
  A | B
```

```agda
module _
  {a b c ℓᵃ₁ ℓᵃ₂ ℓᵇ₁ ℓᵇ₂ ℓᶜ₁ ℓᶜ₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓᵃ₁ ℓᵃ₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓᵇ₁ ℓᵇ₂}
  {𝓜ᶜ : PoLeftSemimodule 𝓡 c ℓᶜ₁ ℓᶜ₂}
  where

  infixr 6 _▵_ _∣_
  _▵_ : 𝓜ᴬ ⊸⁺ 𝓜ᴮ → 𝓜ᴬ ⊸⁺ 𝓜ᶜ → 𝓜ᴬ ⊸⁺ (𝓜ᴮ ⊕ₗ 𝓜ᶜ)
  f ▵ g = (f ⊕ g) ∘ copy

  _∣_ = _▵_
```

## Join (junc)

Vertical stacking:

```md
  A
 ---
  B
```

```agda
  infixr 6 _▿_ _—_
  _▿_ : 𝓜ᴬ ⊸⁺ 𝓜ᶜ → 𝓜ᴮ ⊸⁺ 𝓜ᶜ → (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸⁺ 𝓜ᶜ
  f ▿ g = reduce ∘ (f ⊕ g)

  _—_ = _▿_
```

## Projection/Injection

```agda
module _
  {a b ℓᵃ₁ ℓᵃ₂ ℓᵇ₁ ℓᵇ₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓᵃ₁ ℓᵃ₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓᵇ₁ ℓᵇ₂}
  where

  proj₁ : (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸⁺ 𝓜ᴬ
  proj₁ = id ▿ zero

  proj₂ : (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸⁺ 𝓜ᴮ
  proj₂ = zero ▿ id

  inj₁ : 𝓜ᴬ ⊸⁺ (𝓜ᴬ ⊕ₗ 𝓜ᴮ)
  inj₁ = id ▵ zero

  inj₂ : 𝓜ᴮ ⊸⁺ (𝓜ᴬ ⊕ₗ 𝓜ᴮ)
  inj₂ = zero ▵ id
```

## Swap

```agda
module _
  {a b ℓᵃ₁ ℓᵃ₂ ℓᵇ₁ ℓᵇ₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓᵃ₁ ℓᵃ₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓᵇ₁ ℓᵇ₂}
  where

  private
    module 𝓜ᴬ = PoLeftSemimodule 𝓜ᴬ
    module 𝓜ᴮ = PoLeftSemimodule 𝓜ᴮ

  swap : (𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊸⁺ (𝓜ᴮ ⊕ₗ 𝓜ᴬ)
  swap = mk L.swap
    (×.map
      (𝓜ᴮ.≲-respʳ-≈ (𝓜ᴮ.≈ᴹ-sym (𝓜ᴮ.+ᴹ-identityˡ _)))
      (𝓜ᴬ.≲-respʳ-≈ (𝓜ᴬ.≈ᴹ-sym (𝓜ᴬ.+ᴹ-identityʳ _ ))) ∘ᶠ ×.swap)
```

## Associators

```agda
module _
  {a b c ℓᵃ₁ ℓᵃ₂ ℓᵇ₁ ℓᵇ₂ ℓᶜ₁ ℓᶜ₂}
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓᵃ₁ ℓᵃ₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓᵇ₁ ℓᵇ₂}
  {𝓜ᶜ : PoLeftSemimodule 𝓡 c ℓᶜ₁ ℓᶜ₂}
  where

  assocʳ : ((𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊕ₗ 𝓜ᶜ) ⊸⁺ (𝓜ᴬ ⊕ₗ (𝓜ᴮ ⊕ₗ 𝓜ᶜ))
  assocʳ = mk L.assocʳ λ ((a , b) , c) → a , b , c

  assocˡ : (𝓜ᴬ ⊕ₗ (𝓜ᴮ ⊕ₗ 𝓜ᶜ)) ⊸⁺ ((𝓜ᴬ ⊕ₗ 𝓜ᴮ) ⊕ₗ 𝓜ᶜ)
  assocˡ = mk L.assocˡ λ (a , b , c) → ((a , b) , c)
```
