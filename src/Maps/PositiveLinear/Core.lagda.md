${---
title: Positive Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Ordered.Partial.Bundles using (PoSemiring)

module Maps.PositiveLinear.Core
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : PoSemiring r ℓr₁ ℓr₂)
  where

open PoSemiring 𝓡
  renaming (Carrier to R)
  using (semiring)

open import Algebra.Module.Ordered.Partial.Bundles using (PoLeftSemimodule)
open import Function using (_on_)
open import Function.Definitions using (Congruent)
open import Maps.Linear.Core semiring as L using (_⊸_)
open import Maps.PositiveLinear.Definitions 𝓡
open import Relation.Binary using (IsEquivalence; IsPartialOrder; Poset)

private
  variable
    a b ℓa₁ ℓa₂ ℓb₁ ℓb₂ : Level
```

</details>

```agda
record _⊸⁺_
  (𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓa₁ ℓa₂)
  (𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓb₁ ℓb₂)
  : Set (a ⊔ b ⊔ r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ ℓa₁ ⊔ ℓa₂ ⊔ ℓb₁ ⊔ ℓb₂)
    where
  constructor mk

  open PoLeftSemimodule 𝓜ᴬ
    renaming
      ( leftSemimodule to 𝓜ᴬ*
      ; isPartialOrder to isPartialOrderᴬ
      )

  open PoLeftSemimodule 𝓜ᴮ
    renaming
      ( leftSemimodule to 𝓜ᴮ*
      ; isPartialOrder to isPartialOrderᴮ
      )

  field
    f : 𝓜ᴬ* ⊸ 𝓜ᴮ*
    isPositive : Positive {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ} f

  open _⊸_ f public
```

```agda
module _
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓa₁ ℓa₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓb₁ ℓb₂}
  where

  private
    open module 𝓜ᴬ = PoLeftSemimodule 𝓜ᴬ
      renaming
      ( _+ᴹ_ to _+ᴬ_
      ; _*ₗ_ to _*ᴬ_
      )
      using ()
    open module 𝓜ᴮ = PoLeftSemimodule 𝓜ᴮ
      renaming
      ( _+ᴹ_ to _+ᴮ_
      ; _*ₗ_ to _*ᴮ_
      ; _≈ᴹ_ to _≈ᴮ_
      )

  mk⊸⁺ : (f : 𝓜ᴬ.Carrierᴹ → 𝓜ᴮ.Carrierᴹ)
      → (cong : Congruent 𝓜ᴬ._≈ᴹ_ 𝓜ᴮ._≈ᴹ_ f)
      → (+h : ∀ {a₁ a₂} → f (a₁ +ᴬ a₂) ≈ᴮ f a₁ +ᴮ f a₂)
      → (*h : ∀ {r : R} {a} → r *ᴮ f a ≈ᴮ f (r *ᴬ a))
      → Positive {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ = 𝓜ᴮ} (L.mk⊸ f cong +h *h)
      → 𝓜ᴬ ⊸⁺ 𝓜ᴮ
  mk⊸⁺ to cong +-homo *ₗ-homo pos = mk (L.mk⊸ to cong +-homo *ₗ-homo) pos
```

## Equivalence

```agda
module _
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓa₁ ℓa₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓb₁ ℓb₂}
  where

  private
    module 𝓜ᴬ = PoLeftSemimodule 𝓜ᴬ
    open module 𝓜ᴮ = PoLeftSemimodule 𝓜ᴮ
      hiding (isEquivalence ; isPartialOrder)

  ⟦_⟧ : 𝓜ᴬ ⊸⁺ 𝓜ᴮ → (𝓜ᴬ.leftSemimodule ⊸ 𝓜ᴮ.leftSemimodule)
  ⟦ mk f _ ⟧ = f

  infix 4 _≈_ _≤_
  _≈_ : 𝓜ᴬ ⊸⁺ 𝓜ᴮ → 𝓜ᴬ ⊸⁺ 𝓜ᴮ → Set _
  _≈_ = L._≈_ on ⟦_⟧

  _≤_ : 𝓜ᴬ ⊸⁺ 𝓜ᴮ → 𝓜ᴬ ⊸⁺ 𝓜ᴮ → Set _
  (mk f _) ≤ (mk g _) = ∀ {a} → (f L.⟨$⟩ a) 𝓜ᴮ.≤ᴹ (g L.⟨$⟩ a)

  isEquivalence : IsEquivalence _≈_
  isEquivalence = record
    { refl = 𝓜ᴮ.≈ᴹ-refl
    ; sym =  λ x → 𝓜ᴮ.≈ᴹ-sym x
    ; trans = λ x y → 𝓜ᴮ.≈ᴹ-trans x y
    }

  isPartialOrder : IsPartialOrder _≈_ _≤_
  isPartialOrder = record
    { isPreorder = record
      { isEquivalence = isEquivalence
      ; reflexive = λ x → 𝓜ᴮ.reflexive x
      ; trans = λ x y → 𝓜ᴮ.trans x y
      }
    ; antisym = λ x y → 𝓜ᴮ.antisym x y
    }

  poset : Poset (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ a ⊔ ℓa₁ ⊔ ℓa₂ ⊔ b ⊔ ℓb₁ ⊔ ℓb₂) (a ⊔ ℓb₁) (a ⊔ ℓb₂)
  poset = record { isPartialOrder = isPartialOrder }
```

## Application

```agda
module _
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓa₁ ℓa₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓb₁ ℓb₂}
  where

  open PoLeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A) using ()
  open PoLeftSemimodule 𝓜ᴮ renaming (Carrierᴹ to B) using ()

  infixr -1 _⟨$⟩_
  _⟨$⟩_ : 𝓜ᴬ ⊸⁺ 𝓜ᴮ → A → B
  f ⟨$⟩ a = ⟦ f ⟧ L.⟨$⟩ a
```
