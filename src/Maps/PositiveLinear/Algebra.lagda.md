---
title: Algebra Constructs on Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Ordered.Partial.Bundles

module Maps.PositiveLinear.Algebra
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : PoSemiring r ℓr₁ ℓr₂)
  where

import Algebra.Ordered.Partial.Structures as Struct
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Module.Ordered.Partial.Bundles
  using (PoLeftSemimodule)
open import Maps.PositiveLinear.Core 𝓡
open import Maps.PositiveLinear.Base 𝓡
open import Maps.PositiveLinear.Properties 𝓡
open import Data.Product as × using (_,_)

private
  variable
    a b ℓa₁ ℓa₂ ℓb₁ ℓb₂ : Level
```

</details>

## Algebraic Structures/Bundles with `_+_`

```agda
module _
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓa₁ ℓa₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓb₁ ℓb₂}
  where

  private
    module 𝓜ᴮ = PoLeftSemimodule 𝓜ᴮ

  open Struct (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ}) (_≤_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ})

  +-isPoMagma : IsPoMagma _+_
  +-isPoMagma = record
    { isPartialOrder = record
      { isPreorder = record
        { isEquivalence = isEquivalence
        ; reflexive = λ x → 𝓜ᴮ.reflexive x
        ; trans = λ x y → 𝓜ᴮ.trans x y
        }
      ; antisym = λ x y → 𝓜ᴮ.antisym x y
      }
    ; ∙-cong = λ {x} {y} {u} {v} → +-cong {x = x} {y} {u} {v}
    ; ∙-compat = λ f {a} → 𝓜ᴮ.+ᴹ-mono₂ (f {a}) 𝓜ᴮ.refl
    }

  +-isPoSemigroup : IsPoSemigroup _+_
  +-isPoSemigroup = record
    { isPoMagma = +-isPoMagma
    ; assoc = +-assoc
    }

  +-0-isPoMonoid : IsPoMonoid _+_ zero
  +-0-isPoMonoid  = record
    { isPoSemigroup = +-isPoSemigroup
    ; identity = +-identity
    }

  +-0-isPoCommutativeMonoid : IsPoCommutativeMonoid _+_ zero
  +-0-isPoCommutativeMonoid  = record
    { isPoMonoid = +-0-isPoMonoid
    ; comm = +-comm
    }

  +-poMagma : PoMagma (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ a ⊔ b ⊔ ℓa₁ ⊔ ℓa₂ ⊔ ℓb₁ ⊔ ℓb₂) (a ⊔ ℓb₁) (a ⊔ ℓb₂)
  +-poMagma = record { isPoMagma = +-isPoMagma }

  +-poSemigroup : PoSemigroup (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ a ⊔ b ⊔ ℓa₁ ⊔ ℓa₂ ⊔ ℓb₁ ⊔ ℓb₂) (a ⊔ ℓb₁) (a ⊔ ℓb₂)
  +-poSemigroup = record { isPoSemigroup = +-isPoSemigroup }

  +-0-poMonoid : PoMonoid (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ a ⊔ b ⊔ ℓa₁ ⊔ ℓa₂ ⊔ ℓb₁ ⊔ ℓb₂) (a ⊔ ℓb₁) (a ⊔ ℓb₂)
  +-0-poMonoid = record { isPoMonoid = +-0-isPoMonoid }

  +-0-poCommutativeMonoid : PoCommutativeMonoid (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ a ⊔ b ⊔ ℓa₁ ⊔ ℓa₂ ⊔ ℓb₁ ⊔ ℓb₂) (a ⊔ ℓb₁) (a ⊔ ℓb₂)
  +-0-poCommutativeMonoid =
    record { isPoCommutativeMonoid = +-0-isPoCommutativeMonoid }
```
