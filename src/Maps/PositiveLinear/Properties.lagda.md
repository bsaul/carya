---
title: Properties of Positive Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Ordered.Partial.Bundles using (PoSemiring)

module Maps.PositiveLinear.Properties
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : PoSemiring r ℓr₁ ℓr₂)
  where

open PoSemiring 𝓡 using (semiring)

import Algebra.Definitions as Defs
open import Algebra.Module.Ordered.Partial.Bundles using (PoLeftSemimodule)
open import Data.Product as × using (_,_)
open import Function renaming (_∘_ to _∘ᶠ_) using ()
open import Maps.PositiveLinear.Core 𝓡
open import Maps.PositiveLinear.Base 𝓡
open import Maps.Linear.Properties semiring as P using ()

private
  variable
    a b ℓa₁ ℓa₂ ℓb₁ ℓb₂ : Level
```

</details>

## Properties of `_+_`

```agda
module _
  {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓa₁ ℓa₂}
  {𝓜ᴮ : PoLeftSemimodule 𝓡 b ℓb₁ ℓb₂}
  where

  open Defs (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ})

  +-cong : Congruent₂ _+_
  +-cong {x} {y} {u} {v} = P.+-cong {x = ⟦ x ⟧} {⟦ y ⟧} {⟦ u ⟧} {⟦ v ⟧}

  +-assoc : Associative _+_
  +-assoc f g h = P.+-assoc ⟦ f ⟧ ⟦ g ⟧ ⟦ h ⟧

  +-comm : Commutative _+_
  +-comm f g = P.+-comm ⟦ f ⟧ ⟦ g ⟧

  -- @macedo2013 eq 7
  +-identityˡ : LeftIdentity zero _+_
  +-identityˡ = P.+-identityˡ ∘ᶠ ⟦_⟧

  +-identityʳ : RightIdentity zero _+_
  +-identityʳ = P.+-identityʳ ∘ᶠ ⟦_⟧

  +-identity : Identity zero _+_
  +-identity = +-identityˡ , +-identityʳ
```

## Properties of `_∘_`

```agda
module _ {𝓜ᴬ : PoLeftSemimodule 𝓡 a ℓa₁ ℓa₂} where

  open PoLeftSemimodule 𝓜ᴬ
  open Defs (_≈_ {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴬ})

  ∘-cong : Congruent₂  _∘_
  ∘-cong {x} {y} {u} {v} = P.∘-cong {x = ⟦ x ⟧} {⟦ y ⟧} {⟦ u ⟧} {⟦ v ⟧}

  ∘-assoc : Associative  _∘_
  ∘-assoc T U V = P.∘-assoc ⟦ T ⟧ ⟦ U ⟧ ⟦ V ⟧

  -- @macedo2013 eq 4
  ∘-identityˡ : LeftIdentity id _∘_
  ∘-identityˡ = P.∘-identityˡ ∘ᶠ ⟦_⟧

  ∘-identityʳ : RightIdentity id _∘_
  ∘-identityʳ = P.∘-identityʳ ∘ᶠ ⟦_⟧

  ∘-identity : Identity id _∘_
  ∘-identity = ∘-identityˡ , ∘-identityˡ

  -- @macedo2013 eq 8
  ∘-zeroˡ : LeftZero zero _∘_
  ∘-zeroˡ f {x} = P.∘-zeroˡ ⟦ f ⟧ {x}

  -- @macedo2013 eq 8
  ∘-zeroʳ : RightZero zero _∘_
  ∘-zeroʳ =  P.∘-zeroʳ ∘ᶠ ⟦_⟧

  ∘-zero : Zero zero _∘_
  ∘-zero = ∘-zeroˡ , ∘-zeroʳ
```

## Disributivity of Composition over Addition

```agda
  -- @macedo2013 eq 9
  -- NOTE: linearity makes this property possible
  distribˡ :  _∘_ DistributesOverˡ _+_
  distribˡ f g h = P.distribˡ ⟦ f ⟧ ⟦ g ⟧ ⟦ h ⟧

  -- @macedo2013 eq 10
  distribʳ :  _∘_ DistributesOverʳ _+_
  distribʳ f g h = P.distribʳ ⟦ f ⟧ ⟦ g ⟧ ⟦ h ⟧

  distrib :  _∘_ DistributesOver _+_
  distrib = distribˡ , distribʳ
```
