---
title: Finite Dimensional Positive Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Ordered.Partial.Bundles using (PoCommutativeSemiring)

module Maps.PositiveLinear.Construct.FiniteDimension
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂)
  where

open PoCommutativeSemiring 𝓡
  renaming (Carrier to R)
  using (poSemiring)

-- stdlib
open import Data.Nat as ℕ using (ℕ)
open import Data.Fin using (Fin)

-- carya
open import Maps.PositiveLinear.Related poSemiring
open import Maps.PositiveLinear.Construct.Pointwise 𝓡 as L using ()
open import Maps.PositiveLinear.Construct.Pointwise 𝓡
  using (
      ⟦_⟧
    ; _≈_
    ; isPartialOrder
    ; isEquivalence
    ; poset
    ; mk
    ; mk⊸⁺
    ; _⟨$⟩_
  ) public

private
  variable
    m n o p : ℕ
```

</details>

```agda
_⊸⁺_ : ℕ → ℕ → Set (r ⊔ ℓr₁ ⊔ ℓr₂)
m ⊸⁺ n = (Fin m) L.⊸⁺ (Fin n)
```

```agda
open import Maps.PositiveLinear.Construct.Pointwise 𝓡
  using (
     diag
   ; _*ₗ_
   ; zero
   ; _+_
   ; id
   ; _∘_
  ) public


! : m ⊸⁺ 1
! = ⊤⊸⁺1 L.∘ L.!

¡ : 1 ⊸⁺ m
¡ = L.¡ L.∘ 1⊸⁺⊤

infixr 6 _⊕_
_⊕_ : m ⊸⁺ o → n ⊸⁺ p → (m ℕ.+ n) ⊸⁺ (o ℕ.+ p)
f ⊕ g = m⊎n⊸⁺m+n L.∘ (f L.⊕ g) L.∘ m+n⊸⁺m⊎n

reduce : (m ℕ.+ m) ⊸⁺ m
reduce = L.reduce L.∘ m+n⊸⁺m⊎n

copy : m ⊸⁺ (m ℕ.+ m)
copy = m⊎n⊸⁺m+n L.∘ L.copy

swap : ∀ {m n} → (m ℕ.+ n) ⊸⁺ (n ℕ.+ m)
swap {m} {n} = (m⊎n⊸⁺m+n {n} {m}) L.∘ L.swap L.∘ m+n⊸⁺m⊎n

infixr 6 _▵_
_▵_ : m ⊸⁺ n → m ⊸⁺ o → m ⊸⁺ (n ℕ.+ o)
f ▵ g = (f ⊕ g) ∘ copy

proj₁ : m ⊸⁺ (m ℕ.+ n)
proj₁ = id ▵ zero

proj₂ : n ⊸⁺ (m ℕ.+ n)
proj₂ = zero ▵ id

infixr 6 _▿_
_▿_ : m ⊸⁺ o → n ⊸⁺ o → (m ℕ.+ n) ⊸⁺ o
f ▿ g = reduce ∘ (f ⊕ g)

inj₁ : (m ℕ.+ n) ⊸⁺ m
inj₁ = id ▿ zero

inj₂ : (m ℕ.+ n) ⊸⁺ n
inj₂ = zero ▿ id

assocˡ : ∀ {m n o} → (m ℕ.+ (n ℕ.+ o)) ⊸⁺ ((m ℕ.+ n) ℕ.+ o)
assocˡ {m} {n} {o} = (⊎assoc+ˡ {m} {n} {o}) L.∘ L.assocˡ L.∘ +assoc⊎ʳ

assocʳ : ∀ {m n o} → ((m ℕ.+ n) ℕ.+ o) ⊸⁺ (m ℕ.+ (n ℕ.+ o))
assocʳ {m} {n} {o} = (⊎assoc+ʳ {m} {n} {o}) L.∘ L.assocʳ L.∘ +assoc⊎ˡ
```
