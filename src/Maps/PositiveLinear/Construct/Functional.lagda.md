---
title: Positive Linear Functionals
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Partial.Bundles using (PoCommutativeSemiring)

module Maps.PositiveLinear.Construct.Functional
  {r ℓr₁ ℓr₂}
  (𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂)
  where

private
 open module 𝓡 = PoCommutativeSemiring 𝓡
   renaming (Carrier to R)
   using ()

-- stdlib
open import Level using (Level; _⊔_)
open import Algebra.Module.Ordered.Partial.Bundles using (PoLeftSemimodule)
open import Algebra.Module.Ordered.Partial.Construct.TensorUnit
 renaming (mkPoLeftSemimodule to 𝓡*)
open import Algebra.Module.Ordered.Partial.Construct.DirectProduct
  renaming (mkPoLeftSemimodule to _⊕_) using ()
open import Algebra.Module.Ordered.Partial.Construct.Zero
  renaming (mkPoLeftSemimodule to 𝟘) using ()

-- carya
open import Maps.PositiveLinear.Core 𝓡.poSemiring
  using (
      ⟦_⟧
    ; _≈_
    ; _⊸⁺_
    ; isEquivalence
    ; poset
    ; mk
    ; mk⊸⁺
    ; _⟨$⟩_
  ) public
open import Maps.PositiveLinear.Base 𝓡.poSemiring as L using ()

private
   variable
      a ℓa₁ ℓa₂ b ℓb₁ ℓb₂ : Level
      𝓜ᴬ : PoLeftSemimodule 𝓡.poSemiring a ℓa₁ ℓa₂
      𝓜ᴮ : PoLeftSemimodule 𝓡.poSemiring b ℓb₁ ℓb₂
```

</details>

```agda
PositiveLinearFunctional : PoLeftSemimodule 𝓡.poSemiring a ℓa₁ ℓa₂
  → Set (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ a ⊔ ℓa₁ ⊔ ℓa₂)
PositiveLinearFunctional = _⊸⁺ 𝓡*
```

```agda
_*ₗ_ : (r : R) → ⦃ 𝓡.0# 𝓡.≤ r ⦄
    → PositiveLinearFunctional 𝓜ᴬ
    → PositiveLinearFunctional 𝓜ᴬ
_*ₗ_ = L._*ₗ_ {C𝓡 = 𝓡.isCommutativeSemiring}

0# : PositiveLinearFunctional 𝓜ᴬ
0# = L.zero

infixl 6 _+_
_+_ : PositiveLinearFunctional 𝓜ᴬ
    → PositiveLinearFunctional 𝓜ᴬ
    → PositiveLinearFunctional 𝓜ᴬ
_+_ = L._+_
```

```agda
¡ : PositiveLinearFunctional 𝟘
¡ = L.¡

infixr 6 _▿_
_▿_ : PositiveLinearFunctional 𝓜ᴬ
    → PositiveLinearFunctional 𝓜ᴮ
    → PositiveLinearFunctional (𝓜ᴬ ⊕ 𝓜ᴮ)
_▿_ = L._▿_
```
