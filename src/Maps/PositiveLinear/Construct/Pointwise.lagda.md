---
title: Indexed Positive Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_; 0ℓ)
open import Algebra.Ordered.Partial.Bundles
  using (PoCommutativeSemiring)

module Maps.PositiveLinear.Construct.Pointwise
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂)
  where

open PoCommutativeSemiring 𝓡
  renaming (Carrier to R)
  using (poSemiring; 0#; _≤_ ; isCommutativeSemiring)

-- stdlib
open import Algebra.Module.Ordered.Partial.Construct.Vector
  renaming (mkPoLeftSemimodule to [_])
open import Data.Sum as ⊎ using (_⊎_)
open import Data.Unit.Polymorphic using (⊤)

-- carya
open import Maps.PositiveLinear.Core poSemiring as Core
  using (
      ⟦_⟧
    ; _≈_
    ; isEquivalence
    ; isPartialOrder
    ; poset
    ; mk
    ; mk⊸⁺
    ; _⟨$⟩_
  ) public
open import Maps.PositiveLinear.Base poSemiring as P using ()
open import Maps.PositiveLinear.Related poSemiring

private
  variable
    a b c d : Level
    A : Set a
    B : Set b
    C : Set c
    D : Set d
```

</details>

```agda
_⊸⁺_ : Set a → Set b → Set (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ a ⊔ b)
A ⊸⁺ B = [ A ] Core.⊸⁺ [ B ]
```

```agda
diag : (r : R) → ⦃ 0# ≤ r ⦄ → A ⊸⁺ A
diag = P.diag {C𝓡 = isCommutativeSemiring}

_*ₗ_ : (r : R) → ⦃ 0# ≤ r ⦄ → A ⊸⁺ B → A ⊸⁺ B
r *ₗ f = P._*ₗ_ {C𝓡 = isCommutativeSemiring} r f

zero : A ⊸⁺ B
zero = P.zero

infixl 6 _+_
_+_ : A ⊸⁺ B → A ⊸⁺ B → A ⊸⁺ B
_+_ = P._+_

id : A ⊸⁺ A
id = P.id

infixr 9 _∘_
_∘_ : B ⊸⁺ C → A ⊸⁺ B → A ⊸⁺  C
_∘_ = P._∘_

! : A ⊸⁺ ⊤
! = 𝟘⊸⁺⊤ P.∘ P.!

¡ : ⊤ ⊸⁺ A
¡ = P.¡ P.∘ ⊤⊸⁺𝟘

infixr 6 _⊕_
_⊕_ :  A ⊸⁺ C → B ⊸⁺ D → (A ⊎ B) ⊸⁺ (C ⊎ D)
f ⊕ g = ⊕⊸⊎ P.∘ (f P.⊕ g) P.∘ ⊎⊸⊕

reduce : (A ⊎ A) ⊸⁺ A
reduce = P.reduce P.∘ ⊎⊸⊕

copy : A ⊸⁺(A ⊎ A)
copy = ⊕⊸⊎ P.∘ P.copy

swap : (A ⊎ B) ⊸⁺ (B ⊎ A)
swap = ⊕⊸⊎ P.∘ P.swap P.∘ ⊎⊸⊕

infixr 6 _▵_
_▵_ : A ⊸⁺ B → A ⊸⁺ C → A ⊸⁺ (B ⊎ C)
f ▵ g = (f ⊕ g) ∘ copy

proj₁ : A ⊸⁺ (A ⊎ B)
proj₁ = id ▵ zero

proj₂ : B ⊸⁺ (A ⊎ B)
proj₂ = zero ▵ id

infixr 6 _▿_
_▿_ : A ⊸⁺ C → B ⊸⁺ C → (A ⊎ B) ⊸⁺ C
f ▿ g = reduce ∘ (f ⊕ g)

inj₁ : (A ⊎ B) ⊸⁺ A
inj₁ = id ▿ zero

inj₂ : (A ⊎ B) ⊸⁺ B
inj₂ = zero ▿ id

assocʳ :  ((A ⊎ B) ⊎ C) ⊸⁺ (A ⊎ (B ⊎ C))
assocʳ = ⊕assoc⊎ʳ P.∘ P.assocʳ P.∘ ⊎assoc⊕ˡ

assocˡ : (A ⊎ (B ⊎ C)) ⊸⁺ ((A ⊎ B) ⊎ C)
assocˡ = ⊕assoc⊎ˡ P.∘ P.assocˡ P.∘ ⊎assoc⊕ʳ
```
