---
title: Relations between Linear Spaces
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_; 0ℓ; lift)
open import Algebra.Ordered.Partial.Bundles using (PoSemiring)

module Maps.PositiveLinear.Related
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : PoSemiring r ℓr₁ ℓr₂) where

open PoSemiring 𝓡
  using (semiring ; refl)

open import Algebra.Module.Ordered.Partial.Construct.Vector
  renaming (mkPoLeftSemimodule to [_])
open import Algebra.Module.Ordered.Partial.Construct.DirectProduct
  renaming (mkPoLeftSemimodule to _⊕ₗ_) using ()
open import Algebra.Module.Ordered.Partial.Construct.Zero
  renaming (mkPoLeftSemimodule to 𝟘) using ()
open import Data.Fin using (Fin)
open import Data.Fin.Properties using (+↔⊎)
open import Data.Nat using (ℕ; _+_)
open import Data.Sum as ⊎ using (_⊎_)
open import Data.Product as × using (_,_)
open import Data.Unit.Polymorphic using (⊤ ; tt)
open import Function as F using (Inverse)

-- carya
open import Maps.PositiveLinear.Core 𝓡
open import Maps.Linear.Related semiring as L using ()

private
  variable
    a b c ℓa ℓb : Level
    A : Set a
    B : Set b
    C : Set c
    m n o : ℕ
```

</details>

## Maps relating `_⊎_` and `_⊕ₗ_`

```agda
𝟘⊸⁺⊤ : (𝟘 {a} {ℓa}) ⊸⁺ [ ⊤ ]
𝟘⊸⁺⊤ = mk L.𝟘⊸⊤ λ _ → refl

⊤⊸⁺𝟘 : [ ⊤ ] ⊸⁺ (𝟘 {b} {ℓb})
⊤⊸⁺𝟘 = mk L.⊤⊸𝟘 λ _ → tt

⊎⊸⊕ : [ A ⊎ B ] ⊸⁺ ([ A ] ⊕ₗ [ B ])
⊎⊸⊕ = mk L.⊎⊸⊕ λ x → (λ {a} → x {⊎.inj₁ a}) , λ {b} → x {⊎.inj₂ b}

⊕⊸⊎ : ([ A ] ⊕ₗ [ B ]) ⊸⁺ [ A ⊎ B ]
⊕⊸⊎ = mk L.⊕⊸⊎ λ (f , g) → λ { {⊎.inj₁ a} → f {a} ; {⊎.inj₂ b} → g {b} }

⊎assoc⊕ˡ : [ (A ⊎ B) ⊎ C ] ⊸⁺ (([ A ] ⊕ₗ [ B ]) ⊕ₗ [ C ])
⊎assoc⊕ˡ = mk L.⊎assoc⊕ˡ
  λ x → ((λ {a} → x {⊎.inj₁ (⊎.inj₁ a)})
        , λ {b} → x {⊎.inj₁ (⊎.inj₂ b)})
        , λ {c} → x {⊎.inj₂ c}

⊎assoc⊕ʳ : [ A ⊎ B ⊎ C ] ⊸⁺ ([ A ] ⊕ₗ ([ B ] ⊕ₗ [ C ]))
⊎assoc⊕ʳ = mk L.⊎assoc⊕ʳ
  λ x →   (λ {a} → x { ⊎.inj₁ a })
        , (λ {b} → x { ⊎.inj₂ (⊎.inj₁ b) })
        ,  λ {c} → x { ⊎.inj₂ (⊎.inj₂ c) }

⊕assoc⊎ʳ :  ([ A ] ⊕ₗ ([ B ] ⊕ₗ [ C ])) ⊸⁺ [ A ⊎ B ⊎ C ]
⊕assoc⊎ʳ = mk L.⊕assoc⊎ʳ
    λ (f , g , h)
  → λ { {⊎.inj₁ a} → f {a}
      ; {⊎.inj₂ (⊎.inj₁ b)} → g {b}
      ; {⊎.inj₂ (⊎.inj₂ c)} → h {c}
      }

⊕assoc⊎ˡ : (([ A ] ⊕ₗ [ B ]) ⊕ₗ [ C ]) ⊸⁺ [ (A ⊎ B) ⊎ C ]
⊕assoc⊎ˡ = mk L.⊕assoc⊎ˡ
    λ ((f , g) , h)
  → λ { {⊎.inj₁ (⊎.inj₁ a)} → f {a}
      ; {⊎.inj₁ (⊎.inj₂ b)} → g {b}
      ; { ⊎.inj₂ c } → h {c}
      }
```

## Linear maps relating `Vector`s

```agda
1⊸⁺⊤ : ∀ {ℓ} → [ Fin 1 ] ⊸⁺ [ ⊤ {ℓ} ]
1⊸⁺⊤ = mk L.1⊸⊤ λ x → x

⊤⊸⁺1 : ∀ {ℓ} → [ ⊤ {ℓ} ] ⊸⁺ [ Fin 1 ]
⊤⊸⁺1 = mk L.⊤⊸1 λ x → x

m⊎n⊸⁺m+n : [ (Fin m) ⊎ (Fin n) ] ⊸⁺ [ Fin (m + n) ]
m⊎n⊸⁺m+n = mk L.m⊎n⊸m+n λ x {a} → x { Inverse.to +↔⊎ a }

m+n⊸⁺m⊎n : [ Fin (m + n) ] ⊸⁺ [ (Fin m) ⊎ (Fin n) ]
m+n⊸⁺m⊎n = mk L.m+n⊸m⊎n λ x {a} → x { Inverse.from +↔⊎ a }

⊎assoc+ˡ : [ (Fin m ⊎ Fin n) ⊎ Fin o ] ⊸⁺ [ Fin ((m + n) + o) ]
⊎assoc+ˡ = mk L.⊎assoc+ˡ
  λ x {a} → x { ⊎.map₁ (Inverse.to +↔⊎) (Inverse.to +↔⊎ a) }

⊎assoc+ʳ : [ Fin m ⊎ (Fin n ⊎ Fin o) ] ⊸⁺ [ Fin (m + (n + o)) ]
⊎assoc+ʳ = mk L.⊎assoc+ʳ
  λ x {a} → x { ⊎.map₂ (Inverse.to +↔⊎) (Inverse.to +↔⊎ a) }

+assoc⊎ˡ : [ Fin ((m + n) + o) ] ⊸⁺ [ (Fin m ⊎ Fin n) ⊎ Fin o ]
+assoc⊎ˡ = mk L.+assoc⊎ˡ
  λ x {a} → x { Inverse.from +↔⊎ (⊎.map₁ (Inverse.from +↔⊎) a) }

+assoc⊎ʳ : [ Fin (m + (n + o)) ] ⊸⁺ [ Fin m ⊎ (Fin n ⊎ Fin o) ]
+assoc⊎ʳ = mk L.+assoc⊎ʳ
  λ x {a} → x { Inverse.from +↔⊎ (⊎.map₂ (Inverse.from +↔⊎) a) }
```
