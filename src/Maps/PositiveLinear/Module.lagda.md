---
title: Algebraic Module Constructs on Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Module.Ordered.Partial.Bundles using (PoLeftSemimodule)

module Maps.PositiveLinear.Module
    {a b r ℓa₁ ℓa₂ ℓb₁ ℓb₂ ℓr₁ ℓr₂ : Level}
    (𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂)
    (𝓜ᴬ : PoLeftSemimodule (PoCommutativeSemiring.poSemiring 𝓡) a ℓa₁ ℓa₂)
    (𝓜ᴮ : PoLeftSemimodule (PoCommutativeSemiring.poSemiring 𝓡) b ℓb₁ ℓb₂)
    where

open module 𝓡 = PoCommutativeSemiring 𝓡
 renaming
  ( Carrier to R
  ; _≤_ to _≤ᴿ_
  )

private
  module 𝓜ᴬ = PoLeftSemimodule 𝓜ᴬ
  module 𝓜ᴮ = PoLeftSemimodule 𝓜ᴮ

open import Algebra.Module.Bundles
open import Maps.PositiveLinear.Core 𝓡.poSemiring
open import Maps.PositiveLinear.Base 𝓡.poSemiring
open import Maps.PositiveLinear.Algebra 𝓡.poSemiring
```

</details>

## `PoLeftSemimodule` of positive linear maps

NOTE: this construction requires the scalar space to be positive.

```agda
poLeftSemimodule :
      {∀ r → 0# ≤ᴿ r}
   →  PoLeftSemimodule 𝓡.poSemiring
         (a ⊔ b ⊔ r ⊔ ℓa₁ ⊔ ℓb₁ ⊔ ℓa₂ ⊔ ℓb₂ ⊔ ℓr₁ ⊔ ℓr₂) (a ⊔ ℓb₁) (a ⊔ ℓb₂)
poLeftSemimodule {rPos} = record
  { Carrierᴹ = 𝓜ᴬ ⊸⁺ 𝓜ᴮ
  ; _*ₗ_ = λ r → _*ₗ_ {C𝓡 = 𝓡.isCommutativeSemiring} r ⦃ rPos r ⦄
  ; isPoLeftSemimodule = record
    { isPreleftSemimodule = record
        { *ₗ-cong = λ x y {a} → 𝓜ᴮ.*ₗ-cong x (y {a})
        ; *ₗ-zeroˡ = λ f {a} → 𝓜ᴮ.*ₗ-zeroˡ (f ⟨$⟩ a)
        ; *ₗ-distribʳ =  λ f r₁ r₂ {a} → 𝓜ᴮ.*ₗ-distribʳ (f ⟨$⟩ a) r₁ r₂
        ; *ₗ-identityˡ =  λ f {a} → 𝓜ᴮ.*ₗ-identityˡ (f ⟨$⟩ a)
        ; *ₗ-assoc = λ r₁ r₂ f {a} → 𝓜ᴮ.*ₗ-assoc r₁ r₂ (f ⟨$⟩ a)
        ; *ₗ-zeroʳ = λ r {_} → 𝓜ᴮ.*ₗ-zeroʳ r
        ; *ₗ-distribˡ =  λ r f g {a} → 𝓜ᴮ.*ₗ-distribˡ r (f ⟨$⟩ a) (g ⟨$⟩ a)
        }
    ; +ᴹ-isPoCommutativeMonoid = M.isPoCommutativeMonoid
    ; *ₗ-compatₗ = λ x y {a} → 𝓜ᴮ.*ₗ-compatₗ x (y {a})
    ; *ₗ-compatᵣ = λ x y {a} → 𝓜ᴮ.*ₗ-compatᵣ x (y {a})
    }
  }
  where module M = PoCommutativeMonoid (+-0-poCommutativeMonoid)
```

