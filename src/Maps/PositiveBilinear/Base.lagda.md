---
title: Operations on Bilinear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level)
open import Algebra.Ordered.Partial.Bundles using (PoCommutativeSemiring)

module Maps.PositiveBilinear.Base
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂)
  where

open PoCommutativeSemiring 𝓡 using (commutativeSemiring)

-- stdlib
open import Algebra.Module.Ordered.Partial.Bundles

-- carya
open import Maps.Bilinear.Base commutativeSemiring as B using ()
open import Maps.PositiveBilinear.Core 𝓡 hiding (_≈_)

private
  variable
    a b c d ℓa₁ ℓa₂ ℓb₁ ℓb₂ ℓc₁ ℓc₂ ℓd₁ ℓd₂ : Level
```

</details>

## Zero

```agda
module _
  {𝓜ᴬ : PoSemimodule 𝓡 a ℓa₁ ℓa₂}
  {𝓜ᴮ : PoSemimodule 𝓡 b ℓb₁ ℓb₂}
  {𝓜ᶜ : PoSemimodule 𝓡 c ℓc₁ ℓc₂}
  where

  open PoSemimodule 𝓜ᶜ
  zero : 𝓜ᴬ ⊗ 𝓜ᴮ ⊸⁺ 𝓜ᶜ
  zero = mk B.zero λ _ _ → refl
```

## Addition

```agda
module _
  {𝓜ᴬ : PoSemimodule 𝓡 a ℓa₁ ℓa₂}
  {𝓜ᴮ : PoSemimodule 𝓡 b ℓb₁ ℓb₂}
  {𝓜ᶜ : PoSemimodule 𝓡 c ℓc₁ ℓc₂}
  where

  open PoSemimodule 𝓜ᶜ

  infixl 6 _+_
  _+_ : 𝓜ᴬ ⊗ 𝓜ᴮ ⊸⁺ 𝓜ᶜ → 𝓜ᴬ ⊗ 𝓜ᴮ ⊸⁺ 𝓜ᶜ → 𝓜ᴬ ⊗ 𝓜ᴮ ⊸⁺ 𝓜ᶜ
  f + g =
    mk
      (f.bimap B.+ g.bimap)
      λ 0≤a 0≤b →
        trans
          (reflexive (Eq.sym (identityʳ _)))
          (+ᴹ-mono₂ (f.isBiPositive 0≤a 0≤b) (g.isBiPositive 0≤a 0≤b))
    where module f = _⊗_⊸⁺_ f
          module g = _⊗_⊸⁺_ g
```

## Curry/Uncurry

-- TODO
