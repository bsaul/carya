---
title: Definitions for Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Partial.Bundles
open import Algebra.Module.Ordered.Partial.Bundles using (PoSemimodule)

module Maps.PositiveBilinear.Definitions
  {a b c r ℓa₁ ℓa₂ ℓb₁ ℓb₂ ℓc₁ ℓc₂ ℓr₁ ℓr₂}
  {𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂}
  (𝓜ᴬ : PoSemimodule 𝓡 a ℓa₁ ℓa₂)
  (𝓜ᴮ : PoSemimodule 𝓡 b ℓb₁ ℓb₂)
  (𝓜ᶜ : PoSemimodule 𝓡 c ℓc₁ ℓc₂)
  where

open PoCommutativeSemiring 𝓡
open import Level using (Level; _⊔_; suc)
open import Data.Product using (_,_; _×_)

open PoSemimodule 𝓜ᴬ renaming
    ( Carrierᴹ to A
    ; _≤ᴹ_ to _≤ᴬ_
    ; 0ᴹ to 0ᴬ
    )
    using ()
open PoSemimodule 𝓜ᴮ renaming
    ( Carrierᴹ to B
    ; _≤ᴹ_ to _≤ᴮ_
    ; 0ᴹ to 0ᴮ
    )
    using ()
open PoSemimodule 𝓜ᶜ renaming
    ( Carrierᴹ to C
    ; _≤ᴹ_ to _≤ᶜ_
    ; 0ᴹ to 0ᶜ
    )
```

</details>

```agda
BiPositive : (A × B → C) → Set _
BiPositive f = ∀ {a : A} {b : B} → 0ᴬ ≤ᴬ a → 0ᴮ ≤ᴮ b → 0ᶜ ≤ᶜ f (a , b)
```
