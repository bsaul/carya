---
title: Construct a linear map pointwise
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Ordered.Partial.Bundles

module Maps.PositiveBilinear.Construct.Pointwise
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂) where

-- stdlib
open import Algebra.Module.Ordered.Partial.Construct.Vector
  renaming (mkPoSemimodule to [_])

-- carya
open import Maps.PositiveBilinear.Core 𝓡 as Core using ()
open import Maps.PositiveBilinear.Core 𝓡
  using (
      ⟦_⟧
    ; _≈_
    ; isEquivalence
    ; setoid
    ; mk
    ; mkBilinear⁺
    ; _⟨$⟩_
  ) public
open import Maps.PositiveBilinear.Base 𝓡 as B using ()

private
  variable
    a b c d : Level
    A : Set a
    B : Set b
    C : Set c
    D : Set d
```

</details>

```agda
_⊗_⊸⁺_ : Set a → Set b → Set c → Set _
A ⊗ B ⊸⁺ C = Core._⊗_⊸⁺_ [ A ] [ B ] [ C ]
```

```agda
zero : A ⊗ B ⊸⁺ C
zero = B.zero

infixl 6 _+_
_+_ : A ⊗ B ⊸⁺ C → A ⊗ B ⊸⁺ C → A ⊗ B ⊸⁺ C
_+_ = B._+_

-- TODO
-- uncurry : A ⊗ B ⊸ C → A ⊸ B ⊸ C
-- uncurry = B.uncurry

-- curry :  A ⊸ B ⊸ C → A ⊗ B ⊸ C
-- curry = B.curry
```
