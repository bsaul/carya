---
title: Bilinear Functionals
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Partial.Bundles

module Maps.PositiveBilinear.Construct.Functional
   {r ℓr₁ ℓr₂}
   (𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂)
   where

open import Level using (Level; _⊔_)
open import Algebra.Module.Ordered.Partial.Bundles
open import Algebra.Module.Ordered.Partial.Construct.TensorUnit
 renaming (mkPoSemimodule to 𝓡*)

private
  variable
    a b ℓa₁ ℓa₂ ℓb₁ ℓb₂ : Level
    𝓜ᴬ : PoSemimodule 𝓡 a ℓa₁ ℓa₂
    𝓜ᴮ : PoSemimodule 𝓡 b ℓb₁ ℓb₂

open import Maps.PositiveBilinear.Core 𝓡
  using (
    mkBilinear⁺
   ; ⟦_⟧
   ; _⊗_⊸⁺_
   ; _≈_
   ; isEquivalence
   ; isPartialOrder
   ; setoid
   ; poset
   ; _⟨$⟩_
   )
  public
open import Maps.PositiveBilinear.Base 𝓡 as B
  using ()
```

</details>

```agda
PositiveBilinearFunctional : PoSemimodule 𝓡 a ℓa₁ ℓa₂ → PoSemimodule 𝓡 b ℓb₁ ℓb₂ → Set  _
PositiveBilinearFunctional 𝓜ᴬ 𝓜ᴮ = 𝓜ᴬ ⊗ 𝓜ᴮ ⊸⁺ 𝓡*
```

## Operations

```agda
zero : PositiveBilinearFunctional 𝓜ᴬ 𝓜ᴮ
zero = B.zero

_+_ : PositiveBilinearFunctional 𝓜ᴬ 𝓜ᴮ
    → PositiveBilinearFunctional 𝓜ᴬ 𝓜ᴮ
    → PositiveBilinearFunctional 𝓜ᴬ 𝓜ᴮ
_+_ = B._+_

-- TODO
-- uncurry : BilinearFunctional 𝓜ᴬ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᴮ ⊸ 𝓡*
-- uncurry = B.uncurry

-- curry : 𝓜ᴬ ⊸ 𝓜ᴮ ⊸ 𝓡* → BilinearFunctional 𝓜ᴬ 𝓜ᴮ
-- curry = B.curry
```
