---
title: Positive Bilinear maps
---

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_ ; suc )
open import Algebra.Ordered.Partial.Bundles using (PoCommutativeSemiring)

module Maps.PositiveBilinear.Core
  {r ℓr₁ ℓr₂}
  (𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂)
  where

open PoCommutativeSemiring 𝓡 using (poSemiring; commutativeSemiring)
private
  𝓡* = poSemiring
  variable
    a b c ℓa₁ ℓa₂ ℓb₁ ℓb₂ ℓc₁ ℓc₂ : Level

open import Algebra.Module.Ordered.Partial.Bundles using (PoSemimodule)
open import Algebra.Module.Ordered.Partial.Construct.DirectProduct
  renaming (mkPoSemimodule to _⊕_)
open import Data.Product as × using (_×_ ; _,_)
open import Function using (_on_; Congruent)
open import Relation.Binary.Construct.On as On using ()
open import Relation.Binary
  using (IsEquivalence ; Setoid; Poset ; IsPartialOrder)

-- carya
import Maps.Bilinear.Definitions as Defs
open import Maps.Bilinear.Core commutativeSemiring as B
  using (_⊗_⊸_ ; mkBilinear)
import Maps.PositiveBilinear.Definitions as PDefs
```

## Positive Bilinear Maps

A [*bilinear* map](https://en.wikipedia.org/wiki/Bilinear_map)
is a relation between Semimodules,
namely a map $M^A \times M^B \to M^C$,
which is linear in both arguments
and preserves positive elements.

```agda
infix 3 _⊗_⊸⁺_
record _⊗_⊸⁺_
  (𝓜ᴬ : PoSemimodule 𝓡 a ℓa₁ ℓa₂)
  (𝓜ᴮ : PoSemimodule 𝓡 b ℓb₁ ℓb₂)
  (𝓜ᶜ : PoSemimodule 𝓡 c ℓc₁ ℓc₂)
  : Set (a ⊔ b ⊔ c ⊔ r ⊔ ℓa₁ ⊔ ℓa₂ ⊔ ℓb₁ ⊔ ℓb₂ ⊔ ℓc₁ ⊔ ℓc₂ ⊔ ℓr₁ ⊔ ℓr₂) where

  constructor mk

  private
    module 𝓜ᴬ = PoSemimodule 𝓜ᴬ
    module 𝓜ᴮ = PoSemimodule 𝓜ᴮ
    module 𝓜ᶜ = PoSemimodule 𝓜ᶜ

  open PDefs 𝓜ᴬ 𝓜ᴮ 𝓜ᶜ

  field
    bimap : 𝓜ᴬ.semimodule ⊗ 𝓜ᴮ.semimodule ⊸ 𝓜ᶜ.semimodule

  open _⊗_⊸_ bimap public

  field
    isBiPositive : BiPositive to
```

```agda
module _
  {𝓜ᴬ : PoSemimodule 𝓡 a ℓa₁ ℓa₂}
  {𝓜ᴮ : PoSemimodule 𝓡 b ℓb₁ ℓb₂}
  {𝓜ᶜ : PoSemimodule 𝓡 c ℓc₁ ℓc₂}
  where

  private
    module 𝓜ᴬ = PoSemimodule 𝓜ᴬ
    module 𝓜ᴮ = PoSemimodule 𝓜ᴮ
    module 𝓜ᶜ = PoSemimodule 𝓜ᶜ
    module 𝓜ᴬ⊕𝓜ᴮ = PoSemimodule (𝓜ᴬ ⊕ 𝓜ᴮ)

  open PDefs 𝓜ᴬ 𝓜ᴮ 𝓜ᶜ
  open Defs 𝓜ᴬ.semimodule 𝓜ᴮ.semimodule 𝓜ᶜ.semimodule

  mkBilinear⁺ :
     (f : 𝓜ᴬ.Carrierᴹ × 𝓜ᴮ.Carrierᴹ → 𝓜ᶜ.Carrierᴹ)
      → Congruent 𝓜ᴬ⊕𝓜ᴮ._≈ᴹ_ 𝓜ᶜ._≈ᴹ_ f
      → +-Homomorphic₂ f
      → *-Homomorphic₂ f
      → BiPositive f
      → 𝓜ᴬ ⊗ 𝓜ᴮ ⊸⁺ 𝓜ᶜ
  mkBilinear⁺ f cong +-homo *-homo pos =
    mk (mkBilinear f cong +-homo *-homo) pos
```

## Equivalence and Denotation

```agda
module _
  {𝓜ᴬ : PoSemimodule 𝓡 a ℓa₁ ℓa₂}
  {𝓜ᴮ : PoSemimodule 𝓡 b ℓb₁ ℓb₂}
  {𝓜ᶜ : PoSemimodule 𝓡 c ℓc₁ ℓc₂}
  where

  private
    module 𝓜ᴬ = PoSemimodule 𝓜ᴬ
    module 𝓜ᴮ = PoSemimodule 𝓜ᴮ
    module 𝓜ᶜ = PoSemimodule 𝓜ᶜ

  ⟦_⟧ : 𝓜ᴬ ⊗ 𝓜ᴮ ⊸⁺ 𝓜ᶜ → 𝓜ᴬ.semimodule ⊗ 𝓜ᴮ.semimodule ⊸ 𝓜ᶜ.semimodule
  ⟦_⟧ = _⊗_⊸⁺_.bimap

  infix 4 _≈_ _≤_
  _≈_ : 𝓜ᴬ ⊗ 𝓜ᴮ ⊸⁺ 𝓜ᶜ → 𝓜ᴬ ⊗ 𝓜ᴮ ⊸⁺ 𝓜ᶜ → Set _
  _≈_ = B._≈_ on ⟦_⟧

  _≤_ : 𝓜ᴬ ⊗ 𝓜ᴮ ⊸⁺ 𝓜ᶜ → 𝓜ᴬ ⊗ 𝓜ᴮ ⊸⁺ 𝓜ᶜ → Set _
  (mk f _) ≤ (mk g _) = ∀ {a} {b} → (f B.⟨$⟩ (a , b))  𝓜ᶜ.≤ᴹ (g B.⟨$⟩ (a , b))

  isEquivalence : IsEquivalence _≈_
  isEquivalence = On.isEquivalence ⟦_⟧ B.isEquivalence

  isPartialOrder : IsPartialOrder _≈_ _≤_
  isPartialOrder = record
    { isPreorder = record
      { isEquivalence = isEquivalence
      ; reflexive = λ x {a} {b} → 𝓜ᶜ.reflexive (x { (a , b) })
      ; trans = λ x y {a} {b} → 𝓜ᶜ.trans (x {a} {b}) (y {a} {b})
      }
    ; antisym = λ x y {(a , b)} → 𝓜ᶜ.antisym (x {a} {b}) (y {a} {b})
    }

  setoid : Setoid _ _
  setoid = record {isEquivalence = isEquivalence}

  poset : Poset _ _ _
  poset = record { isPartialOrder = isPartialOrder }
```

## Application

```agda
  infixr -1 _⟨$⟩_
  _⟨$⟩_ : 𝓜ᴬ ⊗ 𝓜ᴮ ⊸⁺ 𝓜ᶜ → 𝓜ᴬ.Carrierᴹ × 𝓜ᴮ.Carrierᴹ → 𝓜ᶜ.Carrierᴹ
  T ⟨$⟩ x = ⟦ T ⟧ B.⟨$⟩ x
-- ```
