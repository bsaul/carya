---
title: Linear Maps with Bundles as Objects
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (Semiring)

module Maps.Linear {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where
```

</details>

```agda
open import Maps.Linear.Core 𝓡 public
open import Maps.Linear.Base 𝓡 public
```
