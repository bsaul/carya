---
title: Affine Maps with Bundles as Objects
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (Semiring; CommutativeMonoid)

module Maps.Affine.Core {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where

open import Maps.Linear.Core 𝓡 as L using (_⊸_; _⟨$⟩_)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Algebra.Module.Affine.Bundles using (AffineLeftSemimodule)
open import Function using (Congruent; Inverse; _on_)
open import Data.Product using (∃; proj₁; proj₂; _×_; _,_)
open import Relation.Binary using (IsEquivalence)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning
```

</details>

## Affine Map

```agda
infix 3 _ₐ⊸_
record _ₐ⊸_ {a b x y ℓˣ ℓʸ ℓᵃ ℓᵇ}
    (𝓐𝓯𝓯ˣ : AffineLeftSemimodule 𝓡 x ℓˣ a ℓᵃ)
    (𝓐𝓯𝓯ʸ : AffineLeftSemimodule 𝓡 y ℓʸ b ℓᵇ)
    : Set (a ⊔ b ⊔ x ⊔ y ⊔ r ⊔ ℓʳ ⊔ ℓˣ ⊔ ℓʸ ⊔ ℓᵃ ⊔ ℓᵇ) where

  constructor mk

  open AffineLeftSemimodule 𝓐𝓯𝓯ˣ
    renaming
    ( Carrierˣ to X
    ; Carrierᴹ to A
    ; leftSemimodule to 𝓜ᴬ
    ; _≈ᴹ_ to _≈ᴬ_
    ; _—_ to _—ˣ_
    )
  open AffineLeftSemimodule 𝓐𝓯𝓯ʸ
    renaming
    ( Carrierˣ to Y
    ; leftSemimodule to 𝓜ᴮ
    ; _≈ˣ_ to _≈ʸ_
    ; _≈ᴹ_ to _≈ᴮ_
    ; ≈ᴹ-refl to 𝓜ᴮ-refl
    ; _—_ to _—ʸ_
    ; _+ˣ_ to _+ʸ_
    ; +ˣ-congˡ to +ʸ-congˡ
    ; ≈ˣ-setoid to setoidʸ
    ; ≈ᴹ-sym to 𝓜ᴮ-sym
    ; affine to affineʸ
    ; hasAffinity to hasAffinityʸ
    ) using ()

  field
    f : X → Y
    ϕ : 𝓜ᴬ ⊸ 𝓜ᴮ
    isCongruent : Congruent _≈ˣ_ _≈ʸ_ f
    -- See affine map:
    -- https://en.wikipedia.org/wiki/Affine_transformation#Affine_maps
    isAffineMap : ∀ {x₁ x₂ : X} → (ϕ ⟨$⟩ x₂ —ˣ x₁) ≈ᴮ f x₂ —ʸ f x₁
    -- welldef : ∀ (a b c d : A)
    --       → b —ᴬ a ≈ᴹᴬ d —ᴬ c
    --       → f b —ᴮ f a ≈ᴹᴮ f d —ᴮ f c

  -- An affine map is a translation plus a linear map
  affineIsLinear+ : ∀ {x : X} {a : A} → f (x +ˣ a) ≈ʸ (f x +ʸ (ϕ ⟨$⟩ a))
  affineIsLinear+ {x} {a} =
     let x* = x +ˣ a
         x*—x≈a : x* —ˣ x ≈ᴬ a
         x*—x≈a = Inverse.inverseʳ (inverse (affine x)) ≈ˣ-refl
     in
     begin
       f (x +ˣ a)
     ≡⟨⟩
       f x*
     ≈˘⟨ proj₂ (proj₂ (hasAffinityʸ (affineʸ (f x))) (f x*)) 𝓜ᴮ-refl ⟩
       f x +ʸ (f x* —ʸ f x)
     ≈⟨ +ʸ-congˡ (f x) (𝓜ᴮ-sym isAffineMap) ⟩
       f x +ʸ (ϕ ⟨$⟩ (x* —ˣ x))
     ≈⟨ +ʸ-congˡ (f x) (_⊸_.cong ϕ x*—x≈a) ⟩
       f x +ʸ (ϕ ⟨$⟩ a)
     ∎
    where open ≈-Reasoning setoidʸ
```

## Equivalence

```agda
module _ {a b x y ℓˣ ℓʸ ℓᵃ ℓᵇ}
    {𝓐𝓯𝓯ˣ : AffineLeftSemimodule 𝓡 x ℓˣ a ℓᵃ}
    {𝓐𝓯𝓯ʸ : AffineLeftSemimodule 𝓡 y ℓʸ b ℓᵇ}
    where

  open AffineLeftSemimodule 𝓐𝓯𝓯ˣ renaming (Carrierˣ to X) using ()
  open AffineLeftSemimodule 𝓐𝓯𝓯ʸ
    renaming
     ( Carrierˣ to Y
     ; _≈ˣ_ to _≈ʸ_
     ; ≈ˣ-refl to ≈ʸ-refl
     ; ≈ˣ-sym to ≈ʸ-sym
     ; ≈ˣ-trans to ≈ʸ-trans
     )
     using ()

  ⟦_⟧ : 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ʸ → (X → Y)
  ⟦ mk f _ _ _ ⟧ = f

  infix 4 _≈_

  _≈_ : 𝓐𝓯𝓯ˣ  ₐ⊸ 𝓐𝓯𝓯ʸ → 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ʸ → Set _
  _≈_ = (λ f g → ∀ {x} → f x ≈ʸ g x) on ⟦_⟧

  isEquivalence : IsEquivalence _≈_
  isEquivalence = record
    { refl = ≈ʸ-refl
    ; sym =  λ x → ≈ʸ-sym x
    ; trans = λ x y → ≈ʸ-trans x y
    }
```

## Application

```agda
module _ {a b x y ℓˣ ℓʸ ℓᵃ ℓᵇ}
    {𝓐𝓯𝓯ˣ : AffineLeftSemimodule 𝓡 x ℓˣ a ℓᵃ}
    {𝓐𝓯𝓯ʸ : AffineLeftSemimodule 𝓡 y ℓʸ b ℓᵇ}
    where

  open AffineLeftSemimodule 𝓐𝓯𝓯ˣ
    renaming (Carrierˣ to X; Carrierᴹ to A)
    using (_+ˣ_)
  open AffineLeftSemimodule 𝓐𝓯𝓯ʸ
    renaming (Carrierˣ to Y)
    using ()

  infixr -1 _·ₐ_

  _·ₐ_ : 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ʸ → X × A → Y
  T ·ₐ (x , a) = ⟦ T ⟧ (x +ˣ a)
```
