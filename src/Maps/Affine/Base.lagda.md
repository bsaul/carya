---
title: Operations on Affine Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; lift)
open import Algebra.Bundles using (Semiring)

module Maps.Affine.Base {r ℓ : Level} (𝓡 : Semiring r ℓ) where

open Semiring 𝓡
  renaming
  ( refl to reflᴿ
  ; Carrier to R
  ; _≈_ to _≈ᴿ_
  ; _+_ to _+ᴿ_
  ; _*_ to _*ᴿ_
  )
  using
  (0# ; 1#)

open import Algebra.Core using (Op₂)
open import Algebra.Structures
open import Algebra.Module.Affine.Bundles using (AffineLeftSemimodule)
open import Algebra.Module.Affine.Construct.DirectProduct
  renaming (affineLeftSemimodule to _⊕ₐ_)
open import Data.Product as × using (_,_; ∃; _×_)
open import Function
  renaming (_∘_ to _∘ᶠ_ ; id to idᶠ; const to constᶠ)
  using (Inverse; flip )
open import Relation.Binary.PropositionalEquality
   using (_≡_; refl; module ≡-Reasoning)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open import Maps.Affine.Core 𝓡
open import Maps.Linear.Core 𝓡 using (_⟨$⟩_; _⊸_)
open import Maps.Linear.Base 𝓡 as L using ()

private
  variable
    v x y z ℓᵛ ℓˣ ℓʸ ℓᶻ a a* b b* c c* d d* ℓᵃ ℓᵃ* ℓᵇ ℓᵇ* ℓᶜ ℓᶜ* ℓᵈ ℓᵈ* : Level
```

</details>

## Special Maps

### Constant Map

```agda
module _
    {𝓐𝓯𝓯ˣ : AffineLeftSemimodule 𝓡 x ℓˣ a ℓᵃ}
    {𝓐𝓯𝓯ʸ : AffineLeftSemimodule 𝓡 y ℓʸ b ℓᵇ}
   where

  open AffineLeftSemimodule 𝓐𝓯𝓯ʸ renaming (Carrierˣ to Y ; ≈ˣ-refl to ≈ʸ-refl)

  const : Y → 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ʸ
  const y = mk (constᶠ y) L.zero (constᶠ ≈ʸ-refl) ε≈a—a

  ⟦const⟧ : ∀ {y} → ⟦ const y ⟧ ≡ constᶠ y
  ⟦const⟧ = refl
```

### Identity

```agda
module _ {𝓐𝓯𝓯ˣ : AffineLeftSemimodule 𝓡 x ℓˣ a ℓᵃ} where

  open AffineLeftSemimodule 𝓐𝓯𝓯ˣ using (≈ᴹ-refl)

  id : 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ˣ
  id = mk idᶠ L.id idᶠ ≈ᴹ-refl

  ⟦id⟧ : ⟦ id ⟧ ≡ idᶠ
  ⟦id⟧ = refl
```

## Composition

```agda
module _
  {𝓐𝓯𝓯ˣ : AffineLeftSemimodule 𝓡 x ℓˣ a ℓᵃ}
  {𝓐𝓯𝓯ʸ : AffineLeftSemimodule 𝓡 y ℓʸ b ℓᵇ}
  {𝓐𝓯𝓯ᶻ : AffineLeftSemimodule 𝓡 z ℓᶻ c ℓᶜ}
  where

  open AffineLeftSemimodule 𝓐𝓯𝓯ˣ using ()
    renaming (_—_ to _—ˣ_)
  open AffineLeftSemimodule 𝓐𝓯𝓯ʸ using ()
    renaming (_—_ to _—ʸ_)
  open AffineLeftSemimodule 𝓐𝓯𝓯ᶻ using (≈ᴹ-setoid)
    renaming (_—_ to _—ᶻ_)

  infixr 9 _∘_
  _∘_ : 𝓐𝓯𝓯ʸ ₐ⊸ 𝓐𝓯𝓯ᶻ → 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ʸ → 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ᶻ
  (mk f f* fcong faffine) ∘ (mk g g* gcong gaffine) =
    mk
      (f ∘ᶠ g)
      (f* L.∘ g*)
      (fcong ∘ᶠ gcong)
      λ {x₁} {x₂} →
        begin
          ((f* L.∘ g*) ⟨$⟩ (x₂ —ˣ x₁))
         ≈⟨ _⊸_.cong f* gaffine ⟩
          (f* ⟨$⟩ ((g x₂) —ʸ (g x₁)))
         ≈⟨ faffine ⟩
          (f ∘ᶠ g) x₂ —ᶻ (f ∘ᶠ g) x₁
        ∎
      where open ≈-Reasoning ≈ᴹ-setoid

  ⟦∘⟧ : ∀ {f : 𝓐𝓯𝓯ʸ ₐ⊸ 𝓐𝓯𝓯ᶻ} {g : 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ʸ} → ⟦ f ∘ g ⟧ ≡ ⟦ f ⟧ ∘ᶠ ⟦ g ⟧
  ⟦∘⟧ = refl
```

## Direct Sum

```agda
module _
  {𝓐𝓯𝓯ᵛ : AffineLeftSemimodule 𝓡 v ℓᵛ a ℓᵃ}
  {𝓐𝓯𝓯ˣ : AffineLeftSemimodule 𝓡 x ℓˣ a ℓᵃ}
  {𝓐𝓯𝓯ʸ : AffineLeftSemimodule 𝓡 y ℓʸ b ℓᵇ}
  {𝓐𝓯𝓯ᶻ : AffineLeftSemimodule 𝓡 z ℓᶻ c ℓᶜ}
  where

  infixr 6 _⊕_
  _⊕_ : 𝓐𝓯𝓯ᵛ ₐ⊸ 𝓐𝓯𝓯ʸ → 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ᶻ → (𝓐𝓯𝓯ᵛ ⊕ₐ 𝓐𝓯𝓯ˣ) ₐ⊸ (𝓐𝓯𝓯ʸ ⊕ₐ 𝓐𝓯𝓯ᶻ)
  (mk f f* fcong faffine) ⊕ (mk g g* gcong gaffine) =
     mk
      (×.map f g)
      (f* L.⊕ g*)
      (×.map fcong gcong)
      (faffine , gaffine)

  ⟦⊕⟧ : ∀ {f : 𝓐𝓯𝓯ᵛ ₐ⊸ 𝓐𝓯𝓯ʸ} {g : 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ᶻ} → ⟦ f ⊕ g ⟧ ≡ ×.map ⟦ f ⟧ ⟦ g ⟧
  ⟦⊕⟧ = refl
```

## Copy

```agda
module _  {𝓐𝓯𝓯ˣ : AffineLeftSemimodule 𝓡 x ℓˣ a ℓᵃ} where

  open AffineLeftSemimodule 𝓐𝓯𝓯ˣ using (≈ᴹ-refl)

  copyᶠ : ∀ {z} {A : Set z} → A → A × A
  copyᶠ a = a , a

  copy : 𝓐𝓯𝓯ˣ ₐ⊸ (𝓐𝓯𝓯ˣ ⊕ₐ 𝓐𝓯𝓯ˣ)
  copy = mk copyᶠ L.copy copyᶠ (≈ᴹ-refl , ≈ᴹ-refl)

  ⟦copy⟧ : ⟦ copy ⟧ ≡ copyᶠ
  ⟦copy⟧ = refl
```

## Reduce ("jam")

```agda
module _
  {𝓐𝓯𝓯ˣ : AffineLeftSemimodule 𝓡 x ℓˣ a ℓᵃ}
  {_+_ : Op₂ (AffineLeftSemimodule.Carrierˣ 𝓐𝓯𝓯ˣ)}
  {isSemigroup : IsSemigroup (AffineLeftSemimodule._≈ˣ_ 𝓐𝓯𝓯ˣ) _+_}
  where

  open AffineLeftSemimodule 𝓐𝓯𝓯ˣ
  open AffineLeftSemimodule (𝓐𝓯𝓯ˣ ⊕ₐ 𝓐𝓯𝓯ˣ)
    renaming (_—_ to _—ₓ_)
    using ()
  open IsSemigroup isSemigroup

  -- TODO
  -- reduce : (𝓐𝓯𝓯ˣ ⊕ₐ 𝓐𝓯𝓯ˣ) ₐ⊸ 𝓐𝓯𝓯ˣ
  -- reduce =
  --   mk
  --     (×.uncurry _+_)
  --     L.reduce
  --     (×.uncurry ∙-cong)
  --     λ {(a , b)} {(c , d)} →
  --       begin
  --         (c — a) +ᴹ (d — b)
  --       ≈⟨ {!   !} ⟩
  --         (c + d) — (a + b)
  --       ∎
  --     where open ≈-Reasoning ≈ᴹ-setoid
```


## Fork (split)

Horizontal stacking:

```md
  A | B
```

```agda
module _
  {𝓐𝓯𝓯ˣ : AffineLeftSemimodule 𝓡 x ℓˣ a ℓᵃ}
  {𝓐𝓯𝓯ʸ : AffineLeftSemimodule 𝓡 y ℓʸ b ℓᵇ}
  {𝓐𝓯𝓯ᶻ : AffineLeftSemimodule 𝓡 z ℓᶻ c ℓᶜ}
  where

  infixr 6 _▵_ _∣_
  _▵_ : 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ʸ → 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ᶻ → 𝓐𝓯𝓯ˣ ₐ⊸ (𝓐𝓯𝓯ʸ ⊕ₐ 𝓐𝓯𝓯ᶻ)
  f ▵ g = (f ⊕ g) ∘ copy

  _∣_ = _▵_
```

## Join (junc)

Vertical stacking:

```md
  A
 ---
  B
```

```agda
-- TODO: once reduce is done
-- module _
--   {𝓐𝓯𝓯ˣ : AffineLeftSemimodule 𝓡 x ℓˣ a ℓᵃ}
--   {𝓐𝓯𝓯ʸ : AffineLeftSemimodule 𝓡 y ℓʸ b ℓᵇ}
--   {𝓐𝓯𝓯ᶻ : AffineLeftSemimodule 𝓡 z ℓᶻ c ℓᶜ}
--   where

--   infixr 6 _▿_ _—_
--   _▿_ : 𝓐𝓯𝓯ˣ ₐ⊸ 𝓐𝓯𝓯ᶻ → 𝓐𝓯𝓯ʸ ₐ⊸ 𝓐𝓯𝓯ᶻ → (𝓐𝓯𝓯ˣ ⊕ₐ 𝓐𝓯𝓯ʸ) ₐ⊸ 𝓐𝓯𝓯ᶻ
--   f ▿ g = reduce ∘ (f ⊕ g)

--   _—_ = _▿_
```


## Swap

```agda
module _
  {𝓐𝓯𝓯ˣ : AffineLeftSemimodule 𝓡 x ℓˣ a ℓᵃ}
  {𝓐𝓯𝓯ʸ : AffineLeftSemimodule 𝓡 y ℓʸ b ℓᵇ}
  where

  private
   module A = AffineLeftSemimodule 𝓐𝓯𝓯ˣ renaming (≈ᴹ-refl to reflᴬ)
   module B = AffineLeftSemimodule 𝓐𝓯𝓯ʸ renaming (≈ᴹ-refl to reflᴮ)

  swap : (𝓐𝓯𝓯ˣ ⊕ₐ 𝓐𝓯𝓯ʸ) ₐ⊸ (𝓐𝓯𝓯ʸ ⊕ₐ 𝓐𝓯𝓯ˣ)
  swap =
    mk
      ×.swap
      L.swap
      ×.swap
      (B.+ᴹ-identityˡ _ , A.+ᴹ-identityʳ _)

  ⟦swap⟧ : ⟦ swap ⟧ ≡ ×.swap
  ⟦swap⟧ = refl
```
