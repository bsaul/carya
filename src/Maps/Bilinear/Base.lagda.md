---
title: Operations on Bilinear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level)
open import Algebra.Bundles using (CommutativeSemiring)

module Maps.Bilinear.Base {r ℓ : Level} (𝓡 : CommutativeSemiring r ℓ) where

-- stdlib
import Algebra.Properties.CommutativeSemigroup as CS
open import Algebra.Structures using (IsCommutativeSemiring)
open import Algebra.Bundles using (CommutativeMonoid)
open import Algebra.Module.Bundles using (Semimodule)
open import Algebra.Module.Construct.DirectProduct
  renaming (semimodule to _⊕ₗ_) using ()
open import Data.Product as × using (_,_; ∃)
open import Function renaming (_∘_ to _∘ᶠ_) using ()
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open CommutativeSemiring 𝓡 using (semiring)

-- carya
open import Maps.Prelinear.Base semiring as P using ()
open import Maps.Prelinear.Base2 semiring as P2 using ()
open import Maps.Prelinear.Core semiring using (_≈_; Prelinear; mkPre)
open import Maps.Bilinear.Core 𝓡 hiding (_≈_)
open import Maps.Linear.Core semiring as L using (mk⊸ ; _⊸_)

private
  variable
    a b c d ℓa ℓb ℓc ℓd : Level
    𝓜ᴬ : Semimodule 𝓡 a ℓa
    𝓜ᴮ : Semimodule 𝓡 b ℓb
```

</details>

## Zero

```agda
module _
  {𝓜ᴬ : Semimodule 𝓡 a ℓa}
  {𝓜ᴮ : Semimodule 𝓡 b ℓb}
  {𝓜ᶜ : Semimodule 𝓡 c ℓc}
  where

  open Semimodule 𝓜ᶜ
  zero : 𝓜ᴬ ⊗ 𝓜ᴮ ⊸ 𝓜ᶜ
  zero =
    mk
      P.zero
      (mk
         ((≈ᴹ-sym (+ᴹ-identityˡ 0ᴹ)) , ≈ᴹ-sym (+ᴹ-identityˡ 0ᴹ))
          (≈ᴹ-sym (*ₗ-zeroʳ _) , ≈ᴹ-sym (*ᵣ-zeroˡ _)))

  ⟦zero⟧ : ⟦ zero ⟧ ≈ (P.zero {𝓜ᴬ = Semimodule.leftSemimodule (𝓜ᴬ ⊕ₗ 𝓜ᴮ) })
  ⟦zero⟧ = ≈ᴹ-refl
```

## Addition

```agda
module _
  {𝓜ᴬ : Semimodule 𝓡 a ℓa}
  {𝓜ᴮ : Semimodule 𝓡 b ℓb}
  {𝓜ᶜ : Semimodule 𝓡 c ℓc}
  where

  open Semimodule 𝓜ᴬ
    renaming ( _+ᴹ_ to _+ᴬ_ ; _*ₗ_ to _*ᴬ_)
    using ()
  open Semimodule 𝓜ᴮ
    renaming ( _+ᴹ_ to _+ᴮ_ ; _*ₗ_ to _*ᴮ_)
    using ()
  open Semimodule 𝓜ᶜ

  infixl 6 _+_
  _+_ : 𝓜ᴬ ⊗ 𝓜ᴮ ⊸ 𝓜ᶜ → 𝓜ᴬ ⊗ 𝓜ᴮ ⊸ 𝓜ᶜ → 𝓜ᴬ ⊗ 𝓜ᴮ ⊸ 𝓜ᶜ
  (mk pref (mk +₁ *₁)) + (mk preg (mk  +₂ *₂)) =
     mk
      (pref P.+ preg)
      (mk
        (λ {a₁} {a₂} {b₁} {b₂} →
            (begin
              f (a₁ , b₁ +ᴮ b₂) +ᴹ g (a₁ , b₁ +ᴮ b₂)
            ≈⟨ +ᴹ-cong
                  (×.proj₁ (+₁ {a₁} {a₁} {b₁} {b₂}))
                  (×.proj₁ (+₂ {a₁} {a₁} {b₁} {b₂}))
              ⟩
              f (a₁ , b₁) +ᴹ f (a₁ , b₂) +ᴹ (g (a₁ , b₁) +ᴹ g (a₁ , b₂))
            ≈⟨ interchange _ _ _ _ ⟩
              f (a₁ , b₁) +ᴹ g (a₁ , b₁) +ᴹ (f (a₁ , b₂) +ᴹ g (a₁ , b₂))
            ∎)
          , (begin
              (f (a₁ +ᴬ a₂ , b₁) +ᴹ g (a₁ +ᴬ a₂ , b₁))
            ≈⟨ +ᴹ-cong
                  (×.proj₂ (+₁ {a₁} {a₂} {b₁} {b₁}))
                  (×.proj₂ (+₂ {a₁} {a₂} {b₁} {b₁}))
              ⟩
              (f (a₁ , b₁) +ᴹ f (a₂ , b₁) +ᴹ (g (a₁ , b₁) +ᴹ g (a₂ , b₁)))
            ≈⟨ interchange _ _ _ _ ⟩
              (f (a₁ , b₁) +ᴹ g (a₁ , b₁) +ᴹ (f (a₂ , b₁) +ᴹ g (a₂ , b₁)))
            ∎)
        )
        λ {r} {a} {b} →
           (begin
              f (a , r *ᴮ b) +ᴹ g (a , r *ᴮ b)
            ≈⟨ +ᴹ-cong (×.proj₁ *₁) (×.proj₁ *₂) ⟩
              r *ₗ f (a , b) +ᴹ r *ₗ g (a , b)
            ≈˘⟨ *ₗ-distribˡ _ _ _ ⟩
              r *ₗ (f (a , b) +ᴹ g (a , b))
            ∎)
          , (begin
              (f (r *ᴬ a , b) +ᴹ g (r *ᴬ a , b))
            ≈⟨ +ᴹ-cong (×.proj₂ *₁) (×.proj₂ *₂) ⟩
              f (a , b) *ᵣ r +ᴹ g (a , b) *ᵣ r
            ≈˘⟨ *ᵣ-distribʳ _ _ _ ⟩
             ((f (a , b) +ᴹ g (a , b)) *ᵣ r)
            ∎)
      )
      where open ≈-Reasoning ≈ᴹ-setoid
            open Prelinear pref renaming (to to f)
            open Prelinear preg renaming (to to g)
            open CS (CommutativeMonoid.commutativeSemigroup +ᴹ-commutativeMonoid)

  ⟦+⟧ : ∀ {x y} → ⟦ x + y ⟧ ≈ ⟦ x ⟧ P.+ ⟦ y ⟧
  ⟦+⟧ = ≈ᴹ-refl
```

## Curry/Uncurry

```agda
module _
  {𝓜ᴬ : Semimodule 𝓡 a ℓa}
  {𝓜ᴮ : Semimodule 𝓡 b ℓb}
  {𝓜ᶜ : Semimodule 𝓡 c ℓc}
  where

  private
    module 𝓜ᴬ = Semimodule 𝓜ᴬ
    module 𝓜ᴮ = Semimodule 𝓜ᴮ
    module 𝓜ᶜ = Semimodule 𝓜ᶜ

  uncurry : 𝓜ᴬ ⊗ 𝓜ᴮ ⊸ 𝓜ᶜ → 𝓜ᴬ ⊸ 𝓜ᴮ ⊸ 𝓜ᶜ
  uncurry T =
    mk⊸
      (λ a → mk⊸ ⟨ a ,_⟩ (cong ∘ᶠ (𝓜ᴬ.≈ᴹ-refl ,_))
                 additiveʳ (𝓜ᶜ.≈ᴹ-sym (×.proj₁ *-homo₂)))
      (λ x → cong (x , 𝓜ᴮ.≈ᴹ-refl))
      additiveˡ
      scaleˡ
      where open _⊗_⊸_ T

  --TODO:
  -- ⟦uncurry⟧ : ∀ {x} → L.⟦ uncurry x ⟧ ≈ {! P2.uncurry ?   !}
  -- ⟦uncurry⟧ = {!   !}

  curry : 𝓜ᴬ ⊸ 𝓜ᴮ ⊸ 𝓜ᶜ → 𝓜ᴬ ⊗ 𝓜ᴮ ⊸ 𝓜ᶜ
  curry T =
    mk
      (mkPre
        (λ (a , b) → (T L.⟨$⟩ a) L.⟨$⟩ b)
         λ {( a₁ , b₁)} {(a₂ , b₂)} (a₁≈a₂ , b₁≈b₂)
           → begin
                ((T L.⟨$⟩ a₁) L.⟨$⟩ b₁)
              ≈⟨ _⊸_.cong (T L.⟨$⟩ a₁) b₁≈b₂ ⟩
                ((T L.⟨$⟩ a₁) L.⟨$⟩ b₂)
              ≈⟨ cong a₁≈a₂ ⟩
                ((T L.⟨$⟩ a₂) L.⟨$⟩ b₂)
              ∎
      )
      (mk
        (_⊸_.+-homo (T L.⟨$⟩ _) , +-homo )
        (λ {r} {a} {b} → 𝓜ᶜ.≈ᴹ-sym (_⊸_.*ₗ-homo (T L.⟨$⟩ a))
           , 𝓜ᶜ.≈ᴹ-trans (𝓜ᶜ.≈ᴹ-sym *ₗ-homo) (𝓜ᶜ.*ₗ-*ᵣ-coincident _ _))
      )
      where open _⊸_ T
            open ≈-Reasoning 𝓜ᶜ.≈ᴹ-setoid

  --TODO:
  -- ⟦curry⟧ : ∀ {x} → ⟦ uncurry x ⟧ ≈ ?
  -- ⟦curry⟧ = ?
```

## Curry/Uncurry

```agda
module _
  {𝓜ᴬ : Semimodule 𝓡 a ℓa}
  {𝓜ᴮ : Semimodule 𝓡 b ℓb}
  {𝓜ᶜ : Semimodule 𝓡 c ℓc}
  where


```
