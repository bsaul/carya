---
title: Bilinear maps
---

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_ ; suc )
open import Algebra.Bundles using (CommutativeSemiring)

module Maps.Bilinear.Core {r ℓr} (𝓡 : CommutativeSemiring r ℓr) where

open CommutativeSemiring 𝓡 using (semiring)
private
  𝓡* = semiring
  variable
    a b c ℓa ℓb ℓc : Level

open import Algebra.Module.Bundles using (Semimodule)
open import Algebra.Module.Construct.DirectProduct
  renaming (semimodule to _⊕_)
open import Data.Product as × using (_×_ ; _,_)
open import Function using (_on_; Congruent)
open import Relation.Binary.Construct.On as On using ()
open import Relation.Binary using (IsEquivalence ; Setoid)

-- carya
open import Maps.Prelinear.Core 𝓡* as P using (Prelinear)
import Maps.Bilinear.Definitions as Defs
```

## Bilinear Maps

A [*bilinear* map](https://en.wikipedia.org/wiki/Bilinear_map)
is a relation between Semimodules,
namely a map $M^A \times M^B \to M^C$,
which is linear in both arguments.

```agda
record IsBilinear
  {𝓜ᴬ : Semimodule 𝓡 a ℓa}
  {𝓜ᴮ : Semimodule 𝓡 b ℓb}
  {𝓜ᶜ : Semimodule 𝓡 c ℓc}
  (map : Prelinear
          (Semimodule.leftSemimodule (𝓜ᴬ ⊕ 𝓜ᴮ))
          (Semimodule.leftSemimodule 𝓜ᶜ)
  )
  : Set (a ⊔ b ⊔ c ⊔ r ⊔ ℓa ⊔ ℓb ⊔ ℓc ⊔ ℓr) where

  constructor mk

  open Prelinear map hiding (map) public
  open Defs 𝓜ᴬ 𝓜ᴮ 𝓜ᶜ

  field
    +-homo₂ : +-Homomorphic₂ to
    *-homo₂ : *-Homomorphic₂ to
```

```agda
infix 3 _⊗_⊸_
record _⊗_⊸_
  (𝓜ᴬ : Semimodule 𝓡 a ℓa)
  (𝓜ᴮ : Semimodule 𝓡 b ℓb)
  (𝓜ᶜ : Semimodule 𝓡 c ℓc)
  : Set (a ⊔ b ⊔ c ⊔ r ⊔ ℓa ⊔ ℓb ⊔ ℓc ⊔ ℓr) where

    constructor mk

    private
      Do = Semimodule.leftSemimodule (𝓜ᴬ ⊕ 𝓜ᴮ)
      Co = Semimodule.leftSemimodule 𝓜ᶜ

    field
      map : Prelinear Do Co
      isBilinear : IsBilinear {𝓜ᴬ = 𝓜ᴬ} {𝓜ᴮ} {𝓜ᶜ} map

    open IsBilinear isBilinear public

    open Semimodule 𝓜ᴬ
      renaming (
          Carrierᴹ to A
        ; _+ᴹ_ to _+ᴬ_
        ; _*ₗ_ to _*ᴬ_
        ) using ()
    open Semimodule 𝓜ᴮ
      renaming (
          Carrierᴹ to B
        ; _+ᴹ_ to _+ᴮ_
        ; _*ₗ_ to _*ᴮ_
        ) using ()
    open Semimodule 𝓜ᶜ renaming (Carrierᴹ to C)

    -- Convenient (and common) syntax for uncurried form of f function.
    ⟨_,_⟩ : A → B → C
    ⟨ x , y ⟩ = to ( x , y )

    additiveˡ : ∀ {a₁ a₂ : A} {b : B}
       → ⟨ a₁ +ᴬ a₂ , b ⟩ ≈ᴹ ⟨ a₁ , b ⟩ +ᴹ ⟨ a₂ , b ⟩
    additiveˡ {a₁} {a₂} {b} = ×.proj₂ (+-homo₂ {a₁} {a₂} {b} {b})

    additiveʳ : ∀ {a : A} {b₁ b₂ : B}
       → ⟨ a , b₁ +ᴮ b₂ ⟩ ≈ᴹ ⟨ a , b₁ ⟩ +ᴹ ⟨ a , b₂ ⟩
    additiveʳ {a} {b₁} {b₂} = ×.proj₁ (+-homo₂ {a} {a} {b₁} {b₂})

    scaleˡ : ∀ {r} {a : A} {b : B}
       → r *ₗ ⟨ a , b ⟩ ≈ᴹ ⟨ r *ᴬ a , b ⟩
    scaleˡ {r} {a} {b} =
      ≈ᴹ-trans (*ₗ-*ᵣ-coincident r _) (≈ᴹ-sym (×.proj₂ (*-homo₂ {r} {a} {b})))

    scaleʳ :  ∀ {r} {a : A} {b : B}
       → ⟨ a , r *ᴮ b ⟩ ≈ᴹ ⟨ a , b ⟩ *ᵣ r
    scaleʳ {r} {a} {b} =
      ≈ᴹ-trans (×.proj₁ (*-homo₂ {r} {a} {b})) (*ₗ-*ᵣ-coincident r _)
```

```agda
module _
  {𝓜ᴬ : Semimodule 𝓡 a ℓa}
  {𝓜ᴮ : Semimodule 𝓡 b ℓb}
  {𝓜ᶜ : Semimodule 𝓡 c ℓc}
  where

  private
    module 𝓜ᴬ = Semimodule 𝓜ᴬ
    module 𝓜ᴮ = Semimodule 𝓜ᴮ
    module 𝓜ᶜ = Semimodule 𝓜ᶜ
    module 𝓜ᴬ⊕𝓜ᴮ = Semimodule (𝓜ᴬ ⊕ 𝓜ᴮ)
  open Defs 𝓜ᴬ 𝓜ᴮ 𝓜ᶜ

  mkBilinear :
     (f : 𝓜ᴬ.Carrierᴹ × 𝓜ᴮ.Carrierᴹ → 𝓜ᶜ.Carrierᴹ)
      → Congruent 𝓜ᴬ⊕𝓜ᴮ._≈ᴹ_ 𝓜ᶜ._≈ᴹ_ f
      → +-Homomorphic₂ f
      → *-Homomorphic₂ f
      → 𝓜ᴬ ⊗ 𝓜ᴮ ⊸ 𝓜ᶜ
  mkBilinear f cong +-homo *-homo =
    mk
      (P.mk (record { to = f ; cong = cong }))
      (mk +-homo *-homo)
```

Syntax for uncurried form.

```agda
open import Maps.Linear.Core semiring using (_⊸_)
open import Maps.Linear.Module 𝓡 renaming (leftSemimodule to _⊸'_)

_⊸_⊸_ : Semimodule 𝓡 a ℓa → Semimodule 𝓡 b ℓb → Semimodule 𝓡 c ℓc → Set _
𝓜ᴬ ⊸ 𝓜ᴮ ⊸ 𝓜ᶜ = 𝓜ᴬ.leftSemimodule ⊸ (𝓜ᴮ.leftSemimodule ⊸' 𝓜ᶜ.leftSemimodule)
  where module 𝓜ᴬ = Semimodule 𝓜ᴬ
        module 𝓜ᴮ = Semimodule 𝓜ᴮ
        module 𝓜ᶜ = Semimodule 𝓜ᶜ
```


## Equivalence and Denotation

```agda
module _
  {𝓜ᴬ : Semimodule 𝓡 a ℓa}
  {𝓜ᴮ : Semimodule 𝓡 b ℓb}
  {𝓜ᶜ : Semimodule 𝓡 c ℓc}
  where

  private
    Do = Semimodule.leftSemimodule (𝓜ᴬ ⊕ 𝓜ᴮ)
    Co = Semimodule.leftSemimodule 𝓜ᶜ

  ⟦_⟧ : 𝓜ᴬ ⊗ 𝓜ᴮ ⊸ 𝓜ᶜ → Prelinear Do Co
  ⟦_⟧ = _⊗_⊸_.map

  infix 4 _≈_
  _≈_ : 𝓜ᴬ ⊗ 𝓜ᴮ ⊸ 𝓜ᶜ → 𝓜ᴬ ⊗ 𝓜ᴮ ⊸ 𝓜ᶜ → Set _
  _≈_ = P._≈_ on ⟦_⟧

  isEquivalence : IsEquivalence _≈_
  isEquivalence = On.isEquivalence ⟦_⟧ P.isEquivalence

  setoid : Setoid _ _
  setoid = record {isEquivalence = isEquivalence}
```

## Application

```agda
module _
  {𝓜ᴬ : Semimodule 𝓡 a ℓa}
  {𝓜ᴮ : Semimodule 𝓡 b ℓb}
  {𝓜ᶜ : Semimodule 𝓡 c ℓc}
  where

  open Semimodule 𝓜ᴬ renaming (Carrierᴹ to A) using ()
  open Semimodule 𝓜ᴮ renaming (Carrierᴹ to B) using ()
  open Semimodule 𝓜ᶜ renaming (Carrierᴹ to C) using ()

  infixr -1 _⟨$⟩_
  _⟨$⟩_ : 𝓜ᴬ ⊗ 𝓜ᴮ ⊸ 𝓜ᶜ → A × B → C
  T ⟨$⟩ x = ⟦ T ⟧ P.⟨$⟩ x
```
