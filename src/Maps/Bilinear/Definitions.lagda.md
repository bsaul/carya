---
title: Definitions for Linear Maps
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (CommutativeSemiring)
open import Algebra.Module.Bundles using (Semimodule)

module Maps.Bilinear.Definitions
  {a b c r ℓa ℓb ℓc ℓr}
  {𝓡 : CommutativeSemiring r ℓr}
  (𝓜ᴬ : Semimodule 𝓡 a ℓa)
  (𝓜ᴮ : Semimodule 𝓡 b ℓb)
  (𝓜ᶜ : Semimodule 𝓡 c ℓc)
  where

open CommutativeSemiring 𝓡
open import Level using (Level; _⊔_; suc)
open import Data.Product using (_,_; _×_)

open Semimodule 𝓜ᴬ renaming
    ( Carrierᴹ to A
    ; _+ᴹ_ to _+ᴬ_
    ; _*ₗ_ to _*ᴬ_
    )
    using ()
open Semimodule 𝓜ᴮ renaming
    ( Carrierᴹ to B
    ; _+ᴹ_ to _+ᴮ_
    ; _*ₗ_ to _*ᴮ_
    )
    using ()
open Semimodule 𝓜ᶜ renaming
    ( Carrierᴹ to C
    -- ; _≈ᴹ_ to _≈ᶜ_
    )
    -- using (_+ᴹ_; _≈ᴹ_)
```

</details>

```agda
+-Homomorphic₂ : (A × B → C) → Set (a ⊔ b ⊔ ℓc)
+-Homomorphic₂ f =
   ∀ {a₁ a₂ : A} {b₁ b₂ : B}
  → f (a₁ , b₁ +ᴮ b₂) ≈ᴹ f (a₁ , b₁) +ᴹ f (a₁ , b₂)
  × f (a₁ +ᴬ a₂ , b₁) ≈ᴹ f (a₁ , b₁) +ᴹ f (a₂ , b₁)

*-Homomorphic₂ : (A × B → C) → Set _
*-Homomorphic₂ f =
  ∀ {r} {a : A} {b : B}
    → f ( a , r *ᴮ b ) ≈ᴹ r *ₗ f ( a , b )
    × f ( r *ᴬ a , b ) ≈ᴹ f ( a , b ) *ᵣ r
```

