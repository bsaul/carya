---
title: Construct a linear map pointwise
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (CommutativeSemiring)

module Maps.Bilinear.Construct.Pointwise
  {r ℓr : Level}
  (𝓡 : CommutativeSemiring r ℓr) where

-- stdlib
open import Algebra.Module.Construct.Vector
  renaming (mkSemimodule to [_])

-- carya
open import Maps.Bilinear.Core 𝓡 as Core using ()
open import Maps.Bilinear.Core 𝓡
  using (
      ⟦_⟧
    ; _≈_
    ; isEquivalence
    ; setoid
    ; mk
    ; mkBilinear
    ; _⟨$⟩_
  ) public
open import Maps.Bilinear.Base 𝓡 as B using ()

private
  variable
    a b c d : Level
    A : Set a
    B : Set b
    C : Set c
    D : Set d
```

</details>

```agda
_⊗_⊸_ : Set a → Set b → Set c → Set _
A ⊗ B ⊸ C = Core._⊗_⊸_ [ A ] [ B ] [ C ]

_⊸_⊸_ : Set a → Set b → Set c → Set _
A ⊸ B ⊸ C = Core._⊸_⊸_ [ A ] [ B ] [ C ]
```

```agda
zero : A ⊗ B ⊸ C
zero = B.zero

infixl 6 _+_
_+_ : A ⊗ B ⊸ C → A ⊗ B ⊸ C → A ⊗ B ⊸ C
_+_ = B._+_

uncurry : A ⊗ B ⊸ C → A ⊸ B ⊸ C
uncurry = B.uncurry

curry :  A ⊸ B ⊸ C → A ⊗ B ⊸ C
curry = B.curry
```
