---
title: Bilinear Functionals
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (CommutativeSemiring)

module Maps.Bilinear.Construct.Functional
   {r ℓr}
   (𝓡 : CommutativeSemiring r ℓr)
   where

open import Level using (Level; _⊔_)
open import Algebra.Module.Bundles using (Semimodule)
open import Algebra.Module.Construct.TensorUnit
 renaming (semimodule to 𝓡*)

private
  variable
    a b ℓa ℓb : Level
    𝓜ᴬ : Semimodule 𝓡 a ℓa
    𝓜ᴮ : Semimodule 𝓡 b ℓb

open import Maps.Bilinear.Core 𝓡
  using (_⊗_⊸_; _⊸_⊸_)
open import Maps.Bilinear.Core 𝓡
  using (
    mkBilinear
   ; ⟦_⟧
   ; _≈_)
  public
open import Maps.Bilinear.Base 𝓡 as B
  using ()
```

</details>

A [bilinear functional](https://en.wikipedia.org/wiki/Bilinear_form)
is a bilinear map from a `Semimodule` (or module or vector)
to its scalar space.
These are also referred to as bilinear forms.

```agda
BilinearFunctional : Semimodule 𝓡 a ℓa → Semimodule 𝓡 b ℓb → Set  _
BilinearFunctional 𝓜ᴬ 𝓜ᴮ = 𝓜ᴬ ⊗ 𝓜ᴮ ⊸ 𝓡*
```

## Operations

```agda
zero : BilinearFunctional 𝓜ᴬ 𝓜ᴮ
zero = B.zero

_+_ : BilinearFunctional 𝓜ᴬ 𝓜ᴮ → BilinearFunctional 𝓜ᴬ 𝓜ᴮ → BilinearFunctional 𝓜ᴬ 𝓜ᴮ
_+_ = B._+_

uncurry : BilinearFunctional 𝓜ᴬ 𝓜ᴮ → 𝓜ᴬ ⊸ 𝓜ᴮ ⊸ 𝓡*
uncurry = B.uncurry

curry : 𝓜ᴬ ⊸ 𝓜ᴮ ⊸ 𝓡* → BilinearFunctional 𝓜ᴬ 𝓜ᴮ
curry = B.curry
```
