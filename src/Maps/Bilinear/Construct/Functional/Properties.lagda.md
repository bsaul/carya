---
title: Properties of Dual spaces and Linear Functionals
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (CommutativeSemiring)

module Maps.Bilinear.Construct.Functional.Properties
   {r ℓr}
   (𝓡 : CommutativeSemiring r ℓr)
   where

open CommutativeSemiring 𝓡
open import Level using (Level; _⊔_)
open import Algebra.Module.Bundles using (Semimodule)
open import Data.Product as × using (_,_)
open import Function using (_∘_)

private
  variable
    a b ℓa ℓb : Level
    𝓜ᴬ : Semimodule 𝓡 a ℓa
    𝓜ᴮ : Semimodule 𝓡 b ℓb

open import Maps.Bilinear.Construct.Functional 𝓡 hiding (_≈_)
open import LinearSpace.Dual.Core 𝓡
open import Maps.Linear.Core semiring hiding (_≈_)
open import Maps.Bilinear.Core 𝓡 hiding (_≈_)
```

</details>

## A Bilinear form projects out two linear maps

A Bilinear form can project out two linear forms (functionals):

* S - a linear map between the Dual
* T - a linear map from M to a Double Dual

See
[generalization to modules](https://en.wikipedia.org/wiki/Bilinear_form#Generalization_to_modules).

```agda
module _  (T : BilinearFunctional 𝓜ᴬ 𝓜ᴮ) where
   private
      module 𝓜ᴬ = Semimodule 𝓜ᴬ
      module 𝓜ᴮ = Semimodule 𝓜ᴮ
   open _⊗_⊸_ T

   proj₁ : 𝓜ᴬ.leftSemimodule ⊸ (𝓜ᴮ.leftSemimodule *)
   proj₁ =
      mk⊸
         (λ x →
            mk⊸
               ⟨ x ,_⟩
               (cong ∘ (𝓜ᴬ.≈ᴹ-refl ,_))
               additiveʳ
               λ {r} {a} → sym (×.proj₁ (*-homo₂ {r} {x} {a}))
               )
         (λ x → cong (x , 𝓜ᴮ.≈ᴹ-refl))
         additiveˡ
         scaleˡ

   proj₂ : 𝓜ᴮ.leftSemimodule ⊸ (𝓜ᴬ.leftSemimodule *)
   proj₂ =
      mk⊸
         (λ x → mk⊸ ⟨_, x ⟩ (cong ∘ (_, 𝓜ᴮ.≈ᴹ-refl)) additiveˡ scaleˡ)
         (λ x → cong (𝓜ᴬ.≈ᴹ-refl , x ))
         additiveʳ
         λ {r} {a} {x} → sym (×.proj₁ (*-homo₂ {r} {x} {a}))
```

## Applying `0ᴹ` to either argument yields `0#`

```agda
module _  (T : BilinearFunctional 𝓜ᴬ 𝓜ᴮ) where
   module 𝓜ᴬ = Semimodule 𝓜ᴬ
   module 𝓜ᴮ = Semimodule 𝓜ᴮ
   open _⊗_⊸_ T

   unitalˡ : ∀ {x} → ⟨ 𝓜ᴬ.0ᴹ , x ⟩ ≈ 0#
   unitalˡ = _⊸_.f0≈0 (proj₁ T)

   unitalʳ : ∀ {x} → ⟨ x , 𝓜ᴮ.0ᴹ ⟩ ≈ 0#
   unitalʳ = _⊸_.f0≈0 (proj₂ T)
```

## Predicates on Bilinear Forms

```agda
module _ {𝓜ᴬ : Semimodule 𝓡 a ℓa} where

  open Semimodule 𝓜ᴬ
    renaming (Carrierᴹ to A; 0ᴹ to 0ᴬ; _≈ᴹ_ to _≈ᴬ_)
    using ()

  NonDegenerate : BilinearFunctional 𝓜ᴬ 𝓜ᴬ → Set (ℓr ⊔ a ⊔ ℓa)
  NonDegenerate B = ∀ (a₁ a₂ : A) → ⟨ a₁ , a₂ ⟩ ≈ 0# → a₂ ≈ᴬ 0ᴬ
    where open _⊗_⊸_ B

  Symmetric : BilinearFunctional 𝓜ᴬ 𝓜ᴬ → Set (ℓr ⊔ a)
  Symmetric B = ∀ (a₁ a₂ : A) → ⟨ a₁ , a₂ ⟩ ≈ ⟨ a₂ , a₁ ⟩
    where open _⊗_⊸_ B

  Alternating : BilinearFunctional 𝓜ᴬ 𝓜ᴬ → Set (ℓr ⊔ a)
  Alternating B = ∀ (a : A) → ⟨ a , a ⟩ ≈ 0#
    where open _⊗_⊸_ B

  Reflexive : BilinearFunctional 𝓜ᴬ 𝓜ᴬ → Set (ℓr ⊔ a)
  Reflexive B = ∀ (a₁ a₂ : A) → ⟨ a₁ , a₂ ⟩ ≈ 0# → ⟨ a₂ , a₁ ⟩ ≈ 0#
    where open _⊗_⊸_ B

  Orthogonal : {B : BilinearFunctional 𝓜ᴬ 𝓜ᴬ} → { _ : Reflexive B } → (A → A → Set ℓr)
  Orthogonal {B} a₁ a₂ = ⟨ a₁ , a₂ ⟩ ≈ 0#
      where open _⊗_⊸_ B
```
