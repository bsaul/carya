---
title: Properties of Bilinear Forms
---

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (_⊔_ ; suc; Level)
open import Algebra.Bundles using (Semiring; CommutativeSemiring)

module Maps.Bilinear.Properties {r ℓʳ} (𝓡 : CommutativeSemiring r ℓʳ) where

open import Algebra.Module.Bundles using (Semimodule; LeftSemimodule)
open import Data.Product as × using (_×_ ; _,_)
open import Function using (_∘_)

private
  𝓡* = CommutativeSemiring.semiring 𝓡

open import Maps.Bilinear.Core 𝓡
open import Maps.Linear.Core 𝓡* hiding (_≈_)
-- open import Maps.Linear.Dual.Core 𝓡

open CommutativeSemiring 𝓡

private
  variable
    a ℓa : Level
```


```agda
-- module _ {a ℓᵃ} {𝓜ᴬ : LeftSemimodule 𝓡* a ℓᵃ} where

--     S* : Bilinear 𝓜ᴬ (𝓜ᴬ *) → 𝓜ᴬ ⊸ (𝓜ᴬ **)
--     S* B = Bilinear.proj₁ B

--     T* : Bilinear 𝓜ᴬ (𝓜ᴬ *) → 𝓜ᴬ * ⊸ 𝓜ᴬ *
--     T* B = Bilinear.proj₂ B
```

Some linear maps also induce bilinear forms:

```agda
-- module _ {a ℓᵃ} {𝓜ᴬ : LeftSemimodule 𝓡* a ℓᵃ} where

--     open LeftSemimodule 𝓜ᴬ
--       renaming (≈ᴹ-refl to reflᴬ ; ≈ᴹ-sym to symᴬ;  ≈ᴹ-trans to transᴬ)

--     Sᴮ : 𝓜ᴬ * ⊸ 𝓜ᴬ * → Bilinear (𝓜ᴬ *) 𝓜ᴬ
--     Sᴮ T =
--        mk
--          (λ (T₁ , a) → _⊸_.f (g T₁) a)
--          (λ {(T₁ , _)}  (x , y) → trans (_⊸_.isCongruent (g T₁) x) (isCongruent y))
--          (λ {a} → _⊸_.isAdditive (g a) , isAdditive )
--          λ {r} {T₁} {a} →
--             (sym (_⊸_.isHomogeneous (g T₁)))
--           , trans (sym isHomogeneous ) (*-comm r (_⊸_.f (g T₁) a))
--       where open _⊸_ T renaming (f to g)

--     Tᴮ : 𝓜ᴬ ⊸ 𝓜ᴬ → Bilinear (𝓜ᴬ *) 𝓜ᴬ
--     Tᴮ T =
--       mk
--         (λ (T₁ , a) → _⊸_.f T₁ (g a))
--         (λ {(T₁ , _)}  (x , y) → trans (_⊸_.isCongruent T₁ (isCongruent x)) y)
--         (λ {T₁} {T₂} →
--               trans (_⊸_.isCongruent T₁ isAdditive) (_⊸_.isAdditive T₁)
--             , +-cong (_⊸_.isCongruent T₁ reflᴬ) (_⊸_.isCongruent T₂ reflᴬ))
--         λ {r} {T₁} {a} →
--               trans
--                 (_⊸_.isCongruent T₁ (symᴬ (isHomogeneous)))
--                 (sym (_⊸_.isHomogeneous T₁ {r}))
--             , trans
--                 (_⊸_.isHomogeneous T₁)
--                 (trans
--                     (sym ( _⊸_.isHomogeneous T₁))
--                     (*-comm r (_⊸_.f T₁ (g a))))
--       where open _⊸_ T renaming (f to g)
```
