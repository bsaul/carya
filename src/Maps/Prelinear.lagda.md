---
title: Functions Maps with Module Bundles as Objects
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (Semiring)

module Maps.Prelinear {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where
```

</details>

```agda
open import Maps.Prelinear.Core 𝓡 public
open import Maps.Prelinear.Base 𝓡 public
```
