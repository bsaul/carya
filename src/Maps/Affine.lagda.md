---
title: Affine Maps with Bundles as Objects
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)
open import Algebra.Bundles using (Semiring)

module Maps.Affine {r ℓʳ : Level} (𝓡 : Semiring r ℓʳ) where
```

</details>

```agda
open import Maps.Affine.Core 𝓡 public
open import Maps.Affine.Base 𝓡 public
```
