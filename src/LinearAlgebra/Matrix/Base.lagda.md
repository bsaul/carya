---
title: Operations on Matrices
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --without-K --safe #-}

open import Algebra.Bundles using (CommutativeSemiring)

module LinearAlgebra.Matrix.Base {r ℓr} (𝓡 : CommutativeSemiring r ℓr) where

open module 𝓡 = CommutativeSemiring 𝓡
  renaming (Carrier to R; _*_ to _*ᴿ_ ;_+_ to _+ᴿ_; _≈_ to _≈ᴿ_)
  using (semiring; 1# ; 0#; setoid)

-- stdlib
open import Algebra.Module.Bundles
open import Algebra.Module.Construct.Matrix semiring
open import Data.Fin using (Fin; splitAt; join; _↑ˡ_; _↑ʳ_); open Fin
open import Data.Fin.Patterns
open import Data.Fin.Properties using (*↔× ; +↔⊎; ¬Fin0)
open import Data.Nat as ℕ using (ℕ)
open import Data.Sum as ⊎ using (_⊎_)
open import Data.Sum.Properties
open import Data.Product as × using ()
open import Function as F
  using (flip; const; Inverse; _-⟨_⟩-_; _-⟨_∣; ∣_⟩-_ ; _-⟪_⟫-_ )
open import Relation.Nullary.Decidable
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning
open import Relation.Binary.PropositionalEquality as ≡ using (_≡_; refl)

-- carya
open import LinearSpace.DotProduct.Core 𝓡
open import LinearAlgebra.Matrix.Core 𝓡 hiding (_≈_)
open import Maps.Linear.Construct.FiniteDimension 𝓡 as L
  using (_∘_ ; _*ₗ_; _⊸_; mk⊸; _≈_; _⟨$⟩_)
open import Maps.Linear.Base 𝓡.semiring as LM using ()
open import Maps.Linear.Properties 𝓡.semiring as LMP using ()
open import Fold.Properties.CommutativeSemiring {{ 𝓡 }}
open import Fold.Properties.Indicator {{ 𝓡 }}

private
  variable
    m n o p : ℕ
```

</details>

## Scale

```agda
_*ₘ_ : R → Matrix m n → Matrix m n
r *ₘ A = λ i j → r *ᴿ A i j

⟦*ₘ⟧ : ∀ {r : R} {A : Matrix m n} → ⟦ r *ₘ A ⟧ ≈ r *ₗ ⟦ A ⟧
⟦*ₘ⟧ {m} {n} {r} {A} {f} {i} = begin
  ∑ (λ j → r *ᴿ A i j *ᴿ f j)   ≈⟨ ∑-cong {n} (λ j → 𝓡.*-assoc r _ _) ⟩
  ∑ (λ j → r *ᴿ (A i j *ᴿ f j)) ≈⟨ c*∑≈∑c* {n} (A i ⟨*⟩ f) r ⟨
  r *ᴿ ∑ (A i ⟨*⟩ f)            ∎
  where open ≈-Reasoning setoid
```

## Zero

```agda
𝟘 : ∀ {m n} → Matrix m n
𝟘 {m} {n} = 0ᴹ
  where open LeftSemimodule (matrix m n)

⟦𝟘⟧ : ⟦ 𝟘 {m} {n} ⟧ ≈ L.zero
⟦𝟘⟧ {m} {n} {a = f} {x = i} = begin
  ∑ (λ j → 0# *ᴿ f j) ≈⟨ ∑-cong (λ i → 𝓡.zeroˡ (f i)) ⟩
  (∑[ n ] (λ _ → 0#)) ≈⟨ ∑-preserves-0 {n = n} ⟩
  0#                  ∎
  where open ≈-Reasoning setoid
```

## Addition

```agda
module _ {m n : ℕ} where

  open LeftSemimodule (matrix m n)

  _+_ : Matrix m n → Matrix m n → Matrix m n
  _+_ = _+ᴹ_

  ⟦+⟧ : ∀ {A B} → ⟦ A + B ⟧ ≈ ⟦ A ⟧ L.+ ⟦ B ⟧
  ⟦+⟧ {A} {B} {f} {i} = begin
    (⟦ A + B ⟧ ⟨$⟩ f) i                                      ≡⟨⟩
    ∑ (λ i' → (A i i' +ᴿ B i i') *ᴿ f i')                    ≈⟨ ∑-cong {n = n} (λ i' → 𝓡.distribʳ _ _ _) ⟩
    ∑ (λ i' → (A i i' *ᴿ f i' +ᴿ B i i' *ᴿ f i'))            ≈⟨ ∑-preserves-+ {n = n} _ _ ⟨
    ∑ (λ i' → A i i' *ᴿ f i') +ᴿ (∑ (λ i' → B i i' *ᴿ f i')) ≡⟨⟩
    ((⟦ A ⟧ L.+ ⟦ B ⟧) ⟨$⟩ f) i                              ∎
    where open ≈-Reasoning setoid
```

## Id

```agda
id : Matrix m m
id = 𝕀

⟦id⟧ : ⟦ id {m} ⟧ ≈ L.id
⟦id⟧ {m} {f} {i} = ∑𝕀*f≈fi i f
```

## Matrix Product

Product of matrix is composition of linear maps.

```agda
_×_ : Matrix m n → Matrix n o → Matrix m o
A × B = λ i k → A i · flip B k

⟦×⟧ : ∀ {A : Matrix m n} {B : Matrix n o}
  → ⟦ A × B ⟧ ≈ ⟦ A ⟧ ∘ ⟦ B ⟧
⟦×⟧ {m} {n} {ℕ.zero} {A = A} {B} {f} {i} = begin
  0#                         ≈⟨ ∑-preserves-0 {n = n} ⟨
  ∑[ n ] (λ _ → 0#)          ≈⟨ ∑-cong {n = n} (λ j → 𝓡.sym (𝓡.zeroʳ _)) ⟩
  ∑[ n ] (λ j → A i j *ᴿ 0#) ∎
  where open ≈-Reasoning setoid
⟦×⟧ {m} {ℕ.zero} {o} {a = f} = begin
  ∑[ o ] (λ k → 0# *ᴿ f k)   ≈⟨ ∑-cong {n = o} (λ k → 𝓡.zeroˡ _) ⟩
  ∑[ o ] (λ _ → 0#)          ≈⟨ ∑-preserves-0 {n = o} ⟩
  0#                         ∎
  where open ≈-Reasoning setoid
⟦×⟧ {m} {ℕ.suc n} {o} {A = A} {B} {f} {i} = begin
    (⟦ A × B ⟧ ⟨$⟩ f) i
  ≡⟨⟩
    ∑ (λ k → (∑ λ j → Ai j *ᴿ B j k) *ᴿ f k)
  ≡⟨⟩
    ∑ (λ k → (∑ (λ j → Ai j *ᴿ B j k)) *ᴿ f k)
  ≈⟨ ∑-cong {n = o} (λ k → 𝓡.distribʳ (f k) _ _) ⟩
    ∑ (λ k → (Ai zero *ᴿ B zero k) *ᴿ f k
       +ᴿ ∑ (λ j → Ai (suc j) *ᴿ B (suc j) k) *ᴿ f k)
  ≈⟨ ∑-cong {n = o} (λ k → 𝓡.+-congʳ (𝓡.*-assoc _ _ _)) ⟩
    ∑ (λ k → Ai zero *ᴿ (B zero ⟨*⟩ f) k
       +ᴿ ∑ (λ j → Ai (suc j) *ᴿ B (suc j) k) *ᴿ f k)
  ≈˘⟨ ∑-preserves-+ {n = o}
        (λ k → Ai zero *ᴿ (B zero ⟨*⟩ f) k)
        (λ k → (∑ (λ j → Ai (suc j) *ᴿ B (suc j) k)) *ᴿ f k)
    ⟩
       ∑ (λ k → Ai zero *ᴿ (B zero ⟨*⟩ f) k)
    +ᴿ ∑ (λ k → ∑ (λ j → Ai (suc j) *ᴿ B (suc j) k) *ᴿ f k)
  ≈⟨ 𝓡.+-cong
      (𝓡.sym (c*∑≈∑c* {n = o} _ (Ai zero)))
      (⟦×⟧ {m} {n} {o} {A = λ i j → A i (suc j)} {B = λ j → B (suc j)})
    ⟩
       Ai zero *ᴿ ∑ (λ k → (B zero ⟨*⟩ f) k)
    +ᴿ ∑ (λ j → Ai (suc j) *ᴿ ∑ (B (suc j) ⟨*⟩ f))
  ≡⟨⟩
    ∑ (λ j → Ai j *ᴿ ∑ (B j ⟨*⟩ f))
  ≡⟨⟩
   ((⟦ A ⟧ ∘ ⟦ B ⟧) ⟨$⟩ f) i
  ∎
  where open ≈-Reasoning setoid
        Ai = A i
```

## Diagonal Matrix

```agda
diag : R → Matrix m m
diag = _*ₘ id

⟦diag⟧ : ∀ {r} → ⟦ diag {m} r ⟧ ≈ L.diag r
⟦diag⟧ {m} {r = r} {f} {i} = begin
  (∑ (λ x → r *ᴿ 𝕀 i x *ᴿ (f x)))   ≈⟨ ∑-cong {m} (λ x → 𝓡.*-assoc r _ _) ⟩
  (∑ (λ x → r *ᴿ (𝕀 i x *ᴿ (f x)))) ≈⟨ c*∑≈∑c* {m} (λ x → 𝕀 i x *ᴿ (f x)) r ⟨
  r *ᴿ (∑ (λ x → 𝕀 i x *ᴿ (f x)))   ≈⟨ 𝓡.*-congˡ (∑𝕀*f≈fi i f) ⟩
  r *ᴿ f i                          ∎
  where open ≈-Reasoning setoid
```

## Terminal

```agda
! : Matrix m 1
! _ _ = 0#

⟦!⟧ : ⟦ ! ⟧ ≈ L.!
⟦!⟧ = 𝓡.trans (𝓡.+-congʳ (𝓡.zeroˡ _)) (𝓡.+-identityˡ _)
```

## Initial

```agda
¡ : Matrix 1 m
¡ _ _ = 0#

⟦¡⟧ : ⟦ ¡ ⟧ ≈ L.¡
⟦¡⟧ = 𝓡.trans (𝓡.+-congʳ (𝓡.zeroˡ _)) (𝓡.+-identityˡ _)
```

## Direct Sum

```agda
private
  _⊕′_ : (Fin m → Fin o → R)
       → (Fin n → Fin p → R)
       → (Fin m ⊎ Fin n) → (Fin o ⊎ Fin p) → R
  A ⊕′ B = ⊎.[ (λ i → ⊎.[ A i , (λ _ → 𝓡.0#) ]′)
             , (λ j → ⊎.[ (λ _ → 𝓡.0#) , B j ]′)
             ]′

infixr 6 _⊕_
_⊕_ : Matrix m o → Matrix n p → Matrix (m ℕ.+ n) (o ℕ.+ p)
A ⊕ B = (Inverse.to +↔⊎) -⟨ A ⊕′ B ⟩- (Inverse.to +↔⊎)

⟦⊕⟧ : ∀ {A : Matrix m o} {B : Matrix n p}
      → ⟦ A ⊕ B ⟧ ≈ ⟦ A ⟧ L.⊕ ⟦ B ⟧
⟦⊕⟧ {m} {o} {n} {p} {A} {B} {f} {x = i} =
  let
    si = splitAt m i
  in
  begin
    (⟦ A ⊕ B ⟧ ⟨$⟩ f ) i
  ≈⟨ ∑-cong {n = o ℕ.+ p} (𝓡.*-congʳ F.∘ 𝓡.reflexive F.∘ lem₁) ⟩
    ∑[ o ℕ.+ p ]
      (λ k → ⊎.[ (λ j l → ⊎.[ A j , (λ _ → 0#) ] (splitAt o l))
               , (λ j l → ⊎.[ (λ _ → 0#) , B j ] (splitAt o l)) ]′
             si k *ᴿ f k)
  ≈⟨ 𝓡.reflexive ([,]-∘ (λ x → ∑[ o ℕ.+ p ] λ k → x k *ᴿ f k) {g = λ j → padʳ p (A j)} si) ⟩
    ⊎.[ (λ j → ∑ λ k → padʳ p (A j) k *ᴿ f k)
      , (λ j → ∑ λ k → padˡ o (B j) k *ᴿ f k) ]′ si
  ≈⟨ lem₂ ⟩
    ⊎.[ ((λ j → ∑[ o ] λ k → A j k *ᴿ f (k ↑ˡ p)))
      , ((λ j → ∑[ p ] λ k → B j k *ᴿ f (o ↑ʳ k))) ]′ si
  ≡⟨⟩
    (⟦ A ⟧ L.⊕ ⟦ B ⟧ ⟨$⟩ f) i
  ∎
  where open ≈-Reasoning setoid
        lem₁ : ∀ (k : Fin (o ℕ.+ p))
              → (A ⊕′ B) (splitAt m i) (splitAt o k) ≡
              ⊎.[ (λ j l → ⊎.[ A j , (λ _ → 𝓡.0#) ]′ (splitAt o l))
                , (λ j l → ⊎.[ (λ _ → 𝓡.0#) , B j ]′ (splitAt o l))
                ]′ (splitAt m i) k
        lem₁ _ with (splitAt m i)
        ... | (⊎.inj₁ x) = refl
        ... | (⊎.inj₂ x) = refl

        lem₂ : ⊎.[ (λ j → ∑ λ k → padʳ p (A j) k *ᴿ f k)
                 , (λ j → ∑ λ k → padˡ o (B j) k *ᴿ f k) ]′ (splitAt m i)
                ≈ᴿ
               ⊎.[ ((λ j → ∑[ o ] λ k → A j k *ᴿ f (k ↑ˡ p)))
                 , ((λ j → ∑[ p ] λ k → B j k *ᴿ f (o ↑ʳ k))) ]′ (splitAt m i)
        lem₂ with (splitAt m i)
        ... | (⊎.inj₁ x) = padʳ-preserves-∑* o p x A f
        ... | (⊎.inj₂ x) = padˡ-preserves-∑* o p x B f
```

## Copy

```agda
copy : Matrix (m ℕ.+ m) m
copy i j = ⊎.[ id j , id j ]′ (splitAt _ i)

⟦copy⟧ : ⟦ copy {m} ⟧ ≈ L.copy
⟦copy⟧ {ℕ.suc m} {f} {i} with splitAt (ℕ.suc m) i
... | (⊎.inj₁ x) = begin
  ∑ (λ j → 𝕀 j x *ᴿ (f j)) ≈⟨ ∑-cong {n = ℕ.suc m} (λ j → 𝓡.*-congʳ {x = f j} (𝕀-sym j x) ) ⟩
  ∑ (λ j → 𝕀 x j *ᴿ (f j)) ≈⟨ ∑𝕀*f≈fi x f ⟩
  f x                      ∎
  where open ≈-Reasoning setoid
... | (⊎.inj₂ x) = begin
  ∑ (λ j → 𝕀 j x *ᴿ (f j)) ≈⟨ ∑-cong {n = ℕ.suc m} (λ j → 𝓡.*-congʳ {x = f j} (𝕀-sym j x) ) ⟩
  ∑ (λ j → 𝕀 x j *ᴿ (f j)) ≈⟨ ∑𝕀*f≈fi x f ⟩
  f x                      ∎
  where open ≈-Reasoning setoid
```

## Reduce

```agda
reduce : Matrix m (m ℕ.+ m)
reduce {m} i j = id (i ↑ˡ m) j +ᴿ id (m ↑ʳ i) j

⟦reduce⟧ : ∀ {m} → ⟦ reduce {m} ⟧ ≈ L.reduce
⟦reduce⟧ {m} {f} {i} = begin
  ∑ (λ x → (𝕀 (i ↑ˡ m) x +ᴿ 𝕀 (m ↑ʳ i) x) *ᴿ f x)                ≈⟨ ∑-cong {n = m ℕ.+ m} (λ x → 𝓡.distribʳ (f x) _ _)  ⟩
  ∑ (λ x → (𝕀 (i ↑ˡ m) x *ᴿ f x +ᴿ 𝕀 (m ↑ʳ i) x *ᴿ f x) )        ≈⟨ ∑-preserves-+ (λ x → (𝕀 (i ↑ˡ m) x *ᴿ f x)) _ ⟨
  ∑ (λ x → 𝕀 (i ↑ˡ m) x *ᴿ f x) +ᴿ ∑ (λ x → 𝕀 (m ↑ʳ i) x *ᴿ f x) ≈⟨ 𝓡.+-cong (∑𝕀*f≈fi (i ↑ˡ m) f) (∑𝕀*f≈fi (m ↑ʳ i) f) ⟩
  f (i ↑ˡ m) +ᴿ f (m ↑ʳ i)                     ∎
  where open ≈-Reasoning setoid
```

## Fork (Row Concatenation)

```agda
infixr 6 _▵_ _—_
_▵_ : Matrix m o → Matrix n o → Matrix (m ℕ.+ n) o
A ▵ B = (A ⊕ B) × copy

_—_ = _▵_

⟦▵⟧ : ∀ {A : Matrix m o} {B : Matrix n o} → ⟦ A ▵ B ⟧ ≈ ⟦ A ⟧ L.▵ ⟦ B ⟧
⟦▵⟧ {o = o} {A = A} {B} {f} {i} = begin
  (⟦ A ▵ B ⟧ ⟨$⟩ f) i                   ≡⟨⟩
  (⟦ (A ⊕ B) × copy ⟧ ⟨$⟩ f) i          ≈⟨ ⟦×⟧ {A = (A ⊕ B)} {B = copy} {a = f} ⟩
  (⟦ A ⊕ B ⟧ L.∘ ⟦ copy ⟧ ⟨$⟩ f) i      ≈⟨ ∑-cong (λ k → 𝓡.*-congˡ (⟦copy⟧ {a = f} {k})) ⟩
   _                                    ≈⟨ ⟦⊕⟧ {A = A} {B} {a = λ z → ⊎.[ f , f ] (splitAt o z)} {i} ⟩
  ((⟦ A ⟧ L.⊕ ⟦ B ⟧) L.∘ L.copy ⟨$⟩ f) i ≡⟨⟩
  (⟦ A ⟧ L.▵ ⟦ B ⟧ ⟨$⟩ f) i ∎
  where open ≈-Reasoning setoid
```

## Join (Column Concatenation)

```agda
infixr 6 _▿_ _∣_
_▿_ : Matrix m n → Matrix m o → Matrix m (n ℕ.+ o)
A ▿ B = reduce × (A ⊕ B)

_∣_ = _▿_

⟦▿⟧ : ∀ {A : Matrix m n} {B : Matrix m o} → ⟦ A ▿ B ⟧ ≈ ⟦ A ⟧ L.▿ ⟦ B ⟧
⟦▿⟧ {m} {o} {n} {A = A} {B} {f} {i} = begin
  (⟦ A ▿ B ⟧ ⟨$⟩ f) i                      ≡⟨⟩
  (⟦ reduce × (A ⊕ B)  ⟧ ⟨$⟩ f) i          ≈⟨ ⟦×⟧ {m} {A = reduce} {B = (A ⊕ B)} {a = f} ⟩
  (⟦ reduce ⟧ L.∘ ⟦ A ⊕ B ⟧ ⟨$⟩ f) i       ≈⟨ ∑-cong (λ i → 𝓡.*-congˡ (⟦⊕⟧ {A = A} {B} {f} {i})) ⟩
   _                                       ≈⟨ ⟦reduce⟧ {m = m} {x = i} ⟩
  (L.reduce L.∘ (⟦ A ⟧ L.⊕ ⟦ B ⟧) ⟨$⟩ f) i ≡⟨⟩
  (⟦ A ⟧ L.▿ ⟦ B ⟧ ⟨$⟩ f) i ∎
  where open ≈-Reasoning setoid
```

## Projections

```agda
proj₁ : Matrix (m ℕ.+ n) m
proj₁ = id ▵ 𝟘

proj₂ : Matrix (m ℕ.+ n) n
proj₂ = 𝟘 ▵ id

private
  [,]-cong′ : ∀ {A B : Set} {f f′ : A → R}  {g g′ : B → R}
     → ((x : A) → f x ≈ᴿ f′ x)
     → ((x : B) → g x ≈ᴿ g′ x )
     → (x : A ⊎ B) → ⊎.[ f , g ]′ x ≈ᴿ ⊎.[ f′ , g′ ]′ x
  [,]-cong′ f≈f′ g≈g′ (⊎.inj₁ x) = f≈f′ x
  [,]-cong′ f≈f′ g≈g′ (⊎.inj₂ x) = g≈g′ x


⟦proj₁⟧ : ⟦ proj₁ ⟧ ≈ L.proj₁ {m} {n}
⟦proj₁⟧ {m} {n} {f} {i} = begin
  (⟦ proj₁ ⟧ ⟨$⟩ f) i
    ≈⟨ ⟦▵⟧ {A = id} {B = 𝟘} {f} {i} ⟩
  ⊎.[ (λ a → ∑ λ x → 𝕀 a x *ᴿ ⊎.[ f , f ] (splitAt m (x ↑ˡ m)))
    , (λ _ → ∑ (λ x → 0# *ᴿ ⊎.[ f , f ] (splitAt m (m ↑ʳ x)))) ]′
    (splitAt m i)
    ≈⟨  [,]-cong′
        (λ x → ∑𝕀*f≈fi x (⊎.[ f , f ] F.∘ splitAt m F.∘ (_↑ˡ m)))
        (λ x → ∑-preserves-0*f (⊎.[ f , f ] F.∘ splitAt m F.∘ (m ↑ʳ_)))
        (splitAt m i)
      ⟩
  ⊎.[ (λ a → ⊎.[ f , f ] (splitAt m (a ↑ˡ m))) , (λ _ → 0#) ] (splitAt m i) ≡⟨⟩
  (L.proj₁ ⟨$⟩ f) i ∎
  where open ≈-Reasoning setoid

⟦proj₂⟧ : ⟦ proj₂ ⟧ ≈ L.proj₂ {n} {m}
⟦proj₂⟧ {m} {n} {f} {i} = begin
  (⟦ proj₂ ⟧ ⟨$⟩ f) i                       ≈⟨ ⟦▵⟧ {A = 𝟘} {B = id} {f} {i} ⟩
  ⊎.[ ((λ _ → ∑ (λ x → 0# *ᴿ ⊎.[ f , f ] (splitAt m (x ↑ˡ m)))))
    , (λ a → ∑ λ x → 𝕀 a x *ᴿ ⊎.[ f , f ] (splitAt m (m ↑ʳ x))) ]′
    (splitAt n i)
     ≈⟨  [,]-cong′
           (λ x → ∑-preserves-0*f (⊎.[ f , f ] F.∘ splitAt m F.∘ (_↑ˡ m)))
           (λ x → ∑𝕀*f≈fi x (⊎.[ f , f ] F.∘ splitAt m F.∘ (m ↑ʳ_)))
           (splitAt n i)
      ⟩
  ⊎.[ (λ _ → 0#) , (λ b → ⊎.[ f , f ] (splitAt m (m ↑ʳ b))) ] (splitAt n i) ≡⟨⟩
  (L.proj₂ ⟨$⟩ f) i ∎
  where open ≈-Reasoning setoid
```

## Injections

```agda
inj₁ : Matrix m (m ℕ.+ n)
inj₁ = id ▿ 𝟘

inj₂ : Matrix n (m ℕ.+ n)
inj₂ = 𝟘 ▿ id

⟦inj₁⟧ : ⟦ inj₁ ⟧ ≈ L.inj₁ {m} {n}
⟦inj₁⟧ {m} {n} {f} {i} = begin
  (⟦ inj₁ ⟧ ⟨$⟩ f) i
    ≈⟨ ⟦▿⟧ {A = id} {B = 𝟘}  {f} {i} ⟩
  ⊎.[ (λ x → ∑ λ j → 𝕀 x j *ᴿ f (j ↑ˡ n)) , (λ _ → ∑ (λ x → 0# *ᴿ f (m ↑ʳ x))) ]′ (splitAt m (i ↑ˡ m))  +ᴿ
  ⊎.[ (λ x → ∑ λ j → 𝕀 x j *ᴿ f (j ↑ˡ n)) , (λ _ → ∑ (λ x → 0# *ᴿ f (m ↑ʳ x))) ]′ (splitAt m (m ↑ʳ i))
     ≈⟨ 𝓡.+-cong
          ([,]-cong′
              (λ x → ∑𝕀*f≈fi x (f F.∘ (_↑ˡ n)))
              (λ x → ∑-preserves-0*f (f F.∘ (m ↑ʳ_)))
              (splitAt m (i ↑ˡ m)))
          ([,]-cong′
              (λ x → ∑𝕀*f≈fi x (f F.∘ (_↑ˡ n)))
              (λ _ → ∑-preserves-0*f (f F.∘ (m ↑ʳ_)))
              (splitAt m (m ↑ʳ i)))
       ⟩
  ⊎.[ (λ a → f (a ↑ˡ n)) , (λ _ → 0#) ] (splitAt m (i ↑ˡ m)) +ᴿ
  ⊎.[ (λ a → f (a ↑ˡ n)) , (λ _ → 0#) ] (splitAt m (m ↑ʳ i))
    ≡⟨⟩
  (L.inj₁ ⟨$⟩ f) i ∎
  where open ≈-Reasoning setoid

⟦inj₂⟧ : ⟦ inj₂ ⟧ ≈ L.inj₂ {m} {n}
⟦inj₂⟧ {m} {n} {f} {i} = begin
  (⟦ inj₂ ⟧ ⟨$⟩ f) i
    ≈⟨ ⟦▿⟧ {A = 𝟘} {B = id}  {f} {i} ⟩
  ⊎.[ (λ _ → ∑ (λ x → 0# *ᴿ f (x ↑ˡ n))) , (λ x → ∑ λ j → 𝕀 x j *ᴿ f (m ↑ʳ j)) ]′ (splitAt n (i ↑ˡ n))  +ᴿ
  ⊎.[ (λ _ → ∑ (λ x → 0# *ᴿ f (x ↑ˡ n))) , (λ x → ∑ λ j → 𝕀 x j *ᴿ f (m ↑ʳ j)) ]′ (splitAt n (n ↑ʳ i))
     ≈⟨ 𝓡.+-cong
          ([,]-cong′
            (λ _ → ∑-preserves-0*f (f F.∘ (_↑ˡ n)))
            (λ x → ∑𝕀*f≈fi x (f F.∘ (m ↑ʳ_)))
            (splitAt n (i ↑ˡ n)))
          ([,]-cong′
            (λ x → ∑-preserves-0*f (f F.∘ (_↑ˡ n)))
            (λ x → ∑𝕀*f≈fi x (f F.∘ (m ↑ʳ_)))
            (splitAt n (n ↑ʳ i)))
       ⟩
  ⊎.[ (λ _ → 0#) , (λ b → f (m ↑ʳ b)) ] (splitAt n (i ↑ˡ n)) +ᴿ
  ⊎.[ (λ _ → 0#) , (λ b → f (m ↑ʳ b)) ] (splitAt n (n ↑ʳ i))
    ≡⟨⟩
  (L.inj₂ ⟨$⟩ f) i ∎
  where open ≈-Reasoning setoid
```

## Swap

```agda
swap : Matrix (m ℕ.+ n) (n ℕ.+ m)
swap {m} {n} = proj₂ {m} ▿ proj₁

-- TODO: complete homomorphism proof
-- ⟦swap⟧ : ⟦ swap {m} {n} ⟧ ≈ L.swap {m} {n}
-- ⟦swap⟧ {m} {n} {f} {i} with splitAt m i
-- ... | (⊎.inj₁ x) = {!   !}
-- ... | (⊎.inj₂ x) = {!   !}
--   where open ≈-Reasoning setoid
```

## Constructors

### Singleton matrix

```agda
singleton : R → Matrix 1 1
singleton x _ _ = x

⟦singleton⟧ : ∀ x → ⟦ singleton x ⟧ ≈ L.fromScalar x
⟦singleton⟧ x {_} {Fin.zero} = 𝓡.+-identityʳ _
```

### Vector to Row

```agda
row : (Fin m → R) → Matrix m 1
row x i _ = x i

-- another construction more aligned with L.row
row′ : (Fin m → R) → Matrix m 1
row′ {ℕ.zero} _ = 𝟘
row′ {ℕ.suc m} x = singleton (x Fin.zero) ▵ row′ (x F.∘ Fin.suc)

private
  row≈row′ : ∀ x i → row {m} x i zero ≈ᴿ row′ x i zero
  row≈row′ f zero =
    𝓡.trans
      (𝓡.sym (𝓡.+-identityʳ _))
      (𝓡.+-cong
        (𝓡.sym (𝓡.*-identityʳ _))
        (𝓡.trans  (𝓡.sym ( 𝓡.*-identityʳ _)) (𝓡.sym (𝓡.+-identityʳ _))))
  row≈row′ f (suc i) = begin
    f (suc i)                            ≈⟨ row≈row′ (f F.∘ suc) i ⟩
    (row′ (λ x → f (suc x)) i zero)      ≈⟨ 𝓡.+-identityˡ  _ ⟨
    0# +ᴿ row′ (λ x → f (suc x)) i zero  ≈⟨ 𝓡.+-cong
                                              (𝓡.sym (𝓡.zeroˡ _))
                                              (𝓡.trans
                                                (𝓡.sym (𝓡.*-identityʳ _))
                                                (𝓡.sym (𝓡.+-identityʳ _)))
                                          ⟩
    0# *ᴿ 1# +ᴿ (row′ (λ x → f (suc x)) i zero *ᴿ 1# +ᴿ 0#) ∎
    where open ≈-Reasoning setoid

⟦row⟧ : ∀ {m} x → ⟦ row′ {m} x ⟧ ≈ L.vecToRow x
⟦row⟧ {ℕ.suc m} x {f} {i} =  begin
  (⟦ row′ x ⟧ ⟨$⟩ f) i
    ≈⟨ ⟦▵⟧ {A = singleton (x Fin.zero)} {B = row′ (x F.∘ suc)} {f} {i} ⟩
  _
    ≈⟨ [,]-cong′
        (λ { 0F → 𝓡.trans (𝓡.+-identityʳ _) (𝓡.*-congˡ 𝓡.refl) })
        (λ j → ⟦row⟧ (x F.∘ suc) {f} {j})
        (splitAt 1 i) ⟩
  (L.vecToRow x ⟨$⟩ f) i ∎
  where open ≈-Reasoning setoid
```

### Vector to Col

```agda
col : (Fin m → R) → Matrix 1 m
col x _ i = x i

-- another construction more aligned with L.col
col′ : (Fin m → R) → Matrix 1 m
col′ {ℕ.zero} _ = 𝟘
col′ {ℕ.suc m} x = singleton (x Fin.zero) ▿ col′ (x F.∘ Fin.suc)

⟦col⟧ : ∀ {m} x → ⟦ col′ {m} x ⟧ ≈ L.vecToCol x
⟦col⟧ {ℕ.zero} _ = 𝓡.refl
⟦col⟧ {ℕ.suc m} x {f} {i} = begin
  (⟦ col′ x ⟧ ⟨$⟩ f) i
    ≈⟨ ⟦▿⟧ {A = singleton (x Fin.zero)} {B = col′ (x F.∘ suc)} {f} {i} ⟩
  _
    ≈⟨ 𝓡.+-cong
        ([,]-cong′
          (λ {0F → 𝓡.+-identityʳ _})
          (λ {0F → ⟦col⟧ (x F.∘ suc) })
          (splitAt 1 (i ↑ˡ 1)))
        (⟦col⟧ (x F.∘ suc)) ⟩
  (L.vecToCol x ⟨$⟩ f) i ∎
  where open ≈-Reasoning setoid
```

## Transpose

```agda
_ᵀ : Matrix m n → Matrix n m
_ᵀ = flip

⟦ᵀ⟧ : ∀ (x : Matrix m n) → ⟦ x ᵀ ⟧ ≈ ⟦ x ⟧ L.ᵀ
⟦ᵀ⟧ {m} {n} x {f} {i} = begin
  (⟦ x ᵀ ⟧ ⟨$⟩ f) i         ≡⟨⟩
  ∑[ m ] (flip x i ⟨*⟩ f)   ≈⟨ ∑-cong {m} (λ j → 𝓡.*-comm (x j i) (f j)) ⟩
  ∑[ m ] (f ⟨*⟩ flip x i)   ≈⟨ ∑-cong {m} (λ j → 𝓡.*-congˡ (𝓡.sym (∑𝕀*f≈fi i (x j)))) ⟩
  ∑[ m ] (f ⟨*⟩ λ j → ∑[ n ] (𝕀 i ⟨*⟩ x j)) ≈⟨ ∑-cong {m} (λ j → 𝓡.*-congˡ (∑-cong {n} λ k → 𝓡.*-comm (𝕀 i k) (x j k))) ⟩
  ∑[ m ] (f ⟨*⟩ λ j → ∑[ n ] (x j ⟨*⟩ 𝕀 i)) ≡⟨⟩
  (⟦ x ⟧ L.ᵀ ⟨$⟩ f) i ∎
  where open ≈-Reasoning setoid
```

## Element-wise product (Schur/Hadamard Product)

```agda
_⊙_ : Matrix m n → Matrix m n → Matrix m n
_⊙_ = _-⟪ _*ᴿ_ ⟫-_

-- TODO: homomorphism proof
```

## Matrix Direct Product (Kronecker Product)

```agda
_⊗_ : Matrix m n → Matrix o p → Matrix (m ℕ.* o) (n ℕ.* p)
A ⊗ B = λ i j →
  let x = Inverse.to *↔× i
      y = Inverse.to *↔× j
  in A (×.proj₁ x) (×.proj₁ y) *ᴿ B (×.proj₂ x) (×.proj₂ y)

-- TODO: homomoorphism proof
```
