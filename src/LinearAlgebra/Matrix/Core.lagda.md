---
title: Matrices
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Algebra.Bundles using (CommutativeSemiring)

module LinearAlgebra.Matrix.Core {r ℓr} (𝓡 : CommutativeSemiring r ℓr) where

open CommutativeSemiring 𝓡
  hiding (_≈_; isEquivalence)
  renaming (Carrier to R)

-- stdlib
open import Algebra.Module.Construct.Vector
  renaming (finLeftSemimodule to vector)
open import Data.Fin using (Fin)
open import Data.Nat as ℕ using (ℕ)
open import Function using (id; _on_)
open import Relation.Binary using (IsEquivalence ; Setoid)
open import Relation.Binary.Construct.On as On using ()

-- carya
open import LinearSpace.DotProduct.Core 𝓡
open import LinearSpace.DotProduct.Properties.CommutativeSemiring 𝓡
open import Maps.Linear semiring as L
  using (_∘_ ; _*ₗ_; _⊸_; mk⊸)

private
  variable
    m n : ℕ
```

</details>

## Matrix Type

```agda
Matrix : ℕ → ℕ → Set r
Matrix m n = Fin m → Fin n → R
```

## Denotation and Equivalence

```agda
⟦_⟧ : Matrix m n → vector n ⊸ vector m
⟦ M ⟧ =
  mk⊸
    (λ x i → M i · x)
    ((λ x≈y {i} → ·-congˡ (M i) λ {i} → x≈y {i}))
    ((λ {a₁} {a₂} {i} → ·-distribˡ-+ (M i) {a₁} {a₂}))
    λ {r} {a} {i} → *-scale (M i) {r} {a}

_≈_ : Matrix m n → Matrix m n → Set _
_≈_ = L._≈_ on ⟦_⟧

isEquivalence : ∀ {m n} → IsEquivalence (_≈_ {m} {n})
isEquivalence = On.isEquivalence ⟦_⟧ L.isEquivalence
```
