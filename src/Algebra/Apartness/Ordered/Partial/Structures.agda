{-# OPTIONS --cubical-compatible --safe #-}

open import Algebra.Core using (Op₁; Op₂)
open import Relation.Binary.Core using (Rel)

module Algebra.Apartness.Ordered.Partial.Structures
  {c ℓ₁ ℓ₂ ℓ₃} {Carrier : Set c}
  (_≈_ : Rel Carrier ℓ₁)
  (_#_ : Rel Carrier ℓ₂)
  (_≤_ : Rel Carrier ℓ₃)
  (_+_ _*_ : Op₂ Carrier) (-_ : Op₁ Carrier) (0# 1# : Carrier)
  where


open import Level using (_⊔_; suc)
open import Algebra.Definitions _≈_ using (Invertible)
open import Algebra.Apartness.Structures _≈_ _#_
open import Algebra.Ordered.Partial.Structures _≈_ _≤_
open import Relation.Binary.Structures using (IsApartnessRelation)
open import Relation.Binary.Definitions using (Tight)

record IsPoHeytingCommutativeRing : Set (c ⊔ ℓ₁ ⊔ ℓ₂ ⊔ ℓ₃) where

  field
    isPoCommutativeRing : IsPoCommutativeRing _+_ _*_ -_ 0# 1#
    isApartnessRelation : IsApartnessRelation _≈_ _#_

  open IsPoCommutativeRing isPoCommutativeRing public
  open IsApartnessRelation isApartnessRelation public

  field
    #⇒invertible : ∀ {x y} → x # y → Invertible 1# _*_ (x - y)
    invertible⇒# : ∀ {x y} → Invertible 1# _*_ (x - y) → x # y

  isHeytingCommutativeRing : IsHeytingCommutativeRing _+_ _*_ -_ 0# 1#
  isHeytingCommutativeRing = record
    { isCommutativeRing = isCommutativeRing
    ; isApartnessRelation = isApartnessRelation
    ; #⇒invertible = #⇒invertible
    ; invertible⇒# = invertible⇒#
    }


record IsPoHeytingField : Set (c ⊔ ℓ₁ ⊔ ℓ₂ ⊔ ℓ₃) where

  field
    isPoHeytingCommutativeRing : IsPoHeytingCommutativeRing
    tight                    : Tight _≈_ _#_

  open IsPoHeytingCommutativeRing isPoHeytingCommutativeRing public
