{-# OPTIONS --cubical-compatible --safe #-}

module Algebra.Apartness.Ordered.Partial.Bundles where

open import Level using (_⊔_; suc)
open import Relation.Binary.Core using (Rel)
open import Relation.Binary.Bundles using (ApartnessRelation)
open import Algebra.Core using (Op₁; Op₂)
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Apartness.Ordered.Partial.Structures

record PoHeytingCommutativeRing c ℓ₁ ℓ₂ ℓ₃ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂ ⊔ ℓ₃)) where
  infix  8 -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _#_ _≤_
  field
    Carrier                  : Set c
    _≈_                      : Rel Carrier ℓ₁
    _#_                      : Rel Carrier ℓ₂
    _≤_                      : Rel Carrier ℓ₃
    _+_                      : Op₂ Carrier
    _*_                      : Op₂ Carrier
    -_                       : Op₁ Carrier
    0#                       : Carrier
    1#                       : Carrier
    isPoHeytingCommutativeRing :
        IsPoHeytingCommutativeRing _≈_ _#_ _≤_ _+_ _*_ -_ 0# 1#

  open IsPoHeytingCommutativeRing isPoHeytingCommutativeRing public

  poCommutativeRing : PoCommutativeRing c ℓ₁ ℓ₃
  poCommutativeRing = record { isPoCommutativeRing = isPoCommutativeRing }

  apartnessRelation : ApartnessRelation c ℓ₁ ℓ₂
  apartnessRelation = record { isApartnessRelation = isApartnessRelation }

  open PoCommutativeRing poCommutativeRing public
    using
    ( poset
    ; poSemiring
    ; commutativeRing
    ; ring
    ; semiring
    )

record PoHeytingField c ℓ₁ ℓ₂ ℓ₃ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂ ⊔ ℓ₃)) where
  infix  8 -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _#_ _≤_
  field
    Carrier        : Set c
    _≈_            : Rel Carrier ℓ₁
    _#_            : Rel Carrier ℓ₂
    _≤_            : Rel Carrier ℓ₃
    _+_            : Op₂ Carrier
    _*_            : Op₂ Carrier
    -_             : Op₁ Carrier
    0#             : Carrier
    1#             : Carrier
    isPoHeytingField : IsPoHeytingField _≈_ _#_ _≤_ _+_ _*_ -_ 0# 1#

  open IsPoHeytingField isPoHeytingField public

  poHeytingCommutativeRing : PoHeytingCommutativeRing c ℓ₁ ℓ₂ ℓ₃
  poHeytingCommutativeRing = record
    { isPoHeytingCommutativeRing = isPoHeytingCommutativeRing }

  apartnessRelation : ApartnessRelation c ℓ₁ ℓ₂
  apartnessRelation = record { isApartnessRelation = isApartnessRelation }

  open PoHeytingCommutativeRing poHeytingCommutativeRing public
    using
    ( poset
    ; poCommutativeRing
    ; poSemiring
    ; commutativeRing
    ; ring
    ; semiring
    )
