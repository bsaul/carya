---
title: Coarsening by a (congruent) Equivalence Relation
bibliography: resources/references.bib
header-includes:
  - <link rel="stylesheet" href="../resources/style.css">
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Level using (Level ; _⊔_)

module Algebra.Construct.Coarsen {c ℓᶜ ℓ} where

open import Algebra.Bundles
open import Algebra.Definitions using (Congruent₂)
open import Data.Product using (_,_ ; proj₁ ; proj₂)
open import Relation.Binary using (Rel ; IsEquivalence; _⇒_)
```

</details>

Take an existing algebraic structure
and modify it by changing out the equivalence relation.
For structures beyond `Magma`,
the new equivalence relation must respect the old,
hence the term "coarsen".

```agda
magma : (M : Magma c ℓᶜ)
   → (_♯_ : Rel (Magma.Carrier M) ℓ)
   → IsEquivalence _♯_
   → Congruent₂ _♯_ (Magma._∙_ M)
   → Magma c ℓ
magma M _♯_ isEquiv cong = record
  { Carrier = Carrier
  ; _≈_ = _♯_
  ; isMagma = record
       { isEquivalence = isEquiv
       ; ∙-cong = cong
       }
 }
 where open Magma M

semigroup : (S : Semigroup c ℓᶜ)
   → (_♯_ : Rel (Semigroup.Carrier S) ℓ)
   → IsEquivalence _♯_
   → Congruent₂ _♯_ (Semigroup._∙_ S)
   → Semigroup._≈_ S ⇒ _♯_
   → Semigroup c ℓ
semigroup S _♯_ isEquiv cong resp = record
   { isSemigroup = record {
        isMagma = isMagma
      ; assoc = λ x y z → resp (Semigroup.assoc S x y z)
      }
   }
   where open Magma (magma (Semigroup.magma S) _♯_ isEquiv cong)

monoid : (M : Monoid c ℓᶜ)
   → (_♯_ : Rel (Monoid.Carrier M) ℓ)
   → IsEquivalence _♯_
   → Congruent₂ _♯_ (Monoid._∙_ M)
   → Monoid._≈_ M ⇒ _♯_
   → Monoid c ℓ
monoid M _♯_ isEquiv cong resp = record
  { isMonoid = record
    { isSemigroup = isSemigroup
    ; identity = (λ x → resp (proj₁ identity x))
                 , λ x → resp (proj₂ identity x)
    }
  }
   where open Semigroup (semigroup (Monoid.semigroup M) _♯_ isEquiv cong resp)
         identity = Monoid.identity M

commutativeMonoid : (M : CommutativeMonoid c ℓᶜ)
   → (_♯_ : Rel (CommutativeMonoid.Carrier M) ℓ)
   → IsEquivalence _♯_
   → Congruent₂ _♯_ (CommutativeMonoid._∙_ M)
   → CommutativeMonoid._≈_ M ⇒ _♯_
   → CommutativeMonoid c ℓ
commutativeMonoid M _♯_ isEquiv cong resp = record
   { isCommutativeMonoid = record {
          isMonoid = isMonoid
        ; comm = λ x y → resp (CommutativeMonoid.comm M x y) }
  }
  where open Monoid (monoid (CommutativeMonoid.monoid M) _♯_ isEquiv cong resp)
```
