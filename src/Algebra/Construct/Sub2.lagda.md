---
title: Restricting pairs of algebraic structures by a relation
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_; 0ℓ)

module Algebra.Construct.Sub2 where

open import Algebra using (Op₁ ; Op₂)
open import Algebra.Bundles
import Relation.Binary.Construct.On as On
open import Data.Product as × using (∃₂ ; _,_ ; proj₁ ; proj₂; _×_; zip)
open import Function using ( _∘_ ; _on_)
open import Relation.Unary using (Pred)
open import Relation.Binary using (Rel)

open import Relation.DependentOps
open Inj₂

private
   variable
      c₁ c₂ : Level -- bundle carrier levels
      ℓ₁ ℓ₂ : Level -- bundle relation levels
      p  : Level -- predicate on carrier levels
```

</details>

Restrict pairs of `Carrier`s from two algebraic structures by a relation,
transporting the properties of the old structures automatically.

```agda
module _ (𝓜₁ : Magma c₁ ℓ₁) (𝓜₂ : Magma c₂ ℓ₂)
         (R : Magma.Carrier 𝓜₁ → Magma.Carrier 𝓜₂ → Set p) where

  open ∃₂op R

  mkMagma : R₂ (Magma._∙_ 𝓜₁) (Magma._∙_ 𝓜₂) → Magma  (c₁ ⊔ c₂ ⊔ p) (ℓ₁ ⊔ ℓ₂)
  mkMagma _∙′_ = record
      { Carrier = ∃₂ R
      ; _≈_ = λ (a₁ , b₁ , _) (a₂ , b₂ , _) → (a₁ M₁.≈ a₂) × (b₁ M₂.≈ b₂)
      ; _∙_ = ∃inj₂ _∙′_
      ; isMagma = record
         { isEquivalence = record
            { refl = M₁.refl , M₂.refl
            ; sym = ×.map M₁.sym M₂.sym
            ; trans = λ (x₁ , y₁) (x₂ , y₂) → M₁.trans x₁ x₂ , M₂.trans y₁ y₂
            }
         ; ∙-cong = zip M₁.∙-cong M₂.∙-cong
         }
      }
      where module M₁ = Magma 𝓜₁
            module M₂ = Magma 𝓜₂

module _ (𝓜₁ : Semigroup c₁ ℓ₁)
         (𝓜₂ : Semigroup c₂ ℓ₂)
         (R : Semigroup.Carrier 𝓜₁ → Semigroup.Carrier 𝓜₂ → Set p) where

   open ∃₂op R

   mkSemigroup : R₂ (Semigroup._∙_ 𝓜₁) (Semigroup._∙_ 𝓜₂)
                → Semigroup (c₁ ⊔ c₂ ⊔ p) (ℓ₁ ⊔ ℓ₂)
   mkSemigroup _∙′_ = record
      { isSemigroup = record
         { isMagma = isMagma
         ; assoc = prop₃ M₁.assoc M₂.assoc
         }
      }
      where module M₁ = Semigroup 𝓜₁
            module M₂ = Semigroup 𝓜₂
            open Magma (mkMagma M₁.magma M₂.magma R _∙′_)

module _ (𝓜₁ : Monoid c₁ ℓ₁)
         (𝓜₂ : Monoid c₂ ℓ₂)
         (R : Monoid.Carrier 𝓜₁ → Monoid.Carrier 𝓜₂ → Set p)
         where

   open ∃₂op R

   mkMonoid : R₂ (Monoid._∙_ 𝓜₁) (Monoid._∙_ 𝓜₂)
              → R₀ (Monoid.ε 𝓜₁) (Monoid.ε 𝓜₂)
              → Monoid (c₁ ⊔ c₂ ⊔ p) (ℓ₁ ⊔ ℓ₂)
   mkMonoid _∙′_ ε′ = record
      { ε = inj ε′
      ; isMonoid = record
            { isSemigroup = isSemigroup
            ; identity = prop₁ M₁.identityˡ M₂.identityˡ
                       , prop₁ M₁.identityʳ M₂.identityʳ
            }
      }
      where module M₁ = Monoid 𝓜₁
            module M₂ = Monoid 𝓜₂
            open Semigroup (mkSemigroup M₁.semigroup M₂.semigroup R  _∙′_)

module _ (𝓜₁ : CommutativeMonoid c₁ ℓ₁)
         (𝓜₂ : CommutativeMonoid c₂ ℓ₂)
         (R : CommutativeMonoid.Carrier 𝓜₁
              → CommutativeMonoid.Carrier 𝓜₂ → Set p)
         where

   open ∃₂op R

   mkCommutativeMonoid :
        R₂ (CommutativeMonoid._∙_ 𝓜₁) (CommutativeMonoid._∙_ 𝓜₂)
      → R₀ (CommutativeMonoid.ε 𝓜₁) (CommutativeMonoid.ε 𝓜₂)
      → CommutativeMonoid (c₁ ⊔ c₂ ⊔ p) (ℓ₁ ⊔ ℓ₂)
   mkCommutativeMonoid _∙′_ ε′ = record
      { isCommutativeMonoid = record
         { isMonoid = isMonoid
         ; comm = prop₂ M₁.comm M₂.comm
         }
      }
      where module M₁ = CommutativeMonoid 𝓜₁
            module M₂ = CommutativeMonoid 𝓜₂
            open Monoid (mkMonoid M₁.monoid M₂.monoid R _∙′_ ε′)

module _ (𝓜₁ : IdempotentCommutativeMonoid c₁ ℓ₁)
         (𝓜₂ : IdempotentCommutativeMonoid c₂ ℓ₂)
         (R : IdempotentCommutativeMonoid.Carrier 𝓜₁
              → IdempotentCommutativeMonoid.Carrier 𝓜₂ → Set p)
         where

   open ∃₂op R

   mkIdempotentCommutativeMonoid :
        R₂ (IdempotentCommutativeMonoid._∙_ 𝓜₁)
           (IdempotentCommutativeMonoid._∙_ 𝓜₂)
      → R₀ (IdempotentCommutativeMonoid.ε 𝓜₁)
           (IdempotentCommutativeMonoid.ε 𝓜₂)
      → IdempotentCommutativeMonoid  (c₁ ⊔ c₂ ⊔ p) (ℓ₁ ⊔ ℓ₂)
   mkIdempotentCommutativeMonoid _∙′_ ε′ = record
      { isIdempotentCommutativeMonoid = record
         { isCommutativeMonoid = isCommutativeMonoid
         ; idem =  prop₁ M₁.idem M₂.idem
         }
      }
      where
        module M₁ = IdempotentCommutativeMonoid 𝓜₁
        module M₂ = IdempotentCommutativeMonoid 𝓜₂
        open CommutativeMonoid
               (mkCommutativeMonoid
                  M₁.commutativeMonoid M₂.commutativeMonoid R _∙′_ ε′)

module _ (𝓜₁ : NearSemiring c₁ ℓ₁)
         (𝓜₂ : NearSemiring c₂ ℓ₂)
         (R : NearSemiring.Carrier 𝓜₁ → NearSemiring.Carrier 𝓜₂ → Set p)
         where

  open ∃₂op R

  mkNearSemiring :
        R₂ (NearSemiring._+_ 𝓜₁) (NearSemiring._+_ 𝓜₂)
      → R₂ (NearSemiring._*_ 𝓜₁) (NearSemiring._*_ 𝓜₂)
      → R₀ (NearSemiring.0# 𝓜₁) (NearSemiring.0# 𝓜₂)
      → NearSemiring (c₁ ⊔ c₂ ⊔ p) (ℓ₁ ⊔ ℓ₂)
  mkNearSemiring _+′_ _*′_ 0#′ = record
      { _*_ = λ (a₁ , b₁ , x₁) (a₂ , b₂ , x₂)
                → a₁ M₁.* a₂ , b₁ M₂.* b₂ , x₁ *′ x₂
      ; isNearSemiring = record
        { +-isMonoid = +-isMonoid
        ; *-cong = ×.zip M₁.*-cong M₂.*-cong
        ; *-assoc = *-assoc
        ; distribʳ = prop₃ M₁.distribʳ M₂.distribʳ
        ; zeroˡ = prop₁ M₁.zeroˡ M₂.zeroˡ
        }
      }
      where module M₁ = NearSemiring 𝓜₁
            module M₂ = NearSemiring 𝓜₂
            open Monoid (mkMonoid M₁.+-monoid M₂.+-monoid R _+′_ 0#′)
               renaming (isMonoid to +-isMonoid) using ()
            open Semigroup (mkSemigroup M₁.*-semigroup M₂.*-semigroup R _*′_)
               renaming (assoc to *-assoc) using ()

module _ (𝓜₁ : SemiringWithoutOne c₁ ℓ₁)
         (𝓜₂ : SemiringWithoutOne c₂ ℓ₂)
         (R : SemiringWithoutOne.Carrier 𝓜₁
              → SemiringWithoutOne.Carrier 𝓜₂ → Set p)
         where

  open ∃₂op R

  mkSemiringWithoutOne :
       R₂ (SemiringWithoutOne._+_ 𝓜₁) (SemiringWithoutOne._+_ 𝓜₂)
     → R₂ (SemiringWithoutOne._*_ 𝓜₁) (SemiringWithoutOne._*_ 𝓜₂)
     → R₀ (SemiringWithoutOne.0# 𝓜₁) (SemiringWithoutOne.0# 𝓜₂)
     → SemiringWithoutOne (c₁ ⊔ c₂ ⊔ p) (ℓ₁ ⊔ ℓ₂)
  mkSemiringWithoutOne _+′_ _*′_ 0#′ = record
    { _*_ = λ (a₁ , b₁ , x₁) (a₂ , b₂ , x₂) → a₁ M₁.* a₂ , b₁ M₂.* b₂ , x₁ *′ x₂
     ; isSemiringWithoutOne = record
        { +-isCommutativeMonoid = +-isCommutativeMonoid
        ; *-cong = ×.zip M₁.*-cong M₂.*-cong
        ; *-assoc = prop₃ M₁.*-assoc M₂.*-assoc
        ; distrib =
                 prop₃ (proj₁ M₁.distrib) (proj₁ M₂.distrib)
               , prop₃ (proj₂ M₁.distrib) (proj₂ M₂.distrib)
        ; zero =  prop₁ M₁.zeroˡ M₂.zeroˡ , prop₁ M₁.zeroʳ M₂.zeroʳ
        }
    }
    where module M₁ = SemiringWithoutOne 𝓜₁
          module M₂ = SemiringWithoutOne 𝓜₂
          open CommutativeMonoid
                  (mkCommutativeMonoid
                     M₁.+-commutativeMonoid M₂.+-commutativeMonoid
                     R _+′_ 0#′)
               renaming (isCommutativeMonoid to +-isCommutativeMonoid)
               using ()
          open Semigroup
                  (mkSemigroup M₁.*-semigroup M₂.*-semigroup R _*′_)
               renaming (isSemigroup to *-isSemigroup)

module _ (𝓜₁ : SemiringWithoutAnnihilatingZero c₁ ℓ₁)
         (𝓜₂ : SemiringWithoutAnnihilatingZero c₂ ℓ₂)
         (R : SemiringWithoutAnnihilatingZero.Carrier 𝓜₁
            → SemiringWithoutAnnihilatingZero.Carrier 𝓜₂ → Set p)
         where

  private
   module M₁ = SemiringWithoutAnnihilatingZero 𝓜₁
   module M₂ = SemiringWithoutAnnihilatingZero 𝓜₂
  open ∃₂op R

  mkSemiringWithoutAnnihilatingZero :
     R₂ M₁._+_ M₂._+_
   → R₂ M₁._*_ M₂._*_
   → R₀ M₁.0# M₂.0#
   → R₀ M₁.1# M₂.1#
   → SemiringWithoutAnnihilatingZero (c₁ ⊔ c₂ ⊔ p) (ℓ₁ ⊔ ℓ₂)
  mkSemiringWithoutAnnihilatingZero _+′_ _*′_ 0#′ 1#′ = record
    { _*_ = λ (a₁ , b₁ , x₁) (a₂ , b₂ , x₂) → a₁ M₁.* a₂ , b₁ M₂.* b₂ , x₁ *′ x₂
    ; 1# = M₁.1# , M₂.1# , 1#′
    ; isSemiringWithoutAnnihilatingZero = record
        { +-isCommutativeMonoid = +-isCommutativeMonoid
        ; *-cong = ×.zip M₁.*-cong M₂.*-cong
        ; *-assoc = prop₃ M₁.*-assoc M₂.*-assoc
        ; *-identity = *-identity
        ; distrib = prop₃ (proj₁ M₁.distrib) (proj₁ M₂.distrib)
                  , prop₃ (proj₂ M₁.distrib) (proj₂ M₂.distrib)
        }
    } where
            open CommutativeMonoid
                   (mkCommutativeMonoid
                     M₁.+-commutativeMonoid M₂.+-commutativeMonoid R _+′_ 0#′)
               renaming (isCommutativeMonoid to +-isCommutativeMonoid)
               using ()
            open Monoid (mkMonoid M₁.*-monoid M₂.*-monoid R _*′_ 1#′)
               renaming (isMonoid to *-isMonoid ; identity to *-identity)
               using ()

module _ (𝓜₁ : Semiring c₁ ℓ₁)
         (𝓜₂ : Semiring c₂ ℓ₂)
         (R : Semiring.Carrier 𝓜₁ → Semiring.Carrier 𝓜₂ → Set p)
         where

  private
   module M₁ = Semiring 𝓜₁
   module M₂ = Semiring 𝓜₂
  open ∃₂op R

  mkSemiring :
       R₂ M₁._+_ M₂._+_
     → R₂ M₁._*_ M₂._*_
     → R₀ M₁.0# M₂.0#
     → R₀ M₁.1# M₂.1#
     → Semiring (c₁ ⊔ c₂ ⊔ p) (ℓ₁ ⊔ ℓ₂)
  mkSemiring _+′_ _*′_ 0#′ 1#′ = record
    { isSemiring = record
        { isSemiringWithoutAnnihilatingZero = isSemiringWithoutAnnihilatingZero
        ; zero = prop₁ M₁.zeroˡ M₂.zeroˡ , prop₁ M₁.zeroʳ M₂.zeroʳ
        }
    } where open SemiringWithoutAnnihilatingZero
                    (mkSemiringWithoutAnnihilatingZero
                      M₁.semiringWithoutAnnihilatingZero
                      M₂.semiringWithoutAnnihilatingZero
                      R _+′_ _*′_ 0#′ 1#′)

module _ (𝓜₁ : CommutativeSemiring c₁ ℓ₁)
         (𝓜₂ : CommutativeSemiring c₂ ℓ₂)
         (R : CommutativeSemiring.Carrier 𝓜₁
             → CommutativeSemiring.Carrier 𝓜₂ → Set p)
         where

  private
   module M₁ = CommutativeSemiring 𝓜₁
   module M₂ = CommutativeSemiring 𝓜₂
  open ∃₂op R

  commutativeSemiring :
       R₂ M₁._+_ M₂._+_
     → R₂ M₁._*_ M₂._*_
     → R₀ M₁.0# M₂.0#
     → R₀ M₁.1# M₂.1#
     → CommutativeSemiring (c₁ ⊔ c₂ ⊔ p) (ℓ₁ ⊔ ℓ₂)
  commutativeSemiring _+′_ _*′_ 0#′ 1#′ = record
    { isCommutativeSemiring =
        record { isSemiring = isSemiring
               ; *-comm = prop₂ M₁.*-comm M₂.*-comm
               }
    } where open Semiring
                   (mkSemiring M₁.semiring M₂.semiring R _+′_ _*′_ 0#′ 1#′)
```
