---
title: Repeat Direct Product
---

```agda
{-# OPTIONS --cubical-compatible --safe #-}

module Algebra.Construct.Exp {c ℓ} where

open import Algebra.Bundles
open import Data.Nat using (ℕ) ; open ℕ
open import Algebra.Construct.DirectProduct as DP using ()
open import Algebra.Construct.Terminal as 𝟘 using ()
```

```agda
magma : Magma c ℓ → ℕ → Magma c ℓ
magma M zero = 𝟘.magma
magma M (suc n) = DP.magma M (magma M n)

semigroup : Semigroup c ℓ → ℕ → Semigroup c ℓ
semigroup S zero = 𝟘.semigroup
semigroup S (suc n) = DP.semigroup S (semigroup S n)

monoid : Monoid c ℓ → ℕ → Monoid c ℓ
monoid S zero = 𝟘.monoid
monoid S (suc n) = DP.monoid S (monoid S n)

commutativeMonoid : CommutativeMonoid c ℓ → ℕ → CommutativeMonoid c ℓ
commutativeMonoid S zero = 𝟘.commutativeMonoid
commutativeMonoid S (suc n) = DP.commutativeMonoid S (commutativeMonoid S n)

semiring : Semiring c ℓ → ℕ → Semiring c ℓ
semiring S zero = 𝟘.semiring
semiring S (suc n) = DP.semiring S (semiring S n)

commutativeSemiring : CommutativeSemiring c ℓ → ℕ → CommutativeSemiring c ℓ
commutativeSemiring S zero = record { 𝟘.𝕆ne {c = c} {ℓ = ℓ} }
commutativeSemiring S (suc n) = DP.commutativeSemiring S (commutativeSemiring S n)
```
