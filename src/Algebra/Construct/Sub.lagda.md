---
title: Restricting an algebraic structure by a predicate
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_; 0ℓ)

module Algebra.Construct.Sub where

open import Algebra using (Op₁ ; Op₂)
open import Algebra.Bundles
import Relation.Binary.Construct.On as On
open import Data.Product using (∃ ; _,_ ; proj₁ ; proj₂; zip)
open import Function using ( _∘_ ; _on_)
open import Relation.Unary using (Pred)
open import Relation.Binary using (Rel)

open import Relation.DependentOps
open Inj

private
   variable
      c p ℓ : Level
```

</details>

Restrict the `Carrier` of an algebraic structure by a predicate,
transporting the properties of the old structure automatically.

```agda
module _ (M : Magma c ℓ) (P : Magma.Carrier M → Set p) where

  open Magma M
  open ∃op P

  mkMagma : D₂ _∙_ → Magma (c ⊔ p) ℓ
  mkMagma _∙′_ = record
      { Carrier = ∃ P
      ; _≈_ = _≈_ Function.on proj₁
      ; _∙_ = ∃inj₂ _∙′_
      ; isMagma = record
         { isEquivalence = On.isEquivalence proj₁ isEquivalence
         ; ∙-cong =  ∙-cong
         }
      }

module _ (S : Semigroup c ℓ) (P : Semigroup.Carrier S → Set p) where

   open Semigroup S hiding (isMagma)
   open ∃op P

   mkSemigroup : D₂ _∙_ → Semigroup (c ⊔ p) ℓ
   mkSemigroup _∙′_ = record
      { isSemigroup = record
         { isMagma = isMagma
         ; assoc = prop₃ assoc
         }
      }
      where open Magma (mkMagma magma P _∙′_)

module _ (M : Monoid c ℓ) (P : Monoid.Carrier M → Set p) where

   open Monoid M hiding (isSemigroup)
   open ∃op P

   mkMonoid : D₂ _∙_ → D₀ ε → Monoid (c ⊔ p) ℓ
   mkMonoid _∙′_ ε′ = record
      { ε = inj ε′
      ; isMonoid = record
            { isSemigroup = isSemigroup
            ; identity = prop₁ identityˡ , prop₁ identityʳ
            }
      }
      where open Semigroup (mkSemigroup semigroup P  _∙′_)

module _ (M : CommutativeMonoid c ℓ) (P : CommutativeMonoid.Carrier M → Set p) where

   open CommutativeMonoid M hiding (isMonoid)
   open ∃op P

   mkCommutativeMonoid : D₂ _∙_ → D₀ ε → CommutativeMonoid (c ⊔ p) ℓ
   mkCommutativeMonoid _∙′_ ε′ = record
      { isCommutativeMonoid = record
         { isMonoid = isMonoid
         ; comm = prop₂ comm
         }
      }
      where open Monoid (mkMonoid monoid P _∙′_ ε′)

module _ (M : IdempotentCommutativeMonoid c ℓ)
         (P : IdempotentCommutativeMonoid.Carrier M → Set p) where

   open IdempotentCommutativeMonoid M hiding (isCommutativeMonoid)
   open ∃op P

   mkIdempotentCommutativeMonoid : D₂ _∙_ → D₀ ε
      → IdempotentCommutativeMonoid (c ⊔ p) ℓ
   mkIdempotentCommutativeMonoid _∙′_ ε′ = record
      { isIdempotentCommutativeMonoid = record
         { isCommutativeMonoid = isCommutativeMonoid
         ; idem =  prop₁ idem
         }
      }
      where
        open CommutativeMonoid (mkCommutativeMonoid commutativeMonoid P _∙′_ ε′)

module _ (R : NearSemiring c ℓ) (P : NearSemiring.Carrier R → Set p) where

  open NearSemiring R hiding (+-isMonoid; *-assoc)
  open ∃op P

  mkNearSemiring : D₂ _+_ → D₂ _*_ → D₀ 0# → NearSemiring (c ⊔ p) ℓ
  mkNearSemiring _+′_ _*′_ 0#′ = record
      { _*_ = zip _*_ _*′_
      ; isNearSemiring = record
        { +-isMonoid = +-isMonoid
        ; *-cong = *-cong
        ; *-assoc = *-assoc
        ; distribʳ = prop₃ distribʳ
        ; zeroˡ = prop₁ zeroˡ
        }
      }
      where open Monoid (mkMonoid +-monoid P _+′_ 0#′)
               renaming (isMonoid to +-isMonoid) using ()
            open Semigroup (mkSemigroup *-semigroup P _*′_)
               renaming (assoc to *-assoc) using ()

module _ (R : SemiringWithoutOne c ℓ)
         (P : SemiringWithoutOne.Carrier R → Set p) where

  open SemiringWithoutOne R hiding (+-isCommutativeMonoid; *-isSemigroup)
  open ∃op P

  mkSemiringWithoutOne : D₂ _+_ → D₂ _*_ → D₀ 0# → SemiringWithoutOne (c ⊔ p) ℓ
  mkSemiringWithoutOne _+′_ _*′_ 0#′ = record
    { _*_ = zip _*_ _*′_
     ; isSemiringWithoutOne = record
        { +-isCommutativeMonoid = +-isCommutativeMonoid
        ; *-cong = *-cong
        ; *-assoc = prop₃ *-assoc
        ; distrib = prop₃ (proj₁ distrib) , prop₃ (proj₂ distrib)
        ; zero = prop₁ zeroˡ , prop₁ zeroʳ
        }
    }
    where open CommutativeMonoid
                  (mkCommutativeMonoid +-commutativeMonoid P _+′_ 0#′)
               renaming (isCommutativeMonoid to +-isCommutativeMonoid)
               using ()
          open Semigroup (mkSemigroup *-semigroup P _*′_)
               renaming (isSemigroup to *-isSemigroup)

module _ (R : SemiringWithoutAnnihilatingZero c ℓ)
         (P : SemiringWithoutAnnihilatingZero.Carrier R → Set p) where

  open SemiringWithoutAnnihilatingZero R
        hiding (+-isCommutativeMonoid; *-isMonoid; *-identity)
  open ∃op P

  mkSemiringWithoutAnnihilatingZero : D₂ _+_ → D₂ _*_ → D₀ 0# → D₀ 1#
                                    → SemiringWithoutAnnihilatingZero (c ⊔ p) ℓ
  mkSemiringWithoutAnnihilatingZero _+′_ _*′_ 0#′ 1#′ = record
    { _*_ = zip _*_ _*′_
    ; 1# = 1# , 1#′
    ; isSemiringWithoutAnnihilatingZero = record
        { +-isCommutativeMonoid = +-isCommutativeMonoid
        ; *-cong = *-cong
        ; *-assoc = prop₃ *-assoc
        ; *-identity = *-identity
        ; distrib = prop₃ (proj₁ distrib) , prop₃ (proj₂ distrib)
        }
    } where open CommutativeMonoid
                   (mkCommutativeMonoid +-commutativeMonoid P _+′_ 0#′)
               renaming (isCommutativeMonoid to +-isCommutativeMonoid)
               using ()
            open Monoid (mkMonoid *-monoid P _*′_ 1#′)
               renaming (isMonoid to *-isMonoid ; identity to *-identity)
               using ()

module _ (R : Semiring c ℓ) (P : Semiring.Carrier R → Set p) where

  open Semiring R
   hiding (isSemiringWithoutAnnihilatingZero)
   renaming (semiringWithoutAnnihilatingZero to sz)
  open ∃op P

  mkSemiring : D₂ _+_ → D₂ _*_ → D₀ 0# → D₀ 1# → Semiring (c ⊔ p) ℓ
  mkSemiring _+′_ _*′_ 0#′ 1#′ = record
    { isSemiring = record
        { isSemiringWithoutAnnihilatingZero = isSemiringWithoutAnnihilatingZero
        ; zero = prop₁ zeroˡ , prop₁ zeroʳ
        }
    } where open SemiringWithoutAnnihilatingZero
                    (mkSemiringWithoutAnnihilatingZero sz P _+′_ _*′_ 0#′ 1#′)

module _ (R : CommutativeSemiring c ℓ)
         (P : CommutativeSemiring.Carrier R → Set p) where

  open CommutativeSemiring R
   hiding (isSemiring)
   renaming (semiring to sr)
  open ∃op P

  mkCommutativeSemiring : D₂ _+_ → D₂ _*_ → D₀ 0# → D₀ 1#
                        → CommutativeSemiring (c ⊔ p) ℓ
  mkCommutativeSemiring _+′_ _*′_ 0#′ 1#′ = record
    { isCommutativeSemiring =
        record { isSemiring = isSemiring
               ; *-comm = prop₂ *-comm
               }
    } where open Semiring (mkSemiring sr P _+′_ _*′_ 0#′ 1#′)
```
