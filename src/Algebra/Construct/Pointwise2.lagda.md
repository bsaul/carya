---
title: Lifting Algebraic Structures to functions to Algebraic Structures
---

```agda
-- TODO: stdlib v2.1.1 is missing some of the bundles below.
--       constribute them to stdlib and remove this module

{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_)

module Algebra.Construct.Pointwise2 {a : Level} (A : Set a) where

open import Algebra.Bundles
open import Data.Product using (_,_ ; proj₁ ; proj₂)

private
  variable
    c ℓ : Level
```

```agda
mkMagma : Magma c ℓ → Magma (a ⊔ c) (a ⊔ ℓ)
mkMagma M = record
  {  Carrier = A → Carrier
  ; _≈_ = λ f g →  ∀ {x : A} → f x ≈ g x
  ; _∙_ = λ f g x → f x ∙ g x
  ; isMagma = record {
       isEquivalence = record {
            refl = λ {f} {x} → refl {x = f x}
          ; sym = λ x → sym x
          ; trans = λ gx≈hx fx≈gx → trans gx≈hx fx≈gx }
       ; ∙-cong = λ x y → ∙-cong x y }
 }
 where open Magma M

mkSemigroup : Semigroup c ℓ → Semigroup (a ⊔ c) (a ⊔ ℓ)
mkSemigroup S = record
   { isSemigroup = record {
        isMagma = isMagma
      ; assoc = λ f g h {x} → Semigroup.assoc S (f x) (g x) (h x)
      }
   }
   where open Magma (mkMagma (Semigroup.magma S))

mkMonoid : Monoid c ℓ → Monoid (a ⊔ c) (a ⊔ ℓ)
mkMonoid M = record
  { isMonoid = record {
          isSemigroup = isSemigroup
        ; identity = (λ f {x} → proj₁ identity (f x))
                    , λ f {x} → proj₂ identity (f x)
        }
   }
   where open Semigroup (mkSemigroup (Monoid.semigroup M))
         identity = Monoid.identity M

mkCommutativeMonoid : CommutativeMonoid c ℓ → CommutativeMonoid (a ⊔ c) (a ⊔ ℓ)
mkCommutativeMonoid CM = record
   { isCommutativeMonoid = record {
          isMonoid = isMonoid
        ; comm = λ f g {x} → CommutativeMonoid.comm CM (f x) (g x) }
  }
  where open Monoid (mkMonoid (CommutativeMonoid.monoid CM))

mkIdempotentCommutativeMonoid : IdempotentCommutativeMonoid c ℓ
    → IdempotentCommutativeMonoid (a ⊔ c) (a ⊔ ℓ)
mkIdempotentCommutativeMonoid ICM = record
   { isIdempotentCommutativeMonoid = record
        { isCommutativeMonoid = isCommutativeMonoid
        ; idem = λ f {x} → idem (f x)
        }
  }
  where open CommutativeMonoid
               (mkCommutativeMonoid
                  (IdempotentCommutativeMonoid.commutativeMonoid ICM))
        open IdempotentCommutativeMonoid ICM using (idem)

mkGroup : Group c ℓ → Group (a ⊔ c) (a ⊔ ℓ)
mkGroup M = record
  {  isGroup =
      record
        { isMonoid = isMonoid
        ; inverse = (λ f {a} → inverseˡ (f a)) , λ f {a} → inverseʳ (f a)
        ; ⁻¹-cong = λ x {a} → ⁻¹-cong (x {a})
        }
  }
  where open Monoid (mkMonoid (Group.monoid M))
        open Group M using (inverseˡ ; inverseʳ; ⁻¹-cong)


mkAbelianGroup : AbelianGroup c ℓ → AbelianGroup (a ⊔ c) (a ⊔ ℓ)
mkAbelianGroup M = record
  {  isAbelianGroup = record
      { isGroup = isGroup
      ; comm = λ f g → λ {x} → comm (f x) (g x)
      }
  }
  where open Group (mkGroup (AbelianGroup.group M))
        open AbelianGroup M using (comm)

mkSemiringWithoutAnnihilatingZero :
    SemiringWithoutAnnihilatingZero c ℓ
  → SemiringWithoutAnnihilatingZero (a ⊔ c) (a ⊔ ℓ)
mkSemiringWithoutAnnihilatingZero S = record
   { isSemiringWithoutAnnihilatingZero = record
        { +-isCommutativeMonoid = CommutativeMonoid.isCommutativeMonoid M+
        ; *-cong = Monoid.∙-cong M*
        ; *-assoc = Monoid.assoc M*
        ; *-identity = Monoid.identity M*
        ; distrib = (λ f g h → λ {x} → proj₁ distrib (f x) (g x) (h x))
                   , λ f g h → λ {x} → proj₂ distrib (f x) (g x) (h x)
        }
   }
   where open SemiringWithoutAnnihilatingZero S
         M+ = mkCommutativeMonoid +-commutativeMonoid
         M* = mkMonoid *-monoid

mkSemiring : Semiring c ℓ → Semiring (a ⊔ c) (a ⊔ ℓ)
mkSemiring S = record
  { isSemiring = record {
         isSemiringWithoutAnnihilatingZero =
            isSemiringWithoutAnnihilatingZero
       ; zero = (λ f → λ {x} → proj₁ zero (f x))
               , λ f → λ {x} → proj₂ zero (f x)
               }
  }
  where open SemiringWithoutAnnihilatingZero (
              mkSemiringWithoutAnnihilatingZero
                (Semiring.semiringWithoutAnnihilatingZero S))
        zero = Semiring.zero S

mkCommutativeSemiring : CommutativeSemiring c ℓ
                    → CommutativeSemiring (a ⊔ c) (a ⊔ ℓ)
mkCommutativeSemiring S = record
  { isCommutativeSemiring = record {
       isSemiring = isSemiring
      ; *-comm = λ f g {x} → CommutativeSemiring.*-comm S (f x) (g x)
      }
  }
  where open Semiring (mkSemiring (CommutativeSemiring.semiring S))

mkRing : Ring c ℓ → Ring (a ⊔ c) (a ⊔ ℓ)
mkRing R = record
  { isRing = record
    { +-isAbelianGroup = isAbelianGroup
    ; *-cong = λ x y {a} → R.*-cong (x {a}) (y {a})
    ; *-assoc = λ x y z {a} → R.*-assoc (x a) (y a) (z a)
    ; *-identity = (λ x {a} → R.*-identityˡ (x a))
                  , λ x {a} → R.*-identityʳ (x a)
    ; distrib = (λ x y z {a} → R.distribˡ (x a) (y a) (z a))
              , (λ x y z {a} → R.distribʳ (x a) (y a) (z a))
    }
  }
  where module R = Ring R
        open AbelianGroup (mkAbelianGroup R.+-abelianGroup)

mkCommutativeRing : CommutativeRing c ℓ → CommutativeRing (a ⊔ c) (a ⊔ ℓ)
mkCommutativeRing R = record
  { isCommutativeRing = record
     { isRing = isRing
     ; *-comm = λ x y {a} → R.*-comm (x a) (y a)
     }
  }
  where module R = CommutativeRing R
        open Ring (mkRing R.ring)
```
