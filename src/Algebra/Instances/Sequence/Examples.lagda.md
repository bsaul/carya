---
title: Examples of using Algebra bundles of `Sequence`s
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --without-K --safe #-}

module Algebra.Instances.Sequence.Examples where

open import Level using (0ℓ; suc)
open import Algebra.Bundles
open import Relation.Binary.PropositionalEquality using (_≡_ ; refl)
```

</details>

```agda
module ExhibitA where
  open import Algebra.Instances.Sequence
    renaming (semiring to sequence)
  open import Data.Nat.Properties using (+-*-semiring)

  exhibitA : Semiring 0ℓ 0ℓ
  exhibitA = sequence +-*-semiring

  open Semiring exhibitA hiding (refl)

  x₁ : Carrier
  x₁ 0 = 2
  x₁ 1 = 3
  x₁ _ = 0

  x₂ : Carrier
  x₂ 0 = 3
  x₂ 1 = 10
  x₂ _ = 1

  _ : (x₁ * 0#) 1 ≡ 0 ; _ = refl

  _ : (x₁ + x₂) 5 ≡ 1 ; _ = refl
  _ : (x₁ + x₂) 1 ≡ 13 ; _ = refl
  _ : (x₁ * x₂) 1 ≡ 30 ; _ = refl
```

```agda
module ExhibitB where
  open import Algebra.Instances.Sequence
    renaming (semiring to sequence)
  open import Data.Fin.Patterns
  open import Algebra.Instances.Fin using (⊔-⊓-semiring)

  exhibitB : Semiring 0ℓ 0ℓ
  exhibitB = sequence (⊔-⊓-semiring 3)

  open Semiring exhibitB hiding (refl)

  x₁ : Carrier
  x₁ 0 = 3F
  x₁ 1 = 2F
  x₁ _ = 2F

  x₂ : Carrier
  x₂ 0 = 0F
  x₂ 1 = 2F
  x₂ 2 = 3F
  x₂ _ = 1F

  -- * is min
  _ : (x₁ * 0#) 0 ≡ 0F ; _ = refl
  _ : (x₁ * x₂) 1 ≡ 2F ; _ = refl

  -- + is max
  _ : (x₁ + x₂) 0 ≡ 3F ; _ = refl
  _ : (x₁ + 0#) 0 ≡ 3F ; _ = refl
  _ : (x₁ + x₂) 1 ≡ 2F ; _ = refl
```

```agda
module ExhibitC where
  open import Algebra.Instances.Sequence
    renaming (semiring to sequence)
  open import Data.Product.Algebra using (×-⊎-commutativeSemiring)
  open import Data.Fin using (Fin)
  open import Data.Nat using (ℕ)
  open import Data.Bool using (Bool)
  open import Data.Product using (_×_)
  open import Data.Sum using (_⊎_)
  open import Data.Unit.Polymorphic using (⊤)
  open import Data.Empty.Polymorphic using (⊥)

  exhibitC : Semiring (suc 0ℓ) 0ℓ
  exhibitC = sequence
     (CommutativeSemiring.semiring (×-⊎-commutativeSemiring 0ℓ))

  open Semiring exhibitC hiding (refl)

  x₁ : Carrier
  x₁ 0 = Fin 2
  x₁ 1 = ℕ
  x₁ _ = ⊥

  x₂ : Carrier
  x₂ 0 = Bool
  x₂ 1 = Fin 3
  x₂ _ = ⊤

  -- * is ×
  _ : (x₁ * 0#) 1 ≡ (ℕ × ⊥) ; _ = refl
  _ : (x₁ * x₂) 1 ≡ (ℕ × Fin 3) ; _ = refl

  -- + is ⊎
  _ : (1# + x₂) 1 ≡ (⊤ ⊎ Fin 3) ; _ = refl
  _ : (x₁ + x₂) 0 ≡ (Fin 2 ⊎ Bool) ; _ = refl
  _ : (x₁ + x₂) 10 ≡ (⊥ ⊎ ⊤) ; _ = refl
```

