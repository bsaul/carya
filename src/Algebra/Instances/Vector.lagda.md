---
title: Algebra Bundles for `Vector A n`s
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Level using (Level)
open import Data.Nat using (ℕ)

module Algebra.Instances.Vector (n : ℕ) where

open import Data.Fin using (Fin)
```

</details>

```agda
open import Algebra.Construct.Pointwise (Fin n) public
```

## Examples

See the
[`Algebra.Instances.Vector.Examples`](src/Algebra/Instances/Vector/Examples.lagda.md)
module for examples.
