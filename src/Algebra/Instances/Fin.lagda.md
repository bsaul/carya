---
title: Min/Max Algebra of Fin
---

```agda
{-# OPTIONS --safe --without-K #-}

module Algebra.Instances.Fin where

open import Level using (Level ; 0ℓ)
open import Algebra.Bundles
open import Algebra.Construct.NaturalChoice.Base
import Algebra.Construct.NaturalChoice.MinMaxOp as MinMaxOp
open import Data.Fin
open import Data.Fin.Properties
open import Data.Nat as ℕ using (ℕ)
open import Data.Product using (_,_)
open import Relation.Nullary.Decidable
open import Relation.Binary.PropositionalEquality
open import Relation.Binary.Structures using (IsTotalPreorder)
open import Relation.Binary.Bundles using (TotalPreorder)

private
  variable
    n : ℕ
```

```agda
-- min
_⊓_ : Fin n → Fin n → Fin n
zero ⊓ n = zero
suc m ⊓ zero  = zero
suc m ⊓ suc n = suc (m ⊓ n)

-- max
_⊔_ : Fin n → Fin n → Fin n
zero  ⊔ n     = n
suc m ⊔ zero  = suc m
suc m ⊔ suc n = suc (m ⊔ n)
```

```agda
≤-isTotalPreorder : IsTotalPreorder {A = Fin n} _≡_ _≤_
≤-isTotalPreorder = record
  { isPreorder = ≤-isPreorder
  ; total = ≤-total
  }

≤-totalPreorder : ℕ → TotalPreorder _ _ _
≤-totalPreorder n = record
  { isTotalPreorder = ≤-isTotalPreorder {n}
  }

m≤n⇒m⊓n≡m : ∀{x y : Fin n} → x ≤ y → x ⊓ y ≡ x
m≤n⇒m⊓n≡m {x = zero} _ = refl
m≤n⇒m⊓n≡m {x = suc m} {y = suc n} (ℕ.s≤s m≤n) = cong suc (m≤n⇒m⊓n≡m m≤n)

m≥n⇒m⊓n≡n : ∀ {x y : Fin n} → x ≥ y → x ⊓ y ≡ y
m≥n⇒m⊓n≡n {x = zero}  {y = zero}  ℕ.z≤n       = refl
m≥n⇒m⊓n≡n {x = suc m} {y = zero}  ℕ.z≤n       = refl
m≥n⇒m⊓n≡n {x = suc m} {y = suc n} (ℕ.s≤s m≤n) = cong suc (m≥n⇒m⊓n≡n m≤n)

⊓-operator : ∀ {n} → MinOperator (≤-totalPreorder n)
⊓-operator = record
  { x≤y⇒x⊓y≈x = m≤n⇒m⊓n≡m
  ; x≥y⇒x⊓y≈y = m≥n⇒m⊓n≡n
  }

m≤n⇒m⊔n≡n : ∀{x y : Fin n} → x ≤ y → x ⊔ y ≡ y
m≤n⇒m⊔n≡n {x = zero}  _         = refl
m≤n⇒m⊔n≡n {x = suc m} {y = suc n} (ℕ.s≤s m≤n) = cong suc (m≤n⇒m⊔n≡n m≤n)

m≥n⇒m⊔n≡m : ∀{x y : Fin n} → x ≥ y → x ⊔ y ≡ x
m≥n⇒m⊔n≡m {x = zero}  {y = zero}  _       = refl
m≥n⇒m⊔n≡m {x = suc m} {y = zero}  _       = refl
m≥n⇒m⊔n≡m {x = suc m} {y = suc n} (ℕ.s≤s m≥n) = cong suc (m≥n⇒m⊔n≡m m≥n)

⊔-operator : ∀{n} → MaxOperator (≤-totalPreorder n)
⊔-operator = record
  { x≤y⇒x⊔y≈y = m≤n⇒m⊔n≡n
  ; x≥y⇒x⊔y≈x = m≥n⇒m⊔n≡m
  }
```


```agda
private
  module ⊓-⊔-properties n = MinMaxOp (⊓-operator {n}) ⊔-operator

module _ (n : ℕ) where

  open import Algebra.Definitions {A = Fin (ℕ.suc n)} _≡_
  open import Algebra.Structures {A = Fin (ℕ.suc n)} _≡_

  open ⊓-⊔-properties (ℕ.suc n) using (

      ⊓-identityˡ
    ; ⊓-identityʳ

    ; ⊓-isMagma                 -- : IsMagma _⊓_
    ; ⊓-isSemigroup             -- : IsSemigroup _⊓_
    ; ⊓-isCommutativeSemigroup  -- : IsCommutativeSemigroup _⊓_
    ; ⊓-isBand                  -- : IsBand _⊓_
    ; ⊓-isSelectiveMagma        -- : IsSelectiveMagma _⊓_

    ; ⊓-magma                   -- : Magma _ _
    ; ⊓-semigroup               -- : Semigroup _ _
    ; ⊓-band                    -- : Band _ _
    ; ⊓-commutativeSemigroup    -- : CommutativeSemigroup _ _
    ; ⊓-selectiveMagma          -- : SelectiveMagma _ _

    ; ⊔-idem                    -- : Idempotent _⊔_
    ; ⊔-sel                     -- : Selective _⊔_
    ; ⊔-assoc                   -- : Associative _⊔_
    ; ⊔-comm                    -- : Commutative _⊔_

    ; ⊓-idem                    -- : Idempotent _⊓_
    ; ⊓-sel                     -- : Selective _⊓_
    ; ⊓-assoc                   -- : Associative _⊓_
    ; ⊓-comm                    -- : Commutative _⊓_

    ; ⊔-isMagma                 -- : IsMagma _⊔_
    ; ⊔-isSemigroup             -- : IsSemigroup _⊔_
    ; ⊔-isCommutativeSemigroup  -- : IsCommutativeSemigroup _⊔_
    ; ⊔-isBand                  -- : IsBand _⊔_
    ; ⊔-isSelectiveMagma        -- : IsSelectiveMagma _⊔_

    ; ⊔-magma                   -- : Magma _ _
    ; ⊔-semigroup               -- : Semigroup _ _
    ; ⊔-band                    -- : Band _ _
    ; ⊔-commutativeSemigroup    -- : CommutativeSemigroup _ _
    ; ⊔-selectiveMagma          -- : SelectiveMagma _ _

    ; ⊓-distrib-⊔
    )

  -- Properties of _⊓_

  ⊓-identityˡ' : LeftIdentity (fromℕ n) _⊓_
  ⊓-identityˡ' = ⊓-identityˡ ≤fromℕ

  ⊓-identityʳ' : RightIdentity (fromℕ n) _⊓_
  ⊓-identityʳ' = ⊓-identityʳ ≤fromℕ

  ⊓-identity : Identity (fromℕ n) _⊓_
  ⊓-identity = ⊓-identityˡ' , ⊓-identityʳ'

  ⊓-zeroˡ : LeftZero zero _⊓_
  ⊓-zeroˡ _ = refl

  ⊓-zeroʳ : RightZero zero _⊓_
  ⊓-zeroʳ zero    = refl
  ⊓-zeroʳ (suc n) = refl

  ⊓-zero : Zero zero _⊓_
  ⊓-zero = ⊓-zeroˡ , ⊓-zeroʳ

  -- Structures on _⊓_
  ⊓-n-isMonoid : IsMonoid _⊓_ (fromℕ n)
  ⊓-n-isMonoid = record
    { isSemigroup = ⊓-isSemigroup
    ; identity    = ⊓-identity
    }

  ⊓-n-isCommutativeMonoid : IsCommutativeMonoid _⊓_ (fromℕ n)
  ⊓-n-isCommutativeMonoid = record
    { isMonoid = ⊓-n-isMonoid
    ; comm     = ⊓-comm
    }

  -- Bundles on _⊓_
  ⊓-n-monoid : Monoid 0ℓ 0ℓ
  ⊓-n-monoid = record
    { isMonoid = ⊓-n-isMonoid
    }

  ⊓-n-commutativeMonoid : CommutativeMonoid 0ℓ 0ℓ
  ⊓-n-commutativeMonoid = record
    { isCommutativeMonoid = ⊓-n-isCommutativeMonoid
    }

  -- Properties of _⊔_
  ⊔-identityˡ : LeftIdentity zero _⊔_
  ⊔-identityˡ _ = refl

  ⊔-identityʳ : RightIdentity zero _⊔_
  ⊔-identityʳ zero = refl
  ⊔-identityʳ (suc m) = refl

  ⊔-identity : Identity zero _⊔_
  ⊔-identity = ⊔-identityˡ , ⊔-identityʳ

  -- Structures on _⊔_
  ⊔-0-isMonoid : IsMonoid _⊔_ zero
  ⊔-0-isMonoid = record
    { isSemigroup = ⊔-isSemigroup
    ; identity    = ⊔-identity
    }

  ⊔-0-isCommutativeMonoid : IsCommutativeMonoid _⊔_ zero
  ⊔-0-isCommutativeMonoid = record
    { isMonoid = ⊔-0-isMonoid
    ; comm     = ⊔-comm
    }

  -- Bundles on _⊔_
  ⊔-0-monoid : Monoid 0ℓ 0ℓ
  ⊔-0-monoid = record
    { isMonoid = ⊔-0-isMonoid
    }

  ⊔-0-commutativeMonoid : CommutativeMonoid 0ℓ 0ℓ
  ⊔-0-commutativeMonoid = record
    { isCommutativeMonoid = ⊔-0-isCommutativeMonoid
    }

  -- Stuctures on _⊔_ and _⊓_
  ⊔-⊓-isSemiringWithoutOne : IsSemiringWithoutOne _⊔_ _⊓_ zero
  ⊔-⊓-isSemiringWithoutOne = record
    { +-isCommutativeMonoid = ⊔-0-isCommutativeMonoid
    ; *-cong                = cong₂ _⊓_
    ; *-assoc               = ⊓-assoc
    ; distrib               = ⊓-distrib-⊔
    ; zero                  = ⊓-zero
    }

  ⊔-⊓-isCommutativeSemiringWithoutOne
    : IsCommutativeSemiringWithoutOne _⊔_ _⊓_ zero
  ⊔-⊓-isCommutativeSemiringWithoutOne = record
    { isSemiringWithoutOne = ⊔-⊓-isSemiringWithoutOne
    ; *-comm               = ⊓-comm
    }

  ⊔-⊓-isSemiringWithoutAnnihilatingZero :
      IsSemiringWithoutAnnihilatingZero _⊔_ _⊓_ zero (fromℕ n)
  ⊔-⊓-isSemiringWithoutAnnihilatingZero = record
    { +-isCommutativeMonoid = ⊔-0-isCommutativeMonoid
    ; *-cong = cong₂ _⊓_
    ; *-assoc = ⊓-assoc
    ; *-identity = ⊓-identity
    ; distrib = ⊓-distrib-⊔
    }

  ⊔-⊓-isSemiring : IsSemiring _⊔_ _⊓_ zero (fromℕ n)
  ⊔-⊓-isSemiring = record
    { isSemiringWithoutAnnihilatingZero = ⊔-⊓-isSemiringWithoutAnnihilatingZero
    ; zero = ⊓-zero
    }

  ⊔-⊓-isCommutativeSemiring : IsCommutativeSemiring _⊔_ _⊓_ zero (fromℕ n)
  ⊔-⊓-isCommutativeSemiring = record
    { isSemiring = ⊔-⊓-isSemiring
    ; *-comm = ⊓-comm
    }

  -- Bundles on _⊔_ and _⊓_
  ⊔-⊓-commutativeSemiringWithoutOne : CommutativeSemiringWithoutOne 0ℓ 0ℓ
  ⊔-⊓-commutativeSemiringWithoutOne = record
    { isCommutativeSemiringWithoutOne =
        ⊔-⊓-isCommutativeSemiringWithoutOne
    }

  ⊔-⊓-semiring : Semiring 0ℓ 0ℓ
  ⊔-⊓-semiring = record
    { isSemiring = ⊔-⊓-isSemiring
    }

  ⊔-⊓-commutativeSemiring : CommutativeSemiring 0ℓ 0ℓ
  ⊔-⊓-commutativeSemiring = record
    { isCommutativeSemiring = ⊔-⊓-isCommutativeSemiring
    }
```
