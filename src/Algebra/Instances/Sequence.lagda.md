---
title: Algebra Bundles for Sequences (Functions from `ℕ`)
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Level using (Level)

module Algebra.Instances.Sequence where

open import Data.Nat using (ℕ)
```

</details>

```agda
open import Algebra.Construct.Pointwise ℕ public
```

## Examples

See the
[`Algebra.Instances.Sequence.Examples`](src/Algebra/Instances/Sequence/Examples.lagda.md)
module for examples.
