---
title: Examples of using Algebra bundles of `Vector A n`s
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --without-K --safe #-}

module Algebra.Instances.Vector.Examples where

open import Data.Fin using (Fin) ; open Fin
open import Level using (0ℓ; suc)
open import Algebra.Bundles
open import Data.Fin.Patterns
open import Relation.Binary.PropositionalEquality using (_≡_ ; refl)
```

</details>

```agda
module ExhibitA where
  open import Algebra.Instances.Vector
  open import Data.Nat.Properties using (+-*-semiring)

  exhibitA : Semiring 0ℓ 0ℓ
  exhibitA = (semiring 2) +-*-semiring

  open Semiring exhibitA hiding (refl)

  x₁ : Carrier
  x₁ 0F = 2
  x₁ 1F = 3

  x₂ : Carrier
  x₂ 0F = 3
  x₂ 1F = 10

  _ : (x₁ * 0#) 0F ≡ 0
  _ = refl

  _ : (x₁ + x₂) 0F ≡ 5
  _ = refl

  _ : (x₁ + x₂) 1F ≡ 13
  _ = refl

  _ : (x₁ * x₂) 1F ≡ 30
  _ = refl
```

```agda
module ExhibitB where
  open import Algebra.Instances.Vector
  open import Algebra.Instances.Fin using (⊔-⊓-semiring)

  exhibitB : Semiring 0ℓ 0ℓ
  exhibitB = (semiring 2) (⊔-⊓-semiring 3)

  open Semiring exhibitB hiding (refl)

  x₁ : Carrier
  x₁ 0F = 3F
  x₁ 1F = 2F

  x₂ : Carrier
  x₂ 0F = 1F
  x₂ 1F = 3F

  -- * is min
  _ : (x₁ * 0#) 0F ≡ 0F
  _ = refl

  -- + is max
  _ : (x₁ + x₂) 0F ≡ 3F
  _ = refl

  _ : (x₁ + x₂) 1F ≡ 3F
  _ = refl

  _ : (x₁ * x₂) 1F ≡ 2F
  _ = refl
```

```agda
module ExhibitC where
  open import Algebra.Instances.Vector
  open import Data.Product.Algebra using (×-⊎-commutativeSemiring)
  open import Data.Nat using (ℕ)
  open import Data.Bool using (Bool)
  open import Data.Product using (_×_)
  open import Data.Sum using (_⊎_)
  open import Data.Unit.Polymorphic using (⊤)
  open import Data.Empty.Polymorphic using (⊥)

  exhibitC : Semiring (Level.suc 0ℓ) 0ℓ
  exhibitC = (semiring 2)
     (CommutativeSemiring.semiring (×-⊎-commutativeSemiring 0ℓ))

  open Semiring exhibitC hiding (refl)

  x₁ : Carrier
  x₁ 0F = Fin 2
  x₁ 1F = ℕ

  x₂ : Carrier
  x₂ 0F = Bool
  x₂ 1F = Fin 3

  -- * is ×
  _ : (x₁ * 0#) 0F ≡ (Fin 2 × ⊥)
  _ = refl

  -- + is ⊎
  _ : (1# + x₂) 1F ≡ (⊤ ⊎ Fin 3)
  _ = refl

  _ : (x₁ + x₂) 0F ≡ (Fin 2 ⊎ Bool)
  _ = refl

  _ : (x₁ + x₂) 1F ≡ (ℕ ⊎ Fin 3)
  _ = refl

  _ : (x₁ * x₂) 1F ≡ (ℕ × Fin 3)
  _ = refl
```
