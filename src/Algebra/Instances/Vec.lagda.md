---
title: Algebra Bundles for `Vec` concatenation
---

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Level using (Level)
open import Algebra.Bundles

module Algebra.Instances.Vec
    {r ℓʳ : Level}
    (𝓡 : Semiring r ℓʳ)
    where

open import Data.Nat using (_+_)
open import Data.Nat.Properties using (+-assoc; +-identityʳ; +-comm)
open import Data.Vec using (Vec; _++_);  open Vec
open import Data.Product using (∃; _,_ ; _×_)
open import Relation.Binary.PropositionalEquality
    using (_≡_; refl ; sym; trans ; cong₂)

open Semiring 𝓡
    renaming (Carrier to R; refl to reflᴿ)
    using (setoid)

open import Data.Vec.Relation.Binary.Equality.Setoid setoid
```

```agda
++-magma : Magma _ _
++-magma = record
    { Carrier = ∃ (Vec R)
    ; _≈_ = λ (n₁ , v₁) (n₂ , v₂) → n₁ ≡ n₂ × v₁ ≋ v₂
    ; _∙_ = λ (_ , v₁) (_ , v₂) → _ , v₁ ++ v₂
    ; isMagma = record
        { isEquivalence = record
            { refl = refl , ≋-refl
            ; sym = λ (n , v) → sym n , ≋-sym v
            ; trans = λ (n₁ , v₁) (n₂ , v₂) → trans n₁ n₂ , ≋-trans v₁ v₂
            }
        ; ∙-cong = λ (x≡y , v₁≋v₂) (u≡v , v₃≋v₄)
                        → cong₂ _+_ x≡y u≡v , ++⁺ v₁≋v₂ v₃≋v₄
        }
    }

++-semigroup : Semigroup _ _
++-semigroup = record
    { isSemigroup = record
        { isMagma = isMagma
        ; assoc = λ (n₁ , v₁) (n₂ , v₂) (n₃ , v₃)
                    → +-assoc n₁ n₂ n₃ , ++-assoc v₁ v₂ v₃
        }
    }
    where open Magma ++-magma

++-monoid : Monoid _ _
++-monoid = record
    { ε = _ , []
    ; isMonoid = record
        { isSemigroup = isSemigroup
        ; identity = (λ (n , v) → refl , ++-identityˡ v)
                    , λ (n , v) → +-identityʳ n , ++-identityʳ v
        }
    }
    where open Semigroup ++-semigroup using (isSemigroup)
```
