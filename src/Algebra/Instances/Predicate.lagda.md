---
title: Algebra on Predicates
---

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Level using (Level ; suc; _⊔_)

module Algebra.Instances.Predicate {a ℓ : Level} (A : Set a) where

open import Algebra.Construct.Pointwise2 A as F using ()
open import Algebra.Bundles
open import Data.Product.Algebra as ×
open import Data.Sum.Algebra as ⊎
```

## Bundles on `_∪_`

```agda
∪-magma : Magma (a ⊔ suc ℓ) (a ⊔ ℓ)
∪-magma = F.mkMagma (⊎-magma ℓ)

∪-semigroup : Semigroup( a ⊔ suc ℓ) (a ⊔ ℓ)
∪-semigroup = F.mkSemigroup (⊎-semigroup ℓ)

∪-monoid : Monoid (a ⊔ suc ℓ) (a ⊔ ℓ)
∪-monoid = F.mkMonoid (⊎-monoid ℓ)

∪-commutativeMonoid : CommutativeMonoid (a ⊔ suc ℓ) (a ⊔ ℓ)
∪-commutativeMonoid = F.mkCommutativeMonoid (⊎-commutativeMonoid ℓ)
```

## Bundles on `_∩_`

```agda
∩-magma : Magma (a ⊔ suc ℓ) (a ⊔ ℓ)
∩-magma = F.mkMagma (×-magma ℓ)

∩-semigroup : Semigroup (a ⊔ suc ℓ) (a ⊔ ℓ)
∩-semigroup = F.mkSemigroup (×-semigroup ℓ)

∩-monoid : Monoid (a ⊔ suc ℓ) (a ⊔ ℓ)
∩-monoid = F.mkMonoid (×-monoid ℓ)

∩-commutativeMonoid : CommutativeMonoid (a ⊔ suc ℓ) (a ⊔ ℓ)
∩-commutativeMonoid = F.mkCommutativeMonoid (×-commutativeMonoid ℓ)
```

## Bundles on `_∪_` and `_∩_`

```agda
-- Semiring bundle not ℓurrently defined in Data.Product.Algebra ¯\_(ツ)_/¯
∩-∪-semiring : Semiring (a ⊔ suc ℓ) (a ⊔ ℓ)
∩-∪-semiring = F.mkSemiring  (record { isSemiring = ⊎-×-isSemiring ℓ })

∩-∪-commutativeSemiring : CommutativeSemiring (a ⊔ suc ℓ) (a ⊔ ℓ)
∩-∪-commutativeSemiring = F.mkCommutativeSemiring (×-⊎-commutativeSemiring ℓ)
```
