{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; suc; _⊔_)
open import Algebra.Structures using (IsUnitalMagma)
open import Relation.Binary using (Setoid; Rel)

module Algebra.Affine.Core {x ℓ c ℓᶜ}
  (𝓧 : Setoid x ℓ)
  {M : Set c}
  {_≈ᴹ_ : Rel M ℓᶜ}
  {_∙_ : M → M → M}
  {ε : M}
  {𝓜 : IsUnitalMagma _≈ᴹ_ _∙_ ε}
  where

open Setoid 𝓧 renaming (Carrier to X)

open IsUnitalMagma 𝓜
 renaming
   ( sym to symᴹ
   ; refl to reflᴹ
   ; trans to transᴹ
   ; setoid to setoidᴹ
   )

open import Algebra.Definitions using (Congruent₁)
open import Data.Product as × using (_,_; ∃!; proj₁ ; proj₂)
open import Function
  using
    ( _∘_
    ; flip
    ; Bijective
    ; Injective
    ; Surjective
    ; Inverse
    ; Bijection
    )
open import Function.Properties.Bijection using (Bijection⇒Inverse)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

{-
An *Affinity* is a congruent operation called "+" of type X → M → X
and a point in X called "o" such that
`o +_` is a bijection.
In essense, an *Affinity* is an origin.
-}
record Affinity (_+_ : X → M → X) (o : X) : Set (x ⊔ ℓ ⊔ c ⊔ ℓᶜ) where
  constructor mk
  field
    +-cong : ∀ {x} {m m′} → o ≈ x → m ≈ᴹ m′ → o + m ≈ x + m′
    hasAffinity : Bijective _≈ᴹ_ _≈_ (o +_)

  -- for all a : A
  -- there exists a unique M, call it a-o,
  -- such that a ≈ o + a-o
  -- This is property 5 here:
  -- https://en.wikipedia.org/wiki/Affine_space#Definition
  hasUniqueSubtraction : ∀ (x : X) → ∃! _≈ᴹ_ λ x–o → x ≈ (o + x–o)
  hasUniqueSubtraction x =
    ×.map₂
       (λ f →
            sym (f reflᴹ)
          , λ x'≈o·x → proj₁ hasAffinity (trans (f reflᴹ) x'≈o·x))
       (proj₂ hasAffinity x)

  –_ : X → M
  –_ = proj₁ ∘ proj₂ hasAffinity

  bijection : Bijection setoidᴹ 𝓧
  bijection = record { cong = +-cong refl ; bijective = hasAffinity }

  inverse : Inverse setoidᴹ 𝓧
  inverse = Bijection⇒Inverse bijection

  +-congʳ : ∀ {o′} {m} → o ≈ o′ → o + m ≈ o′ + m
  +-congʳ x = +-cong x reflᴹ

  +-congˡ : ∀  {m m′} → m ≈ᴹ m′ → o + m ≈ o + m′
  +-congˡ = +-cong refl

  –-cong : Function.Congruent _≈_ _≈ᴹ_ –_
  –-cong = Inverse.from-cong inverse

  selfInverse : o ≈ o + (– o)
  selfInverse = proj₁ (proj₂ (hasUniqueSubtraction o))

{-
The predicate `IsAffine`
determines that (_+_ : X → M → X)
is affine per the definition here:
https://en.wikipedia.org/wiki/Affine_space#Definition
-}
record IsAffine (_+_ : X → M → X) : Set (x ⊔ ℓ ⊔ c ⊔ ℓᶜ) where
  field
    +-identityʳ : ∀ x → (x + ε) ≈ x
    assoc : ∀ x c₁ c₂ → ((x + c₁) + c₂) ≈ (x + (c₁ ∙ c₂))
    affine : ∀ (x : X) → Affinity _+_ x

  affinity : ∀ (x : X) → Bijective _≈ᴹ_ _≈_ (x +_)
  affinity = Affinity.hasAffinity ∘ affine

  _—_ : X → X → M
  x₂ — x₁ = – x₂
    where open Affinity (affine x₁)

  —-congʳ : ∀ {a b b′} → b ≈ b′ → (b — a) ≈ᴹ (b′ — a)
  —-congʳ {a} b≈b′ = –-cong b≈b′
     where open Affinity (affine a)

  -- TODO when/if needed
  -- —-congˡ : ∀ {b a a′} → a ≈ a′ → (b — a) ≈ᴹ (b — a′)
  -- —-congˡ {b} {a} {a′} a≈a′ =
  --   begin
  --     b — a
  --   ≈⟨ {!  !} ⟩
  --     b — a′
  --   ∎
  --   where open Affinity (affine a)
  --         open ≈-Reasoning (setoidᴹ)

  ε≈a—a : ∀ {x} → ε ≈ᴹ (x — x)
  ε≈a—a {x} = symᴹ (proj₂ (proj₂ (hasUniqueSubtraction x)) (sym (+-identityʳ x)))
    where open Affinity (affine x)

