{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; suc; _⊔_)
open import Algebra.Structures using (IsInvertibleUnitalMagma)
open import Relation.Binary using (Setoid; Rel)

module Algebra.Affine.Properties {x ℓ c ℓᶜ}
  (𝓧 : Setoid x ℓ)
  {M : Set c}
  {_+_ : Setoid.Carrier 𝓧 → M → Setoid.Carrier 𝓧}
  {_≈ᴹ_ : Rel M ℓᶜ}
  {_∙_ : M → M → M}
  {ε : M} {_⁻¹ : M → M }
  where

open Setoid 𝓧 renaming (Carrier to X)

open import Algebra.Affine.Core
  𝓧 {_≈ᴹ_ = _≈ᴹ_} {_∙_ = _∙_} {ε = ε}

open import Data.Product
open import Function
  using
    ( _∘_
    ; flip
    ; Bijective
    ; Injective
    ; Surjective
    ; Inverse
    )

import Relation.Binary.Reasoning.Setoid as ≈-Reasoning


module _
   {𝓜 : IsInvertibleUnitalMagma _≈ᴹ_ _∙_ ε _⁻¹}
   {isAffine : IsAffine {𝓜 = IsInvertibleUnitalMagma.isUnitalMagma 𝓜} _+_}
   where

  open IsInvertibleUnitalMagma 𝓜
    renaming
      ( sym to symᴹ
      ; refl to reflᴹ
      ; trans to transᴹ
      ; setoid to setoidᴹ
      ; inverse to inverseᴹ
      )

  open IsAffine isAffine

  _∔_ : M → X → X
  _∔_ = flip _+_

  translate-injective : ∀ (m : M) → Injective _≈_ _≈_ (_+ m)
  translate-injective m {x} {x′} x+m≈x′+m =
    begin
      x
    ≈˘⟨ +-identityʳ x ⟩
      x + ε
    ≈˘⟨ Affinity.+-congˡ (affine x) (inverseʳ m)  ⟩
      x + (m ∙ (m ⁻¹))
    ≈˘⟨ assoc x m (m ⁻¹) ⟩
      (x + m) + (m ⁻¹)
    ≈⟨ Affinity.+-congʳ (affine (x + m)) x+m≈x′+m ⟩
      (x′ + m) + (m ⁻¹)
    ≈⟨ assoc x′ m (m ⁻¹) ⟩
      (x′ + (m ∙ (m ⁻¹)))
    ≈⟨ Affinity.+-congˡ (affine x′) (inverseʳ m) ⟩
      x′ + ε
    ≈⟨ +-identityʳ x′ ⟩
      x′
    ∎
     where open ≈-Reasoning 𝓧

  translate-surjective : ∀ (m : M) → Surjective _≈_ _≈_ (_+ m)
  translate-surjective m x =
       (x + (m ⁻¹))
     , λ {x′} x′≈∙xm⁻¹ →
      begin
        x′ + m
      ≈⟨ Affinity.+-congʳ (affine x′) x′≈∙xm⁻¹ ⟩
        (x + (m ⁻¹)) + m
      ≈⟨ assoc x (m ⁻¹) m ⟩
        x + ((m ⁻¹) ∙ m)
      ≈⟨ Affinity.+-congˡ (affine x) (inverseˡ m) ⟩
        x + ε
      ≈⟨ +-identityʳ x ⟩
        x
      ∎
     where open ≈-Reasoning 𝓧

  -- This is property 4 here:
  -- https://en.wikipedia.org/wiki/Affine_space#Definition
  Translative : ∀ (m : M) → Bijective _≈_ _≈_ (_+ m)
  Translative m = translate-injective m , translate-surjective m

  -- Subtraction and Weyl's axioms
  -- https://en.wikipedia.org/wiki/Affine_space#Subtraction_and_Weyl's_axioms
  -- https://proofwiki.org/wiki/Definition:Affine_Space/Weyl%27s_Axioms
  weyl₁ : ∀ (x : X) (m : M) → ∃! _≈_ λ x′ → (x′ — x) ≈ᴹ m
  weyl₁ x m =
      x + m
    , Inverse.inverseʳ inverse refl
    , Inverse.inverseˡ inverse ∘ symᴹ
    where open Affinity (affine x)

  module _ {x : X} where
    open Affinity (affine x)

    —-distrib : ∀ (y : X) → (– (x + (– y))) ≈ᴹ (y — x)
    —-distrib y = —-congʳ (sym (proj₁ (proj₂ (hasUniqueSubtraction y))))


  module _ {a b : X} where
    open Affinity (affine a) renaming (–_ to –ᵃ_)
    open Affinity (affine b) renaming (–_ to –ᵇ_)

    -- TODO
    -- weyl₂ : ∀ (a b c : X) → ((c — b) ∙ (b — a)) ≈ᴹ (c — a)
    -- weyl₂ a b c =
    --   begin
    --     (c — b) ∙ (b — a)
    --   ≈⟨ {!   !} ⟩
    --     c — a
    --   ∎
    --   where open ≈-Reasoning setoidᴹ
