{-
Relationships between modules (vector spaces)
-}

{-# OPTIONS --safe --without-K #-}

open import Algebra.Bundles using (Semiring)

module Algebra.Module.Properties.Related
  {r ℓr}
  {𝓡 : Semiring r ℓr}
  where

open import Algebra.Module.Bundles
open import Algebra.Module.Construct.TensorUnit as TU using ()
open import Algebra.Module.Construct.Exp renaming (leftSemimodule to _^_)
open import Algebra.Module.Construct.Vector

open import Data.Fin using (Fin) ; open Fin
open import Data.Nat using (ℕ; NonZero)
open import Data.Product
open import Data.Unit.Polymorphic
open import Function using (Inverse; _∘_)
open Inverse


{-
LeftSemimodule of (Fin n → R) (Vector construct)
is isomorphic to
LeftSemimodule of Rⁿ (Exp construct)

BS 20240722 - I think this is a special case of
Claim 6.3 in @Aluffi2021
(namely the finite case)
-}

vec[_]≃Rⁿ : (n : ℕ) → Set _
vec[ n ]≃Rⁿ = Inverse M.≈ᴹ-setoid N.≈ᴹ-setoid
   where module M = LeftSemimodule (finLeftSemimodule {𝓡 = 𝓡} n)
         module N = LeftSemimodule (TU.leftSemimodule {R = 𝓡} ^ n)

vecn≃Rⁿ : (n : ℕ) → vec[ n ]≃Rⁿ
vecn≃Rⁿ ℕ.zero = record
  { to = λ _ → tt
  ; from = λ _ ()
  ; to-cong = λ _ → tt
  ; from-cong = λ { _ {()} }
  ; inverse =
      (λ _ → tt)
    , λ { _ {()}}
  }
vecn≃Rⁿ (ℕ.suc n) = record
  { to = λ f → f zero , to (vecn≃Rⁿ n) (f ∘ suc)
  ; from =
    λ { (R , _)  zero → R
      ; ( _ , LM) (suc i) → (from (vecn≃Rⁿ n) LM) i
      }
  ; to-cong = λ x → x {zero} , to-cong (vecn≃Rⁿ n) λ {i} → x {suc i}
  ; from-cong =
    λ { (x , _) {zero} → x
      ; (_ , y) {suc i} → from-cong (vecn≃Rⁿ n) y { i } }
  ; inverse =
       (λ x → (x { zero }) , (inverseˡ (vecn≃Rⁿ n) x))
     , λ { (x , _) {zero} → x
         ; (_ , y) { suc i} → inverseʳ (vecn≃Rⁿ n) y }
  }
