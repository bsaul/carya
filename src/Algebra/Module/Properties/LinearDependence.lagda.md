---
title: Linearly Dependence within a module
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --cubical-compatible --safe #-}

open import Algebra.Bundles using (Semiring)
open import Algebra.Module.Bundles using (LeftSemimodule)

module Algebra.Module.Properties.LinearDependence
  {r ℓʳ a ℓᵃ}
  {𝓡 : Semiring r ℓʳ}
  (𝓜ᴬ : LeftSemimodule 𝓡 a ℓᵃ)
  where

open Semiring 𝓡
open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A)

open import Data.Product using (∃ ; ∃₂; _,_)
open import Data.Sum using (_⊎_)
open import Level using (_⊔_)
open import Relation.Nullary.Negation using (¬_)
```

</details>

Two vectors are linearly *dependent*
if there exist two scalars
-- at least one of which is not zero --
such that the scaled sum is equal to zero.

```agda
-- LinearlyDependent : A → A → Set _
-- LinearlyDependent x y =
--   ∃₂ λ c₁ c₂ → (c₁ ≉ 0#) ⊎ (c₂ ≉ 0#) → c₁ *ₗ x +ᴹ c₂ *ₗ y ≈ᴹ 0ᴹ
```

This can also be stated
[as follows](https://en.wikipedia.org/wiki/Linear_independence#Linear_dependence_and_independence_of_two_vectors):

```agda
LinearlyDependent : A → A → Set (r ⊔ ℓᵃ)
LinearlyDependent x y = ∃ λ c → c *ₗ x ≈ᴹ y ⊎ c *ₗ y ≈ᴹ x

-- TODO: Prove the equivalence of this relation
--       and the one above.
--       It seems like the equivalence requires subtraction and division
--       For example if c₁ is not 0,
--       then c₁ *ₗ x +ᴹ c₂ *ₗ y ≈ᴹ 0ᴹ
--            ==> c₁ *ₗ x ≈ᴹ - (c₂ *ₗ y)
--            ==> x ≈ᴹ - (c₂ *ₗ y) / c₁
```

Two vectors are linearly *independent*
if they are not linearly dependent.

```agda
LinearlyIndependent : A → A → Set (r ⊔ ℓᵃ)
LinearlyIndependent x y = ¬ (LinearlyDependent x y)
```
