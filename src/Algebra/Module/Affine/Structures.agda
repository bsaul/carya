{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; suc; _⊔_)
open import Relation.Binary using (Rel ; IsEquivalence)

module Algebra.Module.Affine.Structures
  {x ℓ} {X : Set x}
  (_≈ˣ_ : Rel X ℓ)
  where

open import Algebra.Bundles
open import Algebra.Core
import Algebra.Definitions as Defs
open import Algebra.Structures
open import Algebra.Module.Core
open import Algebra.Module.Definitions
open import Algebra.Module.Structures
open import Algebra.Affine.Core as Aff

private
  variable
    m ℓm r ℓr s ℓs : Level
    M : Set m

module _ (semiring : Semiring r ℓr)
         (≈ᴹ : Rel {m} M ℓm) (+ᴹ : Op₂ M) (0ᴹ : M)
  where

  open Semiring semiring renaming (Carrier to R)

  record IsAffineLeftSemimodule (+ˣ : X → M → X) (*ₗ : Opₗ R M)
      : Set (x ⊔ r ⊔ m ⊔ ℓ ⊔ ℓr ⊔ ℓm) where
    field
      isLeftSemimodule : IsLeftSemimodule semiring ≈ᴹ +ᴹ 0ᴹ *ₗ
      ≈ˣ-isEquivalence : IsEquivalence _≈ˣ_
      isAffine : IsAffine (record {isEquivalence = ≈ˣ-isEquivalence })
                   {𝓜 = IsMonoid.isUnitalMagma
                          (IsLeftSemimodule.+ᴹ-isMonoid isLeftSemimodule)}
                   +ˣ

    open IsLeftSemimodule isLeftSemimodule public
    open IsAffine isAffine public

    +ˣ-cong : ∀ (x : X) {x′ : X} {m m′ : M}
              → x ≈ˣ x′ → ≈ᴹ m m′ → +ˣ x m ≈ˣ +ˣ x′ m′
    +ˣ-cong x = Affinity.+-cong (affine x)

    +ˣ-congʳ : ∀ (x : X) {x′ : X} {m} → x ≈ˣ x′ → +ˣ x m ≈ˣ +ˣ x′ m
    +ˣ-congʳ x = Affinity.+-congʳ (affine x)

    +ˣ-congˡ : ∀ (x : X) {m m′} → ≈ᴹ m m′ → +ˣ x m ≈ˣ +ˣ x m′
    +ˣ-congˡ x = Affinity.+-congˡ (affine x)


module _ (ring : Ring r ℓr)
         (≈ᴹ : Rel {m} M ℓm) (+ᴹ : Op₂ M) (0ᴹ : M) (-ᴹ : Op₁ M)
  where

  open Ring ring renaming (Carrier to R)

  record IsAffineLeftModule (+ᴬ : X → M → X) (*ₗ : Opₗ R M)
      : Set (x ⊔ r ⊔ m ⊔ ℓ ⊔ ℓr ⊔ ℓm) where
    open Defs ≈ᴹ
    field
      isAffineLeftSemimodule : IsAffineLeftSemimodule semiring ≈ᴹ +ᴹ 0ᴹ +ᴬ *ₗ
      -ᴹ‿cong : Congruent₁ -ᴹ
      -ᴹ‿inverse : Inverse 0ᴹ -ᴹ +ᴹ

    open IsAffineLeftSemimodule isAffineLeftSemimodule public
