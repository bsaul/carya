{-# OPTIONS --safe --cubical-compatible #-}

module Algebra.Module.Affine.Bundles  where

open import Algebra.Bundles
open import Algebra.Core
open import Algebra.Structures
open import Algebra.Module.Core
open import Algebra.Module.Bundles
open import Algebra.Module.Definitions
open import Algebra.Module.Affine.Structures
import Algebra.Affine.Core as Aff
open import Level using (Level ; suc; _⊔_)
open import Relation.Binary using (Rel ; Setoid)

private
  variable
    r ℓr s ℓs : Level

record AffineLeftSemimodule (semiring : Semiring r ℓr) x ℓ m ℓm
    : Set (r ⊔ ℓr ⊔ suc (x ⊔ m ⊔ ℓ ⊔ ℓm)) where
  open Semiring semiring

  infixr 7 _*ₗ_
  infixl 6 _+ᴹ_
  infixl 6 _+ˣ_
  infix 4 _≈ᴹ_

  field
    Carrierᴹ : Set m
    Carrierˣ : Set x
    _≈ᴹ_ : Rel Carrierᴹ ℓm
    _≈ˣ_ : Rel Carrierˣ ℓ
    _+ᴹ_ : Op₂ Carrierᴹ
    _*ₗ_ : Opₗ Carrier Carrierᴹ
    0ᴹ : Carrierᴹ
    _+ˣ_ : Carrierˣ → Carrierᴹ → Carrierˣ
    isAffineLeftSemimodule :
      IsAffineLeftSemimodule _≈ˣ_ semiring _≈ᴹ_ _+ᴹ_ 0ᴹ _+ˣ_ _*ₗ_

  open IsAffineLeftSemimodule isAffineLeftSemimodule public
  open Aff.Affinity public

  leftSemimodule : LeftSemimodule semiring m ℓm
  leftSemimodule = record { isLeftSemimodule = isLeftSemimodule }

  ≈ˣ-setoid : Setoid x ℓ
  ≈ˣ-setoid = record { isEquivalence = ≈ˣ-isEquivalence }

  open Setoid ≈ˣ-setoid public
    renaming
      ( refl to ≈ˣ-refl
      ; sym to ≈ˣ-sym
      ; trans to ≈ˣ-trans
      )

record AffineLeftModule (ring : Ring r ℓr) x ℓ m ℓm
    : Set (r ⊔ ℓr ⊔ suc (x ⊔ m ⊔ ℓ ⊔ ℓm)) where
  open Ring ring

  infixr 8 -ᴹ_
  infixr 7 _*ₗ_
  infixl 6 _+ᴹ_
  infixl 6 _+ˣ_
  infix 4 _≈ᴹ_

  field
    Carrierᴹ : Set m
    Carrierˣ : Set x
    _≈ᴹ_ : Rel Carrierᴹ ℓm
    _≈ᴬ_ : Rel Carrierˣ ℓ
    _+ᴹ_ : Op₂ Carrierᴹ
    _*ₗ_ : Opₗ Carrier Carrierᴹ
    0ᴹ : Carrierᴹ
    -ᴹ_ : Op₁ Carrierᴹ
    _+ˣ_ : Carrierˣ → Carrierᴹ → Carrierˣ
    isAffineLeftModule : IsAffineLeftModule _≈ᴬ_ ring _≈ᴹ_ _+ᴹ_ 0ᴹ -ᴹ_ _+ˣ_ _*ₗ_

  open IsAffineLeftModule isAffineLeftModule public
  open Aff.Affinity public

  leftSemimodule : LeftSemimodule semiring m ℓm
  leftSemimodule = record { isLeftSemimodule = isLeftSemimodule }
