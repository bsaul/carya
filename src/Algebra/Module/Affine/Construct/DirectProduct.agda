{-# OPTIONS --cubical-compatible --safe #-}

module Algebra.Module.Affine.Construct.DirectProduct where

open import Algebra.Bundles using (Semiring)
open import Algebra.Module.Bundles
open import Algebra.Module.Construct.DirectProduct
open import Algebra.Module.Affine.Bundles
open import Data.Product
open import Data.Product.Relation.Binary.Pointwise.NonDependent using (×-setoid)
open import Level using (Level; _⊔_)
open import Relation.Binary using (Setoid)

private
  variable
    x y ℓˣ ℓʸ r s ℓr ℓs m m′ ℓm ℓm′ : Level

affineLeftSemimodule : {R : Semiring r ℓr} →
                 AffineLeftSemimodule R x ℓˣ m ℓm →
                 AffineLeftSemimodule R y ℓʸ m′ ℓm′ →
                 AffineLeftSemimodule R (x ⊔ y) (ℓˣ ⊔ ℓʸ) (m ⊔ m′) (ℓm ⊔ ℓm′)
affineLeftSemimodule A₁ A₂ = record
    { Carrierˣ = A₁.Carrierˣ × A₂.Carrierˣ
    ; _+ˣ_ = zip A₁._+ˣ_ A₂._+ˣ_
    ; isAffineLeftSemimodule = record
        { isLeftSemimodule = A₁⊕A₂.isLeftSemimodule
        ; ≈ˣ-isEquivalence = X×Y.isEquivalence
        ; isAffine = record
           { +-identityʳ = λ (x , y) → (A₁.+-identityʳ x) , (A₂.+-identityʳ y)
           ; assoc = λ (x , y) (a₁ , b₁) (a₂ , b₂) →
                 A₁.assoc x a₁ a₂
               , A₂.assoc y b₁ b₂
           ; affine = λ (x , y) → record
               { +-cong = λ (A₁≈ , A₂≈) (A₁≈′ , A₂≈′) →
                   A₁.+-cong (A₁.affine x) A₁≈ A₁≈′
                 , A₂.+-cong (A₂.affine y) A₂≈ A₂≈′
               ; hasAffinity =
                  (λ (A₁≈ , A₂≈) →
                      proj₁ (A₁.hasAffinity (A₁.affine x)) A₁≈
                    , proj₁ (A₂.hasAffinity (A₂.affine y)) A₂≈)
                  , λ (x′ , y′) →
                      let (m₁ , sur₁) = proj₂ (A₁.hasAffinity (A₁.affine x)) x′
                          (m₂ , sur₂) = proj₂ (A₂.hasAffinity (A₂.affine y)) y′
                      in (m₁ , m₂) , λ (A₁≈ , A₂≈) → sur₁ A₁≈ , sur₂ A₂≈
               }
           }
        }
    }
  where module A₁ = AffineLeftSemimodule A₁
        module A₂ = AffineLeftSemimodule A₂
        module A₁⊕A₂ = LeftSemimodule
                          (leftSemimodule A₁.leftSemimodule A₂.leftSemimodule)
        module X×Y = Setoid (×-setoid A₁.≈ˣ-setoid A₂.≈ˣ-setoid)

