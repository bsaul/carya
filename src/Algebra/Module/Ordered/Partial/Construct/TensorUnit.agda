{-# OPTIONS --safe --cubical-compatible #-}

module Algebra.Module.Ordered.Partial.Construct.TensorUnit where

open import Level using (Level; _⊔_)
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Module.Ordered.Partial.Bundles
open import Algebra.Module.Bundles
open import Algebra.Module.Construct.TensorUnit as TU
open import Function using (flip)

private
  variable
    r ℓʳ₁ ℓʳ₂ : Level

mkPoLeftSemimodule : {R : PoSemiring r ℓʳ₁ ℓʳ₂}
   → PoLeftSemimodule R r ℓʳ₁ ℓʳ₂
mkPoLeftSemimodule {R = R} = record
  { Carrierᴹ = R.Carrier
  ; isPoLeftSemimodule = record
    { isPreleftSemimodule = L.isPreleftSemimodule
    ; +ᴹ-isPoCommutativeMonoid = R.+-isPoCommutativeMonoid
    ; *ₗ-compatₗ = flip R.*-compatˡ
    ; *ₗ-compatᵣ = R.*-compatʳ
    }
  }
  where module R = PoSemiring R
        module L = LeftSemimodule TU.leftSemimodule

mkPoRightSemimodule : {R : PoSemiring r ℓʳ₁ ℓʳ₂}
   → PoRightSemimodule R r ℓʳ₁ ℓʳ₂
mkPoRightSemimodule {R = R} = record
  { Carrierᴹ = R.Carrier
  ; isPoRightSemimodule = record
    { isPrerightSemimodule = L.isPrerightSemimodule
    ; +ᴹ-isPoCommutativeMonoid = R.+-isPoCommutativeMonoid
    ; *ᵣ-compatₗ = R.*-compatˡ
    ; *ᵣ-compatᵣ = flip R.*-compatʳ
    }
  }
  where module R = PoSemiring R
        module L = RightSemimodule TU.rightSemimodule

mkPoBisemimodule : {R : PoSemiring r ℓʳ₁ ℓʳ₂}
   → PoBisemimodule R R r ℓʳ₁ ℓʳ₂
mkPoBisemimodule {R = R} = record
  { Carrierᴹ = R.Carrier
  ; isPoBisemimodule = record
    { isBisemimodule = B.isBisemimodule
    ; +ᴹ-isPoCommutativeMonoid = R.+-isPoCommutativeMonoid
    ; *ₗ-compatₗ = flip R.*-compatˡ
    ; *ₗ-compatᵣ = R.*-compatʳ
    ; *ᵣ-compatₗ = R.*-compatˡ
    ; *ᵣ-compatᵣ = flip R.*-compatʳ
    }
  }
  where module R = PoSemiring R
        module B = Bisemimodule TU.bisemimodule

mkPoSemimodule : {R : PoCommutativeSemiring r ℓʳ₁ ℓʳ₂}
   → PoSemimodule R r ℓʳ₁ ℓʳ₂
mkPoSemimodule {R = R} = record
  { Carrierᴹ = R.Carrier
  ; isPoSemimodule = record
     { isPoBisemimodule = PoBisemimodule.isPoBisemimodule mkPoBisemimodule
     ; *ₗ-*ᵣ-coincident = λ x m  → B.*ₗ-*ᵣ-coincident x m
     }
  }
  where module R = PoCommutativeSemiring R
        module B = Semimodule (TU.semimodule {R = R.commutativeSemiring})

mkPoBimodule : {R : PoRing r ℓʳ₁ ℓʳ₂}
   → PoBimodule R R r ℓʳ₁ ℓʳ₂
mkPoBimodule {R = R} = record
  { Carrierᴹ = R.Carrier
  ; isPoBimodule = record
    { isPoBisemimodule = PoBisemimodule.isPoBisemimodule mkPoBisemimodule
    ; -ᴹ‿cong = B.-ᴹ‿cong
    ; -ᴹ‿inverse = B.-ᴹ‿inverse
    }
  }
  where module R = PoRing R
        module B = Bimodule (TU.bimodule {R = R.ring})

mkPoModule :  {R : PoCommutativeRing r ℓʳ₁ ℓʳ₂}
   → PoModule R r ℓʳ₁ ℓʳ₂
mkPoModule {R = R} = record
  { Carrierᴹ = R.Carrier
  ; isPoModule = record
    { isPoBimodule = PoBimodule.isPoBimodule mkPoBimodule
    ; *ₗ-*ᵣ-coincident = B.*ₗ-*ᵣ-coincident
    }
  }
  where module R = PoCommutativeRing R
        module B = Module (TU.⟨module⟩ {R = R.commutativeRing})
