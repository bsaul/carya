{-# OPTIONS --safe --cubical-compatible #-}

module Algebra.Module.Ordered.Partial.Construct.DirectProduct where

open import Level using (Level; _⊔_)
open import Algebra.Module.Bundles
open import Algebra.Module.Construct.DirectProduct
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Ordered.Partial.Construct.DirectProduct
open import Algebra.Module.Ordered.Partial.Bundles

open import Data.Product as × using (_,_; _×_)
open import Function

private
  variable
    r ℓr₁ ℓr₂ s ℓs₁ ℓs₂ m m' ℓm₁ ℓm₂ ℓm'₁ ℓm'₂ : Level

mkPoLeftSemimodule :
  {R : PoSemiring r ℓr₁ ℓr₂}
  → PoLeftSemimodule R m ℓm₁ ℓm₂
  → PoLeftSemimodule R m' ℓm'₁ ℓm'₂
  → PoLeftSemimodule R (m ⊔ m') (ℓm₁ ⊔ ℓm'₁) (ℓm₂ ⊔ ℓm'₂)
mkPoLeftSemimodule {R = R} M N = record
  { Carrierᴹ = M.Carrierᴹ × N.Carrierᴹ
  ;  _≤ᴹ_ = λ x y → ×.uncurry _×_ (×.zip M._≤ᴹ_ N._≤ᴹ_ x y)
  ; isPoLeftSemimodule = record
    { isPreleftSemimodule = M⊗N.isPreleftSemimodule
    ; +ᴹ-isPoCommutativeMonoid = M×N.isPoCommutativeMonoid
    ; *ₗ-compatₗ = λ x → ×.map (M.*ₗ-compatₗ x) (N.*ₗ-compatₗ x)
    ; *ₗ-compatᵣ = λ x → ×.map (M.*ₗ-compatᵣ x) (N.*ₗ-compatᵣ x)
    }
  }
  where module M = PoLeftSemimodule M
        module N = PoLeftSemimodule N
        module M⊗N = LeftSemimodule (leftSemimodule M.leftSemimodule N.leftSemimodule)
        module M×N =
            PoCommutativeMonoid
              (mkPoCommutativeMonoid M.+ᴹ-poCommutativeMonoid N.+ᴹ-poCommutativeMonoid)

mkPoRightSemimodule :
  {R : PoSemiring r ℓr₁ ℓr₂}
  → PoRightSemimodule R m ℓm₁ ℓm₂
  → PoRightSemimodule R m' ℓm'₁ ℓm'₂
  → PoRightSemimodule R (m ⊔ m') (ℓm₁ ⊔ ℓm'₁) (ℓm₂ ⊔ ℓm'₂)
mkPoRightSemimodule {R = R} M N = record
  { Carrierᴹ = M.Carrierᴹ × N.Carrierᴹ
  ;  _≤ᴹ_ = λ x y → ×.uncurry _×_ (×.zip M._≤ᴹ_ N._≤ᴹ_ x y)
  ; isPoRightSemimodule = record
    { isPrerightSemimodule = M⊗N.isPrerightSemimodule
    ; +ᴹ-isPoCommutativeMonoid = M×N.isPoCommutativeMonoid
    ; *ᵣ-compatₗ = λ x → ×.map (M.*ᵣ-compatₗ x) (N.*ᵣ-compatₗ x)
    ; *ᵣ-compatᵣ = λ x → ×.map (M.*ᵣ-compatᵣ x) (N.*ᵣ-compatᵣ x)
    }
  }
  where module M = PoRightSemimodule M
        module N = PoRightSemimodule N
        module M⊗N = RightSemimodule (rightSemimodule M.rightSemimodule N.rightSemimodule)
        module M×N =
            PoCommutativeMonoid
              (mkPoCommutativeMonoid M.+ᴹ-poCommutativeMonoid N.+ᴹ-poCommutativeMonoid)

mkPoBisemimodule :
  {R : PoSemiring r ℓr₁ ℓr₂}
  {S : PoSemiring s ℓs₁ ℓs₂}
  → PoBisemimodule R S m ℓm₁ ℓm₂
  → PoBisemimodule R S m' ℓm'₁ ℓm'₂
  → PoBisemimodule R S (m ⊔ m') (ℓm₁ ⊔ ℓm'₁) (ℓm₂ ⊔ ℓm'₂)
mkPoBisemimodule {R = R} {S = S} M N = record
  { Carrierᴹ = M.Carrierᴹ × N.Carrierᴹ
  ;  _≤ᴹ_ = λ x y → ×.uncurry _×_ (×.zip M._≤ᴹ_ N._≤ᴹ_ x y)
  ; isPoBisemimodule = record
    { isBisemimodule = M⊗N.isBisemimodule
    ; +ᴹ-isPoCommutativeMonoid = M×N.isPoCommutativeMonoid
    ; *ₗ-compatₗ = λ x → ×.map (M.*ₗ-compatₗ x) (N.*ₗ-compatₗ x)
    ; *ₗ-compatᵣ = λ x → ×.map (M.*ₗ-compatᵣ x) (N.*ₗ-compatᵣ x)
    ; *ᵣ-compatₗ = λ x → ×.map (M.*ᵣ-compatₗ x) (N.*ᵣ-compatₗ x)
    ; *ᵣ-compatᵣ = λ x → ×.map (M.*ᵣ-compatᵣ x) (N.*ᵣ-compatᵣ x)
    }
  }
  where module M = PoBisemimodule M
        module N = PoBisemimodule N
        module M⊗N = Bisemimodule (bisemimodule M.bisemimodule N.bisemimodule)
        module M×N =
            PoCommutativeMonoid
              (mkPoCommutativeMonoid M.+ᴹ-poCommutativeMonoid N.+ᴹ-poCommutativeMonoid)

mkPoSemimodule :
  {R : PoCommutativeSemiring r ℓr₁ ℓr₂}
  → PoSemimodule R m ℓm₁ ℓm₂
  → PoSemimodule R m' ℓm'₁ ℓm'₂
  → PoSemimodule R (m ⊔ m') (ℓm₁ ⊔ ℓm'₁) (ℓm₂ ⊔ ℓm'₂)
mkPoSemimodule {R = R} M N = record
  { Carrierᴹ = M.Carrierᴹ × N.Carrierᴹ
  ;  _≤ᴹ_ = λ x y → ×.uncurry _×_ (×.zip M._≤ᴹ_ N._≤ᴹ_ x y)
  ; isPoSemimodule = record
    { isPoBisemimodule = M⊗N.isPoBisemimodule
    ; *ₗ-*ᵣ-coincident =
      λ x (m , n) → M.*ₗ-*ᵣ-coincident x m , N.*ₗ-*ᵣ-coincident x n
    }
  }
  where module M = PoSemimodule M
        module N = PoSemimodule N
        module M⊗N = PoBisemimodule (mkPoBisemimodule M.poBisemimodule N.poBisemimodule)
