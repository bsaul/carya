{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_)
open import Algebra.Ordered.Partial.Bundles

module Algebra.Module.Ordered.Partial.Construct.Pointwise
  {a : Level}
  (A : Set a)
  where

open import Algebra.Ordered.Partial.Construct.Pointwise
  using (mkPoCommutativeMonoid)
open import Algebra.Module.Bundles
open import Algebra.Module.Construct.Pointwise
open import Algebra.Module.Ordered.Partial.Bundles
open import Data.Product using (_,_)

private
  variable
    b ℓb₁ ℓb₂ r ℓr₁ ℓr₂ s ℓs₁ ℓs₂ : Level

mkPoLeftSemimodule : ∀  {𝓡 : PoSemiring r ℓr₁ ℓr₂}
   → PoLeftSemimodule 𝓡 b ℓb₁ ℓb₂
   → PoLeftSemimodule 𝓡 (a ⊔ b) (a ⊔ ℓb₁) (a ⊔ ℓb₂)
mkPoLeftSemimodule M = record
  { Carrierᴹ = A → M.Carrierᴹ
  ; isPoLeftSemimodule = record
    { isPreleftSemimodule = L.isPreleftSemimodule
    ; +ᴹ-isPoCommutativeMonoid = C.isPoCommutativeMonoid
    ; *ₗ-compatₗ = λ x y {a} → M.*ₗ-compatₗ x (y {a})
    ; *ₗ-compatᵣ = λ x y {a} → M.*ₗ-compatᵣ x (y {a})
    }
  }
  where module M = PoLeftSemimodule M
        module C = PoCommutativeMonoid
                      (mkPoCommutativeMonoid A M.+ᴹ-poCommutativeMonoid)
        module L = LeftSemimodule (mkLeftSemimodule A M.leftSemimodule)

mkPoRightSemimodule : ∀ {𝓡 : PoSemiring r ℓr₁ ℓr₂}
   → PoRightSemimodule 𝓡 b ℓb₁ ℓb₂
   → PoRightSemimodule 𝓡 (a ⊔ b) (a ⊔ ℓb₁) (a ⊔ ℓb₂)
mkPoRightSemimodule M = record
  { Carrierᴹ = A → M.Carrierᴹ
  ; isPoRightSemimodule = record
    { isPrerightSemimodule = R.isPrerightSemimodule
    ; +ᴹ-isPoCommutativeMonoid = C.isPoCommutativeMonoid
    ; *ᵣ-compatₗ = λ x y {a} → M.*ᵣ-compatₗ x (y {a})
    ; *ᵣ-compatᵣ = λ x y {a} → M.*ᵣ-compatᵣ x (y {a})
    }
  }
  where module M = PoRightSemimodule M
        module C = PoCommutativeMonoid
                      (mkPoCommutativeMonoid A M.+ᴹ-poCommutativeMonoid)
        module R = RightSemimodule (mkRightSemimodule A M.rightSemimodule)

mkPoBisemimodule :
   ∀ {𝓡 : PoSemiring r ℓr₁ ℓr₂}
     {𝓢 : PoSemiring s ℓs₁ ℓs₂}
   → PoBisemimodule 𝓡 𝓢 b ℓb₁ ℓb₂
   → PoBisemimodule 𝓡 𝓢 (a ⊔ b) (a ⊔ ℓb₁) (a ⊔ ℓb₂)
mkPoBisemimodule M = record
  { Carrierᴹ = A → M.Carrierᴹ
  ; isPoBisemimodule = record
    { isBisemimodule = B.isBisemimodule
    ; +ᴹ-isPoCommutativeMonoid = C.isPoCommutativeMonoid
    ; *ₗ-compatₗ = λ x y {a} → M.*ₗ-compatₗ x (y {a})
    ; *ₗ-compatᵣ = λ x y {a} → M.*ₗ-compatᵣ x (y {a})
    ; *ᵣ-compatₗ = λ x y {a} → M.*ᵣ-compatₗ x (y {a})
    ; *ᵣ-compatᵣ = λ x y {a} → M.*ᵣ-compatᵣ x (y {a})
    }
  }
  where module M = PoBisemimodule M
        module C = PoCommutativeMonoid
                      (mkPoCommutativeMonoid A M.+ᴹ-poCommutativeMonoid)
        module B = Bisemimodule (mkBisemimodule A M.bisemimodule)

mkPoSemimodule : ∀ {𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂}
   → PoSemimodule 𝓡 b ℓb₁ ℓb₂
   → PoSemimodule 𝓡 (a ⊔ b) (a ⊔ ℓb₁) (a ⊔ ℓb₂)
mkPoSemimodule M = record
  { Carrierᴹ = A → M.Carrierᴹ
  ; isPoSemimodule = record
    { isPoBisemimodule = C.isPoBisemimodule
    ; *ₗ-*ᵣ-coincident = B.*ₗ-*ᵣ-coincident
    }
  }
  where module M = PoSemimodule M
        module C = PoBisemimodule
                      (mkPoBisemimodule M.poBisemimodule)
        module B = Semimodule (mkSemimodule A M.semimodule)

mkPoBimodule : ∀ {𝓡 : PoRing r ℓr₁ ℓr₂}
   → PoBimodule 𝓡 𝓡 b ℓb₁ ℓb₂
   → PoBimodule 𝓡 𝓡 (a ⊔ b) (a ⊔ ℓb₁) (a ⊔ ℓb₂)
mkPoBimodule M = record
  { Carrierᴹ = A → M.Carrierᴹ
  ; isPoBimodule = record
    { isPoBisemimodule = S.isPoBisemimodule
    ; -ᴹ‿cong = λ x {a} → M.-ᴹ‿cong (x {a})
    ; -ᴹ‿inverse = (λ x {a} → M.-ᴹ‿inverseˡ (x a))
                  , λ x {a} → M.-ᴹ‿inverseʳ (x a)
    }
  }
  where module M = PoBimodule M
        module S = PoBisemimodule (mkPoBisemimodule M.poBisemimodule)

mkPoModule : ∀ {𝓡 : PoCommutativeRing r ℓr₁ ℓr₂}
   → PoModule 𝓡 b ℓb₁ ℓb₂
   → PoModule 𝓡 (a ⊔ b) (a ⊔ ℓb₁) (a ⊔ ℓb₂)
mkPoModule M = record
  { Carrierᴹ = A → M.Carrierᴹ
  ; isPoModule = record
    { isPoBimodule = C.isPoBimodule
    ; *ₗ-*ᵣ-coincident =  B.*ₗ-*ᵣ-coincident
    }
  }
  where module M = PoModule M
        module C = PoBimodule
                      (mkPoBimodule M.poBimodule)
        module B = Module (mkModule A M.⟨module⟩)
