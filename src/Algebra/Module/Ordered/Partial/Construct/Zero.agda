{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)

module Algebra.Module.Ordered.Partial.Construct.Zero {c ℓ : Level} where

open import Algebra.Ordered.Partial.Bundles
open import Data.Unit.Polymorphic
open import Algebra.Module.Ordered.Partial.Bundles
open import Relation.Binary using (Rel)

private
  variable
    r ℓr₁ ℓr₂ s ℓs₁ ℓs₂ : Level

module ℤero where

  infix  4 _≈ᴹ_
  Carrierᴹ : Set c
  Carrierᴹ = ⊤

  _≈ᴹ_     : Rel Carrierᴹ ℓ
  _ ≈ᴹ _ = ⊤

  _≤ᴹ_ : Rel Carrierᴹ ℓ
  _ ≤ᴹ _ = ⊤
open ℤero

mkPoLeftSemimodule : {R : PoSemiring r ℓr₁ ℓr₂} → PoLeftSemimodule R c ℓ ℓ
mkPoLeftSemimodule = record { ℤero }

mkPoRightSemimodule : {R : PoSemiring r ℓr₁ ℓr₂} → PoRightSemimodule R c ℓ ℓ
mkPoRightSemimodule = record { ℤero }

mkPoBisemimodule :
  {R : PoSemiring r ℓr₁ ℓr₂} {S : PoSemiring s ℓs₁ ℓs₂}
  → PoBisemimodule R S c ℓ ℓ
mkPoBisemimodule = record { ℤero }

mkSemimodule : {R : PoCommutativeSemiring r ℓr₁ ℓr₂} → PoSemimodule R c ℓ ℓ
mkSemimodule = record { ℤero }
