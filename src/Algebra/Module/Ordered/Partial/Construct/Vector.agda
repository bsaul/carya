{-# OPTIONS --safe --cubical-compatible #-}

module Algebra.Module.Ordered.Partial.Construct.Vector where

open import Algebra.Ordered.Partial.Bundles
open import Algebra.Module.Ordered.Partial.Bundles
open import Algebra.Module.Ordered.Partial.Construct.Pointwise as PW using ()
open import Algebra.Module.Ordered.Partial.Construct.TensorUnit as TU using ()
open import Data.Fin using (Fin)
open import Data.Nat using (ℕ)
open import Function using (_∘_)
open import Level using (Level ; _⊔_)

private
  variable
    a r ℓr₁ ℓr₂ : Level

-- "vectors" with arbitrary index set
mkPoLeftSemimodule : {𝓡 : PoSemiring r ℓr₁ ℓr₂}
  → Set a → PoLeftSemimodule 𝓡 (a ⊔ r) (a ⊔ ℓr₁) (a ⊔ ℓr₂)
mkPoLeftSemimodule A = PW.mkPoLeftSemimodule A TU.mkPoLeftSemimodule

mkPoRightSemimodule : {𝓡 : PoSemiring r ℓr₁ ℓr₂}
  → Set a → PoRightSemimodule 𝓡 (a ⊔ r) (a ⊔ ℓr₁) (a ⊔ ℓr₂)
mkPoRightSemimodule A = PW.mkPoRightSemimodule A TU.mkPoRightSemimodule

mkPoBisemimodule : {𝓡 : PoSemiring r ℓr₁ ℓr₂}
  → Set a → PoBisemimodule 𝓡 𝓡 (a ⊔ r) (a ⊔ ℓr₁) (a ⊔ ℓr₂)
mkPoBisemimodule A = PW.mkPoBisemimodule A TU.mkPoBisemimodule

mkPoSemimodule : {𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂}
  → Set a → PoSemimodule 𝓡 (a ⊔ r) (a ⊔ ℓr₁) (a ⊔ ℓr₂)
mkPoSemimodule A = PW.mkPoSemimodule A TU.mkPoSemimodule

-- Specialized to `Vector: Fin n → R`
mkFinPoLeftSemimodule : {𝓡 : PoSemiring r ℓr₁ ℓr₂}
  → ℕ → PoLeftSemimodule 𝓡 r ℓr₁ ℓr₂
mkFinPoLeftSemimodule = mkPoLeftSemimodule ∘ Fin

mkFinPoRightSemimodule : {𝓡 : PoSemiring r ℓr₁ ℓr₂}
  → ℕ → PoRightSemimodule 𝓡 r ℓr₁ ℓr₂
mkFinPoRightSemimodule = mkPoRightSemimodule ∘ Fin

mkFinPoBisemimodule : {𝓡 : PoSemiring r ℓr₁ ℓr₂}
  → ℕ → PoBisemimodule 𝓡 𝓡 r ℓr₁ ℓr₂
mkFinPoBisemimodule = mkPoBisemimodule ∘ Fin

mkFinPoSemimodule : {𝓡 : PoCommutativeSemiring r ℓr₁ ℓr₂}
  → ℕ → PoSemimodule 𝓡 r ℓr₁ ℓr₂
mkFinPoSemimodule = mkPoSemimodule ∘ Fin
