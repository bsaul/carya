{-# OPTIONS --safe --cubical-compatible #-}

module Algebra.Module.Ordered.Partial.Structures where

open import Level using (Level; _⊔_)
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Ordered.Partial.Structures
  using (IsPoCommutativeMonoid)
import Algebra.Definitions as Defs
open import Algebra.Core using (Op₁ ; Op₂)
open import Algebra.Module.Core
open import Algebra.Module.Definitions
open import Algebra.Module.Structures
open import Relation.Binary using (Rel)

private
  variable
    m ℓm₁ ℓm₂ r ℓr₁ ℓr₂ s ℓs₁ ℓs₂ : Level
    M : Set m

module _
  (poSemiring : PoSemiring r ℓr₁ ℓr₂)
  (≈ᴹ : Rel {m} M ℓm₁)
  (≤ᴹ : Rel {m} M ℓm₂)
  (+ᴹ : Op₂ M)
  (0ᴹ : M)
  where

  open PoSemiring poSemiring renaming (Carrier to R)

  record IsPoLeftSemimodule
    (*ₗ : Opₗ R M)
    : Set (r ⊔ m ⊔ ℓr₁ ⊔ ℓr₂ ⊔ ℓm₁ ⊔ ℓm₂)
    where

    field
      isPreleftSemimodule : IsPreleftSemimodule semiring ≈ᴹ +ᴹ 0ᴹ *ₗ
      +ᴹ-isPoCommutativeMonoid : IsPoCommutativeMonoid  ≈ᴹ ≤ᴹ +ᴹ 0ᴹ
      *ₗ-compatₗ : ∀ {r : R} {m₁ m₂ : M} → 0# ≤ r → ≤ᴹ m₁ m₂ → ≤ᴹ (*ₗ r m₁) (*ₗ r m₂)
      *ₗ-compatᵣ : ∀ {r₁ r₂ : R} {m : M} → r₁ ≤ r₂ → ≤ᴹ 0ᴹ m → ≤ᴹ (*ₗ r₁ m) (*ₗ r₂ m)

    open IsPoCommutativeMonoid +ᴹ-isPoCommutativeMonoid public
      renaming
        ( ∙-compat to +ᴹ-compat
        ; ∙-mono₂ to +ᴹ-mono₂
        )

    isLeftSemimodule : IsLeftSemimodule semiring ≈ᴹ +ᴹ 0ᴹ *ₗ
    isLeftSemimodule = record
      { +ᴹ-isCommutativeMonoid = isCommutativeMonoid
      ; isPreleftSemimodule = isPreleftSemimodule
      }

    open IsLeftSemimodule isLeftSemimodule public

  record IsPoRightSemimodule
    (*ᵣ : Opᵣ R M)
    : Set (r ⊔ m ⊔ ℓr₁ ⊔ ℓr₂ ⊔ ℓm₁ ⊔ ℓm₂)
    where

    field
      isPrerightSemimodule : IsPrerightSemimodule semiring ≈ᴹ +ᴹ 0ᴹ *ᵣ
      +ᴹ-isPoCommutativeMonoid : IsPoCommutativeMonoid  ≈ᴹ ≤ᴹ +ᴹ 0ᴹ
      *ᵣ-compatᵣ : ∀ {r} {m₁ m₂ : M} → 0# ≤ r → ≤ᴹ m₁ m₂ → ≤ᴹ (*ᵣ m₁ r) (*ᵣ m₂ r)
      *ᵣ-compatₗ : ∀ {r₁ r₂} {m : M} → r₁ ≤ r₂ → ≤ᴹ 0ᴹ m → ≤ᴹ (*ᵣ m r₁) (*ᵣ m r₂)

    open IsPoCommutativeMonoid +ᴹ-isPoCommutativeMonoid public
      renaming
        ( ∙-compat to +ᴹ-compat
        ; ∙-mono₂ to +ᴹ-mono₂
        )

    isRightSemimodule : IsRightSemimodule semiring ≈ᴹ +ᴹ 0ᴹ *ᵣ
    isRightSemimodule = record
      { +ᴹ-isCommutativeMonoid = isCommutativeMonoid
      ; isPrerightSemimodule = isPrerightSemimodule
      }

    open IsRightSemimodule isRightSemimodule public

module _
  (poSemiring₁ : PoSemiring r ℓr₁ ℓr₂)
  (poSemiring₂ : PoSemiring s ℓs₁ ℓs₂)
  (≈ᴹ : Rel {m} M ℓm₁)
  (≤ᴹ : Rel {m} M ℓm₂)
  (+ᴹ : Op₂ M)
  (0ᴹ : M)
  where

  private
    module 𝓡 = PoSemiring poSemiring₁
    module 𝓢 = PoSemiring poSemiring₂

  record IsPoBisemimodule
    (*ₗ : Opₗ 𝓡.Carrier M)
    (*ᵣ : Opᵣ 𝓢.Carrier M)
    : Set (r ⊔ s ⊔ m ⊔ ℓr₁ ⊔ ℓr₂ ⊔ ℓs₁ ⊔ ℓs₂ ⊔ ℓm₁ ⊔ ℓm₂) where

    field
      isBisemimodule :
        IsBisemimodule 𝓡.semiring 𝓢.semiring ≈ᴹ +ᴹ 0ᴹ *ₗ *ᵣ
      +ᴹ-isPoCommutativeMonoid : IsPoCommutativeMonoid ≈ᴹ ≤ᴹ +ᴹ 0ᴹ
      *ₗ-compatₗ : ∀ {r} {m₁ m₂ : M} → 𝓡.0# 𝓡.≤ r → ≤ᴹ m₁ m₂ → ≤ᴹ (*ₗ r m₁) (*ₗ r m₂)
      *ₗ-compatᵣ : ∀ {r₁ r₂} {m : M} → r₁ 𝓡.≤ r₂ → ≤ᴹ 0ᴹ m → ≤ᴹ (*ₗ r₁ m) (*ₗ r₂ m)
      *ᵣ-compatᵣ : ∀ {s} {m₁ m₂ : M} → 𝓢.0# 𝓢.≤ s → ≤ᴹ m₁ m₂ → ≤ᴹ (*ᵣ m₁ s) (*ᵣ m₂ s)
      *ᵣ-compatₗ : ∀ {s₁ s₂} {m : M} → s₁ 𝓢.≤ s₂ → ≤ᴹ 0ᴹ m → ≤ᴹ (*ᵣ m s₁) (*ᵣ m s₂)

    open IsBisemimodule isBisemimodule public

    open IsPoCommutativeMonoid +ᴹ-isPoCommutativeMonoid public
      renaming
        ( ∙-compat to +ᴹ-compat
        ; ∙-mono₂ to +ᴹ-mono₂
        )

    isPoLeftSemimodule : IsPoLeftSemimodule poSemiring₁ ≈ᴹ ≤ᴹ +ᴹ 0ᴹ *ₗ
    isPoLeftSemimodule = record
      { isPreleftSemimodule =
        IsBisemimodule.isPreleftSemimodule isBisemimodule
      ; +ᴹ-isPoCommutativeMonoid = +ᴹ-isPoCommutativeMonoid
      ; *ₗ-compatₗ = *ₗ-compatₗ
      ; *ₗ-compatᵣ = *ₗ-compatᵣ
      }

    isPoRightSemimodule : IsPoRightSemimodule poSemiring₂ ≈ᴹ ≤ᴹ +ᴹ 0ᴹ *ᵣ
    isPoRightSemimodule = record
      { isPrerightSemimodule =
          IsBisemimodule.isPrerightSemimodule isBisemimodule
      ; +ᴹ-isPoCommutativeMonoid = +ᴹ-isPoCommutativeMonoid
      ; *ᵣ-compatₗ = *ᵣ-compatₗ
      ; *ᵣ-compatᵣ = *ᵣ-compatᵣ
      }

module _
  (poCommutativeSemiring : PoCommutativeSemiring r ℓr₁ ℓr₂)
  (≈ᴹ : Rel {m} M ℓm₁)
  (≤ᴹ : Rel {m} M ℓm₂)
  (+ᴹ : Op₂ M)
  (0ᴹ : M)
  where

  private
    module 𝓡 = PoCommutativeSemiring poCommutativeSemiring

  open SimultaneousBiDefs 𝓡.Carrier ≈ᴹ

  record IsPoSemimodule
    (*ₗ : Opₗ 𝓡.Carrier M)
    (*ᵣ : Opᵣ 𝓡.Carrier M)
    : Set (r ⊔ m ⊔ ℓr₁ ⊔ ℓr₂ ⊔ ℓm₁ ⊔ ℓm₂) where
    field
      isPoBisemimodule :
        IsPoBisemimodule 𝓡.poSemiring 𝓡.poSemiring ≈ᴹ ≤ᴹ +ᴹ  0ᴹ *ₗ *ᵣ
      *ₗ-*ᵣ-coincident : Coincident *ₗ *ᵣ

    open IsPoBisemimodule isPoBisemimodule public

    isSemimodule : IsSemimodule 𝓡.commutativeSemiring ≈ᴹ +ᴹ  0ᴹ *ₗ *ᵣ
    isSemimodule = record
      { isBisemimodule = isBisemimodule
      ; *ₗ-*ᵣ-coincident = *ₗ-*ᵣ-coincident
      }

module _
  (poRing : PoRing r ℓr₁ ℓr₂)
  (≈ᴹ : Rel {m} M ℓm₁)
  (≤ᴹ : Rel {m} M ℓm₂)
  (+ᴹ : Op₂ M)
  (0ᴹ : M)
  (-ᴹ : Op₁ M)
  where

  private
    module 𝓡 = PoRing poRing

  record IsPoLeftModule
    (*ₗ : Opₗ 𝓡.Carrier M)
    : Set (r ⊔ m ⊔ ℓr₁ ⊔ ℓr₂ ⊔ ℓm₁ ⊔ ℓm₂) where

    open Defs ≈ᴹ
    field
      isPoLeftSemimodule : IsPoLeftSemimodule 𝓡.poSemiring ≈ᴹ ≤ᴹ +ᴹ 0ᴹ *ₗ
      -ᴹ‿cong : Congruent₁ -ᴹ
      -ᴹ‿inverse : Inverse 0ᴹ -ᴹ +ᴹ

    open IsPoLeftSemimodule isPoLeftSemimodule public

    isLeftModule : IsLeftModule 𝓡.ring ≈ᴹ +ᴹ 0ᴹ -ᴹ *ₗ
    isLeftModule = record
      { isLeftSemimodule = isLeftSemimodule
      ; -ᴹ‿cong = -ᴹ‿cong
      ; -ᴹ‿inverse = -ᴹ‿inverse
      }

  record IsPoRightModule
    (*ᵣ : Opᵣ 𝓡.Carrier M)
    : Set (r ⊔ m ⊔ ℓr₁ ⊔ ℓr₂ ⊔ ℓm₁ ⊔ ℓm₂) where

    open Defs ≈ᴹ
    field
      isPoRightSemimodule : IsPoRightSemimodule 𝓡.poSemiring ≈ᴹ ≤ᴹ +ᴹ 0ᴹ *ᵣ
      -ᴹ‿cong : Congruent₁ -ᴹ
      -ᴹ‿inverse : Inverse 0ᴹ -ᴹ +ᴹ

    open IsPoRightSemimodule isPoRightSemimodule public

    isRightModule : IsRightModule 𝓡.ring ≈ᴹ +ᴹ 0ᴹ -ᴹ *ᵣ
    isRightModule = record
      { isRightSemimodule = isRightSemimodule
      ; -ᴹ‿cong = -ᴹ‿cong
      ; -ᴹ‿inverse = -ᴹ‿inverse
      }

module _
  (poRing₁ : PoRing r ℓr₁ ℓr₂)
  (poRing₂ : PoRing s ℓs₁ ℓs₂)
  (≈ᴹ : Rel {m} M ℓm₁)
  (≤ᴹ : Rel {m} M ℓm₂)
  (+ᴹ : Op₂ M)
  (0ᴹ : M)
  (-ᴹ : Op₁ M)
  where

  private
    module 𝓡 = PoRing poRing₁
    module 𝓢 = PoRing poRing₂

  record IsPoBimodule
    (*ₗ : Opₗ 𝓡.Carrier M)
    (*ᵣ : Opᵣ 𝓢.Carrier M)
    : Set (r ⊔ s ⊔ m ⊔ ℓr₁ ⊔ ℓr₂ ⊔ ℓs₁ ⊔ ℓs₂ ⊔ ℓm₁ ⊔ ℓm₂) where

    open Defs ≈ᴹ

    field
      isPoBisemimodule :
        IsPoBisemimodule 𝓡.poSemiring 𝓢.poSemiring ≈ᴹ ≤ᴹ +ᴹ 0ᴹ *ₗ *ᵣ
      -ᴹ‿cong : Congruent₁ -ᴹ
      -ᴹ‿inverse : Inverse 0ᴹ -ᴹ +ᴹ

    open IsPoBisemimodule isPoBisemimodule public

    isBimodule : IsBimodule 𝓡.ring 𝓢.ring ≈ᴹ +ᴹ 0ᴹ -ᴹ *ₗ *ᵣ
    isBimodule = record
      { isBisemimodule = isBisemimodule
      ; -ᴹ‿cong = -ᴹ‿cong
      ; -ᴹ‿inverse = -ᴹ‿inverse
      }

    open IsBimodule isBimodule public
      using
       ( -ᴹ‿inverseˡ
       ; -ᴹ‿inverseʳ
      --  ; *ₗ-distribˡ
       )

module _
  (poCommutativeRing : PoCommutativeRing r ℓr₁ ℓr₂)
  (≈ᴹ : Rel {m} M ℓm₁)
  (≤ᴹ : Rel {m} M ℓm₂)
  (+ᴹ : Op₂ M)
  (0ᴹ : M)
  (-ᴹ : Op₁ M)
  where

  private
    module 𝓡 = PoCommutativeRing poCommutativeRing

  record IsPoModule
    (*ₗ : Opₗ 𝓡.Carrier M)
    (*ᵣ : Opᵣ 𝓡.Carrier M)
    : Set (r ⊔ m ⊔ ℓr₁ ⊔ ℓr₂ ⊔ ℓm₁ ⊔ ℓm₂) where

    open Defs ≈ᴹ
    open SimultaneousBiDefs 𝓡.Carrier ≈ᴹ

    field
      isPoBimodule : IsPoBimodule 𝓡.poRing 𝓡.poRing ≈ᴹ ≤ᴹ +ᴹ 0ᴹ -ᴹ *ₗ *ᵣ
      *ₗ-*ᵣ-coincident : Coincident *ₗ *ᵣ

    open IsPoBimodule isPoBimodule public

    isModule : IsModule 𝓡.commutativeRing ≈ᴹ +ᴹ 0ᴹ -ᴹ *ₗ *ᵣ
    isModule = record
      { isBimodule = isBimodule
      ; *ₗ-*ᵣ-coincident = *ₗ-*ᵣ-coincident
      }
