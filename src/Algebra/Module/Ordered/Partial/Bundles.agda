{-# OPTIONS --safe --cubical-compatible #-}

module Algebra.Module.Ordered.Partial.Bundles where

open import Level using (Level; _⊔_; suc)
open import Algebra.Bundles using (Semiring)
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Core using (Op₁ ; Op₂)
open import Algebra.Module.Core
open import Algebra.Module.Bundles
open import Algebra.Module.Ordered.Partial.Structures
open import Relation.Binary using (Rel)

private
  variable
    r ℓr₁ ℓr₂ s ℓs₁ ℓs₂ : Level

record PoLeftSemimodule
  (poSemiring : PoSemiring r ℓr₁ ℓr₂) m ℓm₁ ℓm₂
  : Set (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ suc (m ⊔ ℓm₁ ⊔ ℓm₂))
  where

  open PoSemiring poSemiring

  infixr 7 _*ₗ_
  infixl 6 _+ᴹ_
  infix 4 _≈ᴹ_ _≤ᴹ_

  field
    Carrierᴹ : Set m
    _≈ᴹ_ : Rel Carrierᴹ ℓm₁
    _≤ᴹ_ : Rel Carrierᴹ ℓm₂
    _+ᴹ_ : Op₂ Carrierᴹ
    _*ₗ_ : Opₗ Carrier Carrierᴹ
    0ᴹ : Carrierᴹ
    isPoLeftSemimodule : IsPoLeftSemimodule poSemiring _≈ᴹ_ _≤ᴹ_ _+ᴹ_ 0ᴹ _*ₗ_

  open IsPoLeftSemimodule isPoLeftSemimodule public

  +ᴹ-poCommutativeMonoid : PoCommutativeMonoid _ _ _
  +ᴹ-poCommutativeMonoid = record
    { isPoCommutativeMonoid = +ᴹ-isPoCommutativeMonoid
    }

  leftSemimodule : LeftSemimodule semiring m ℓm₁
  leftSemimodule = record { isLeftSemimodule = isLeftSemimodule }

record PoRightSemimodule
  (poSemiring : PoSemiring r ℓr₁ ℓr₂) m ℓm₁ ℓm₂
  : Set (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ suc (m ⊔ ℓm₁ ⊔ ℓm₂))
  where

  open PoSemiring poSemiring

  infixr 7 _*ᵣ_
  infixl 6 _+ᴹ_
  infix 4 _≈ᴹ_ _≤ᴹ_

  field
    Carrierᴹ : Set m
    _≈ᴹ_ : Rel Carrierᴹ ℓm₁
    _≤ᴹ_ : Rel Carrierᴹ ℓm₂
    _+ᴹ_ : Op₂ Carrierᴹ
    _*ᵣ_ : Opᵣ Carrier Carrierᴹ
    0ᴹ : Carrierᴹ
    isPoRightSemimodule : IsPoRightSemimodule poSemiring _≈ᴹ_ _≤ᴹ_ _+ᴹ_ 0ᴹ _*ᵣ_

  open IsPoRightSemimodule isPoRightSemimodule public

  +ᴹ-poCommutativeMonoid : PoCommutativeMonoid _ _ _
  +ᴹ-poCommutativeMonoid = record
    { isPoCommutativeMonoid = +ᴹ-isPoCommutativeMonoid
    }

  rightSemimodule : RightSemimodule semiring m ℓm₁
  rightSemimodule = record { isRightSemimodule = isRightSemimodule }

record PoBisemimodule
  (poSemiring₁ : PoSemiring r ℓr₁ ℓr₂)
  (poSemiring₂ : PoSemiring s ℓs₁ ℓs₂)
  m ℓm₁ ℓm₂
  : Set (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ s ⊔ ℓs₁ ⊔ ℓs₂ ⊔ suc (m ⊔ ℓm₁ ⊔ ℓm₂))
  where

  private
    module 𝓡 = PoSemiring poSemiring₁
    module 𝓢 = PoSemiring poSemiring₂

  infixr 7 _*ₗ_ _*ᵣ_
  infixl 6 _+ᴹ_
  infix 4 _≈ᴹ_ _≤ᴹ_

  field
    Carrierᴹ : Set m
    _≈ᴹ_ : Rel Carrierᴹ ℓm₁
    _≤ᴹ_ : Rel Carrierᴹ ℓm₂
    _+ᴹ_ : Op₂ Carrierᴹ
    _*ₗ_ : Opₗ 𝓡.Carrier Carrierᴹ
    _*ᵣ_ : Opᵣ 𝓢.Carrier Carrierᴹ
    0ᴹ : Carrierᴹ
    isPoBisemimodule :
      IsPoBisemimodule poSemiring₁ poSemiring₂ _≈ᴹ_ _≤ᴹ_ _+ᴹ_ 0ᴹ _*ₗ_ _*ᵣ_

  open IsPoBisemimodule isPoBisemimodule public

  +ᴹ-poCommutativeMonoid : PoCommutativeMonoid _ _ _
  +ᴹ-poCommutativeMonoid = record
    { isPoCommutativeMonoid = +ᴹ-isPoCommutativeMonoid
    }

  bisemimodule : Bisemimodule 𝓡.semiring 𝓢.semiring m ℓm₁
  bisemimodule = record { isBisemimodule = isBisemimodule }

record PoSemimodule
  (S : PoCommutativeSemiring r ℓr₁ ℓr₂)
  m ℓm₁ ℓm₂
  : Set (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ suc (m ⊔ ℓm₁ ⊔ ℓm₂))
  where

  open PoCommutativeSemiring S
  infixr 7 _*ₗ_ _*ᵣ_
  infixl 6 _+ᴹ_
  infix 4 _≈ᴹ_ _≤ᴹ_

  field
    Carrierᴹ : Set m
    _≈ᴹ_ : Rel Carrierᴹ ℓm₁
    _≤ᴹ_ : Rel Carrierᴹ ℓm₂
    _+ᴹ_ : Op₂ Carrierᴹ
    _*ₗ_ : Opₗ Carrier Carrierᴹ
    _*ᵣ_ : Opᵣ Carrier Carrierᴹ
    0ᴹ : Carrierᴹ
    isPoSemimodule : IsPoSemimodule S _≈ᴹ_ _≤ᴹ_ _+ᴹ_ 0ᴹ _*ₗ_ _*ᵣ_

  open IsPoSemimodule isPoSemimodule public

  +ᴹ-poCommutativeMonoid : PoCommutativeMonoid _ _ _
  +ᴹ-poCommutativeMonoid = record
    { isPoCommutativeMonoid = +ᴹ-isPoCommutativeMonoid
    }

  semimodule : Semimodule commutativeSemiring m ℓm₁
  semimodule = record { isSemimodule = isSemimodule }

  poBisemimodule : PoBisemimodule poSemiring poSemiring m ℓm₁ ℓm₂
  poBisemimodule = record { isPoBisemimodule = isPoBisemimodule }

  poLeftSemimodule : PoLeftSemimodule poSemiring m ℓm₁ ℓm₂
  poLeftSemimodule = record
    { isPoLeftSemimodule = isPoLeftSemimodule }

  poRightSemimodule : PoRightSemimodule poSemiring m ℓm₁ ℓm₂
  poRightSemimodule = record
    { isPoRightSemimodule = isPoRightSemimodule }

record PoBimodule
  (poRing₁ : PoRing r ℓr₁ ℓr₂)
  (poRing₂ : PoRing s ℓs₁ ℓs₂)
  m ℓm₁ ℓm₂
  : Set (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ s ⊔ ℓs₁ ⊔ ℓs₂ ⊔ suc (m ⊔ ℓm₁ ⊔ ℓm₂))
  where

  private
    module 𝓡 = PoRing poRing₁
    module 𝓢 = PoRing poRing₂

  infix  8 -ᴹ_
  infixr 7 _*ₗ_ _*ᵣ_
  infixl 6 _+ᴹ_
  infix 4 _≈ᴹ_ _≤ᴹ_

  field
    Carrierᴹ : Set m
    _≈ᴹ_ : Rel Carrierᴹ ℓm₁
    _≤ᴹ_ : Rel Carrierᴹ ℓm₂
    _+ᴹ_ : Op₂ Carrierᴹ
    -ᴹ_  : Op₁ Carrierᴹ
    _*ₗ_ : Opₗ 𝓡.Carrier Carrierᴹ
    _*ᵣ_ : Opᵣ 𝓢.Carrier Carrierᴹ
    0ᴹ : Carrierᴹ
    isPoBimodule : IsPoBimodule poRing₁ poRing₂ _≈ᴹ_ _≤ᴹ_ _+ᴹ_ 0ᴹ -ᴹ_ _*ₗ_ _*ᵣ_

  open IsPoBimodule isPoBimodule public

  poBisemimodule : PoBisemimodule 𝓡.poSemiring 𝓢.poSemiring _ _ _
  poBisemimodule = record { isPoBisemimodule = isPoBisemimodule }

  bisemimodule : Bisemimodule 𝓡.semiring 𝓢.semiring _ _
  bisemimodule = record { isBisemimodule = isBisemimodule }


record PoModule
  (S : PoCommutativeRing r ℓr₁ ℓr₂)
  m ℓm₁ ℓm₂
  : Set (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ suc (m ⊔ ℓm₁ ⊔ ℓm₂))
  where

  open PoCommutativeRing S
  infix  8 -ᴹ_
  infixr 7 _*ₗ_ _*ᵣ_
  infixl 6 _+ᴹ_
  infix 4 _≈ᴹ_ _≤ᴹ_

  field
    Carrierᴹ : Set m
    _≈ᴹ_ : Rel Carrierᴹ ℓm₁
    _≤ᴹ_ : Rel Carrierᴹ ℓm₂
    _+ᴹ_ : Op₂ Carrierᴹ
    -ᴹ_  : Op₁ Carrierᴹ
    _*ₗ_ : Opₗ Carrier Carrierᴹ
    _*ᵣ_ : Opᵣ Carrier Carrierᴹ
    0ᴹ : Carrierᴹ
    isPoModule : IsPoModule S _≈ᴹ_ _≤ᴹ_ _+ᴹ_ 0ᴹ -ᴹ_ _*ₗ_ _*ᵣ_

  open IsPoModule isPoModule public

  poBimodule : PoBimodule poRing poRing _ _ _
  poBimodule = record { isPoBimodule = isPoBimodule }

  ⟨module⟩ : Module commutativeRing _ _
  ⟨module⟩ = record { isModule = isModule }

  leftSemimodule : LeftSemimodule semiring _ _
  leftSemimodule = record { isLeftSemimodule = isLeftSemimodule }
