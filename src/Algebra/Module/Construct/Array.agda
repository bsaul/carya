{-# OPTIONS --cubical-compatible --safe #-}

open import Algebra.Bundles using (Semiring)

module Algebra.Module.Construct.Array {c ℓ} (𝓡 : Semiring c ℓ) where

open import Algebra.Module.Bundles
  using (LeftSemimodule)
open import Algebra.Module.Construct.Pointwise
  using (mkLeftSemimodule)
open import Algebra.Module.Construct.TensorUnit
  renaming (leftSemimodule to tensor)
open import Data.Fin using (Fin)
open import Data.Nat using (ℕ)
open import Data.Vec.Functional using (foldr; map)
open import Level using (Level)

private
  variable
    a : Level

-- TODO: generalized to `Sets`
array : ∀ (n : ℕ) → (Fin n → Set) → LeftSemimodule 𝓡 c ℓ
array n f = foldr (λ s → mkLeftSemimodule s) tensor {n = n} f

finArray : ∀ (n : ℕ) → (Fin n → ℕ) → LeftSemimodule 𝓡 c ℓ
finArray n f = array n (map Fin f)
