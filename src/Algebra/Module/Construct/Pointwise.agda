{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_; 0ℓ)
open import Algebra.Bundles

module Algebra.Module.Construct.Pointwise
  {a : Level}
  (A : Set a)
  where

private
  variable
    r s ℓr ℓs : Level

open import Algebra.Construct.Pointwise2
open import Algebra.Module.Bundles
open import Data.Product using (_,_)

mkLeftSemimodule : ∀ {b ℓᵇ} {𝓡 : Semiring r ℓr}
   → LeftSemimodule 𝓡 b ℓᵇ
   → LeftSemimodule 𝓡 (a ⊔ b) (a ⊔ ℓᵇ)
mkLeftSemimodule M = record
  { Carrierᴹ = A → Carrierᴹ
  ; isLeftSemimodule = record
     { +ᴹ-isCommutativeMonoid =
          CommutativeMonoid.isCommutativeMonoid
            (mkCommutativeMonoid A +ᴹ-commutativeMonoid)
     ; isPreleftSemimodule = record
          { *ₗ-cong = λ x fx≈ᴹgx → *ₗ-cong x fx≈ᴹgx
          ; *ₗ-zeroˡ = λ f → λ {x} → *ₗ-zeroˡ (f x)
          ; *ₗ-distribʳ = λ f m n → λ {x} → *ₗ-distribʳ (f x) m n
          ; *ₗ-identityˡ = λ f → λ {x} → *ₗ-identityˡ (f x)
          ; *ₗ-assoc = λ m n f → λ {x} → *ₗ-assoc m n (f x)
          ; *ₗ-zeroʳ = λ m → *ₗ-zeroʳ m
          ; *ₗ-distribˡ = λ m f g → λ {x} → *ₗ-distribˡ m (f x) (g x)
          }
     }
 }
 where open LeftSemimodule M

mkLeftSemimoduleLiftSemiring : ∀ {b ℓᵇ} {𝓡 : Semiring r ℓr}
   → LeftSemimodule 𝓡 b ℓᵇ
   → LeftSemimodule (mkSemiring A 𝓡) (a ⊔ b) (a ⊔ ℓᵇ)
mkLeftSemimoduleLiftSemiring M = record
  { Carrierᴹ = A → Carrierᴹ
  ; _*ₗ_ = λ f g a → f a *ₗ g a
  ; isLeftSemimodule = record
    { +ᴹ-isCommutativeMonoid =
      CommutativeMonoid.isCommutativeMonoid
            (mkCommutativeMonoid A +ᴹ-commutativeMonoid)
    ; isPreleftSemimodule = record
        { *ₗ-cong = λ x y {a} → *ₗ-cong (x {a}) (y {a})
        ; *ₗ-zeroˡ = λ x {a} → *ₗ-zeroˡ (x a)
        ; *ₗ-distribʳ = λ x m n {a} → *ₗ-distribʳ (x a) (m a) (n a)
        ; *ₗ-identityˡ = λ m {a} → *ₗ-identityˡ (m a)
        ; *ₗ-assoc = λ x y m {a} → *ₗ-assoc (x a) (y a) (m a)
        ; *ₗ-zeroʳ = λ x {a} → *ₗ-zeroʳ (x a)
        ; *ₗ-distribˡ = λ x m n {a} → *ₗ-distribˡ (x a) (m a) (n a)
        }
    }
  }
 where open LeftSemimodule M

mkRightSemimodule : ∀ {b ℓᵇ} {𝓡 : Semiring r ℓr}
   → RightSemimodule 𝓡 b ℓᵇ
   → RightSemimodule 𝓡 (a ⊔ b) (a ⊔ ℓᵇ)
mkRightSemimodule M = record
  { Carrierᴹ = A → Carrierᴹ
  ; isRightSemimodule = record
     { +ᴹ-isCommutativeMonoid =
          CommutativeMonoid.isCommutativeMonoid
            (mkCommutativeMonoid A +ᴹ-commutativeMonoid)
     ; isPrerightSemimodule = record
        { *ᵣ-cong = λ x y {a} → *ᵣ-cong (x {a}) y
        ; *ᵣ-zeroʳ = λ x {a} → *ᵣ-zeroʳ (x a)
        ; *ᵣ-distribˡ = λ m x y {a} → *ᵣ-distribˡ (m a) x y
        ; *ᵣ-identityʳ = λ m {a} → *ᵣ-identityʳ (m a)
        ; *ᵣ-assoc = λ m x y {a} → *ᵣ-assoc (m a) x y
        ; *ᵣ-zeroˡ = λ x → *ᵣ-zeroˡ x
        ; *ᵣ-distribʳ = λ x m n {a} → *ᵣ-distribʳ x (m a) (n a)
        }
     }
 }
 where open RightSemimodule M

mkBisemimodule : ∀ {b ℓᵇ}
     {𝓡 : Semiring r ℓr} {𝓢 : Semiring s ℓs}
   → Bisemimodule 𝓡 𝓢 b ℓᵇ
   → Bisemimodule 𝓡 𝓢 (a ⊔ b) (a ⊔ ℓᵇ)
mkBisemimodule M = record
  { Carrierᴹ = A → Carrierᴹ
  ; isBisemimodule = record
    { +ᴹ-isCommutativeMonoid = CommutativeMonoid.isCommutativeMonoid
            (mkCommutativeMonoid A +ᴹ-commutativeMonoid)
    ; isPreleftSemimodule =
       LeftSemimodule.isPreleftSemimodule (mkLeftSemimodule leftSemimodule)
    ; isPrerightSemimodule =
      RightSemimodule.isPrerightSemimodule (mkRightSemimodule rightSemimodule)
    ; *ₗ-*ᵣ-assoc = λ x m y {a} → *ₗ-*ᵣ-assoc x (m a) y
    }
  }
 where open Bisemimodule M

mkSemimodule : ∀ {b ℓᵇ} {𝓡 : CommutativeSemiring r ℓr}
   → Semimodule 𝓡 b ℓᵇ
   → Semimodule 𝓡 (a ⊔ b) (a ⊔ ℓᵇ)
mkSemimodule M = record
  { Carrierᴹ = A → Carrierᴹ
  ; isSemimodule = record
    { isBisemimodule = Bisemimodule.isBisemimodule (mkBisemimodule bisemimodule)
    ; *ₗ-*ᵣ-coincident = λ x m {a} → *ₗ-*ᵣ-coincident x (m a)
    }
 }
 where open Semimodule M

mkBimodule : ∀ {b ℓᵇ}
     {𝓡 : Ring r ℓr} {𝓢 : Ring s ℓs}
   → Bimodule 𝓡 𝓢 b ℓᵇ
   → Bimodule 𝓡 𝓢 (a ⊔ b) (a ⊔ ℓᵇ)
mkBimodule M = record
  { Carrierᴹ = A → Carrierᴹ
  ; isBimodule = record
     { isBisemimodule = B.isBisemimodule
    ; -ᴹ‿cong = λ x {a} → -ᴹ‿cong (x {a})
    ; -ᴹ‿inverse = (λ x {a} → -ᴹ‿inverseˡ (x a))
                  , λ x {a} → -ᴹ‿inverseʳ (x a)
     }
  }
 where open Bimodule M
       module B = Bisemimodule (mkBisemimodule bisemimodule)

mkModule : ∀ {b ℓᵇ}
     {𝓡 : CommutativeRing r ℓr}
   → Module 𝓡 b ℓᵇ
   → Module 𝓡 (a ⊔ b) (a ⊔ ℓᵇ)
mkModule M = record
  { Carrierᴹ = A → Carrierᴹ
  ; isModule = record
    { isBimodule = B.isBimodule
    ; *ₗ-*ᵣ-coincident = S.*ₗ-*ᵣ-coincident
    }
  }
 where open Module M
       module B = Bimodule (mkBimodule bimodule)
       module S = Semimodule (mkSemimodule semimodule)
