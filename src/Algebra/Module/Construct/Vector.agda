{-# OPTIONS --cubical-compatible --safe #-}

-- TODO: use the array construct once generalized to higher universe levels

module Algebra.Module.Construct.Vector where

open import Level using (Level; _⊔_)
open import Algebra.Bundles
open import Algebra.Module.Bundles
open import Algebra.Module.Construct.Pointwise as P using ()
open import Algebra.Module.Construct.TensorUnit as U
open import Data.Nat using (ℕ)
open import Data.Fin using (Fin)
open import Function using (_∘_)

private
  variable
    a r ℓr : Level


mkLeftSemimodule : {𝓡 : Semiring r ℓr} → Set a → LeftSemimodule 𝓡 (r ⊔ a) (ℓr ⊔ a)
mkLeftSemimodule A = P.mkLeftSemimodule A U.leftSemimodule

mkRightSemimodule : {𝓡 : Semiring r ℓr} → Set a → RightSemimodule 𝓡 (r ⊔ a) (ℓr ⊔ a)
mkRightSemimodule A = P.mkRightSemimodule A U.rightSemimodule

mkSemimodule : {𝓡 : CommutativeSemiring r ℓr} → Set a → Semimodule 𝓡 (r ⊔ a) (ℓr ⊔ a)
mkSemimodule A = P.mkSemimodule A U.semimodule

{-
The `Carrierᴹ` of this `LeftSemimodule` has type
`Fin n → R` where `R` is the carrier of the `Semiring`.
-}

finLeftSemimodule : {𝓡 : Semiring r ℓr} → ℕ → LeftSemimodule 𝓡 r ℓr
finLeftSemimodule = mkLeftSemimodule ∘ Fin

finRightSemimodule : {𝓡 : Semiring r ℓr} → ℕ → RightSemimodule 𝓡 r ℓr
finRightSemimodule = mkRightSemimodule ∘ Fin

finSemimodule : {𝓡 : CommutativeSemiring r ℓr} → ℕ → Semimodule 𝓡 r ℓr
finSemimodule = mkSemimodule ∘ Fin
