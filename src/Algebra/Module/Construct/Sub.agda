{-# OPTIONS --safe --cubical-compatible #-}

{-
Restricting an algebraic (module) structure by a predicate
-}
module Algebra.Module.Construct.Sub where

open import Algebra.Bundles using (Semiring ; CommutativeMonoid)
open import Algebra.Construct.Sub
  using (mkCommutativeMonoid; mkSemiring)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Data.Product using (_,_; _×_; ∃)
open import Level using (Level ; _⊔_)

open import Relation.DependentOps
open Inj
open Inj'

private
   variable
      a b c ℓ ℓᵃ : Level

module _ {R : Semiring c ℓ}
         (M : LeftSemimodule R a ℓᵃ)
         (P : LeftSemimodule.Carrierᴹ M → Set b) where

  open Semiring R
  open LeftSemimodule M
  open ∃op P
  open ∃op' {A = Carrierᴹ} {B = Carrier} P

  leftSemimodule :
          D₂ _+ᴹ_
        → D₂' _*ₗ_
        → D₀ 0ᴹ
        → LeftSemimodule R (a ⊔ b) ℓᵃ
  leftSemimodule _+'_  _*ₗ'_ 0' = record
      { Carrierᴹ = ∃ P
      ; _*ₗ_ = ∃inj₂' _*ₗ'_
      ; isLeftSemimodule = record
        { +ᴹ-isCommutativeMonoid =  isCommutativeMonoid
        ; isPreleftSemimodule = record
          { *ₗ-cong =  *ₗ-cong
          ; *ₗ-zeroˡ = prop₁ *ₗ-zeroˡ
          ; *ₗ-distribʳ = prop₁ *ₗ-distribʳ
          ; *ₗ-identityˡ = prop₁ *ₗ-identityˡ
          ; *ₗ-assoc = λ x y → prop₁ (*ₗ-assoc x y)
          ; *ₗ-zeroʳ = *ₗ-zeroʳ
          ; *ₗ-distribˡ = λ x → prop₂ (*ₗ-distribˡ x)
          } }
      }
      where open CommutativeMonoid
                   (mkCommutativeMonoid +ᴹ-commutativeMonoid P _+'_ 0')
