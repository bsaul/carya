{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_; 0ℓ)
open import Algebra.Bundles using (Semiring ; CommutativeMonoid)

module Algebra.Module.Construct.Coarsen {r ℓʳ : Level} {𝓡 : Semiring r ℓʳ} where

open import Algebra.Bundles using (Semiring ; CommutativeMonoid)
open import Algebra.Definitions using (Congruent₂)
open import Algebra.Construct.Coarsen using (commutativeMonoid)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Function using (_∘_)
open import Relation.Binary using (Rel ; IsEquivalence ; _⇒_ )

open Semiring 𝓡 renaming (Carrier to R)

module _ {a ℓ ℓᵃ} (𝓜  : LeftSemimodule 𝓡 a ℓᵃ) where
   open LeftSemimodule 𝓜 renaming (Carrierᴹ to A)

   leftSemimodule :
        (_♯_ : Rel A ℓ)
      → IsEquivalence _♯_
      → Congruent₂ _♯_ _+ᴹ_
      → _≈ᴹ_ ⇒ _♯_
      → (∀ {r₁ r₂} {a₁ a₂} → r₁ ≈ r₂ → a₁ ♯ a₂ → (r₁ *ₗ a₁) ♯ (r₂ *ₗ a₂))
      → LeftSemimodule 𝓡 a ℓ
   leftSemimodule _♯_ isEquiv +ᴹ-cong ≈ᴹ-resp *ₗ-cong = record {
        Carrierᴹ = A
      ; isLeftSemimodule = record
         { +ᴹ-isCommutativeMonoid = isCommutativeMonoid
         ; isPreleftSemimodule = record
            { *ₗ-cong = *ₗ-cong
            ; *ₗ-zeroˡ = ≈ᴹ-resp ∘ *ₗ-zeroˡ
            ; *ₗ-distribʳ = λ a r₁ r₂ → ≈ᴹ-resp (*ₗ-distribʳ a r₁ r₂)
            ; *ₗ-identityˡ = ≈ᴹ-resp ∘ *ₗ-identityˡ
            ; *ₗ-assoc = λ a r₁ r₂ → ≈ᴹ-resp (*ₗ-assoc a r₁ r₂)
            ; *ₗ-zeroʳ = ≈ᴹ-resp ∘ *ₗ-zeroʳ
            ; *ₗ-distribˡ = λ r a₁ a₂ → ≈ᴹ-resp (*ₗ-distribˡ r a₁ a₂)
            }
         }
      }
      where open CommutativeMonoid
                  (commutativeMonoid
                     +ᴹ-commutativeMonoid _♯_ isEquiv +ᴹ-cong ≈ᴹ-resp)
