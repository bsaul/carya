---
title: Extensions of Modules and the Expectation Semiring type
---

Toying around with idea here (and references therein):
https://kevinbinz.com/2014/11/22/the-semiring-lifting-trick/

Also see [Algebra extension](https://en.wikipedia.org/wiki/Algebra_extension)

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (Semiring)

module Algebra.Module.Construct.Extension.Core {r ℓʳ} (𝓡 : Semiring r ℓʳ) where

open import Level using (Level; _⊔_; suc)
open import Algebra.Module.Bundles using (LeftSemimodule)
open import Data.Product using (map ; _×_; _,_)

open Semiring 𝓡 renaming (Carrier to R)
```

The carrier type for the expectation semiring.

```agda
record E {a ℓᵃ} (𝓜 : LeftSemimodule 𝓡 a ℓᵃ) : Set (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) where
  constructor ⟨_,_⟩

  open LeftSemimodule 𝓜 renaming (Carrierᴹ to A)
  field
    scalar : R
    m : A
```

## Equivalence

```agda
module _ {a ℓᵃ} {𝓜 : LeftSemimodule 𝓡 a ℓᵃ} where

  open LeftSemimodule 𝓜 renaming (Carrierᴹ to A)

  infix 4 _≈ₑ_
  _≈ₑ_ : E 𝓜 → E 𝓜 → Set _
  ⟨ r₁ , a₁ ⟩ ≈ₑ ⟨ r₂ , a₂ ⟩ = r₁ ≈ r₂ × a₁ ≈ᴹ a₂

  open import Algebra.Definitions (_≈ₑ_)
  open import Relation.Binary.Structures using (IsEquivalence)

  ≈ₑ-isEquivalence : IsEquivalence _≈ₑ_
  ≈ₑ-isEquivalence = record
    { refl = refl , ≈ᴹ-refl
    ; sym = map sym ≈ᴹ-sym
    ; trans = λ (a , b) (c , d) → trans a c , ≈ᴹ-trans b d
    }
```
