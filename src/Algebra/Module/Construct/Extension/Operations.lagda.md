---
title: Operations on the Module Extension
---

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_; suc)
open import Algebra.Bundles using (CommutativeSemiring ; Semiring)
open import Algebra.Module.Bundles using (LeftSemimodule)

module  Algebra.Module.Construct.Extension.Operations {a r ℓᵃ ℓʳ}
  ⦃ S : CommutativeSemiring r ℓʳ ⦄
  (𝓜ᴬ : LeftSemimodule (CommutativeSemiring.semiring S) a ℓᵃ)
  where

open import Algebra.Module.Construct.Extension.Core (CommutativeSemiring.semiring S)
open import Algebra.Definitions (_≈ₑ_ { 𝓜 = 𝓜ᴬ } )
open import Data.Product as × using (_×_; _,_)

open CommutativeSemiring S renaming (Carrier to R)
open LeftSemimodule 𝓜ᴬ renaming (Carrierᴹ to A)
```

## Operations and Identities

```agda
_⊕_ : E 𝓜ᴬ → E 𝓜ᴬ → E 𝓜ᴬ
⟨ r₁ , a₁ ⟩ ⊕ ⟨ r₂ ,  a₂ ⟩ = ⟨ r₁ + r₂ , a₁ +ᴹ a₂ ⟩

𝟘 : E 𝓜ᴬ
𝟘 = ⟨ 0# , 0ᴹ ⟩

_⊗_ : E 𝓜ᴬ → E 𝓜ᴬ → E 𝓜ᴬ
⟨ r₁ , a₁ ⟩ ⊗ ⟨ r₂ ,  a₂ ⟩ = ⟨ r₁ * r₂ , (r₁ *ₗ a₂) +ᴹ (r₂ *ₗ a₁) ⟩

𝟙 : E 𝓜ᴬ
𝟙 = ⟨ 1# , 0ᴹ ⟩
```

## Properties

```agda
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning
open ≈-Reasoning (record {isEquivalence = ≈ᴹ-isEquivalence})

⊕-assoc : Associative _⊕_ -- ∀ p q r → (p ⊕ q) ⊕ r ≈ₑ p ⊕ (q ⊕ r)
⊕-assoc ⟨ r₁ , a₁ ⟩ ⟨ r₂ , a₂ ⟩ ⟨ p₃ , r₃ ⟩ =
   (+-assoc r₁ r₂ p₃) , (+ᴹ-assoc a₁ a₂ r₃)

⊕-comm : Commutative _⊕_ --  ∀ p q → p ⊕ q ≈ₑ q ⊕ p
⊕-comm ⟨ r₁ , a₁ ⟩ ⟨ r₂ , a₂ ⟩ = (+-comm r₁ r₂) , (+ᴹ-comm a₁ a₂)

𝟘-⊕-identityʳ : RightIdentity 𝟘 _⊕_ -- ∀ p → p ⊕ 𝟘 ≈ₑ p
𝟘-⊕-identityʳ ⟨ p , r ⟩ =  (+-identityʳ p) , (+ᴹ-identityʳ r)

𝟘-⊕-identityˡ : LeftIdentity 𝟘 _⊕_ -- ∀ p → 𝟘 ⊕ p ≈ₑ p
𝟘-⊕-identityˡ ⟨ p , r ⟩ =  (+-identityˡ p) , (+ᴹ-identityˡ r)

𝟘-⊕-identity : Identity 𝟘 _⊕_
𝟘-⊕-identity = 𝟘-⊕-identityˡ , 𝟘-⊕-identityʳ

𝟙-⊗-identityʳ : RightIdentity 𝟙 _⊗_ -- ∀ p → p ⊗ 𝟙 ≈ₑ p
𝟙-⊗-identityʳ ⟨ p , r ⟩ =
   (*-identityʳ p)
   , (begin
      p *ₗ 0ᴹ +ᴹ 1# *ₗ r
    ≈⟨ +ᴹ-congʳ (*ₗ-zeroʳ p) ⟩
      0ᴹ +ᴹ 1# *ₗ r
    ≈⟨ +ᴹ-identityˡ (1# *ₗ r) ⟩
      1# *ₗ r
    ≈⟨ *ₗ-identityˡ r ⟩
      r
    ∎)

𝟙-⊗-identityˡ : LeftIdentity 𝟙 _⊗_  -- ∀ p → 𝟙 ⊗ p ≈ₑ p
𝟙-⊗-identityˡ ⟨ p , r ⟩ =
   (*-identityˡ p)
   , (begin
      1# *ₗ r +ᴹ p *ₗ 0ᴹ
    ≈⟨ +ᴹ-congˡ (*ₗ-zeroʳ p) ⟩
      1# *ₗ r +ᴹ 0ᴹ
    ≈⟨ +ᴹ-identityʳ (1# *ₗ r) ⟩
      1# *ₗ r
    ≈⟨ *ₗ-identityˡ r ⟩
      r
    ∎)

𝟙-⊗-identity : Identity 𝟙 _⊗_
𝟙-⊗-identity = 𝟙-⊗-identityˡ , 𝟙-⊗-identityʳ

𝟘-⊗-cancelˡ : LeftZero 𝟘 _⊗_ -- ∀ p → 𝟘 ⊗ p ≈ₑ 𝟘
𝟘-⊗-cancelˡ ⟨ p , r ⟩ =
   (×.proj₁ zero p)
   , (begin
      0# *ₗ r +ᴹ p *ₗ 0ᴹ
    ≈⟨ +ᴹ-congˡ (*ₗ-zeroʳ p) ⟩
      0# *ₗ r +ᴹ 0ᴹ
    ≈⟨ +ᴹ-identityʳ (0# *ₗ r) ⟩
      0# *ₗ r
    ≈⟨ *ₗ-zeroˡ r ⟩
      0ᴹ
    ∎)

𝟘-⊗-cancelʳ : RightZero 𝟘 _⊗_ -- ∀ p → p ⊗ 𝟘 ≈ₑ 𝟘
𝟘-⊗-cancelʳ ⟨ p , r ⟩ =
   (×.proj₂ zero p)
   , (begin
      p *ₗ 0ᴹ +ᴹ 0# *ₗ r
    ≈⟨ +ᴹ-congʳ (*ₗ-zeroʳ p) ⟩
      0ᴹ +ᴹ  0# *ₗ r
    ≈⟨ +ᴹ-identityˡ (0# *ₗ r) ⟩
      0# *ₗ r
    ≈⟨ *ₗ-zeroˡ r ⟩
      0ᴹ
    ∎)

𝟘-⊗-cancel : Zero 𝟘 _⊗_
𝟘-⊗-cancel = 𝟘-⊗-cancelˡ , 𝟘-⊗-cancelʳ

⊗-assoc : Associative _⊗_ -- ∀ p q r → (p ⊗ q) ⊗ r ≈ₑ p ⊗ (q ⊗ r)
⊗-assoc ⟨ r₁ , a₁ ⟩ ⟨ r₂ , a₂ ⟩ ⟨ p₃ , r₃ ⟩ =
     (*-assoc r₁ r₂ p₃)
     , (begin
      (r₁ * r₂) *ₗ r₃ +ᴹ p₃ *ₗ (r₁ *ₗ a₂ +ᴹ r₂ *ₗ a₁)
    ≈⟨ +ᴹ-congˡ (*ₗ-distribˡ p₃ (r₁ *ₗ a₂) (r₂ *ₗ a₁)) ⟩
      (r₁ * r₂) *ₗ r₃ +ᴹ (p₃ *ₗ r₁ *ₗ a₂ +ᴹ p₃ *ₗ r₂ *ₗ a₁)
    ≈˘⟨ +ᴹ-assoc ((r₁ * r₂) *ₗ r₃) (p₃ *ₗ r₁ *ₗ a₂) (p₃ *ₗ r₂ *ₗ a₁) ⟩
      ((r₁ * r₂) *ₗ r₃ +ᴹ p₃ *ₗ r₁ *ₗ a₂) +ᴹ p₃ *ₗ r₂ *ₗ a₁
    ≈⟨ +ᴹ-congˡ (≈ᴹ-sym (*ₗ-assoc p₃ r₂ a₁)) ⟩
      ((r₁ * r₂) *ₗ r₃ +ᴹ p₃ *ₗ r₁ *ₗ a₂) +ᴹ (p₃ * r₂) *ₗ a₁
    ≈⟨ +ᴹ-congˡ (*ₗ-congʳ (CommutativeSemiring.*-comm S p₃ r₂)) ⟩
      ((r₁ * r₂) *ₗ r₃ +ᴹ p₃ *ₗ r₁ *ₗ a₂) +ᴹ (r₂ * p₃) *ₗ a₁
    ≈⟨ +ᴹ-congʳ (+ᴹ-congˡ (≈ᴹ-sym (*ₗ-assoc p₃ r₁ a₂))) ⟩
      ((r₁ * r₂) *ₗ r₃ +ᴹ (p₃ * r₁) *ₗ a₂) +ᴹ (r₂ * p₃) *ₗ a₁
    ≈⟨ +ᴹ-congʳ (+ᴹ-congˡ (*ₗ-congʳ (CommutativeSemiring.*-comm S p₃ r₁))) ⟩
      ((r₁ * r₂) *ₗ r₃ +ᴹ (r₁ * p₃) *ₗ a₂) +ᴹ (r₂ * p₃) *ₗ a₁
    ≈⟨ +ᴹ-congʳ (+ᴹ-congˡ (*ₗ-assoc r₁ p₃ a₂)) ⟩
      ((r₁ * r₂) *ₗ r₃ +ᴹ r₁ *ₗ p₃ *ₗ a₂) +ᴹ (r₂ * p₃) *ₗ a₁
    ≈⟨ +ᴹ-congʳ (+ᴹ-congʳ (*ₗ-assoc r₁ r₂ r₃)) ⟩
      (r₁ *ₗ r₂ *ₗ r₃ +ᴹ r₁ *ₗ p₃ *ₗ a₂) +ᴹ (r₂ * p₃) *ₗ a₁
    ≈⟨ +ᴹ-congʳ (≈ᴹ-sym (*ₗ-distribˡ r₁ (r₂ *ₗ r₃) (p₃ *ₗ a₂))) ⟩
      r₁ *ₗ (r₂ *ₗ r₃ +ᴹ p₃ *ₗ a₂) +ᴹ (r₂ * p₃) *ₗ a₁
    ∎)

⊗-comm : Commutative _⊗_
⊗-comm ⟨ r₁ , a₁ ⟩ ⟨ r₂ , a₂ ⟩ =
    (*-comm r₁ r₂) , (+ᴹ-comm (r₁ *ₗ a₂) (r₂ *ₗ a₁))

⊗-distrˡ-⊕ : _⊗_ DistributesOverˡ _⊕_ -- ∀ p q r → p ⊗ (q ⊕ r) ≈ₑ (p ⊗ q) ⊕ (p ⊗ r)
⊗-distrˡ-⊕ ⟨ r₁ , a₁ ⟩ ⟨ r₂ , a₂ ⟩ ⟨ p₃ , r₃ ⟩ =
    (distribˡ r₁ r₂ p₃)
    , (begin
      r₁ *ₗ (a₂ +ᴹ r₃) +ᴹ (r₂ + p₃) *ₗ a₁
    ≈⟨ +ᴹ-congʳ (*ₗ-distribˡ r₁ a₂ r₃) ⟩
      r₁ *ₗ a₂ +ᴹ r₁ *ₗ r₃ +ᴹ (r₂ + p₃) *ₗ a₁
    ≈⟨ +ᴹ-congˡ (*ₗ-distribʳ a₁ r₂ p₃) ⟩
      (r₁ *ₗ a₂ +ᴹ r₁ *ₗ r₃) +ᴹ (r₂ *ₗ a₁ +ᴹ p₃ *ₗ a₁)
    ≈⟨ +ᴹ-assoc (r₁ *ₗ a₂) (r₁ *ₗ r₃) (r₂ *ₗ a₁ +ᴹ p₃ *ₗ a₁) ⟩
      r₁ *ₗ a₂ +ᴹ (r₁ *ₗ r₃ +ᴹ (r₂ *ₗ a₁ +ᴹ p₃ *ₗ a₁))
    ≈⟨ +ᴹ-congˡ (≈ᴹ-sym (+ᴹ-assoc (r₁ *ₗ r₃) (r₂ *ₗ a₁) (p₃ *ₗ a₁))) ⟩
      r₁ *ₗ a₂ +ᴹ ((r₁ *ₗ r₃ +ᴹ r₂ *ₗ a₁) +ᴹ p₃ *ₗ a₁)
    ≈⟨ +ᴹ-congˡ (+ᴹ-congʳ (+ᴹ-comm (r₁ *ₗ r₃) (r₂ *ₗ a₁))) ⟩
      r₁ *ₗ a₂ +ᴹ ((r₂ *ₗ a₁ +ᴹ r₁ *ₗ r₃) +ᴹ p₃ *ₗ a₁)
    ≈⟨ +ᴹ-congˡ ( +ᴹ-assoc (r₂ *ₗ a₁) (r₁ *ₗ r₃) (p₃ *ₗ a₁)) ⟩
      r₁ *ₗ a₂ +ᴹ (r₂ *ₗ a₁ +ᴹ (r₁ *ₗ r₃ +ᴹ p₃ *ₗ a₁))
    ≈˘⟨ +ᴹ-assoc (r₁ *ₗ a₂) (r₂ *ₗ a₁) (r₁ *ₗ r₃ +ᴹ p₃ *ₗ a₁) ⟩
      r₁ *ₗ a₂ +ᴹ r₂ *ₗ a₁ +ᴹ (r₁ *ₗ r₃ +ᴹ p₃ *ₗ a₁)
    ∎)

⊗-distrʳ-⊕ : _⊗_ DistributesOverʳ _⊕_ -- ∀ p q r → (q ⊕ r) ⊗ p ≈ₑ (q ⊗ p) ⊕ (r ⊗ p)
⊗-distrʳ-⊕ ⟨ r₁ , a₁ ⟩ ⟨ r₂ , a₂ ⟩ ⟨ p₃ , r₃ ⟩ =
    (distribʳ r₁ r₂ p₃)
    , (begin
       (r₂ + p₃) *ₗ a₁ +ᴹ r₁ *ₗ (a₂ +ᴹ r₃)
     ≈⟨ +ᴹ-congʳ (*ₗ-distribʳ a₁ r₂ p₃) ⟩
       (r₂ *ₗ a₁ +ᴹ p₃ *ₗ a₁) +ᴹ r₁ *ₗ (a₂ +ᴹ r₃)
     ≈⟨ +ᴹ-congˡ (*ₗ-distribˡ r₁ a₂ r₃) ⟩
       (r₂ *ₗ a₁ +ᴹ p₃ *ₗ a₁) +ᴹ (r₁ *ₗ a₂ +ᴹ r₁ *ₗ r₃)
     ≈⟨ +ᴹ-assoc (r₂ *ₗ a₁) (p₃ *ₗ a₁) (r₁ *ₗ a₂ +ᴹ r₁ *ₗ r₃) ⟩
       r₂ *ₗ a₁ +ᴹ (p₃ *ₗ a₁ +ᴹ (r₁ *ₗ a₂ +ᴹ r₁ *ₗ r₃))
     ≈⟨ +ᴹ-congˡ (≈ᴹ-sym (+ᴹ-assoc (p₃ *ₗ a₁) (r₁ *ₗ a₂) (r₁ *ₗ r₃))) ⟩
       r₂ *ₗ a₁ +ᴹ ((p₃ *ₗ a₁ +ᴹ r₁ *ₗ a₂) +ᴹ r₁ *ₗ r₃)
     ≈⟨ +ᴹ-congˡ (+ᴹ-congʳ (+ᴹ-comm (p₃ *ₗ a₁) (r₁ *ₗ a₂))) ⟩
       r₂ *ₗ a₁ +ᴹ ((r₁ *ₗ a₂ +ᴹ p₃ *ₗ a₁) +ᴹ r₁ *ₗ r₃)
     ≈⟨ +ᴹ-congˡ ( +ᴹ-assoc (r₁ *ₗ a₂) (p₃ *ₗ a₁) (r₁ *ₗ r₃)) ⟩
       r₂ *ₗ a₁ +ᴹ (r₁ *ₗ a₂ +ᴹ (p₃ *ₗ a₁ +ᴹ r₁ *ₗ r₃))
     ≈˘⟨ +ᴹ-assoc (r₂ *ₗ a₁) (r₁ *ₗ a₂)  (p₃ *ₗ a₁ +ᴹ r₁ *ₗ r₃) ⟩
       r₂ *ₗ a₁ +ᴹ r₁ *ₗ a₂ +ᴹ (p₃ *ₗ a₁ +ᴹ r₁ *ₗ r₃)
     ∎)

⊗-distr-⊕ : _⊗_ DistributesOver _⊕_
⊗-distr-⊕ = ⊗-distrˡ-⊕ , ⊗-distrʳ-⊕
```
