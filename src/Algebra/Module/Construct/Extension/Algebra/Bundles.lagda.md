---
title: Algebraic Bundles on the Expectation Semiring
---

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_; suc)
open import Algebra.Bundles
open import Algebra.Module.Bundles using (LeftSemimodule)

module  Algebra.Module.Construct.Extension.Algebra.Bundles {a r ℓᵃ ℓʳ}
  ⦃ S : CommutativeSemiring r ℓʳ ⦄
  (𝓜 : LeftSemimodule (CommutativeSemiring.semiring S) a ℓᵃ)
  where

open import Algebra.Module.Construct.Extension.Core (CommutativeSemiring.semiring S)
open import Algebra.Module.Construct.Extension.Operations 𝓜
open import Algebra.Definitions (_≈ₑ_ { 𝓜 = 𝓜 } )
open import Data.Product as × using (_×_; _,_)

open CommutativeSemiring S renaming (Carrier to P) using (_+_ ; +-cong; *-cong)
open LeftSemimodule 𝓜 renaming (Carrierᴹ to A)
```

## Bundles

### Bundles on `_⊕_`

```agda
⊕-magma : Magma (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (ℓᵃ ⊔ ℓʳ)
⊕-magma = record {
    Carrier = E 𝓜
  ; _≈_ = _≈ₑ_
  ; _∙_ = _⊕_
  ; isMagma = record {
      isEquivalence = ≈ₑ-isEquivalence
    ; ∙-cong = λ (a , b) (c , d) →  +-cong a c , +ᴹ-cong  b d
    }
  }

⊕-semigroup : Semigroup (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (ℓᵃ ⊔ ℓʳ)
⊕-semigroup = record { isSemigroup = record {
     isMagma = isMagma
    ; assoc = ⊕-assoc
    }
  }
  where open Magma ⊕-magma

⊕-monoid : Monoid (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (ℓᵃ ⊔ ℓʳ)
⊕-monoid = record {
  ε = 𝟘
  ; isMonoid = record {
        isSemigroup = isSemigroup
      ; identity = 𝟘-⊕-identity
      }
  }
  where open Semigroup ⊕-semigroup

⊕-commutativeMonoid : CommutativeMonoid (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (ℓᵃ ⊔ ℓʳ)
⊕-commutativeMonoid = record
  { isCommutativeMonoid = record {
     isMonoid = isMonoid
    ; comm = ⊕-comm }
  }
  where open Monoid ⊕-monoid
```

### Bundles on `_⊗_`

```agda
⊗-magma : Magma (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (ℓᵃ ⊔ ℓʳ)
⊗-magma = record {
    Carrier = E 𝓜
  ; _≈_ = _≈ₑ_
  ; _∙_ = _⊗_
  ; isMagma = record {
      isEquivalence = ≈ₑ-isEquivalence
    ; ∙-cong = λ (a , b) (c , d) →
        (*-cong a c) , +ᴹ-cong (*ₗ-cong a d) (*ₗ-cong c b)
    }
  }

⊗-semigroup : Semigroup (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (ℓᵃ ⊔ ℓʳ)
⊗-semigroup = record {
   isSemigroup = record { isMagma = isMagma ; assoc = ⊗-assoc }
  }
  where open Magma ⊗-magma

⊗-monoid : Monoid (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (ℓᵃ ⊔ ℓʳ)
⊗-monoid = record {
  ε = 𝟙
  ; isMonoid = record {
        isSemigroup = isSemigroup
      ; identity = 𝟙-⊗-identity
      }
  }
  where open Semigroup ⊗-semigroup

⊗-commutativeMonoid : CommutativeMonoid (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (ℓᵃ ⊔ ℓʳ)
⊗-commutativeMonoid = record
  { isCommutativeMonoid = record {
     isMonoid = isMonoid
    ; comm = ⊗-comm }
  }
  where open Monoid ⊗-monoid
```

### Bundles on `_⊕_` and `_⊗_`

```agda
⊕-⊗-semiringWithoutAnnihilatingZero :
  SemiringWithoutAnnihilatingZero (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (ℓᵃ ⊔ ℓʳ)
⊕-⊗-semiringWithoutAnnihilatingZero = record
  { isSemiringWithoutAnnihilatingZero = record
        { +-isCommutativeMonoid = isCommutativeMonoid
        ; *-cong = ⊗-cong
        ; *-assoc = ⊗-assoc
        ; *-identity = 𝟙-⊗-identity
        ; distrib = ⊗-distr-⊕
        }
  }
  where open CommutativeMonoid ⊕-commutativeMonoid using (isCommutativeMonoid)
        open Monoid ⊗-monoid renaming (∙-cong to ⊗-cong )

⊕-⊗-semiring : Semiring (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (ℓᵃ ⊔ ℓʳ)
⊕-⊗-semiring = record
    {  isSemiring = record {
          isSemiringWithoutAnnihilatingZero = isSemiringWithoutAnnihilatingZero
        ; zero = 𝟘-⊗-cancel }
    }
    where open
            SemiringWithoutAnnihilatingZero ⊕-⊗-semiringWithoutAnnihilatingZero

⊕-⊗-commutativeSemiring : CommutativeSemiring (a ⊔ r ⊔ ℓᵃ ⊔ ℓʳ) (ℓᵃ ⊔ ℓʳ)
⊕-⊗-commutativeSemiring = record
    {  isCommutativeSemiring = record {
          isSemiring = isSemiring
        ; *-comm = ⊗-comm }
    }
    where open Semiring ⊕-⊗-semiring
```
