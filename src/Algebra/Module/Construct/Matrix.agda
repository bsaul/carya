{-# OPTIONS --without-K --safe #-}

open import Algebra.Bundles using (Semiring)

module Algebra.Module.Construct.Matrix
  {c ℓ}
  (𝓡 : Semiring c ℓ)
  where

open import Algebra.Module.Bundles
  using (LeftSemimodule)
open import Data.Fin using (Fin; zero; suc)
open import Data.Nat using (ℕ)

open import Algebra.Module.Construct.Array 𝓡

{-
The `Carrierᴹ` of this `LeftSemimodule` has type
`Fin m → Fin n → R` where `R` is the carrier of the `Semiring`.
-}

matrix : ℕ → ℕ → LeftSemimodule 𝓡 c ℓ
matrix m n = finArray 2 λ {zero → m ; (suc _) → n}
