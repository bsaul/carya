{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (Semiring)

module Algebra.Module.Construct.Exp {r ℓʳ} {𝓡 : Semiring r ℓʳ} where

open import Algebra.Module.Bundles
import Algebra.Module.Construct.DirectProduct as DP
open import Algebra.Module.Construct.Zero as 𝟘 using ()

open import Data.Nat using (ℕ) ; open ℕ

leftSemimodule : ∀ {a ℓ}
   → LeftSemimodule 𝓡 a ℓ
   → ℕ
   → LeftSemimodule 𝓡 a ℓ
leftSemimodule M zero = 𝟘.leftSemimodule
leftSemimodule M (suc n) = M ⊕ leftSemimodule M n
  where open DP renaming (leftSemimodule to _⊕_) using ()
