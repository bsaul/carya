{-# OPTIONS --safe --cubical-compatible #-}

open import Relation.Binary using (Rel)

module Algebra.Ordered.Total.Bundles where

open import Level using (Level; _⊔_; suc)
open import Algebra.Bundles
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Ordered.Total.Structures
import Algebra.Construct.NaturalChoice.Min as MIN
import Algebra.Construct.NaturalChoice.Max as MAX
open import Algebra.Core using (Op₁ ; Op₂)
open import Data.Sum using (inj₁ ; inj₂)
open import Relation.Binary.Bundles
import Relation.Binary.Reasoning.PartialOrder as <-Reasoning

record ToMagma c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _∙_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_     : Rel Carrier ℓ₁
    _≤_     : Rel Carrier ℓ₂
    _∙_     : Op₂ Carrier
    isToMagma : IsToMagma _≈_ _≤_ _∙_

  open IsToMagma isToMagma public

  magma : Magma c ℓ₁
  magma = record { isMagma = isMagma }

  poMagma : PoMagma c ℓ₁ ℓ₂
  poMagma = record { isPoMagma = isPoMagma }

  totalOrder : TotalOrder c ℓ₁ ℓ₂
  totalOrder = record { isTotalOrder = isTotalOrder }

  open TotalOrder totalOrder using (_≰_) public
  open module Min = MIN totalOrder public
  open module Max = MAX totalOrder public

  poset : Poset c ℓ₁ ℓ₂
  poset = record { isPartialOrder = isPartialOrder }

  open Magma magma public
    using (rawMagma ; _≉_)

record ToSemigroup c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _∙_
  infix  4 _≈_ _≤_
  field
    Carrier     : Set c
    _≈_         : Rel Carrier ℓ₁
    _≤_         : Rel Carrier ℓ₂
    _∙_         : Op₂ Carrier
    isToSemigroup : IsToSemigroup _≈_ _≤_ _∙_

  open IsToSemigroup isToSemigroup public

  toMagma : ToMagma c ℓ₁ ℓ₂
  toMagma = record { isToMagma = isToMagma }

  open ToMagma toMagma public
    using (
        totalOrder
      ; poset
      ; magma
      ; module Min
      ; module Max
      ; _≰_
      )

  poSemigroup : PoSemigroup c ℓ₁ ℓ₂
  poSemigroup = record { isPoSemigroup = isPoSemigroup }

  semigroup : Semigroup c ℓ₁
  semigroup = record { isSemigroup = isSemigroup }

  open Semigroup semigroup public
    hiding (isMagma; isSemigroup; magma; refl; reflexive; setoid)

record ToMonoid c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _∙_
  infix  4 _≈_ _≤_
  field
    Carrier  : Set c
    _≈_      : Rel Carrier ℓ₁
    _≤_      : Rel Carrier ℓ₂
    _∙_      : Op₂ Carrier
    ε        : Carrier
    isToMonoid : IsToMonoid _≈_ _≤_ _∙_ ε

  open IsToMonoid isToMonoid public

  toSemigroup : ToSemigroup c ℓ₁ ℓ₂
  toSemigroup = record { isToSemigroup = isToSemigroup }

  poMonoid : PoMonoid c ℓ₁ ℓ₂
  poMonoid = record { isPoMonoid = isPoMonoid }

  open ToSemigroup toSemigroup public
    using
      ( totalOrder
      ; poset
      ; toMagma
      ; magma
      ; rawMagma
      ; semigroup
      ; module Min
      ; module Max
      ; _≉_
      ; _≰_
      )

  monoid : Monoid c ℓ₁
  monoid = record { isMonoid = isMonoid }

  open Monoid monoid public
    using (rawMonoid)

record ToCommutativeMonoid c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _∙_
  infix  4 _≈_ _≤_
  field
    Carrier             : Set c
    _≈_                 : Rel Carrier ℓ₁
    _≤_                 : Rel Carrier ℓ₂
    _∙_                 : Op₂ Carrier
    ε                   : Carrier
    isToCommutativeMonoid : IsToCommutativeMonoid _≈_ _≤_ _∙_ ε

  open IsToCommutativeMonoid isToCommutativeMonoid public

  toMonoid : ToMonoid c ℓ₁ ℓ₂
  toMonoid = record { isToMonoid = isToMonoid }

  open ToMonoid toMonoid public
    using
     ( totalOrder
     ; poset
     ; toMagma
     ; toSemigroup
     ; magma
     ; semigroup
     ; rawMonoid
     ; poMonoid
     ; module Min
     ; module Max
     ; _≉_
     ; _≰_
     )

  poCommutativeMonoid : PoCommutativeMonoid c ℓ₁ ℓ₂
  poCommutativeMonoid = record { isPoCommutativeMonoid = isPoCommutativeMonoid }

  commutativeMonoid : CommutativeMonoid c ℓ₁
  commutativeMonoid = record { isCommutativeMonoid = isCommutativeMonoid}

record ToGroup c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⁻¹
  infixl 7 _∙_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_     : Rel Carrier ℓ₁
    _≤_     : Rel Carrier ℓ₂
    _∙_     : Op₂ Carrier
    ε       : Carrier
    _⁻¹     : Op₁ Carrier
    isToGroup : IsToGroup _≈_ _≤_ _∙_ ε _⁻¹

  open IsToGroup isToGroup public

  toMonoid : ToMonoid c ℓ₁ ℓ₂
  toMonoid = record { isToMonoid = isToMonoid }

  open ToMonoid toMonoid public
    using
      ( totalOrder
      ; poset
      ; toMagma
      ; toSemigroup
      ; magma
      ; semigroup
      ; rawMonoid
      ; module Min
      ; module Max
      ; _≉_
      ; _≰_
      )

  poGroup : PoGroup c ℓ₁ ℓ₂
  poGroup = record { isPoGroup = isPoGroup }

  group : Group c ℓ₁
  group = record { isGroup = isGroup }

record ToAbelianGroup c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⁻¹
  infixl 7 _∙_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_     : Rel Carrier ℓ₁
    _≤_     : Rel Carrier ℓ₂
    _∙_     : Op₂ Carrier
    ε       : Carrier
    _⁻¹     : Op₁ Carrier
    isToAbelianGroup : IsToAbelianGroup _≈_ _≤_ _∙_ ε _⁻¹

  open IsToAbelianGroup isToAbelianGroup public

  toGroup : ToGroup c ℓ₁ ℓ₂
  toGroup = record { isToGroup = isToGroup }

  toCommutativeMonoid : ToCommutativeMonoid c ℓ₁ ℓ₂
  toCommutativeMonoid = record { isToCommutativeMonoid = isToCommutativeMonoid }

  open ToCommutativeMonoid toCommutativeMonoid public
    using
     ( totalOrder
     ; poset
     ; toMagma
     ; toSemigroup
     ; commutativeMonoid
     )

  poAbelianGroup : PoAbelianGroup c ℓ₁ ℓ₂
  poAbelianGroup = record { isPoAbelianGroup = isPoAbelianGroup }

  abelianGroup : AbelianGroup c ℓ₁
  abelianGroup = record { isAbelianGroup = isAbelianGroup }


record ToSemiringWithoutAnnihilatingZero c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier    : Set c
    _≈_        : Rel Carrier ℓ₁
    _≤_        : Rel Carrier ℓ₂
    _+_        : Op₂ Carrier
    _*_        : Op₂ Carrier
    0#         : Carrier
    1#         : Carrier
    isToSemiringWithoutAnnihilatingZero :
      IsToSemiringWithoutAnnihilatingZero _≈_ _≤_ _+_ _*_ 0# 1#

  open IsToSemiringWithoutAnnihilatingZero isToSemiringWithoutAnnihilatingZero public

  +-toCommutativeMonoid : ToCommutativeMonoid c ℓ₁ ℓ₂
  +-toCommutativeMonoid = record { isToCommutativeMonoid = +-isToCommutativeMonoid }

  +-commutativeMonoid : CommutativeMonoid c ℓ₁
  +-commutativeMonoid = record { isCommutativeMonoid = +-isCommutativeMonoid }

  open ToCommutativeMonoid +-toCommutativeMonoid public
    using (
        totalOrder
      ; poset
      ; setoid
      ; module Min
      ; module Max
      ; _≉_
      ; _≰_
    )
    renaming
     ( toMonoid to +-toMonoid
     ; poMonoid to +-poMonoid
     ; toMagma  to +-toMagma
     ; toSemigroup to +-toSemigroup
     )

  poSemiringWithoutAnnihilatingZero : PoSemiringWithoutAnnihilatingZero c ℓ₁ ℓ₂
  poSemiringWithoutAnnihilatingZero =
    record
      { isPoSemiringWithoutAnnihilatingZero = isPoSemiringWithoutAnnihilatingZero }

  open PoSemiringWithoutAnnihilatingZero poSemiringWithoutAnnihilatingZero public
    using
     ( +-commutativeSemigroup
     ; +-magma
     ; +-semigroup
     )

  semiringWithoutAnnihilatingZero : SemiringWithoutAnnihilatingZero c ℓ₁
  semiringWithoutAnnihilatingZero =
    record
      { isSemiringWithoutAnnihilatingZero = isSemiringWithoutAnnihilatingZero }

record ToSemiring c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier    : Set c
    _≈_        : Rel Carrier ℓ₁
    _≤_        : Rel Carrier ℓ₂
    _+_        : Op₂ Carrier
    _*_        : Op₂ Carrier
    0#         : Carrier
    1#         : Carrier
    isToSemiring : IsToSemiring _≈_ _≤_ _+_ _*_ 0# 1#

  open IsToSemiring isToSemiring public

  toSemiringWithoutAnnihilatingZero : ToSemiringWithoutAnnihilatingZero c ℓ₁ ℓ₂
  toSemiringWithoutAnnihilatingZero =
    record { isToSemiringWithoutAnnihilatingZero =
              isToSemiringWithoutAnnihilatingZero }

  open ToSemiringWithoutAnnihilatingZero toSemiringWithoutAnnihilatingZero public
    using (
        totalOrder
      ; poset
      ; setoid
      ; +-toMagma
      ; +-toSemigroup
      ; +-toMonoid
      ; +-poMonoid
      ; +-commutativeSemigroup
      ; +-magma
      ; +-semigroup
      ; module Min
      ; module Max
      ; _≉_
      ; _≰_
      )

  poSemiring : PoSemiring c ℓ₁ ℓ₂
  poSemiring = record { isPoSemiring = isPoSemiring }

  semiring : Semiring c ℓ₁
  semiring = record { isSemiring = isSemiring }

  open Semiring semiring public using
     ( +-rawMonoid
     ; +-monoid
     ; rawSemiring
     )

record ToCommutativeSemiring c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier               : Set c
    _≈_                   : Rel Carrier ℓ₁
    _≤_                   : Rel Carrier ℓ₂
    _+_                   : Op₂ Carrier
    _*_                   : Op₂ Carrier
    0#                    : Carrier
    1#                    : Carrier
    isToCommutativeSemiring : IsToCommutativeSemiring _≈_ _≤_ _+_ _*_ 0# 1#

  open IsToCommutativeSemiring isToCommutativeSemiring public

  toSemiring : ToSemiring c ℓ₁ ℓ₂
  toSemiring = record { isToSemiring = isToSemiring }

  open ToSemiring toSemiring public
    using (totalOrder; poset; setoid; semiring)

  poCommutativeSemiring : PoCommutativeSemiring c ℓ₁ ℓ₂
  poCommutativeSemiring = record
   { isPoCommutativeSemiring = isPoCommutativeSemiring }

  commutativeSemiring : CommutativeSemiring c ℓ₁
  commutativeSemiring = record
   { isCommutativeSemiring = isCommutativeSemiring }

record ToRing c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier  : Set c
    _≈_      : Rel Carrier ℓ₁
    _≤_      : Rel Carrier ℓ₂
    _+_      : Op₂ Carrier
    _*_      : Op₂ Carrier
    -_       : Op₁ Carrier
    0#       : Carrier
    1#       : Carrier
    isToRing : IsToRing _≈_ _≤_ _+_ _*_ -_ 0# 1#

  open IsToRing isToRing public

  poRing : PoRing c ℓ₁ ℓ₂
  poRing = record { isPoRing = isPoRing }

  ring : Ring c ℓ₁
  ring = record { isRing = isRing }

  ringWithoutOne : RingWithoutOne c ℓ₁
  ringWithoutOne = record { isRingWithoutOne = isRingWithoutOne }

  toSemiring : ToSemiring c ℓ₁ ℓ₂
  toSemiring = record { isToSemiring = isToSemiring }

  +-toAbelianGroup : ToAbelianGroup c ℓ₁ ℓ₂
  +-toAbelianGroup = record { isToAbelianGroup = +-isToAbelianGroup }

  +-toGroup : ToGroup c ℓ₁ ℓ₂
  +-toGroup = record { isToGroup = +-isToGroup }

  open ToSemiring toSemiring public
    using
      ( totalOrder
      ; poset
      ; semiring
      ; poSemiring
      ; rawSemiring
      ; +-commutativeSemigroup
      ; +-toMonoid
      ; +-poMonoid
      ; module Min
      ; module Max
      ; _≉_
      ; _≰_
      )

record ToCommutativeRing c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier  : Set c
    _≈_      : Rel Carrier ℓ₁
    _≤_      : Rel Carrier ℓ₂
    _+_      : Op₂ Carrier
    _*_      : Op₂ Carrier
    -_       : Op₁ Carrier
    0#       : Carrier
    1#       : Carrier
    isToCommutativeRing : IsToCommutativeRing _≈_ _≤_ _+_ _*_ -_ 0# 1#

  open IsToCommutativeRing isToCommutativeRing public

  poCommutativeRing : PoCommutativeRing c ℓ₁ ℓ₂
  poCommutativeRing = record { isPoCommutativeRing = isPoCommutativeRing }

  open PoCommutativeRing poCommutativeRing public
    using
      ( poCommutativeSemiring
      ; commutativeRing
      ; commutativeSemiring
      ; *-commutativeSemigroup)

  toRing : ToRing c ℓ₁ ℓ₂
  toRing = record { isToRing = isToRing }

  open ToRing toRing public
    using
     ( totalOrder
     ; poset
     ; ring
     ; semiring
     ; poSemiring
     ; rawSemiring
     ; toSemiring
     ; +-toAbelianGroup
     ; +-toMonoid
     ; +-poMonoid
     ; +-commutativeSemigroup
     ; ringWithoutOne
     ; module Min
     ; module Max
     ; _≉_
     ; _≰_
     )

  toCommutativeSemiring : ToCommutativeSemiring c ℓ₁ ℓ₂
  toCommutativeSemiring = record
    { isToCommutativeSemiring = isToCommutativeSemiring }
