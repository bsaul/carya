{-# OPTIONS --safe --cubical-compatible #-}

module Algebra.Ordered.Total.Construct.NonTrivialToRing where

open import Algebra.Ordered.Total.Bundles
open import Data.Product using (∃)
open import Level using (Level)

private
  variable
    c ℓ₁ ℓ₂ : Level

IsNonTrivial : ToRing c ℓ₁ ℓ₂ → Set _
IsNonTrivial 𝓡 = 1# ≰ 0#
 where open ToRing 𝓡

NonTrivialToRing : (c ℓ₁ ℓ₂ : Level) → Set _
NonTrivialToRing c ℓ₁ ℓ₂ = ∃ λ (𝓡 : ToRing c ℓ₁ ℓ₂) → IsNonTrivial 𝓡
