{-# OPTIONS --safe --cubical-compatible #-}

open import Relation.Binary using (Rel)

module Algebra.Ordered.Total.Structures
  {a ℓ₁ ℓ₂} {A : Set a}
  (_≈_ : Rel A ℓ₁)
  (_≤_ : Rel A ℓ₂)
  where

open import Level using (Level; _⊔_)
open import Algebra.Structures _≈_
open import Algebra.Core using (Op₁ ; Op₂)
open import Algebra.Definitions _≤_ as ≤Defs using ()
open import Algebra.Definitions _≈_
open import Algebra.Ordered.Partial.Structures _≈_ _≤_
open import Data.Sum using (inj₁ ; inj₂)
open import Data.Product
open import Relation.Binary.Structures using (IsTotalOrder)
open import Relation.Binary.Consequences

record IsToMagma (∙ : Op₂ A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isTotalOrder : IsTotalOrder _≈_ _≤_
    ∙-cong         : Congruent₂ ∙
    ∙-compat       : ∀ {x y z} → x ≤ y → ∙ x z ≤ ∙ y z

  open IsTotalOrder isTotalOrder public

  isPoMagma : IsPoMagma ∙
  isPoMagma = record
     { isPartialOrder = isPartialOrder
     ; ∙-cong = ∙-cong
     ; ∙-compat = ∙-compat
     }

  isMagma : IsMagma ∙
  isMagma = record { isEquivalence = isEquivalence ; ∙-cong = ∙-cong }

  open IsMagma isMagma public using (setoid)

record IsToSemigroup (∙ : Op₂ A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isToMagma : IsToMagma ∙
    assoc   : Associative ∙

  open IsToMagma isToMagma public

  isPoSemigroup : IsPoSemigroup ∙
  isPoSemigroup = record { isPoMagma = isPoMagma ; assoc = assoc }

  isSemigroup : IsSemigroup ∙
  isSemigroup = record { isMagma = isMagma ; assoc = assoc }

record IsToMonoid (∙ : Op₂ A) (ε : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isToSemigroup : IsToSemigroup ∙
    identity    : Identity ε ∙

  open IsToSemigroup isToSemigroup public

  isPoMonoid : IsPoMonoid ∙ ε
  isPoMonoid = record { isPoSemigroup = isPoSemigroup ; identity = identity }

  isMonoid : IsMonoid ∙ ε
  isMonoid = record { isSemigroup = isSemigroup ; identity = identity }

  open IsMonoid isMonoid public
    using (identityˡ ; identityʳ ; isUnitalMagma)

record IsToCommutativeMonoid (∙ : Op₂ A) (ε : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isToMonoid : IsToMonoid ∙ ε
    comm       : Commutative ∙

  open IsToMonoid isToMonoid public

  isPoCommutativeMonoid : IsPoCommutativeMonoid ∙ ε
  isPoCommutativeMonoid = record { isPoMonoid = isPoMonoid ; comm = comm }

  isCommutativeMonoid : IsCommutativeMonoid ∙ ε
  isCommutativeMonoid = record { isMonoid = isMonoid ; comm = comm }

  ∙-mono₂ : Relation.Binary.Monotonic₂ _≤_ _≤_ _≤_ ∙
  ∙-mono₂ x≤y u≤v =
      trans (∙-compat x≤y)
        (trans (≤-respˡ-≈ (comm _ _) (∙-compat u≤v)) (reflexive (comm _ _)))

record IsToGroup (∙ : Op₂ A) (ε : A) (_⁻¹ : Op₁ A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isToMonoid  : IsToMonoid ∙ ε
    inverse   : Inverse ε _⁻¹ ∙
    ⁻¹-cong   : Congruent₁ _⁻¹

  open IsToMonoid isToMonoid public

  -- Absolute value
  ∣_∣ : Op₁ A
  ∣ x ∣ with total x ε
  ... | (inj₁ x≤ε) = x ⁻¹
  ... | (inj₂ x≥ε) = x

  -- Magnitude
  ∣_-_∣ : Op₂ A
  ∣ x - y ∣ = ∣ ∙ x (y ⁻¹) ∣

  isPoGroup : IsPoGroup ∙ ε _⁻¹
  isPoGroup = record
    { isPoMonoid = isPoMonoid
    ; inverse = inverse
    ; ⁻¹-cong = ⁻¹-cong
    }

  isGroup : IsGroup ∙ ε _⁻¹
  isGroup = record
    { isMonoid = isMonoid
    ; inverse = inverse
    ; ⁻¹-cong = ⁻¹-cong
    }

  open IsGroup isGroup public
    using
     ( _-_
     ; inverseʳ
     ; inverseˡ
     ; uniqueˡ-⁻¹
     ; uniqueʳ-⁻¹
     ; ∙-congˡ
     ; ∙-congʳ
     )

record IsToAbelianGroup (∙ : Op₂ A) (ε : A) (_⁻¹ : Op₁ A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isToGroup : IsToGroup ∙ ε _⁻¹
    comm    : Commutative ∙

  open IsToGroup isToGroup public

  isPoAbelianGroup : IsPoAbelianGroup ∙ ε _⁻¹
  isPoAbelianGroup = record { isPoGroup = isPoGroup ; comm = comm }

  isToCommutativeMonoid : IsToCommutativeMonoid ∙ ε
  isToCommutativeMonoid = record
    { isToMonoid = isToMonoid
    ; comm     = comm
    }

  open IsToCommutativeMonoid isToCommutativeMonoid public
    using
     ( ∙-mono₂
     ; isToMonoid
     )

  isAbelianGroup : IsAbelianGroup ∙ ε _⁻¹
  isAbelianGroup = record { isGroup = isGroup ; comm = comm }

record IsToNearSemiring (+ * : Op₂ A) (0# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isToMonoid : IsToMonoid + 0#
    *-cong       : Congruent₂ *
    *-assoc      : Associative *
    distribʳ     : * DistributesOverʳ +
    zeroˡ        : LeftZero 0# *
    *-compat     : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z × * z x ≤ * z y

  open IsToMonoid +-isToMonoid public
    renaming
      ( isMonoid to +-isMonoid
      ; isPoMonoid to +-isPoMonoid
      ; ∙-compat to +-compat
      )

  isPoNearSemiring : IsPoNearSemiring + * 0#
  isPoNearSemiring = record
    { +-isPoMonoid = +-isPoMonoid
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; distribʳ = distribʳ
    ; zeroˡ = zeroˡ
    ; *-compat = *-compat
    }

  open IsPoNearSemiring isPoNearSemiring public
    using (*-compatˡ ; *-compatʳ)

  isNearSemiring : IsNearSemiring + * 0#
  isNearSemiring = record
    { +-isMonoid = +-isMonoid
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; distribʳ = distribʳ
    ; zeroˡ = zeroˡ
    }

record IsToSemiringWithoutAnnihilatingZero
  (+ * : Op₂ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isToCommutativeMonoid : IsToCommutativeMonoid + 0#
    *-cong                  : Congruent₂ *
    *-assoc                 : Associative *
    *-identity              : Identity 1# *
    distrib                 : * DistributesOver +
    *-compat     : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z × * z x ≤ * z y

  open IsToCommutativeMonoid +-isToCommutativeMonoid public
    renaming
      ( isCommutativeMonoid to +-isCommutativeMonoid
      ; isPoCommutativeMonoid to +-isPoCommutativeMonoid
      ; ∙-mono₂ to +-mono₂
      )
    using
      ( module Eq
      ; refl
      ; trans
      ; reflexive
      )

  isPoSemiringWithoutAnnihilatingZero : IsPoSemiringWithoutAnnihilatingZero + * 0# 1#
  isPoSemiringWithoutAnnihilatingZero = record
    { +-isPoCommutativeMonoid = +-isPoCommutativeMonoid
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; *-identity = *-identity
    ; distrib = distrib
    ; *-compat = *-compat
    }

  open IsPoSemiringWithoutAnnihilatingZero isPoSemiringWithoutAnnihilatingZero public
    using (*-compatˡ ; *-compatʳ)

  isSemiringWithoutAnnihilatingZero : IsSemiringWithoutAnnihilatingZero + * 0# 1#
  isSemiringWithoutAnnihilatingZero = record
    { +-isCommutativeMonoid = +-isCommutativeMonoid
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; *-identity = *-identity
    ; distrib = distrib
    }

  open IsSemiringWithoutAnnihilatingZero isSemiringWithoutAnnihilatingZero public
    using
     ( +-cong
      ; +-congʳ
      ; +-congˡ
      ; *-congʳ
      ; *-congˡ
      ; distribʳ
      ; distribˡ
      ; +-identity
      ; +-identityʳ
      ; +-identityˡ
      ; *-identityˡ
      ; *-identityʳ
      )

record IsToSemiring (+ * : Op₂ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isToSemiringWithoutAnnihilatingZero :
      IsToSemiringWithoutAnnihilatingZero + * 0# 1#
    zero : Zero 0# *

  open IsToSemiringWithoutAnnihilatingZero
    isToSemiringWithoutAnnihilatingZero public

  isPoSemiring : IsPoSemiring + * 0# 1#
  isPoSemiring = record
    { isPoSemiringWithoutAnnihilatingZero = isPoSemiringWithoutAnnihilatingZero
    ; zero = zero
    }

  isSemiring : IsSemiring + * 0# 1#
  isSemiring = record
    { isSemiringWithoutAnnihilatingZero = isSemiringWithoutAnnihilatingZero
    ; zero = zero
    }

  open IsSemiring isSemiring public
    using (
      zeroˡ
    ; zeroʳ
    )

record IsToCommutativeSemiring (+ * : Op₂ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isToSemiring : IsToSemiring + * 0# 1#
    *-comm     : Commutative *

  open IsToSemiring isToSemiring public

  isPoCommutativeSemiring : IsPoCommutativeSemiring + * 0# 1#
  isPoCommutativeSemiring = record
    { isPoSemiring = isPoSemiring
    ; *-comm = *-comm
    }

  isCommutativeSemiring : IsCommutativeSemiring + * 0# 1#
  isCommutativeSemiring = record
    { isSemiring = isSemiring
    ; *-comm   = *-comm
    }

record IsToQuasiring (+ * : Op₂ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isToMonoid    : IsToMonoid + 0#
    *-cong        : Congruent₂ *
    *-assoc       : Associative *
    *-identity    : Identity 1# *
    distrib       : * DistributesOver +
    zero          : Zero 0# *
    *-compat     : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z × * z x ≤ * z y

  open IsToMonoid +-isToMonoid public
    renaming (isPoMonoid to +-isPoMonoid)

  isPoQuasiring : IsPoQuasiring  + * 0# 1#
  isPoQuasiring = record
    { +-isPoMonoid = +-isPoMonoid
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; *-identity = *-identity
    ; distrib = distrib
    ; zero = zero
    ; *-compat = *-compat
    }

  open IsPoQuasiring isPoQuasiring public
    using (*-compatˡ ; *-compatʳ)

  isQuasiring : IsQuasiring + * 0# 1#
  isQuasiring = record
    { +-isMonoid = IsToMonoid.isMonoid +-isToMonoid
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; *-identity = *-identity
    ; distrib = distrib
    ; zero = zero
    }

  open IsQuasiring isQuasiring public
    using
    ( distribˡ
    ; distribʳ
    ; zeroʳ
    ; zeroˡ
    )

record IsToRingWithoutOne (+ * : Op₂ A) (-_ : Op₁ A) (0# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isToAbelianGroup : IsToAbelianGroup + 0# -_
    *-cong           : Congruent₂ *
    *-assoc          : Associative *
    distrib          : * DistributesOver +
    *-compat     : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z × * z x ≤ * z y

  open IsToAbelianGroup +-isToAbelianGroup public
    renaming (
        isAbelianGroup to +-isAbelianGroup
      ; isPoAbelianGroup to +-isPoAbelianGroup
    )

  isPoRingWithoutOne : IsPoRingWithoutOne + * -_ 0#
  isPoRingWithoutOne = record
    { +-isPoAbelianGroup = +-isPoAbelianGroup
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; distrib = distrib
    ; *-compat = *-compat
    }

  open IsPoRingWithoutOne isPoRingWithoutOne public
    using (*-compatˡ ; *-compatʳ)

  isRingWithoutOne : IsRingWithoutOne + * -_ 0#
  isRingWithoutOne = record
    { +-isAbelianGroup = +-isAbelianGroup
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; distrib = distrib
    }

record IsToNonAssociativeRing (+ * : Op₂ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isToAbelianGroup : IsToAbelianGroup + 0# -_
    *-cong           : Congruent₂ *
    *-identity       : Identity 1# *
    distrib          : * DistributesOver +
    zero             : Zero 0# *
    *-compat     : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z × * z x ≤ * z y

  open IsToAbelianGroup +-isToAbelianGroup public
    renaming
     ( isAbelianGroup to +-isAbelianGroup
     ; isPoAbelianGroup to +-isPoAbelianGroup
     )

  isPoNonAssociativeRing : IsPoNonAssociativeRing + * -_ 0# 1#
  isPoNonAssociativeRing = record
    { +-isPoAbelianGroup = +-isPoAbelianGroup
    ; *-cong = *-cong
    ; *-identity = *-identity
    ; distrib = distrib
    ; zero = zero
    ; *-compat = *-compat
    }

  isNonAssociativeRing  : IsNonAssociativeRing + * -_ 0# 1#
  isNonAssociativeRing = record
    { +-isAbelianGroup = +-isAbelianGroup
    ; *-cong = *-cong
    ; *-identity = *-identity
    ; distrib = distrib
    ; zero = zero
    }

record IsToNearring (+ * : Op₂ A) (0# 1# : A) (_⁻¹ : Op₁ A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isToQuasiring : IsToQuasiring + * 0# 1#
    +-inverse   : Inverse 0# _⁻¹ +
    ⁻¹-cong     : Congruent₁ _⁻¹

  open IsToQuasiring isToQuasiring public

  isPoNearring : IsPoNearring + * 0# 1# _⁻¹
  isPoNearring = record
   { isPoQuasiring = isPoQuasiring
   ; +-inverse = +-inverse
   ; ⁻¹-cong = ⁻¹-cong
   }

  isNearring : IsNearring + * 0# 1# _⁻¹
  isNearring = record
   { isQuasiring = isQuasiring
   ; +-inverse = +-inverse
   ; ⁻¹-cong = ⁻¹-cong
   }

record IsToRing (+ * : Op₂ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isToAbelianGroup : IsToAbelianGroup + 0# -_
    *-cong           : Congruent₂ *
    *-assoc          : Associative *
    *-identity       : Identity 1# *
    distrib          : * DistributesOver +
    *-compat     : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z × * z x ≤ * z y

  open IsToAbelianGroup +-isToAbelianGroup public
    renaming
    ( isAbelianGroup to +-isAbelianGroup
    ; isToCommutativeMonoid to +-isToCommutativeMonoid
    ; isToMonoid to +-isToMonoid
    ; isPoAbelianGroup to +-isPoAbelianGroup
    ; isToGroup to +-isToGroup
    )

  isPoRing : IsPoRing + * -_ 0# 1#
  isPoRing = record
     { +-isPoAbelianGroup = +-isPoAbelianGroup
     ; *-cong = *-cong
     ; *-assoc = *-assoc
     ; *-identity = *-identity
     ; distrib = distrib
     ; *-compat = *-compat
     }

  open IsPoRing isPoRing public
    using (*-compatˡ ; *-compatʳ)

  isRing : IsRing + * -_ 0# 1#
  isRing = record
    { +-isAbelianGroup = +-isAbelianGroup
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; *-identity = *-identity
    ; distrib = distrib
    }

  open IsRing isRing public
    using
     ( *-congˡ
     ; *-congʳ
     ; *-identityˡ
     ; *-identityʳ
     ; +-cong
     ; +-congʳ
     ; +-congˡ
     ; +-assoc
     ; +-comm
     ; +-identityʳ
     ; +-identityˡ
     ; -‿cong
     ; -‿inverseˡ
     ; -‿inverseʳ
     ; distribʳ
     ; distribˡ
     ; zeroʳ
     ; zeroˡ
     ; isRingWithoutOne
     )

  isToSemiringWithoutAnnihilatingZero : IsToSemiringWithoutAnnihilatingZero + * 0# 1#
  isToSemiringWithoutAnnihilatingZero = record
    { +-isToCommutativeMonoid = +-isToCommutativeMonoid
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; *-identity = *-identity
    ; distrib = distrib
    ; *-compat = *-compat
    }

  isToSemiring : IsToSemiring + * 0# 1#
  isToSemiring = record
    { isToSemiringWithoutAnnihilatingZero = isToSemiringWithoutAnnihilatingZero
    ; zero = IsRing.zero isRing
    }

record IsToCommutativeRing
         (+ * : Op₂ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isToRing : IsToRing + * -_ 0# 1#
    *-comm : Commutative *

  open IsToRing isToRing public

  isPoCommutativeRing : IsPoCommutativeRing + * -_ 0# 1#
  isPoCommutativeRing = record { isPoRing = isPoRing ; *-comm = *-comm }

  open IsPoCommutativeRing isPoCommutativeRing public
    using
     ( isPoCommutativeSemiring
     ; isCommutativeRing
     ; isCommutativeSemiring
     )

  isToCommutativeSemiring : IsToCommutativeSemiring + * 0# 1#
  isToCommutativeSemiring = record
    { isToSemiring = isToSemiring ; *-comm = *-comm }

  open IsToCommutativeMonoid +-isToCommutativeMonoid public
    renaming
      ( isCommutativeMonoid to +-isCommutativeMonoid
      ; ∙-mono₂ to +-mono₂
      )
      using ()
