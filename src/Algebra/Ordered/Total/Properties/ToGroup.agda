{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Total.Bundles

module Algebra.Ordered.Total.Properties.ToGroup
  {c ℓ₁ ℓ₂}
  (R : ToGroup c ℓ₁ ℓ₂)
  where

open ToGroup R

open import Algebra.Properties.Group group
open import Algebra.Ordered.Total.Properties.ToMagma toMagma public
open import Data.Sum
open import Function.Definitions as F using ()
open import Function.Metric.Definitions
open import Function.Metric.Structures _≈_ _≈_ _≤_ ε
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning
import Relation.Binary.Reasoning.PartialOrder as <-Reasoning

ε≤x⇒x⁻¹≤ε : ∀ {x} → ε ≤ x → x ⁻¹ ≤ ε
ε≤x⇒x⁻¹≤ε {x} ε<x =
  begin
    x ⁻¹     ≈⟨ identityˡ _ ⟨
    ε ∙ x ⁻¹ ≤⟨ ∙-compat ε<x ⟩
    x ∙ x ⁻¹ ≈⟨ inverseʳ _ ⟩
    ε
  ∎
  where open <-Reasoning poset

x≤ε⇒ε≤x⁻¹ : ∀ {x} → x ≤ ε → ε ≤ x ⁻¹
x≤ε⇒ε≤x⁻¹ {x} x<ε =
  begin
    ε        ≈⟨ inverseʳ _ ⟨
    x ∙ x ⁻¹ ≤⟨ ∙-compat x<ε ⟩
    ε ∙ x ⁻¹ ≈⟨ identityˡ _  ⟩
    x ⁻¹
  ∎
  where open <-Reasoning poset

-- Absolute value lemmas
-x≤∣x∣ : ∀ {x} → x ⁻¹ ≤ ∣ x ∣
-x≤∣x∣ {x} with total x ε
... | (inj₁ _) = refl
... | (inj₂ ε<x) = trans (ε≤x⇒x⁻¹≤ε ε<x) ε<x

∣x∣≤x : ∀ {x} → ε ≤ x → ∣ x ∣ ≤ x
∣x∣≤x {x} pos with total x ε
... | (inj₁ _) = trans (ε≤x⇒x⁻¹≤ε pos) pos
... | (inj₂ _) = refl

-∣x∣≤x : ∀ {x} → ∣ x ∣ ⁻¹ ≤ x
-∣x∣≤x {x} with total x ε
... | (inj₁ _) = reflexive (⁻¹-involutive x)
... | (inj₂ ε<x) = trans (ε≤x⇒x⁻¹≤ε ε<x) ε<x

x≤∣x∣ : ∀ {x} → x ≤ ∣ x ∣
x≤∣x∣  {x} with total x ε
... | (inj₁ x<ε) = trans x<ε (x≤ε⇒ε≤x⁻¹ x<ε)
... | (inj₂ _) = refl

∣x∣≤y⇒x≤y : ∀ {x y} → ∣ x ∣ ≤ y → x ≤ y
∣x∣≤y⇒x≤y {x} {y} ∣x∣≤y = trans x≤∣x∣ ∣x∣≤y

abs-nonNegative : ∀ {x} → ε ≤ ∣ x ∣
abs-nonNegative {x} with total x ε
... | (inj₁ x≤ε) =
        trans
          (reflexive (Eq.sym (inverseʳ x)))
          (trans
            (∙-compat {x = x} {y = ε} {z = x ⁻¹} x≤ε)
            (reflexive (identityˡ (x ⁻¹)))
            )
... | (inj₂ ε≤x) = ε≤x

-- TODO : surely this could be simplified
abs-even : ∀ x → ∣ (x ⁻¹) ∣ ≈ ∣ x ∣
abs-even x with total (x ⁻¹) ε | total x ε | total x (x ⁻¹)
... | (inj₁ x⁻¹≤ε) | (inj₁ x≤ε) | (inj₁ x≤x⁻¹)  =
  antisym
   (trans (reflexive (⁻¹-involutive x)) x≤x⁻¹)
   (≤-respˡ-≈ (antisym (x≤ε⇒ε≤x⁻¹ x≤ε) x⁻¹≤ε) (x≤ε⇒ε≤x⁻¹ x⁻¹≤ε))
... | (inj₁ _) | (inj₁ x≤ε) | (inj₂ x≤x⁻¹)  =
  ⁻¹-cong (antisym x≤x⁻¹ (trans x≤ε (x≤ε⇒ε≤x⁻¹ x≤ε)))
... | (inj₁ _) | (inj₂ _) | _ = ⁻¹-involutive x
... | (inj₂ _) | (inj₁ _) | _ = Eq.refl
... | (inj₂ _) | (inj₂ ε≤x) | (inj₁ x≤x⁻¹) =
  antisym
    (trans (ε≤x⇒x⁻¹≤ε ε≤x) ε≤x)
    x≤x⁻¹
... | (inj₂ ε≤x⁻¹) | (inj₂ ε≤x) | (inj₂ x⁻¹≤x) =
  antisym
    x⁻¹≤x
    (≤-respˡ-≈
     (antisym ε≤x
      (trans (reflexive (Eq.sym (⁻¹-involutive x))) (ε≤x⇒x⁻¹≤ε ε≤x⁻¹))) ε≤x⁻¹)

abs-definite : ∀ x → x ≈ ε → ∣ x ∣ ≈ ε
abs-definite x x≈ε with total x ε
... | (inj₁ _) = Eq.sym (uniqueˡ-⁻¹ ε x (Eq.trans (identityˡ x) x≈ε))
... | (inj₂ _) = x≈ε

-- TODO: simplify proofs
abs-cong : F.Congruent _≈_ _≈_ ∣_∣
abs-cong {x} {y} x≈y with total x ε | total y ε | total (x ⁻¹) y | total (y ⁻¹) x
... | (inj₁ x≤ε) | (inj₁ y≤ε) | _            | _ = ⁻¹-cong x≈y
... | (inj₁ x≤ε) | (inj₂ ε≤y) | (inj₁ x⁻¹≤y) | _ =
  antisym x⁻¹≤y
    (begin
      y    ≈⟨ x≈y ⟨
      x    ≤⟨ x≤ε ⟩
      ε    ≈⟨ uniqueʳ-⁻¹ x ε
               (Eq.trans
                 (identityʳ _)
                 (antisym x≤ε (trans ε≤y (reflexive (Eq.sym x≈y))))) ⟩
      x ⁻¹
    ∎)
  where open <-Reasoning poset
... | (inj₁ x≤ε) | (inj₂ ε≤y) | (inj₂ x⁻¹≥y) | _ =
  antisym
    (begin
      x ⁻¹ ≈⟨ uniqueʳ-⁻¹ x ε
               (Eq.trans
                 (identityʳ _)
                 (antisym x≤ε (trans ε≤y ((reflexive (Eq.sym x≈y)))))) ⟨
      ε    ≤⟨ ε≤y ⟩
      y
    ∎)
    x⁻¹≥y
  where open <-Reasoning poset
... | (inj₂ ε≤x) | (inj₁ y≤ε) | _ | (inj₁ y⁻¹≤x) =
  antisym
    (begin
      x    ≈⟨ x≈y ⟩
      y    ≤⟨ y≤ε ⟩
      ε    ≈⟨ uniqueʳ-⁻¹ y ε
                (Eq.trans
                   (identityʳ _)
                   (antisym  y≤ε (trans ε≤x (reflexive x≈y)) )) ⟩
      y ⁻¹
    ∎)
  y⁻¹≤x
  where open <-Reasoning poset
... | (inj₂ ε≤x) | (inj₁ y≤ε) | _ | (inj₂ y⁻¹≥x) =
  antisym y⁻¹≥x ((begin
      y ⁻¹ ≈⟨ uniqueʳ-⁻¹ y ε
               (Eq.trans
                 (identityʳ _)
                 (antisym y≤ε (trans ε≤x ((reflexive (x≈y)))))) ⟨
      ε    ≤⟨ ε≤x ⟩
      x
    ∎))
  where open <-Reasoning poset
... | (inj₂ ε≤x) | (inj₂ ε≤y) | _ | _ = x≈y

abs-indiscernable : ∀ x → ∣ x ∣ ≈ ε → x ≈ ε
abs-indiscernable x ∣x∣≈ε with total x ε | total (x ⁻¹) x
... | (inj₁ x≤ε) | (inj₁ x⁻¹≤x) =
  antisym x≤ε (trans (reflexive (Eq.sym ∣x∣≈ε)) x⁻¹≤x)
... | (inj₁ x≤ε) | (inj₂ x≤x⁻¹) =
  begin
    x          ≈⟨ identityʳ x ⟨
    x ∙ ε      ≈⟨ ∙-cong Eq.refl (Eq.sym ∣x∣≈ε) ⟩
    x ∙ x ⁻¹   ≈⟨ inverseʳ x ⟩
    ε
  ∎
  where open ≈-Reasoning setoid
... | (inj₂ _)   | _            = ∣x∣≈ε

norm-cong : Congruent _≈_ _≈_ ∣_-_∣
norm-cong {x} {y} {u} {v} x≈y u≈v = abs-cong (∙-cong x≈y (⁻¹-cong u≈v))

norm-nonNegative : NonNegative _≤_ ∣_-_∣ ε
norm-nonNegative {x} {y} = abs-nonNegative

norm-definite : Definite _≈_ _≈_ ∣_-_∣ ε
norm-definite {x} {y} x≈y =
  abs-definite
    (x ∙ y ⁻¹)
    (Eq.trans (∙-congˡ (⁻¹-cong (Eq.sym x≈y))) (inverseʳ x))

norm-indiscernable : Indiscernable _≈_ _≈_ ∣_-_∣ ε
norm-indiscernable {x} {y} ∣x-y∣≈0 =
  begin
    x         ≈⟨ uniqueˡ-⁻¹ x (y ⁻¹) (abs-indiscernable (x ∙ y ⁻¹) ∣x-y∣≈0) ⟩
    (y ⁻¹) ⁻¹ ≈⟨ ⁻¹-involutive y ⟩
    y
  ∎
  where open ≈-Reasoning setoid

norm-isProtoMetric : IsProtoMetric ∣_-_∣
norm-isProtoMetric = record
  { isPartialOrder = isPartialOrder
  ; ≈-isEquivalence = isEquivalence
  ; cong = norm-cong
  ; nonNegative = norm-nonNegative
  }

norm-isPreMetric : IsPreMetric ∣_-_∣
norm-isPreMetric = record
  { isProtoMetric = norm-isProtoMetric
  ; ≈⇒0 = norm-definite
  }

norm-isQuasiSemiMetric : IsQuasiSemiMetric ∣_-_∣
norm-isQuasiSemiMetric = record
  { isPreMetric = norm-isPreMetric
  ; 0⇒≈ = norm-indiscernable
  }
