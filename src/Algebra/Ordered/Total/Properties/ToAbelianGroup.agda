{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Total.Bundles

module Algebra.Ordered.Total.Properties.ToAbelianGroup
  {c ℓ₁ ℓ₂}
  (R : ToAbelianGroup c ℓ₁ ℓ₂)
  where

open ToAbelianGroup R

open import Algebra.Ordered.Total.Properties.ToGroup toGroup public
open import Algebra.Properties.AbelianGroup abelianGroup
open import Data.Sum
open import Function.Metric.Definitions
open import Function.Metric.Structures _≈_ _≈_ _≤_ ε
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning
import Relation.Binary.Reasoning.PartialOrder as <-Reasoning

abs-triangle : ∀ x y → ∣ x ∙ y ∣ ≤ ∣ x ∣ ∙ ∣ y ∣
abs-triangle x y with total (x ∙ y) ε | total x ε | total y ε
... | (inj₁ ∣x+y∣≤0) | (inj₁ x≤0) | (inj₁ y≤0) =
  trans (reflexive (⁻¹-anti-homo-∙ x y)) (reflexive (comm _ _))
... | (inj₁ ∣x+y∣≤0) | (inj₁ x≤0) | (inj₂ 0≤y) =
  (begin
    (x ∙ y) ⁻¹  ≤⟨ reflexive (⁻¹-anti-homo-∙ x y) ⟩
    y ⁻¹ ∙ x ⁻¹ ≤⟨ ∙-mono₂ (ε≤x⇒x⁻¹≤ε 0≤y) refl ⟩
    ε ∙ x ⁻¹    ≈⟨ comm _ _ ⟩
    x ⁻¹ ∙ ε    ≤⟨ ∙-mono₂ refl 0≤y ⟩
    x ⁻¹ ∙ y
  ∎)
  where open <-Reasoning poset
... | (inj₁ ∣x+y∣≤0) | (inj₂ 0≤x) | (inj₁ y≤0) =
  (begin
    (x ∙ y) ⁻¹  ≤⟨ reflexive (⁻¹-anti-homo-∙ x y) ⟩
    y ⁻¹ ∙ x ⁻¹ ≤⟨ ∙-mono₂ refl  (ε≤x⇒x⁻¹≤ε 0≤x) ⟩
    y ⁻¹ ∙ ε    ≈⟨ comm _ _ ⟩
    ε ∙ y ⁻¹    ≤⟨ ∙-mono₂ 0≤x refl ⟩
    x ∙ y ⁻¹
  ∎)
  where open <-Reasoning poset
... | (inj₁ ∣x+y∣≤0) | (inj₂ 0≤x) | (inj₂ 0≤y) =
  (begin
    (x ∙ y) ⁻¹ ≤⟨ reflexive (⁻¹-anti-homo-∙ x y) ⟩
    y ⁻¹ ∙ x ⁻¹ ≤⟨ ∙-mono₂ (ε≤x⇒x⁻¹≤ε 0≤y) (ε≤x⇒x⁻¹≤ε 0≤x) ⟩
    ε ∙ ε       ≤⟨ ∙-mono₂ 0≤x 0≤y ⟩
    x ∙ y
  ∎)
  where open <-Reasoning poset
... | (inj₂ 0≤∣x+y∣) | (inj₁ x≤0) | (inj₁ y≤0) =
  (begin
    (x ∙ y)  ≤⟨ ∙-mono₂ x≤0 y≤0 ⟩
    ε ∙ ε             ≤⟨ ∙-mono₂ (x≤ε⇒ε≤x⁻¹ x≤0) (x≤ε⇒ε≤x⁻¹ y≤0) ⟩
    x ⁻¹ ∙ y ⁻¹
  ∎)
  where open <-Reasoning poset
... | (inj₂ 0≤∣x+y∣) | (inj₁ x≤0) | (inj₂ 0≤y) =
  (begin
    x ∙ y     ≤⟨ ∙-mono₂ x≤0 refl ⟩
    ε ∙ y     ≤⟨ ∙-mono₂ (x≤ε⇒ε≤x⁻¹ x≤0) refl ⟩
    x ⁻¹ ∙ y
  ∎)
  where open <-Reasoning poset
... | (inj₂ 0≤∣x+y∣) | (inj₂ 0≤x) | (inj₁ y≤0) =
  (begin
    x ∙ y     ≤⟨ ∙-mono₂ refl y≤0 ⟩
    x ∙ ε     ≤⟨ ∙-mono₂ refl (x≤ε⇒ε≤x⁻¹ y≤0) ⟩
    x ∙ y ⁻¹
  ∎)
  where open <-Reasoning poset
... | (inj₂ 0≤∣x+y∣) | (inj₂ 0≤x) | (inj₂ 0≤y) = refl

norm-symmetric : Symmetric _≈_ ∣_-_∣
norm-symmetric x y =
  Eq.trans
   (abs-cong
     (begin
      x ∙ y ⁻¹             ≈⟨ comm _ _ ⟩
      (y ⁻¹) ∙ x           ≈⟨ ∙-congˡ (Eq.sym (⁻¹-involutive _)) ⟩
      (y ⁻¹) ∙ ((x ⁻¹) ⁻¹) ≈⟨ ⁻¹-∙-comm  y (x ⁻¹) ⟩
      (y ∙ (x ⁻¹)) ⁻¹
     ∎))
   (abs-even (y ∙ x ⁻¹))
  where open ≈-Reasoning setoid

norm-triangle : TriangleInequality _≤_ _∙_ ∣_-_∣
norm-triangle x y z =
  begin
    ∣ x ∙ z ⁻¹  ∣               ≈⟨ abs-cong (∙-congʳ (Eq.sym (identityʳ x))) ⟩
    ∣ x ∙ ε ∙ z ⁻¹ ∣            ≈⟨ abs-cong (∙-congʳ (∙-congˡ (Eq.sym (inverseˡ y)))) ⟩
    ∣ x ∙ (y ⁻¹ ∙ y) ∙ z ⁻¹ ∣   ≈⟨ abs-cong (assoc _ _ _) ⟩
    ∣ x ∙ (y ⁻¹ ∙ y ∙ z ⁻¹) ∣   ≈⟨ abs-cong (∙-congˡ (assoc _ _ _)) ⟩
    ∣ x ∙ (y ⁻¹ ∙ (y ∙ z ⁻¹)) ∣ ≈⟨ abs-cong (Eq.sym (assoc _ _ _)) ⟩
    ∣ x ∙ y ⁻¹ ∙ (y ∙ z ⁻¹) ∣   ≤⟨ abs-triangle (x ∙ y ⁻¹) (y ∙ z ⁻¹)  ⟩
    ∣ x ∙ y ⁻¹ ∣ ∙ ∣ y ∙ z ⁻¹ ∣
  ∎
  where open <-Reasoning poset

norm-isSemiMetric : IsSemiMetric ∣_-_∣
norm-isSemiMetric = record
 { isQuasiSemiMetric = norm-isQuasiSemiMetric
 ; sym = norm-symmetric
 }

norm-isGeneralMetric : IsGeneralMetric _∙_ ∣_-_∣
norm-isGeneralMetric = record
 { isSemiMetric = norm-isSemiMetric
 ; triangle = norm-triangle
 }
