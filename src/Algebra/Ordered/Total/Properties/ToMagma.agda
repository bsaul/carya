{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Total.Bundles

module Algebra.Ordered.Total.Properties.ToMagma
  {c ℓ₁ ℓ₂}
  (R : ToMagma c ℓ₁ ℓ₂)
  where

open ToMagma R

open import Algebra.Lattice.Structures _≈_
open import Algebra.Construct.NaturalChoice.MinMaxOp minOperator maxOperator
  using
   ( ⊔-absorbs-⊓
   ; ⊓-absorbs-⊔
   ; ⊓-distribˡ-⊔
   ; ⊔-distrib-⊓
   ; ⊓-distribʳ-⊔
   ; ⊓-distrib-⊔
   ; ⊔-distribˡ-⊓
   ; ⊔-distribʳ-⊓
   )
  public
open import Data.Product using (_,_)

-- min = _⊓_
⊓-isSemilattice : IsMeetSemilattice _⊓_
⊓-isSemilattice = record
  { isBand = ⊓-isBand
  ; comm = ⊓-comm
  }

-- max = _⊔_
⊔-isSemilattice : IsJoinSemilattice _⊔_
⊔-isSemilattice = record
  { isBand = ⊔-isBand
  ; comm = ⊔-comm
  }

⊔-⊓-isLattice : IsLattice _⊔_ _⊓_
⊔-⊓-isLattice = record
 { isEquivalence = isEquivalence
 ; ∨-comm = ⊔-comm
 ; ∨-assoc = ⊔-assoc
 ; ∨-cong = ⊔-cong
 ; ∧-comm = ⊓-comm
 ; ∧-assoc = ⊓-assoc
 ; ∧-cong = ⊓-cong
 ; absorptive = ⊔-absorbs-⊓ , ⊓-absorbs-⊔
 }

⊔-⊓-isDistributiveLattice : IsDistributiveLattice _⊔_ _⊓_
⊔-⊓-isDistributiveLattice = record
 { isLattice = ⊔-⊓-isLattice
 ; ∨-distrib-∧ = ⊔-distrib-⊓
 ; ∧-distrib-∨ = ⊓-distrib-⊔
 }
