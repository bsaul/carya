{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Total.Bundles

module Algebra.Ordered.Total.Properties.ToRing
  {c ℓ₁ ℓ₂}
  (R : ToRing c ℓ₁ ℓ₂)
  where

open ToRing R

open import Algebra.Properties.Ring ring public
open import Algebra.Ordered.Total.Properties.ToAbelianGroup
  +-toAbelianGroup public

open import Data.Sum

open import Relation.Binary.Reasoning.PartialOrder poset

-- TODO: PR in stdlib?
-- or find better home as doesn't need ordering
-x*-y≈x*y : ∀ x y → - x * - y ≈ x * y
-x*-y≈x*y x y =
  Eq.trans
   (Eq.trans
    (Eq.sym ( -‿distribˡ-* x (- y)))
    (-‿cong (Eq.sym (-‿distribʳ-* x y))))
   (-‿involutive (x * y))

0≤x² : ∀ {x} → 0# ≤ x * x
0≤x² {x} with total x 0#
... | (inj₁ x≤0) =
  trans
    ((reflexive (Eq.sym (zeroˡ _))))
    (trans
       (*-compatʳ (x≤ε⇒ε≤x⁻¹ x≤0) (x≤ε⇒ε≤x⁻¹ x≤0))
       (reflexive (-x*-y≈x*y x x)))
... | (inj₂ 0≤x) = trans (reflexive (Eq.sym (zeroˡ x))) (*-compatʳ 0≤x 0≤x)

∣x∣²≈x² : ∀ {x} → ∣ x ∣ * ∣ x ∣ ≈ x * x
∣x∣²≈x² {x} with total x 0#
... | (inj₁ _) = -x*-y≈x*y x x
... | (inj₂ _) = Eq.refl

abs-*-homo≤ : ∀ x y → ∣ x * y ∣ ≤ ∣ x ∣ * ∣ y ∣
abs-*-homo≤ x y  with total (x * y) 0# | total x 0# | total y 0#
... | (inj₁ _) | (inj₁ x≤0) | (inj₁ y≤0)  =
  begin
    - (x * y)   ≈⟨ -‿distribʳ-* _ _ ⟩
    x  * - y    ≤⟨ *-compatʳ x≤0 (x≤ε⇒ε≤x⁻¹ y≤0) ⟩
    0# * - y    ≤⟨ *-compatʳ (x≤ε⇒ε≤x⁻¹ x≤0) (x≤ε⇒ε≤x⁻¹ y≤0) ⟩
    - x * - y
  ∎
... | (inj₁ _) | (inj₁ _)   | (inj₂ _)  = reflexive (-‿distribˡ-* _ _)
... | (inj₁ _) | (inj₂ _)   | (inj₁ _)  = reflexive (-‿distribʳ-* _ _)
... | (inj₁ _) | (inj₂ 0≤x) | (inj₂ 0≤y)  =
  begin
    - (x * y)   ≈⟨ -‿distribˡ-* _ _ ⟩
    - x  * y    ≤⟨ *-compatʳ (ε≤x⇒x⁻¹≤ε 0≤x) 0≤y ⟩
    0# * y      ≤⟨ *-compatʳ 0≤x 0≤y ⟩
    x * y
  ∎
... | (inj₂ _) | (inj₁ _) | (inj₁ _)  = reflexive (Eq.sym (-x*-y≈x*y x y))
... | (inj₂ _) | (inj₁ x≤0) | (inj₂ 0≤y)  =
  begin
    x * y    ≤⟨ *-compatʳ x≤0 0≤y ⟩
    0# * y   ≤⟨ *-compatʳ (x≤ε⇒ε≤x⁻¹ x≤0) 0≤y ⟩
    - x * y
  ∎
... | (inj₂ _) | (inj₂ 0≤x) | (inj₁ y≤0)  =
  begin
    x * y    ≤⟨ *-compatˡ y≤0 0≤x ⟩
    x * 0#   ≤⟨ *-compatˡ (x≤ε⇒ε≤x⁻¹ y≤0) 0≤x ⟩
    x * - y
  ∎
... | (inj₂ _) | (inj₂ _) | (inj₂ _)  = refl

abs-*-homo≥ : ∀ x y → ∣ x ∣ * ∣ y ∣ ≤ ∣ x * y ∣
abs-*-homo≥ x y  with total (x * y) 0# | total x 0# | total y 0#
... | (inj₁ x*y≤0) | (inj₁ x≤0) | (inj₁ y≤0)  =
  ≤-respˡ-≈
    (antisym
      (trans
         (reflexive (Eq.sym (zeroˡ (- y))))
         (*-compatʳ (x≤ε⇒ε≤x⁻¹ x≤0) (x≤ε⇒ε≤x⁻¹ y≤0)))
      (trans
        (reflexive (-x*-y≈x*y x y))
        (x*y≤0)))
    (x≤ε⇒ε≤x⁻¹ x*y≤0)
... | (inj₁ _) | (inj₁ _)   | (inj₂ _)    = reflexive (Eq.sym (-‿distribˡ-* _ _))
... | (inj₁ _) | (inj₂ _)   | (inj₁ _)    = reflexive (Eq.sym (-‿distribʳ-* _ _))
... | (inj₁ x*y≤0) | (inj₂ 0≤x) | (inj₂ 0≤y)  =
  ≤-respˡ-≈
    (antisym
      (trans (reflexive (Eq.sym (zeroˡ y))) (*-compatʳ 0≤x 0≤y))
      x*y≤0)
    (x≤ε⇒ε≤x⁻¹ x*y≤0)
... | (inj₂ _) | (inj₁ _) | (inj₁ _)      = reflexive (-x*-y≈x*y x y)
... | (inj₂ 0≤x*y) | (inj₁ x≤0) | (inj₂ 0≤y)  =
  ≤-respˡ-≈
    (antisym
      (trans (reflexive (Eq.sym (zeroˡ y))) (*-compatʳ (x≤ε⇒ε≤x⁻¹ x≤0) 0≤y))
      (trans (reflexive (Eq.sym (-‿distribˡ-* x y))) (ε≤x⇒x⁻¹≤ε 0≤x*y)))
    0≤x*y
... | (inj₂ 0≤x*y) | (inj₂ 0≤x) | (inj₁ y≤0)  =
  ≤-respˡ-≈
    (antisym
      (trans (reflexive (Eq.sym (zeroʳ x))) (*-compatˡ (x≤ε⇒ε≤x⁻¹ y≤0) 0≤x))
      (trans (reflexive (Eq.sym (-‿distribʳ-* x y))) (ε≤x⇒x⁻¹≤ε 0≤x*y)))
    0≤x*y
... | (inj₂ _) | (inj₂ _) | (inj₂ _)      = refl

abs-*-homo : ∀ x y → ∣ x * y ∣ ≈ ∣ x ∣ * ∣ y ∣
abs-*-homo x y  = antisym (abs-*-homo≤ x y) (abs-*-homo≥ x y)
