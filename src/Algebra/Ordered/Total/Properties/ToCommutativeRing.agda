{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Total.Bundles

module Algebra.Ordered.Total.Properties.ToCommutativeRing
  {c ℓ₁ ℓ₂}
  (R : ToCommutativeRing c ℓ₁ ℓ₂)
  where

open ToCommutativeRing R

open import Algebra.Ordered.Total.Properties.ToRing toRing public
open import Algebra.Ordered.Partial.Properties.PoCommutativeRing
 poCommutativeRing public
