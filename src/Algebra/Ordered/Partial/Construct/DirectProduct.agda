{-# OPTIONS --safe --cubical-compatible #-}

module Algebra.Ordered.Partial.Construct.DirectProduct where

open import Level using (Level; _⊔_)
open import Algebra.Construct.DirectProduct as mk
open import Algebra.Bundles
open import Algebra.Ordered.Partial.Bundles

open import Data.Product as × using (_,_; _×_)
open import Function

private
  variable
    b ℓb₁ ℓb₂ a ℓa₁ ℓa₂ : Level

mkPoMagma : PoMagma a ℓa₁ ℓa₂ → PoMagma b ℓb₁ ℓb₂
  → PoMagma (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoMagma A B = record
  { Carrier = A×B.Carrier
  ; _≤_ = λ x y → ×.uncurry _×_ (×.zip A._≤_ B._≤_ x y)
  ; _∙_ = A×B._∙_
  ; isPoMagma = record
    { isPartialOrder = record
      { isPreorder = record
        { isEquivalence = A×B.isEquivalence
        ; reflexive = ×.map A.reflexive B.reflexive
        ; trans = ×.zip A.trans B.trans
        }
      ; antisym = ×.zip A.antisym B.antisym
      }
    ; ∙-cong = A×B.∙-cong
    ; ∙-compat = ×.map A.∙-compat B.∙-compat
    }
  }
  where module A = PoMagma A
        module B = PoMagma B
        module A×B = Magma (mk.magma A.magma B.magma)

mkPoSemigroup :
    PoSemigroup a ℓa₁ ℓa₂
  → PoSemigroup b ℓb₁ ℓb₂
  → PoSemigroup (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoSemigroup A B = record
  { isPoSemigroup = record
    { isPoMagma = C.isPoMagma
    ; assoc = A×B.assoc
    }
  }
  where module A = PoSemigroup A
        module B = PoSemigroup B
        module C = PoMagma (mkPoMagma A.poMagma B.poMagma)
        module A×B = Semigroup (mk.semigroup A.semigroup B.semigroup)

mkPoMonoid :
    PoMonoid a ℓa₁ ℓa₂
  → PoMonoid b ℓb₁ ℓb₂
  → PoMonoid (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoMonoid A B = record
  { isPoMonoid = record
    { isPoSemigroup = C.isPoSemigroup
    ; identity = A×B.identity
    }
  }
  where module A = PoMonoid A
        module B = PoMonoid B
        module C = PoSemigroup (mkPoSemigroup A.poSemigroup B.poSemigroup)
        module A×B = Monoid (mk.monoid A.monoid B.monoid)

mkPoCommutativeMonoid :
    PoCommutativeMonoid a ℓa₁ ℓa₂
  → PoCommutativeMonoid b ℓb₁ ℓb₂
  → PoCommutativeMonoid (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoCommutativeMonoid A B = record
  { isPoCommutativeMonoid = record
    { isPoMonoid = C.isPoMonoid
    ; comm = A×B.comm
    }
  }
  where module A = PoCommutativeMonoid A
        module B = PoCommutativeMonoid B
        module C = PoMonoid (mkPoMonoid A.poMonoid B.poMonoid)
        module A×B = CommutativeMonoid
            (mk.commutativeMonoid A.commutativeMonoid B.commutativeMonoid)

mkPoGroup :
    PoGroup a ℓa₁ ℓa₂
  → PoGroup b ℓb₁ ℓb₂
  → PoGroup (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoGroup A B = record
  { isPoGroup =
     record
      { isPoMonoid = C.isPoMonoid
      ; inverse = A×B.inverse
      ; ⁻¹-cong = A×B.⁻¹-cong
      }
  }
  where module A = PoGroup A
        module B = PoGroup B
        module C = PoMonoid (mkPoMonoid A.poMonoid B.poMonoid)
        module A×B = Group (mk.group A.group B.group)

mkPoAbelianGroup :
    PoAbelianGroup a ℓa₁ ℓa₂
  → PoAbelianGroup b ℓb₁ ℓb₂
  → PoAbelianGroup (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoAbelianGroup A B = record
  { isPoAbelianGroup = record
    { isPoGroup = C.isPoGroup
    ; comm = A×B.comm
    }
  }
  where module A = PoAbelianGroup A
        module B = PoAbelianGroup B
        module C = PoGroup (mkPoGroup A.poGroup B.poGroup)
        module A×B = AbelianGroup (mk.abelianGroup A.abelianGroup B.abelianGroup)

mkPoSemiringWithoutAnnihilatingZero :
    PoSemiringWithoutAnnihilatingZero a ℓa₁ ℓa₂
  → PoSemiringWithoutAnnihilatingZero b ℓb₁ ℓb₂
  → PoSemiringWithoutAnnihilatingZero (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoSemiringWithoutAnnihilatingZero A B = record
  { isPoSemiringWithoutAnnihilatingZero = record
    { +-isPoCommutativeMonoid = C.isPoCommutativeMonoid
    ; *-cong = A×B.*-cong
    ; *-assoc = A×B.*-assoc
    ; *-identity = A×B.*-identity
    ; distrib = A×B.distrib
    ; *-compat = λ (a₁ , b₁) (a₂ , b₂) →
          (×.proj₁ (A.*-compat a₁ a₂) , ×.proj₁ (B.*-compat b₁ b₂))
        ,  ×.proj₂ (A.*-compat a₁ a₂) , ×.proj₂ (B.*-compat b₁ b₂)
    }
  }
  where module A = PoSemiringWithoutAnnihilatingZero A
        module B = PoSemiringWithoutAnnihilatingZero B
        module C = PoCommutativeMonoid
                      (mkPoCommutativeMonoid
                        A.+-poCommutativeMonoid
                        B.+-poCommutativeMonoid)
        module A×B = SemiringWithoutAnnihilatingZero
                      (mk.semiringWithoutAnnihilatingZero
                        A.semiringWithoutAnnihilatingZero
                        B.semiringWithoutAnnihilatingZero)

mkPoSemiring :
    PoSemiring a ℓa₁ ℓa₂
  → PoSemiring b ℓb₁ ℓb₂
  → PoSemiring (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoSemiring A B = record
  { isPoSemiring = record
    { isPoSemiringWithoutAnnihilatingZero = A×B.isPoSemiringWithoutAnnihilatingZero
    ; zero =  (λ (a , b) → A.zeroˡ a , B.zeroˡ b)
            , (λ (a , b) → A.zeroʳ a , B.zeroʳ b)
    }
  }
  where module A = PoSemiring A
        module B = PoSemiring B
        module A×B = PoSemiringWithoutAnnihilatingZero
                      (mkPoSemiringWithoutAnnihilatingZero
                        A.poSemiringWithoutAnnihilatingZero
                        B.poSemiringWithoutAnnihilatingZero)

mkPoCommutativeSemiring :
    PoCommutativeSemiring a ℓa₁ ℓa₂
  → PoCommutativeSemiring b ℓb₁ ℓb₂
  → PoCommutativeSemiring (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoCommutativeSemiring A B = record
  { isPoCommutativeSemiring = record
    { isPoSemiring = A×B.isPoSemiring
    ; *-comm = λ (a₁ , b₁) (a₂ , b₂) → A.*-comm a₁ a₂ , B.*-comm b₁ b₂
    }
  }
  where module A = PoCommutativeSemiring A
        module B = PoCommutativeSemiring B
        module A×B = PoSemiring (mkPoSemiring A.poSemiring B.poSemiring)

mkPoRing :
    PoRing a ℓa₁ ℓa₂
  → PoRing b ℓb₁ ℓb₂
  → PoRing (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoRing A B = record
  { isPoRing = record
    { +-isPoAbelianGroup = C.isPoAbelianGroup
    ; *-cong = A×B.*-cong
    ; *-assoc = A×B.*-assoc
    ; *-identity = A×B.*-identity
    ; distrib = A×B.distrib
    ; *-compat = A×B.*-compat
    }
  }
  where module A = PoRing A
        module B = PoRing B
        module C = PoAbelianGroup
                    (mkPoAbelianGroup A.+-poAbelianGroup B.+-poAbelianGroup)
        module A×B = PoSemiring (mkPoSemiring A.poSemiring B.poSemiring)

mkPoCommutativeRing :
    PoCommutativeRing a ℓa₁ ℓa₂
  → PoCommutativeRing b ℓb₁ ℓb₂
  → PoCommutativeRing (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoCommutativeRing A B = record
 { isPoCommutativeRing = record
   { isPoRing = A×B.isPoRing
    ; *-comm = λ (a₁ , b₁) (a₂ , b₂) → A.*-comm a₁ a₂ , B.*-comm b₁ b₂
   }
 }
  where module A = PoCommutativeRing A
        module B = PoCommutativeRing B
        module A×B = PoRing (mkPoRing A.poRing B.poRing)
