{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level ; _⊔_)

module Algebra.Ordered.Partial.Construct.Pointwise
  {a : Level}
  (A : Set a)
  where

open import Algebra.Construct.Pointwise2 A as F using ()
open import Algebra.Bundles
open import Algebra.Ordered.Partial.Bundles
open import Data.Product using (_,_ ; proj₁ ; proj₂)

private
  variable
    c ℓ₁ ℓ₂ : Level

mkPoMagma : PoMagma c ℓ₁ ℓ₂ → PoMagma (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoMagma M = record
  { Carrier = A → Carrier
  ; _≤_ = λ f g → ∀ {a} → f a ≤ g a
  ; isPoMagma = record
    { isPartialOrder = record
      { isPreorder = record
        { isEquivalence = X.isEquivalence
        ; reflexive = λ x → reflexive x
        ; trans = λ x y → trans x y
        }
      ; antisym = λ x y → antisym x y
      }
    ; ∙-cong = λ x y {a} → ∙-cong x (y {a})
    ; ∙-compat = λ x {a} → ∙-compat (x {a})
    }
  }
 where open PoMagma M
       module X = Magma (F.mkMagma magma)

mkPoSemigroup : PoSemigroup c ℓ₁ ℓ₂ → PoSemigroup (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoSemigroup S = record
   { isPoSemigroup = record
      { isPoMagma = isPoMagma
      ; assoc = X.assoc
      }
   }
   where open PoMagma (mkPoMagma (PoSemigroup.poMagma S))
         module X = Semigroup (F.mkSemigroup (PoSemigroup.semigroup S))

mkPoMonoid : PoMonoid c ℓ₁ ℓ₂ → PoMonoid (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoMonoid M = record
  { isPoMonoid = record
    { isPoSemigroup = isPoSemigroup
    ; identity = X.identity
    }
   }
   where open PoSemigroup (mkPoSemigroup (PoMonoid.poSemigroup M))
         module X = Monoid (F.mkMonoid (PoMonoid.monoid M))

mkPoCommutativeMonoid :
    PoCommutativeMonoid c ℓ₁ ℓ₂
  → PoCommutativeMonoid (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoCommutativeMonoid M = record
  { isPoCommutativeMonoid = record
    { isPoMonoid = isPoMonoid
    ; comm = X.comm
    }
  }
  where open PoMonoid (mkPoMonoid (PoCommutativeMonoid.poMonoid M))
        module X = CommutativeMonoid
                    (F.mkCommutativeMonoid
                      (PoCommutativeMonoid.commutativeMonoid M))

mkPoGroup :
    PoGroup c ℓ₁ ℓ₂
  → PoGroup (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoGroup M = record
  { isPoGroup =
    record
      { isPoMonoid = isPoMonoid
      ; inverse = X.inverse
      ; ⁻¹-cong = X.⁻¹-cong
      }
  }
  where open PoMonoid (mkPoMonoid (PoGroup.poMonoid M))
        module X = Group (F.mkGroup (PoGroup.group M))

mkPoAbelianGroup :
    PoAbelianGroup c ℓ₁ ℓ₂
  → PoAbelianGroup (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoAbelianGroup M = record
  { isPoAbelianGroup = record
    { isPoGroup = isPoGroup
    ; comm = X.comm
    }
  }
  where open PoGroup (mkPoGroup (PoAbelianGroup.poGroup M))
        module X = CommutativeMonoid
                    (F.mkCommutativeMonoid
                      (PoAbelianGroup.commutativeMonoid M))

mkPoSemiringWithoutAnnihilatingZero :
    PoSemiringWithoutAnnihilatingZero c ℓ₁ ℓ₂
  → PoSemiringWithoutAnnihilatingZero (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoSemiringWithoutAnnihilatingZero M = record
  { isPoSemiringWithoutAnnihilatingZero = record
    { +-isPoCommutativeMonoid = M+.isPoCommutativeMonoid
    ; *-cong = λ x y {a} → *-cong (x {a}) (y {a})
    ; *-assoc = X.*-assoc
    ; *-identity = X.*-identity
    ; distrib = X.distrib
    ; *-compat = λ x y →
         (λ {a} → proj₁ (*-compat (x {a}) (y {a})))
        , λ {a} → proj₂ (*-compat (x {a}) (y {a}))
    }
  }
   where open PoSemiringWithoutAnnihilatingZero M
         module M+ = PoCommutativeMonoid
                      (mkPoCommutativeMonoid +-poCommutativeMonoid)
         module X = SemiringWithoutAnnihilatingZero
             (F.mkSemiringWithoutAnnihilatingZero semiringWithoutAnnihilatingZero)

mkPoSemiring : PoSemiring c ℓ₁ ℓ₂ → PoSemiring (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoSemiring M = record
  { isPoSemiring = record
    { isPoSemiringWithoutAnnihilatingZero = S.isPoSemiringWithoutAnnihilatingZero
    ; zero = X.zero
    }
  }
  where open PoSemiring M
        module S = PoSemiringWithoutAnnihilatingZero
            (mkPoSemiringWithoutAnnihilatingZero poSemiringWithoutAnnihilatingZero)
        module X = Semiring (F.mkSemiring semiring)

mkPoCommutativeSemiring :
    PoCommutativeSemiring c ℓ₁ ℓ₂
  → PoCommutativeSemiring (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoCommutativeSemiring S = record
  { isPoCommutativeSemiring = record {
       isPoSemiring = S.isPoSemiring
      ; *-comm = X.*-comm
      }
  }
  where open PoCommutativeSemiring S
        module S = PoSemiring (mkPoSemiring (poSemiring))
        module X = CommutativeSemiring (F.mkCommutativeSemiring commutativeSemiring)

mkPoRing :
    PoRing c ℓ₁ ℓ₂
  → PoRing (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoRing R = record
  { Carrier = A → Carrier
  ;  isPoRing = record
    { +-isPoAbelianGroup = G.isPoAbelianGroup
    ; *-cong = S.*-cong
    ; *-assoc = S.*-assoc
    ; *-identity = S.*-identity
    ; distrib = S.distrib
    ; *-compat = S.*-compat
    }
  }
  where open PoRing R
        module G = PoAbelianGroup (mkPoAbelianGroup +-poAbelianGroup)
        module S = PoSemiring (mkPoSemiring (poSemiring))

mkPoCommutativeRing :
    PoCommutativeRing c ℓ₁ ℓ₂
  → PoCommutativeRing (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoCommutativeRing R = record
  { Carrier = A → Carrier
  ; isPoCommutativeRing = record
    { isPoRing = S.isPoRing
    ; *-comm = X.*-comm }
  }
  where open PoCommutativeRing R
        module S = PoRing (mkPoRing poRing)
        module X = CommutativeRing (F.mkCommutativeRing commutativeRing)
