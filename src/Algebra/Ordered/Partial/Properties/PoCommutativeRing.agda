{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Partial.Bundles

module Algebra.Ordered.Partial.Properties.PoCommutativeRing
  {c ℓ₁ ℓ₂}
  (R : PoCommutativeRing c ℓ₁ ℓ₂)
  where

import Relation.Binary.Reasoning.PartialOrder as <-Reasoning

open PoCommutativeRing R

0≤y-x⇒x≤y : (x y : Carrier) → 0# ≤ (y - x) → x ≤ y
0≤y-x⇒x≤y x y 0≤y-x =
  begin
    x
  ≈˘⟨ +-identityˡ _ ⟩
    0# + x
  ≤⟨ +-mono₂ 0≤y-x refl ⟩
    y + - x + x
  ≈⟨ +-assoc _ _ _ ⟩
    y + (- x + x)
  ≈⟨ +-congˡ (-‿inverseˡ _) ⟩
    y + 0#
  ≈⟨ +-identityʳ _ ⟩
    y
  ∎
  where open <-Reasoning poset

x≤y⇒0≤y-x : (x y : Carrier) → x ≤ y → 0# ≤ (y - x)
x≤y⇒0≤y-x x y x≤y =
  begin
    0#
  ≈˘⟨ -‿inverseʳ _ ⟩
    (x + - x)
  ≤⟨ +-mono₂ x≤y refl ⟩
    y - x
  ∎
  where open <-Reasoning poset
