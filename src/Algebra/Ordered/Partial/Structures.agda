{-# OPTIONS --safe --cubical-compatible #-}
{-
https://en.wikipedia.org/wiki/Ordered_semigroup
https://link.springer.com/chapter/10.1007/978-94-017-0383-3_2
-}
open import Relation.Binary using (Rel)

module Algebra.Ordered.Partial.Structures
  {a ℓ₁ ℓ₂} {A : Set a}
  (_≈_ : Rel A ℓ₁)
  (_≤_ : Rel A ℓ₂)
  where

open import Level using (Level; _⊔_)
open import Algebra.Structures _≈_
open import Algebra.Core using (Op₁ ; Op₂)
open import Algebra.Definitions _≤_ as ≤Defs using ()
open import Algebra.Definitions _≈_
open import Data.Product
open import Relation.Binary.Structures using (IsPartialOrder)
open import Relation.Binary.Consequences

record IsPoMagma (∙ : Op₂ A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isPartialOrder : IsPartialOrder _≈_ _≤_
    ∙-cong         : Congruent₂ ∙
    ∙-compat       : ∀ {x y z} → x ≤ y → ∙ x z ≤ ∙ y z

  open IsPartialOrder isPartialOrder public

  isMagma : IsMagma ∙
  isMagma = record { isEquivalence = isEquivalence ; ∙-cong = ∙-cong }

  open IsMagma isMagma public using (setoid)

record IsPoSemigroup (∙ : Op₂ A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isPoMagma : IsPoMagma ∙
    assoc   : Associative ∙

  open IsPoMagma isPoMagma public

  isSemigroup : IsSemigroup ∙
  isSemigroup = record { isMagma = isMagma ; assoc = assoc }

record IsPoMonoid (∙ : Op₂ A) (ε : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isPoSemigroup : IsPoSemigroup ∙
    identity    : Identity ε ∙

  open IsPoSemigroup isPoSemigroup public

  isMonoid : IsMonoid ∙ ε
  isMonoid = record { isSemigroup = isSemigroup ; identity = identity }

  open IsMonoid isMonoid public
    using (identityˡ ; identityʳ ; isUnitalMagma)

record IsPoCommutativeMonoid (∙ : Op₂ A) (ε : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isPoMonoid : IsPoMonoid ∙ ε
    comm       : Commutative ∙

  open IsPoMonoid isPoMonoid public

  isCommutativeMonoid : IsCommutativeMonoid ∙ ε
  isCommutativeMonoid = record { isMonoid = isMonoid ; comm = comm }

  ∙-mono₂ : Relation.Binary.Monotonic₂ _≤_ _≤_ _≤_ ∙
  ∙-mono₂ x≤y u≤v =
      trans (∙-compat x≤y)
        (trans (≤-respˡ-≈ (comm _ _) (∙-compat u≤v)) (reflexive (comm _ _)))

record IsPoGroup (∙ : Op₂ A) (ε : A) (_⁻¹ : Op₁ A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isPoMonoid  : IsPoMonoid ∙ ε
    inverse   : Inverse ε _⁻¹ ∙
    ⁻¹-cong   : Congruent₁ _⁻¹

  open IsPoMonoid isPoMonoid public

  isGroup : IsGroup ∙ ε _⁻¹
  isGroup = record
    { isMonoid = isMonoid
    ; inverse = inverse
    ; ⁻¹-cong = ⁻¹-cong
    }

  open IsGroup isGroup public
    using (_-_)

record IsPoAbelianGroup (∙ : Op₂ A) (ε : A) (_⁻¹ : Op₁ A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isPoGroup : IsPoGroup ∙ ε _⁻¹
    comm    : Commutative ∙

  open IsPoGroup isPoGroup public

  isPoCommutativeMonoid : IsPoCommutativeMonoid ∙ ε
  isPoCommutativeMonoid = record
    { isPoMonoid = isPoMonoid
    ; comm     = comm
    }

  open IsPoCommutativeMonoid isPoCommutativeMonoid public
    using (∙-mono₂; isPoMonoid)

  isAbelianGroup : IsAbelianGroup ∙ ε _⁻¹
  isAbelianGroup = record { isGroup = isGroup ; comm = comm }

record IsPoNearSemiring (+ * : Op₂ A) (0# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isPoMonoid : IsPoMonoid + 0#
    *-cong       : Congruent₂ *
    *-assoc      : Associative *
    distribʳ     : * DistributesOverʳ +
    zeroˡ        : LeftZero 0# *
    *-compat     : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z × * z x ≤ * z y

  *-compatˡ : ∀ {x y z} → x ≤ y → 0# ≤ z → * z x ≤ * z y
  *-compatˡ x≤y 0≤z = proj₂ (*-compat x≤y 0≤z)

  *-compatʳ : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z
  *-compatʳ x≤y 0≤z = proj₁ (*-compat x≤y 0≤z)

  open IsPoMonoid +-isPoMonoid public
    renaming
      ( isMonoid to +-isMonoid
      ; ∙-compat to +-compat
      )

  isNearSemiring : IsNearSemiring + * 0#
  isNearSemiring = record
    { +-isMonoid = +-isMonoid
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; distribʳ = distribʳ
    ; zeroˡ = zeroˡ
    }

record IsPoSemiringWithoutAnnihilatingZero
  (+ * : Op₂ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isPoCommutativeMonoid : IsPoCommutativeMonoid + 0#
    *-cong                  : Congruent₂ *
    *-assoc                 : Associative *
    *-identity              : Identity 1# *
    distrib                 : * DistributesOver +
    *-compat     : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z × * z x ≤ * z y

  *-compatˡ : ∀ {x y z} → x ≤ y → 0# ≤ z → * z x ≤ * z y
  *-compatˡ x≤y 0≤z = proj₂ (*-compat x≤y 0≤z)

  *-compatʳ : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z
  *-compatʳ x≤y 0≤z = proj₁ (*-compat x≤y 0≤z)

  open IsPoCommutativeMonoid +-isPoCommutativeMonoid public
    renaming
      ( isCommutativeMonoid to +-isCommutativeMonoid
      ; ∙-mono₂ to +-mono₂
      )
    using
      ( module Eq
      ; refl
      ; trans
      ; reflexive
      )

  isSemiringWithoutAnnihilatingZero : IsSemiringWithoutAnnihilatingZero + * 0# 1#
  isSemiringWithoutAnnihilatingZero = record
    { +-isCommutativeMonoid = +-isCommutativeMonoid
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; *-identity = *-identity
    ; distrib = distrib
    }

  open IsSemiringWithoutAnnihilatingZero isSemiringWithoutAnnihilatingZero public
    using
     ( +-cong
      ; +-congʳ
      ; +-congˡ
      ; *-congʳ
      ; *-congˡ
      ; distribʳ
      ; distribˡ
      ; +-identity
      ; +-identityʳ
      ; +-identityˡ
      ; *-identityˡ
      ; *-identityʳ
      )

record IsPoSemiring (+ * : Op₂ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isPoSemiringWithoutAnnihilatingZero :
      IsPoSemiringWithoutAnnihilatingZero + * 0# 1#
    zero : Zero 0# *

  open IsPoSemiringWithoutAnnihilatingZero
    isPoSemiringWithoutAnnihilatingZero public

  isSemiring : IsSemiring + * 0# 1#
  isSemiring = record
    { isSemiringWithoutAnnihilatingZero = isSemiringWithoutAnnihilatingZero
    ; zero = zero
    }

  open IsSemiring isSemiring public
    using (
      zeroˡ
    ; zeroʳ
    )

record IsPoCommutativeSemiring (+ * : Op₂ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isPoSemiring : IsPoSemiring + * 0# 1#
    *-comm     : Commutative *

  open IsPoSemiring isPoSemiring public

  isCommutativeSemiring : IsCommutativeSemiring + * 0# 1#
  isCommutativeSemiring = record
    { isSemiring = isSemiring
    ; *-comm   = *-comm
    }

record IsPoQuasiring (+ * : Op₂ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isPoMonoid    : IsPoMonoid + 0#
    *-cong        : Congruent₂ *
    *-assoc       : Associative *
    *-identity    : Identity 1# *
    distrib       : * DistributesOver +
    zero          : Zero 0# *
    *-compat     : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z × * z x ≤ * z y

  *-compatˡ : ∀ {x y z} → x ≤ y → 0# ≤ z → * z x ≤ * z y
  *-compatˡ x≤y 0≤z = proj₂ (*-compat x≤y 0≤z)

  *-compatʳ : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z
  *-compatʳ x≤y 0≤z = proj₁ (*-compat x≤y 0≤z)

  open IsPoMonoid +-isPoMonoid public

  isQuasiring : IsQuasiring + * 0# 1#
  isQuasiring = record
    { +-isMonoid = IsPoMonoid.isMonoid +-isPoMonoid
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; *-identity = *-identity
    ; distrib = distrib
    ; zero = zero
    }

  open IsQuasiring isQuasiring public
    using
    ( distribˡ
    ; distribʳ
    ; zeroʳ
    ; zeroˡ
    )

record IsPoRingWithoutOne (+ * : Op₂ A) (-_ : Op₁ A) (0# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isPoAbelianGroup : IsPoAbelianGroup + 0# -_
    *-cong           : Congruent₂ *
    *-assoc          : Associative *
    distrib          : * DistributesOver +
    *-compat     : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z × * z x ≤ * z y

  *-compatˡ : ∀ {x y z} → x ≤ y → 0# ≤ z → * z x ≤ * z y
  *-compatˡ x≤y 0≤z = proj₂ (*-compat x≤y 0≤z)

  *-compatʳ : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z
  *-compatʳ x≤y 0≤z = proj₁ (*-compat x≤y 0≤z)

  open IsPoAbelianGroup +-isPoAbelianGroup public
    renaming (
      isAbelianGroup to +-isAbelianGroup)

  isRingWithoutOne : IsRingWithoutOne + * -_ 0#
  isRingWithoutOne = record
    { +-isAbelianGroup = +-isAbelianGroup
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; distrib = distrib
    }

record IsPoNonAssociativeRing (+ * : Op₂ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isPoAbelianGroup : IsPoAbelianGroup + 0# -_
    *-cong           : Congruent₂ *
    *-identity       : Identity 1# *
    distrib          : * DistributesOver +
    zero             : Zero 0# *
    *-compat     : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z × * z x ≤ * z y

  *-compatˡ : ∀ {x y z} → x ≤ y → 0# ≤ z → * z x ≤ * z y
  *-compatˡ x≤y 0≤z = proj₂ (*-compat x≤y 0≤z)

  *-compatʳ : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z
  *-compatʳ x≤y 0≤z = proj₁ (*-compat x≤y 0≤z)

  open IsPoAbelianGroup +-isPoAbelianGroup public
    renaming (isAbelianGroup to +-isAbelianGroup)

  isNonAssociativeRing  : IsNonAssociativeRing  + * -_ 0# 1#
  isNonAssociativeRing = record
    { +-isAbelianGroup = +-isAbelianGroup
    ; *-cong = *-cong
    ; *-identity = *-identity
    ; distrib = distrib
    ; zero = zero
    }

record IsPoNearring (+ * : Op₂ A) (0# 1# : A) (_⁻¹ : Op₁ A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isPoQuasiring : IsPoQuasiring + * 0# 1#
    +-inverse   : Inverse 0# _⁻¹ +
    ⁻¹-cong     : Congruent₁ _⁻¹

  open IsPoQuasiring isPoQuasiring public

  isNearring : IsNearring + * 0# 1# _⁻¹
  isNearring = record
   { isQuasiring = isQuasiring
   ; +-inverse = +-inverse
   ; ⁻¹-cong = ⁻¹-cong
   }

record IsPoRing (+ * : Op₂ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isPoAbelianGroup : IsPoAbelianGroup + 0# -_
    *-cong           : Congruent₂ *
    *-assoc          : Associative *
    *-identity       : Identity 1# *
    distrib          : * DistributesOver +
    *-compat     : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z × * z x ≤ * z y

  *-compatˡ : ∀ {x y z} → x ≤ y → 0# ≤ z → * z x ≤ * z y
  *-compatˡ x≤y 0≤z = proj₂ (*-compat x≤y 0≤z)

  *-compatʳ : ∀ {x y z} → x ≤ y → 0# ≤ z → * x z ≤ * y z
  *-compatʳ x≤y 0≤z = proj₁ (*-compat x≤y 0≤z)

  open IsPoAbelianGroup +-isPoAbelianGroup public
    renaming
    ( isAbelianGroup to +-isAbelianGroup
    ; isPoCommutativeMonoid to +-isPoCommutativeMonoid
    ; isPoMonoid to +-isPoMonoid
    )

  isRing : IsRing + * -_ 0# 1#
  isRing = record
    { +-isAbelianGroup = +-isAbelianGroup
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; *-identity = *-identity
    ; distrib = distrib
    }

  open IsRing isRing public
    using
     ( *-congˡ
     ; *-congʳ
     ; *-identityˡ
     ; *-identityʳ
     ; +-cong
     ; +-congʳ
     ; +-congˡ
     ; +-assoc
     ; +-comm
     ; +-identityʳ
     ; +-identityˡ
     ; -‿cong
     ; -‿inverseˡ
     ; -‿inverseʳ
     ; zeroʳ
     ; zeroˡ
     ; isRingWithoutOne
     )

  isPoSemiringWithoutAnnihilatingZero : IsPoSemiringWithoutAnnihilatingZero + * 0# 1#
  isPoSemiringWithoutAnnihilatingZero = record
    { +-isPoCommutativeMonoid = +-isPoCommutativeMonoid
    ; *-cong = *-cong
    ; *-assoc = *-assoc
    ; *-identity = *-identity
    ; distrib = distrib
    ; *-compat = *-compat
    }

  isPoSemiring : IsPoSemiring + * 0# 1#
  isPoSemiring = record
    { isPoSemiringWithoutAnnihilatingZero = isPoSemiringWithoutAnnihilatingZero
    ; zero = IsRing.zero isRing
    }

record IsPoCommutativeRing
         (+ * : Op₂ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    isPoRing : IsPoRing + * -_ 0# 1#
    *-comm : Commutative *

  open IsPoRing isPoRing public

  isCommutativeRing : IsCommutativeRing + * -_ 0# 1#
  isCommutativeRing = record { isRing = isRing ; *-comm = *-comm }

  isPoCommutativeSemiring : IsPoCommutativeSemiring + * 0# 1#
  isPoCommutativeSemiring = record
    { isPoSemiring = isPoSemiring ; *-comm = *-comm }

  open IsPoCommutativeSemiring isPoCommutativeSemiring public
    using (isCommutativeSemiring)

  open IsPoCommutativeMonoid +-isPoCommutativeMonoid public
    renaming
      ( isCommutativeMonoid to +-isCommutativeMonoid
      ; ∙-mono₂ to +-mono₂
      )
      using ()
