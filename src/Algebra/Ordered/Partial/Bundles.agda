{-# OPTIONS --safe --cubical-compatible #-}

open import Relation.Binary using (Rel)

module Algebra.Ordered.Partial.Bundles where

open import Level using (Level; _⊔_; suc)
open import Algebra.Bundles
open import Algebra.Ordered.Partial.Structures
open import Algebra.Core using (Op₁ ; Op₂)
open import Relation.Binary.Bundles

record PoMagma c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _∙_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_     : Rel Carrier ℓ₁
    _≤_     : Rel Carrier ℓ₂
    _∙_     : Op₂ Carrier
    isPoMagma : IsPoMagma _≈_ _≤_ _∙_

  open IsPoMagma isPoMagma public

  magma : Magma c ℓ₁
  magma = record { isMagma = isMagma }

  poset : Poset c ℓ₁ ℓ₂
  poset = record { isPartialOrder = isPartialOrder }

  open Magma magma public
    using (rawMagma)

record PoSemigroup c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _∙_
  infix  4 _≈_ _≤_
  field
    Carrier     : Set c
    _≈_         : Rel Carrier ℓ₁
    _≤_         : Rel Carrier ℓ₂
    _∙_         : Op₂ Carrier
    isPoSemigroup : IsPoSemigroup _≈_ _≤_ _∙_

  open IsPoSemigroup isPoSemigroup public

  poMagma : PoMagma c ℓ₁ ℓ₂
  poMagma = record { isPoMagma = isPoMagma }

  open PoMagma poMagma public
    using (
        poset
      ; magma
      )

  semigroup : Semigroup c ℓ₁
  semigroup = record { isSemigroup = isSemigroup }

  open Semigroup semigroup public
    hiding (isMagma; isSemigroup; magma; refl; reflexive; setoid)

record PoMonoid c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _∙_
  infix  4 _≈_ _≤_
  field
    Carrier  : Set c
    _≈_      : Rel Carrier ℓ₁
    _≤_      : Rel Carrier ℓ₂
    _∙_      : Op₂ Carrier
    ε        : Carrier
    isPoMonoid : IsPoMonoid _≈_ _≤_ _∙_ ε

  open IsPoMonoid isPoMonoid public

  poSemigroup : PoSemigroup c ℓ₁ ℓ₂
  poSemigroup = record { isPoSemigroup = isPoSemigroup }

  open PoSemigroup poSemigroup public
    using
      ( poset
      ; poMagma
      ; magma
      ; rawMagma
      ; semigroup
      )

  monoid : Monoid c ℓ₁
  monoid = record { isMonoid = isMonoid }

  open Monoid monoid public
    using (rawMonoid)

record PoCommutativeMonoid c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _∙_
  infix  4 _≈_ _≤_
  field
    Carrier             : Set c
    _≈_                 : Rel Carrier ℓ₁
    _≤_                 : Rel Carrier ℓ₂
    _∙_                 : Op₂ Carrier
    ε                   : Carrier
    isPoCommutativeMonoid : IsPoCommutativeMonoid _≈_ _≤_ _∙_ ε

  open IsPoCommutativeMonoid isPoCommutativeMonoid public

  poMonoid : PoMonoid c ℓ₁ ℓ₂
  poMonoid = record { isPoMonoid = isPoMonoid }

  open PoMonoid poMonoid public
    using
     ( poset
     ; poMagma
     ; poSemigroup
     ; magma
     ; semigroup
     ; rawMonoid
     )

  commutativeMonoid : CommutativeMonoid c ℓ₁
  commutativeMonoid = record { isCommutativeMonoid = isCommutativeMonoid }

  open CommutativeMonoid commutativeMonoid public
    using (commutativeSemigroup)

record PoGroup c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⁻¹
  infixl 7 _∙_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_     : Rel Carrier ℓ₁
    _≤_     : Rel Carrier ℓ₂
    _∙_     : Op₂ Carrier
    ε       : Carrier
    _⁻¹     : Op₁ Carrier
    isPoGroup : IsPoGroup _≈_ _≤_ _∙_ ε _⁻¹

  open IsPoGroup isPoGroup public

  poMonoid : PoMonoid c ℓ₁ ℓ₂
  poMonoid = record { isPoMonoid = isPoMonoid }

  open PoMonoid poMonoid public
    using
    ( poset
    ; poMagma
    ; poSemigroup
    ; magma
    ; semigroup
    ; rawMonoid
    )

  group : Group c ℓ₁
  group = record { isGroup = isGroup }

record PoAbelianGroup c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⁻¹
  infixl 7 _∙_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_     : Rel Carrier ℓ₁
    _≤_     : Rel Carrier ℓ₂
    _∙_     : Op₂ Carrier
    ε       : Carrier
    _⁻¹     : Op₁ Carrier
    isPoAbelianGroup : IsPoAbelianGroup _≈_ _≤_ _∙_ ε _⁻¹

  open IsPoAbelianGroup isPoAbelianGroup public

  poGroup : PoGroup c ℓ₁ ℓ₂
  poGroup = record { isPoGroup = isPoGroup }

  poCommutativeMonoid : PoCommutativeMonoid c ℓ₁ ℓ₂
  poCommutativeMonoid = record { isPoCommutativeMonoid = isPoCommutativeMonoid }

  open PoCommutativeMonoid poCommutativeMonoid public
    using
     ( poset
     ; poMagma
     ; poSemigroup
     ; commutativeMonoid
     ; commutativeSemigroup
     )

  abelianGroup : AbelianGroup c ℓ₁
  abelianGroup = record { isAbelianGroup = isAbelianGroup }


record PoSemiringWithoutAnnihilatingZero c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier    : Set c
    _≈_        : Rel Carrier ℓ₁
    _≤_        : Rel Carrier ℓ₂
    _+_        : Op₂ Carrier
    _*_        : Op₂ Carrier
    0#         : Carrier
    1#         : Carrier
    isPoSemiringWithoutAnnihilatingZero :
      IsPoSemiringWithoutAnnihilatingZero _≈_ _≤_ _+_ _*_ 0# 1#

  open IsPoSemiringWithoutAnnihilatingZero isPoSemiringWithoutAnnihilatingZero public

  +-poCommutativeMonoid : PoCommutativeMonoid c ℓ₁ ℓ₂
  +-poCommutativeMonoid = record { isPoCommutativeMonoid = +-isPoCommutativeMonoid }

  +-commutativeMonoid : CommutativeMonoid c ℓ₁
  +-commutativeMonoid = record { isCommutativeMonoid = +-isCommutativeMonoid }

  open PoCommutativeMonoid +-poCommutativeMonoid public
    using (
        poset
      ; setoid
    )
    renaming
     ( poMonoid to +-poMonoid
     ; poMagma to +-poMagma
     ; poSemigroup to +-poSemigroup
     )

  semiringWithoutAnnihilatingZero : SemiringWithoutAnnihilatingZero c ℓ₁
  semiringWithoutAnnihilatingZero =
    record
      { isSemiringWithoutAnnihilatingZero = isSemiringWithoutAnnihilatingZero }

  open SemiringWithoutAnnihilatingZero semiringWithoutAnnihilatingZero public
    using
     ( +-magma
     ; +-commutativeSemigroup
     ; +-semigroup
     )

record PoSemiring c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier    : Set c
    _≈_        : Rel Carrier ℓ₁
    _≤_        : Rel Carrier ℓ₂
    _+_        : Op₂ Carrier
    _*_        : Op₂ Carrier
    0#         : Carrier
    1#         : Carrier
    isPoSemiring : IsPoSemiring _≈_ _≤_ _+_ _*_ 0# 1#

  open IsPoSemiring isPoSemiring public

  poSemiringWithoutAnnihilatingZero : PoSemiringWithoutAnnihilatingZero c ℓ₁ ℓ₂
  poSemiringWithoutAnnihilatingZero =
    record { isPoSemiringWithoutAnnihilatingZero =
              isPoSemiringWithoutAnnihilatingZero }

  open PoSemiringWithoutAnnihilatingZero poSemiringWithoutAnnihilatingZero public
    using (
        poset
      ; setoid
      ; +-poMagma
      ; +-poSemigroup
      ; +-poMonoid
      )

  semiring : Semiring c ℓ₁
  semiring = record { isSemiring = isSemiring }

  open Semiring semiring public using
     ( rawSemiring
     ; +-rawMonoid
     ; +-monoid
     ; +-magma
     ; +-semigroup
     ; +-commutativeSemigroup
     )

record PoCommutativeSemiring c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier               : Set c
    _≈_                   : Rel Carrier ℓ₁
    _≤_                   : Rel Carrier ℓ₂
    _+_                   : Op₂ Carrier
    _*_                   : Op₂ Carrier
    0#                    : Carrier
    1#                    : Carrier
    isPoCommutativeSemiring : IsPoCommutativeSemiring _≈_ _≤_ _+_ _*_ 0# 1#

  open IsPoCommutativeSemiring isPoCommutativeSemiring public

  poSemiring : PoSemiring c ℓ₁ ℓ₂
  poSemiring = record { isPoSemiring = isPoSemiring }

  open PoSemiring poSemiring public
    using (poset; setoid; semiring)

  commutativeSemiring : CommutativeSemiring c ℓ₁
  commutativeSemiring = record
   { isCommutativeSemiring = isCommutativeSemiring }

  open CommutativeSemiring commutativeSemiring public
    using (*-commutativeSemigroup)

record PoRing c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier  : Set c
    _≈_      : Rel Carrier ℓ₁
    _≤_      : Rel Carrier ℓ₂
    _+_      : Op₂ Carrier
    _*_      : Op₂ Carrier
    -_       : Op₁ Carrier
    0#       : Carrier
    1#       : Carrier
    isPoRing : IsPoRing _≈_ _≤_ _+_ _*_ -_ 0# 1#

  open IsPoRing isPoRing public

  ring : Ring c ℓ₁
  ring = record { isRing = isRing }

  ringWithoutOne : RingWithoutOne c ℓ₁
  ringWithoutOne = record { isRingWithoutOne = isRingWithoutOne }

  poSemiring : PoSemiring c ℓ₁ ℓ₂
  poSemiring = record { isPoSemiring = isPoSemiring }

  +-poAbelianGroup : PoAbelianGroup c ℓ₁ ℓ₂
  +-poAbelianGroup = record { isPoAbelianGroup = +-isPoAbelianGroup }

  open PoSemiring poSemiring public
    using
     ( poset
     ; semiring
     ; rawSemiring
     ; +-poMonoid
     ; +-commutativeSemigroup
     )

record PoCommutativeRing c ℓ₁ ℓ₂ : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier  : Set c
    _≈_      : Rel Carrier ℓ₁
    _≤_      : Rel Carrier ℓ₂
    _+_      : Op₂ Carrier
    _*_      : Op₂ Carrier
    -_       : Op₁ Carrier
    0#       : Carrier
    1#       : Carrier
    isPoCommutativeRing : IsPoCommutativeRing _≈_ _≤_ _+_ _*_ -_ 0# 1#

  open IsPoCommutativeRing isPoCommutativeRing public

  commutativeRing : CommutativeRing c ℓ₁
  commutativeRing = record { isCommutativeRing = isCommutativeRing }

  poRing : PoRing c ℓ₁ ℓ₂
  poRing = record { isPoRing = isPoRing }

  open PoRing poRing public
    using
     ( poset
     ; ring
     ; semiring
     ; rawSemiring
     ; poSemiring
     ; +-poAbelianGroup
     ; +-poMonoid
     ; ringWithoutOne
     ; +-commutativeSemigroup
     )

  poCommutativeSemiring : PoCommutativeSemiring c ℓ₁ ℓ₂
  poCommutativeSemiring = record
    { isPoCommutativeSemiring = isPoCommutativeSemiring }

  open PoCommutativeSemiring poCommutativeSemiring public
    using
     ( commutativeSemiring
     ; *-commutativeSemigroup)
