
{-# OPTIONS --cubical-compatible --safe #-}

open import Relation.Binary using (Rel)

module Algebra.Star.Definitions {a ℓ} {A : Set a} (_≈_ : Rel A ℓ) where

open import Level using (_⊔_)
open import Algebra

AntiHomomorphism : Op₂ A → Op₁ A → Set _
AntiHomomorphism _∙_  _⋆ = ∀ (x y : A) → ((x ∙ y) ⋆) ≈ ((y ⋆) ∙ (x ⋆))

ConjugateSymmetric : Op₂ A → Op₁ A → Set _
ConjugateSymmetric _*_  _⋆ = ∀ {x y} → (x * y) ≈ ((y * x) ⋆)
