{-# OPTIONS --cubical-compatible --safe #-}

open import Level using (Level ; _⊔_)

module Algebra.Star.Construct.Pointwise
   {a : Level}
   (A : Set a)
   where

open import Algebra.Bundles
open import Algebra.Star.Bundles
open import Algebra.Construct.Pointwise2 A

private
   variable
      c ℓ : Level

mkStar : Star c ℓ → Star (a ⊔ c) (a ⊔ ℓ)
mkStar S = record
   { Carrier = A → S.Carrier
   ; _≈_ = λ f g → ∀ {a} → f a ≈ g a
   ; _∙_ = λ f g a → f a ∙ g a
   ; _⋆ = λ f a → (f a) ⋆
   ; isStar = record
      { ⋆-cong = λ x {a} → ⋆-cong (x {a})
      ; ⋆-involutive = λ x {a} → S.⋆-involutive (x a)
      ; ⋆-anti-homo-∙ = λ f g {a} → ⋆-anti-homo-∙ (f a) (g a)
      }
   }
   where open module S = Star S

mkStarSemiring : StarSemiring c ℓ → StarSemiring (a ⊔ c) (a ⊔ ℓ)
mkStarSemiring S = record
   { Carrier = A → S.Carrier
   ; isStarSemiring = record
      { +-isStar = Star.isStar (mkStar S.+-star)
      ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar S.*-star)
      ; isSemiring = SS.isSemiring
      }
   }
   where module S = StarSemiring S
         module SS = Semiring (mkSemiring S.semiring)

mkStarCommutativeSemiring :
   StarCommutativeSemiring c ℓ → StarCommutativeSemiring (a ⊔ c) (a ⊔ ℓ)
mkStarCommutativeSemiring S = record
   { Carrier = A → S.Carrier
   ; isStarCommutativeSemiring = record
      { +-isStar = Star.isStar (mkStar S.+-star)
      ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar S.*-star)
      ; isCommutativeSemiring = SS.isCommutativeSemiring
      }
   }
   where module S = StarCommutativeSemiring S
         module SS = CommutativeSemiring
                    (mkCommutativeSemiring S.commutativeSemiring)

mkConjugateStarCommutativeSemiring :
     ConjugateStarCommutativeSemiring c ℓ
   → ConjugateStarCommutativeSemiring (a ⊔ c) (a ⊔ ℓ)
mkConjugateStarCommutativeSemiring S = record
   { Carrier = A → S.Carrier
   ; isConjugateStarCommutativeSemiring = record
      { *-conjugateSymmetric = λ {f} {g} {a} → S.*-conjugateSymmetric {f a} {g a}
      ; isStarCommutativeSemiring = SS.isStarCommutativeSemiring
      }
   }
   where module S = ConjugateStarCommutativeSemiring S
         module SS = StarCommutativeSemiring
                    (mkStarCommutativeSemiring S.starCommutativeSemiring)

mkStarRing : StarRing c ℓ → StarRing (a ⊔ c) (a ⊔ ℓ)
mkStarRing S = record
   { Carrier = A → S.Carrier
   ; isStarRing = record
      { +-isStar = Star.isStar (mkStar S.+-star)
      ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar S.*-star)
      ; isRing = SS.isRing
      }
   }
   where module S = StarRing S
         module SS = Ring (mkRing S.ring)

mkConjugateStarRing : ConjugateStarRing c ℓ → ConjugateStarRing (a ⊔ c) (a ⊔ ℓ)
mkConjugateStarRing S = record
   { Carrier = A → S.Carrier
   ; isConjugateStarRing = record
      { *-conjugateSymmetric = λ {f} {g} {a} → S.*-conjugateSymmetric {f a} {g a}
      ; isStarRing = SS.isStarRing
      }
   }
   where module S = ConjugateStarRing S
         module SS = StarRing (mkStarRing S.starRing)

mkStarCommutativeRing : StarCommutativeRing c ℓ → StarCommutativeRing (a ⊔ c) (a ⊔ ℓ)
mkStarCommutativeRing S = record
   { Carrier = A → S.Carrier
   ; isStarCommutativeRing = record
      { +-isStar = Star.isStar (mkStar S.+-star)
      ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar S.*-star)
      ; isCommutativeRing = SS.isCommutativeRing
      }
   }
   where module S = StarCommutativeRing S
         module SS = CommutativeRing (mkCommutativeRing S.commutativeRing)

mkConjugateStarCommutativeRing : ConjugateStarCommutativeRing c ℓ →
                                 ConjugateStarCommutativeRing (a ⊔ c) (a ⊔ ℓ)
mkConjugateStarCommutativeRing S = record
   { Carrier = A → S.Carrier
   ; isConjugateStarCommutativeRing = record
      { *-conjugateSymmetric = λ {f} {g} {a} → S.*-conjugateSymmetric {f a} {g a}
      ; isStarCommutativeRing = SS.isStarCommutativeRing
      }
   }
   where module S = ConjugateStarCommutativeRing S
         module SS = StarCommutativeRing
                       (mkStarCommutativeRing S.starCommutativeRing)
