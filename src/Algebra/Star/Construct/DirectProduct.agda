{-# OPTIONS --safe --cubical-compatible #-}

module Algebra.Star.Construct.DirectProduct where

open import Level using (Level; _⊔_)
open import Algebra.Construct.DirectProduct as mk
open import Algebra.Bundles
open import Algebra.Star.Bundles

open import Data.Product as × using (_,_; _×_)

private
  variable
    a b ℓa ℓb : Level

mkStar : Star a ℓa → Star b ℓb → Star (a ⊔ b) (ℓa ⊔ ℓb)
mkStar A B = record
  { Carrier = A.Carrier × B.Carrier
  ; _≈_ = λ (a₁ , b₁) (a₂ , b₂) → a₁ A.≈ a₂ × (b₁ B.≈ b₂)
  ; _∙_ = ×.zip A._∙_ B._∙_
  ; _⋆ = ×.map A._⋆ B._⋆
  ; isStar = record
    { ⋆-cong = ×.map A.⋆-cong B.⋆-cong
    ; ⋆-involutive = λ (a , b) → (A.⋆-involutive a) , (B.⋆-involutive b)
    ; ⋆-anti-homo-∙ =  λ (a₁ , b₁) (a₂ , b₂) →
        A.⋆-anti-homo-∙ a₁ a₂ , B.⋆-anti-homo-∙ b₁ b₂
    }
  }
  where module A = Star A
        module B = Star B

mkStarSemiring : StarSemiring a ℓa → StarSemiring b ℓb
  → StarSemiring (a ⊔ b) (ℓa ⊔ ℓb)
mkStarSemiring A B = record
  { Carrier = A.Carrier × B.Carrier
  ; _⋆ = ×.map A._⋆ B._⋆
  ; isStarSemiring = record
    { +-isStar = Star.isStar (mkStar A.+-star B.+-star)
    ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar A.*-star B.*-star)
    ; isSemiring = A×B.isSemiring
    }
  }
  where module A = StarSemiring A
        module B = StarSemiring B
        module A×B = Semiring (mk.semiring A.semiring B.semiring)

mkStarCommutativeSemiring :
    StarCommutativeSemiring a ℓa
  → StarCommutativeSemiring b ℓb
  → StarCommutativeSemiring (a ⊔ b) (ℓa ⊔ ℓb)
mkStarCommutativeSemiring A B = record
  { Carrier = A.Carrier × B.Carrier
  ; _⋆ = ×.map A._⋆ B._⋆
  ; isStarCommutativeSemiring = record
    { +-isStar = Star.isStar (mkStar A.+-star B.+-star)
    ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar A.*-star B.*-star)
    ; isCommutativeSemiring = A×B.isCommutativeSemiring
    }
  }
  where module A = StarCommutativeSemiring A
        module B = StarCommutativeSemiring B
        module A×B = CommutativeSemiring
                        (mk.commutativeSemiring
                            A.commutativeSemiring
                            B.commutativeSemiring)

mkConjugateStarCommutativeSemiring :
    ConjugateStarCommutativeSemiring a ℓa
  → ConjugateStarCommutativeSemiring b ℓb
  → ConjugateStarCommutativeSemiring (a ⊔ b) (ℓa ⊔ ℓb)
mkConjugateStarCommutativeSemiring A B = record
  { Carrier = A.Carrier × B.Carrier
  ; isConjugateStarCommutativeSemiring = record
    { *-conjugateSymmetric =
          A.*-conjugateSymmetric
        , B.*-conjugateSymmetric
    ; isStarCommutativeSemiring = A×B.isStarCommutativeSemiring
    }
  }
  where module A = ConjugateStarCommutativeSemiring A
        module B = ConjugateStarCommutativeSemiring B
        module A×B = StarCommutativeSemiring
                        (mkStarCommutativeSemiring
                          A.starCommutativeSemiring
                          B.starCommutativeSemiring)

mkStarRing : StarRing a ℓa → StarRing b ℓb → StarRing (a ⊔ b) (ℓa ⊔ ℓb)
mkStarRing A B = record
  { Carrier = A.Carrier × B.Carrier
  ; _⋆ = ×.map A._⋆ B._⋆
  ; isStarRing = record
    { +-isStar = Star.isStar (mkStar A.+-star B.+-star)
    ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar A.*-star B.*-star)
    ; isRing = A×B.isRing
    }
  }
  where module A = StarRing A
        module B = StarRing B
        module A×B = Ring (mk.ring A.ring B.ring)

mkConjugateStarRing : ConjugateStarRing a ℓa →
                      ConjugateStarRing b ℓb →
                      ConjugateStarRing (a ⊔ b) (ℓa ⊔ ℓb)
mkConjugateStarRing A B = record
  { Carrier = A.Carrier × B.Carrier
  ; isConjugateStarRing = record
     { *-conjugateSymmetric = A.*-conjugateSymmetric , B.*-conjugateSymmetric
     ; isStarRing = A×B.isStarRing }
  }
  where module A = ConjugateStarRing A
        module B = ConjugateStarRing B
        module A×B = StarRing (mkStarRing A.starRing B.starRing)

mkStarCommutativeRing : StarCommutativeRing a ℓa →
                        StarCommutativeRing b ℓb →
                        StarCommutativeRing (a ⊔ b) (ℓa ⊔ ℓb)
mkStarCommutativeRing A B = record
  { Carrier = A.Carrier × B.Carrier
  ; _⋆ = ×.map A._⋆ B._⋆
  ; isStarCommutativeRing = record
    { +-isStar = Star.isStar (mkStar A.+-star B.+-star)
    ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar A.*-star B.*-star)
    ; isCommutativeRing = A×B.isCommutativeRing
    }
  }
  where module A = StarCommutativeRing A
        module B = StarCommutativeRing B
        module A×B = CommutativeRing
          (mk.commutativeRing A.commutativeRing B.commutativeRing)

mkConjugateStarCommutativeRing :
  ConjugateStarCommutativeRing a ℓa →
  ConjugateStarCommutativeRing b ℓb →
  ConjugateStarCommutativeRing (a ⊔ b) (ℓa ⊔ ℓb)
mkConjugateStarCommutativeRing A B = record
  { Carrier = A.Carrier × B.Carrier
  ; isConjugateStarCommutativeRing = record
    { *-conjugateSymmetric = A.*-conjugateSymmetric , B.*-conjugateSymmetric
    ; isStarCommutativeRing = A×B.isStarCommutativeRing
    }
  }
  where module A = ConjugateStarCommutativeRing A
        module B = ConjugateStarCommutativeRing B
        module A×B = StarCommutativeRing
          (mkStarCommutativeRing A.starCommutativeRing B.starCommutativeRing)
