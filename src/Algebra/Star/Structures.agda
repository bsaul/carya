{-# OPTIONS --cubical-compatible --safe #-}
{-
See:
* https://ncatlab.org/nlab/show/star-algebra
* https://en.wikipedia.org/wiki/*-algebra

-}

open import Relation.Binary using (Rel)

module Algebra.Star.Structures {a ℓ} {A : Set a} (_≈_ : Rel A ℓ) where

open import Level using (_⊔_)
open import Algebra
open import Algebra.Star.Definitions _≈_
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

record IsStar (_∙_ : Op₂ A) (_⋆ : Op₁ A) : Set (a ⊔ ℓ) where
  field
    ⋆-cong : Congruent₁  _≈_ _⋆
    ⋆-involutive : Involutive _≈_ _⋆
    ⋆-anti-homo-∙ : AntiHomomorphism _∙_ _⋆

record IsStarMagma (_∙_ : Op₂ A) (_⋆ : Op₁ A) : Set (a ⊔ ℓ) where
  field
    isMagma : IsMagma _≈_  _∙_
    isStar : IsStar _∙_ _⋆

  open IsMagma isMagma public
  open IsStar isStar public

record IsStarSemigroup (_∙_ : Op₂ A) (_⋆ : Op₁ A) : Set (a ⊔ ℓ) where
  field
    isSemigroup : IsSemigroup _≈_ _∙_
    isStar : IsStar _∙_ _⋆

  open IsSemigroup isSemigroup
  open IsStar isStar public

record IsStarCommutativeSemigroup (_∙_ : Op₂ A) (_⋆ : Op₁ A) : Set (a ⊔ ℓ) where
  field
    isCommutativeSemigroup : IsCommutativeSemigroup _≈_ _∙_
    isStar : IsStar _∙_ _⋆

  open IsCommutativeSemigroup isCommutativeSemigroup
  open IsStar isStar public

  ⋆-homo-∙ : ∀ (x y : A) → ((x ∙ y) ⋆) ≈ ((x ⋆) ∙ (y ⋆))
  ⋆-homo-∙ x y = trans (⋆-anti-homo-∙ x y) (comm (y ⋆) (x ⋆))

record IsStarMonoid (_∙_ : Op₂ A) (_⋆ : Op₁ A) (ε : A) : Set (a ⊔ ℓ) where
  field
    isMonoid : IsMonoid _≈_ _∙_ ε
    isStar : IsStar _∙_ _⋆

  open IsMonoid isMonoid public
  open IsStar isStar public

  isStarSemigroup : IsStarSemigroup _∙_ _⋆
  isStarSemigroup = record { isSemigroup = isSemigroup ; isStar = isStar }

  ε⋆≈ε : (ε ⋆) ≈ ε
  ε⋆≈ε = begin
    ε ⋆               ≈⟨ identityʳ (ε ⋆) ⟨
    (ε ⋆) ∙ ε         ≈⟨ ∙-congˡ (sym (⋆-involutive ε)) ⟩
    (ε ⋆) ∙ ((ε ⋆) ⋆) ≈⟨ ⋆-anti-homo-∙ (ε ⋆) ε ⟨
    ((ε ⋆) ∙ ε) ⋆     ≈⟨ ⋆-cong (identityʳ (ε ⋆)) ⟩
    (ε ⋆) ⋆           ≈⟨ ⋆-involutive ε ⟩
    ε                 ∎
    where open ≈-Reasoning setoid

record IsStarCommutativeMonoid (_∙_ : Op₂ A) (_⋆ : Op₁ A) (ε : A) : Set (a ⊔ ℓ) where
  field
    isCommutativeMonoid : IsCommutativeMonoid _≈_ _∙_ ε
    isStar : IsStar _∙_ _⋆

  open IsCommutativeMonoid isCommutativeMonoid public
  open IsStar isStar public

  isStarMonoid : IsStarMonoid _∙_ _⋆ ε
  isStarMonoid = record { isMonoid = isMonoid ; isStar = isStar }

  open IsStarMonoid isStarMonoid public
    using (ε⋆≈ε)

  isStarCommutativeSemigroup : IsStarCommutativeSemigroup _∙_ _⋆
  isStarCommutativeSemigroup = record
    { isCommutativeSemigroup = isCommutativeSemigroup
    ; isStar = isStar
    }

  open IsStarCommutativeSemigroup isStarCommutativeSemigroup public
    using (⋆-homo-∙)

record IsStarSemiring
    (+ * : Op₂ A) (⋆ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ) where
  field
    +-isStar : IsStar + ⋆
    ⋆-anti-homo-* : AntiHomomorphism * ⋆
    isSemiring : IsSemiring _≈_ + * 0# 1#

  open IsStar +-isStar public
    renaming
     (⋆-anti-homo-∙ to ⋆-anti-homo-+)
  open IsSemiring isSemiring public

  *-isStar : IsStar * ⋆
  *-isStar = record
    { ⋆-cong = ⋆-cong
    ; ⋆-involutive = ⋆-involutive
    ; ⋆-anti-homo-∙ = ⋆-anti-homo-*
    }

  +-isStarCommutativeMonoid : IsStarCommutativeMonoid + ⋆ 0#
  +-isStarCommutativeMonoid = record
    { isCommutativeMonoid = +-isCommutativeMonoid
    ; isStar = +-isStar
    }

  open IsStarCommutativeMonoid +-isStarCommutativeMonoid public
    using ()
    renaming (ε⋆≈ε to 0⋆≈0 ; ⋆-homo-∙ to ⋆-homo-+)

  *-isStarMonoid : IsStarMonoid * ⋆ 1#
  *-isStarMonoid = record
    { isMonoid = *-isMonoid
    ; isStar = *-isStar
    }

  open IsStarMonoid *-isStarMonoid public
    using ()
    renaming (ε⋆≈ε to 1⋆≈1)

record IsStarCommutativeSemiring
    (+ * : Op₂ A) (⋆ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ) where
  field
    +-isStar : IsStar + ⋆
    ⋆-anti-homo-* : AntiHomomorphism * ⋆
    isCommutativeSemiring : IsCommutativeSemiring _≈_ + * 0# 1#

  open IsCommutativeSemiring isCommutativeSemiring public
  open IsStar +-isStar public

  isStarSemiring : IsStarSemiring + * ⋆ 0# 1#
  isStarSemiring = record
    { +-isStar = +-isStar
    ; ⋆-anti-homo-* = ⋆-anti-homo-*
    ; isSemiring = isSemiring
    }

  open IsStarSemiring isStarSemiring public
    using (*-isStar ; 0⋆≈0 ; 1⋆≈1; ⋆-homo-+)

  *-isStarCommutativeMonoid : IsStarCommutativeMonoid * ⋆ 1#
  *-isStarCommutativeMonoid = record
    { isCommutativeMonoid = *-isCommutativeMonoid
    ; isStar = *-isStar
    }

  open IsStarCommutativeMonoid *-isStarCommutativeMonoid public
    using ()
    renaming (⋆-homo-∙ to ⋆-homo-*)

record IsConjugateStarCommutativeSemiring
    (+ * : Op₂ A) (⋆ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ) where
  field
    *-conjugateSymmetric : ConjugateSymmetric * ⋆
    isStarCommutativeSemiring : IsStarCommutativeSemiring + * ⋆ 0# 1#

  open IsStarCommutativeSemiring isStarCommutativeSemiring public

record IsStarRing
    (+ * : Op₂ A) (⋆ : Op₁ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ) where
  field
    +-isStar : IsStar + ⋆
    ⋆-anti-homo-* : AntiHomomorphism * ⋆
    isRing : IsRing _≈_ + * -_ 0# 1#

  open IsStar +-isStar public
    renaming (⋆-anti-homo-∙ to ⋆-anti-homo-+)
  open IsRing isRing public

  isStarSemiring : IsStarSemiring + * ⋆ 0# 1#
  isStarSemiring = record
    { +-isStar = +-isStar
    ; ⋆-anti-homo-* = ⋆-anti-homo-*
    ; isSemiring = isSemiring
    }

  open IsStarSemiring isStarSemiring public
    using
    ( *-isStar ; 0⋆≈0 ; 1⋆≈1; ⋆-homo-+ )

record IsConjugateStarRing
    (+ * : Op₂ A) (⋆ : Op₁ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ) where
  field
    *-conjugateSymmetric : ConjugateSymmetric * ⋆
    isStarRing : IsStarRing + * ⋆ -_ 0# 1#

  open IsStarRing isStarRing public

record IsStarCommutativeRing
    (+ * : Op₂ A) (⋆ : Op₁ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ) where
  field
    +-isStar : IsStar + ⋆
    ⋆-anti-homo-* : AntiHomomorphism * ⋆
    isCommutativeRing : IsCommutativeRing _≈_ + * -_ 0# 1#

  open IsStar +-isStar public
    renaming (⋆-anti-homo-∙ to ⋆-anti-homo-+)
  open IsCommutativeRing isCommutativeRing public

  isStarCommutativeSemiring : IsStarCommutativeSemiring + * ⋆ 0# 1#
  isStarCommutativeSemiring = record
    { +-isStar = +-isStar
    ; ⋆-anti-homo-* = ⋆-anti-homo-*
    ; isCommutativeSemiring = isCommutativeSemiring
    }

  open IsStarCommutativeSemiring isStarCommutativeSemiring public
    using
    ( *-isStar ; 0⋆≈0 ; 1⋆≈1; ⋆-homo-+ ; ⋆-homo-*  )

record IsConjugateStarCommutativeRing
    (+ * : Op₂ A) (⋆ : Op₁ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ) where
  field
    *-conjugateSymmetric : ConjugateSymmetric * ⋆
    isStarCommutativeRing : IsStarCommutativeRing + * ⋆ -_ 0# 1#

  open IsStarCommutativeRing isStarCommutativeRing public
