
{-# OPTIONS --cubical-compatible --safe #-}
{-
See:
* https://ncatlab.org/nlab/show/star-algebra
* https://en.wikipedia.org/wiki/*-algebra
-}

module Algebra.Star.Bundles  where

open import Level using (suc; _⊔_; Level)
open import Algebra
open import Relation.Binary using (Rel; Poset; IsPartialOrder)
open import Algebra.Star.Structures

record Star c ℓ : Set (suc (c ⊔ ℓ)) where
  field
    Carrier : Set c
    _≈_     : Rel Carrier ℓ
    _∙_     : Op₂ Carrier
    _⋆      : Op₁ Carrier
    isStar  : IsStar _≈_ _∙_  _⋆

  open IsStar isStar public

record StarSemiring c ℓ : Set (suc (c ⊔ ℓ)) where
  infix  8 _⋆
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_
  field
    Carrier               : Set c
    _≈_                   : Rel Carrier ℓ
    _+_                   : Op₂ Carrier
    _*_                   : Op₂ Carrier
    _⋆                    : Op₁ Carrier
    0#                    : Carrier
    1#                    : Carrier
    isStarSemiring       : IsStarSemiring _≈_ _+_ _*_ _⋆ 0# 1#

  open IsStarSemiring isStarSemiring public

  semiring : Semiring _ _
  semiring = record { isSemiring = isSemiring }

  open Semiring semiring using (_≉_) public

  +-commutativeMonoid : CommutativeMonoid _ _
  +-commutativeMonoid = record { isCommutativeMonoid = +-isCommutativeMonoid }

  +-star : Star _ _
  +-star = record { isStar = +-isStar }

  *-star : Star _ _
  *-star = record { isStar = *-isStar }

record StarCommutativeSemiring c ℓ : Set (suc (c ⊔ ℓ)) where
  infix  8 _⋆
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_
  field
    Carrier               : Set c
    _≈_                   : Rel Carrier ℓ
    _+_                   : Op₂ Carrier
    _*_                   : Op₂ Carrier
    _⋆                    : Op₁ Carrier
    0#                    : Carrier
    1#                    : Carrier
    isStarCommutativeSemiring : IsStarCommutativeSemiring _≈_ _+_ _*_ _⋆ 0# 1#

  open IsStarCommutativeSemiring isStarCommutativeSemiring public

  semiring : Semiring c ℓ
  semiring = record { isSemiring = isSemiring }

  open Semiring semiring using (_≉_)

  commutativeSemiring : CommutativeSemiring c ℓ
  commutativeSemiring = record { isCommutativeSemiring = isCommutativeSemiring }

  +-commutativeSemigroup : CommutativeSemigroup _ _
  +-commutativeSemigroup = record
    { isCommutativeSemigroup = +-isCommutativeSemigroup }

  *-commutativeSemigroup : CommutativeSemigroup _ _
  *-commutativeSemigroup = record
    { isCommutativeSemigroup = *-isCommutativeSemigroup }

  +-commutativeMonoid : CommutativeMonoid _ _
  +-commutativeMonoid = record { isCommutativeMonoid = +-isCommutativeMonoid }

  +-star : Star _ _
  +-star = record { isStar = +-isStar }

  *-star : Star _ _
  *-star = record { isStar = *-isStar }

record ConjugateStarCommutativeSemiring c ℓ : Set (suc (c ⊔ ℓ)) where
  infix  8 _⋆
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_
  field
    Carrier               : Set c
    _≈_                   : Rel Carrier ℓ
    _+_                   : Op₂ Carrier
    _*_                   : Op₂ Carrier
    _⋆                    : Op₁ Carrier
    0#                    : Carrier
    1#                    : Carrier
    isConjugateStarCommutativeSemiring :
      IsConjugateStarCommutativeSemiring _≈_ _+_ _*_ _⋆ 0# 1#

  open IsConjugateStarCommutativeSemiring
        isConjugateStarCommutativeSemiring public

  semiring : Semiring c ℓ
  semiring = record { isSemiring = isSemiring }

  open Semiring semiring using (_≉_) public

  commutativeSemiring : CommutativeSemiring c ℓ
  commutativeSemiring = record { isCommutativeSemiring = isCommutativeSemiring }

  starCommutativeSemiring : StarCommutativeSemiring c ℓ
  starCommutativeSemiring = record
    { isStarCommutativeSemiring = isStarCommutativeSemiring }

  open StarCommutativeSemiring starCommutativeSemiring public
    using
    ( +-commutativeMonoid
    ; +-commutativeSemigroup
    ; *-commutativeSemigroup
    ; +-star
    ; *-star
    )

record StarRing c ℓ : Set (suc (c ⊔ ℓ)) where
  infix  8 _⋆ -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_
  field
    Carrier    : Set c
    _≈_        : Rel Carrier ℓ
    _+_        : Op₂ Carrier
    _*_        : Op₂ Carrier
    -_         : Op₁ Carrier
    _⋆         : Op₁ Carrier
    0#         : Carrier
    1#         : Carrier
    isStarRing : IsStarRing _≈_ _+_ _*_ _⋆ -_ 0# 1#

  open IsStarRing isStarRing public

  ring : Ring c ℓ
  ring = record { isRing = isRing }

  semiring : Semiring c ℓ
  semiring = record { isSemiring = isSemiring }

  +-star : Star _ _
  +-star = record { isStar = +-isStar }

  *-star : Star _ _
  *-star = record { isStar = *-isStar }

record ConjugateStarRing c ℓ : Set (suc (c ⊔ ℓ)) where
  infix  8 _⋆ -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_
  field
    Carrier    : Set c
    _≈_        : Rel Carrier ℓ
    _+_        : Op₂ Carrier
    _*_        : Op₂ Carrier
    -_         : Op₁ Carrier
    _⋆         : Op₁ Carrier
    0#         : Carrier
    1#         : Carrier
    isConjugateStarRing : IsConjugateStarRing _≈_ _+_ _*_ _⋆ -_ 0# 1#

  open IsConjugateStarRing isConjugateStarRing public

  starRing : StarRing c ℓ
  starRing = record { isStarRing = isStarRing }

  ring : Ring c ℓ
  ring = record { isRing = isRing }

  semiring : Semiring c ℓ
  semiring = record { isSemiring = isSemiring }

  +-star : Star _ _
  +-star = record { isStar = +-isStar }

  *-star : Star _ _
  *-star = record { isStar = *-isStar }

record StarCommutativeRing c ℓ : Set (suc (c ⊔ ℓ)) where
  infix  8 _⋆ -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_
  field
    Carrier    : Set c
    _≈_        : Rel Carrier ℓ
    _+_        : Op₂ Carrier
    _*_        : Op₂ Carrier
    -_         : Op₁ Carrier
    _⋆         : Op₁ Carrier
    0#         : Carrier
    1#         : Carrier
    isStarCommutativeRing : IsStarCommutativeRing _≈_ _+_ _*_ _⋆ -_ 0# 1#

  open IsStarCommutativeRing isStarCommutativeRing public

  starCommutativeRing : StarCommutativeRing c ℓ
  starCommutativeRing = record { isStarCommutativeRing = isStarCommutativeRing }

  commutativeRing : CommutativeRing c ℓ
  commutativeRing = record { isCommutativeRing = isCommutativeRing }

  +-star : Star _ _
  +-star = record { isStar = +-isStar }

  *-star : Star _ _
  *-star = record { isStar = *-isStar }

record ConjugateStarCommutativeRing c ℓ : Set (suc (c ⊔ ℓ)) where
  infix  8 _⋆ -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_
  field
    Carrier    : Set c
    _≈_        : Rel Carrier ℓ
    _+_        : Op₂ Carrier
    _*_        : Op₂ Carrier
    -_         : Op₁ Carrier
    _⋆         : Op₁ Carrier
    0#         : Carrier
    1#         : Carrier
    isConjugateStarCommutativeRing :
      IsConjugateStarCommutativeRing _≈_ _+_ _*_ _⋆ -_ 0# 1#

  open IsConjugateStarCommutativeRing isConjugateStarCommutativeRing public

  starCommutativeRing : StarCommutativeRing c ℓ
  starCommutativeRing = record { isStarCommutativeRing = isStarCommutativeRing }

  commutativeRing : CommutativeRing c ℓ
  commutativeRing = record { isCommutativeRing = isCommutativeRing }

  +-star : Star _ _
  +-star = record { isStar = +-isStar }

  *-star : Star _ _
  *-star = record { isStar = *-isStar }
