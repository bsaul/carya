{-# OPTIONS --cubical-compatible --safe #-}

open import Relation.Binary using (Rel; Poset; IsTotalOrder)

module Algebra.Star.Ordered.Total.Structures
  {a ℓ₁ ℓ₂}
  {A : Set a}
  (_≈_ : Rel A ℓ₁)
  (_≤_ : Rel A ℓ₂)
  where

open import Level using (_⊔_)
open import Algebra
open import Algebra.Star.Definitions _≈_
open import Algebra.Star.Structures _≈_
open import Algebra.Ordered.Total.Structures

record IsToStarSemiring
    (+ * : Op₂ A) (⋆ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isStar : IsStar + ⋆
    ⋆-anti-homo-* : AntiHomomorphism * ⋆
    isToSemiring : IsToSemiring _≈_ _≤_ + * 0# 1#

  open IsToSemiring isToSemiring public

  isStarSemiring : IsStarSemiring + * ⋆ 0# 1#
  isStarSemiring = record
    { +-isStar = +-isStar
    ; ⋆-anti-homo-* = ⋆-anti-homo-*
    ; isSemiring = isSemiring
    }

  open IsStarSemiring isStarSemiring public
    using
     ( *-isStar
     ; 0⋆≈0
     ; 1⋆≈1
     ; ⋆-cong
     ; ⋆-involutive
     ; ⋆-anti-homo-+
     ; ⋆-homo-+
     ; ⋆-anti-homo-*
     )

record IsToStarCommutativeSemiring
    (+ * : Op₂ A) (⋆ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isStar : IsStar + ⋆
    ⋆-anti-homo-* : AntiHomomorphism * ⋆
    isToCommutativeSemiring : IsToCommutativeSemiring _≈_ _≤_ + * 0# 1#

  open IsToCommutativeSemiring isToCommutativeSemiring public

  isStarCommutativeSemiring : IsStarCommutativeSemiring + * ⋆ 0# 1#
  isStarCommutativeSemiring = record
    { +-isStar = +-isStar
    ; ⋆-anti-homo-* = ⋆-anti-homo-*
    ; isCommutativeSemiring = isCommutativeSemiring
    }

  open IsStarCommutativeSemiring isStarCommutativeSemiring public
    using (*-isStar; 0⋆≈0 ; 1⋆≈1; ⋆-homo-+ ; ⋆-homo-*)

record IsToStarRing
    (+ * : Op₂ A) (⋆ : Op₁ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isStar : IsStar + ⋆
    ⋆-anti-homo-* : AntiHomomorphism * ⋆
    isToRing : IsToRing _≈_ _≤_ + * -_ 0# 1#

  open IsToRing isToRing public

  isStarRing : IsStarRing + * ⋆ -_ 0# 1#
  isStarRing = record
    { +-isStar = +-isStar
    ; ⋆-anti-homo-* = ⋆-anti-homo-*
    ; isRing = isRing
    }

  open IsStarRing isStarRing public
    using
     ( *-isStar
     ; 0⋆≈0
     ; 1⋆≈1
     ; ⋆-cong
     ; ⋆-involutive
     ; ⋆-anti-homo-+
     ; ⋆-homo-+
     )

record IsToConjugateStarRing
    (+ * : Op₂ A) (⋆ : Op₁ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    *-conjugateSymmetric : ConjugateSymmetric * ⋆
    isToStarRing : IsToStarRing + * ⋆ -_ 0# 1#

  open IsToStarRing isToStarRing public

record IsToStarCommutativeRing
    (+ * : Op₂ A) (⋆ : Op₁ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isStar : IsStar + ⋆
    ⋆-anti-homo-* : AntiHomomorphism * ⋆
    isToCommutativeRing : IsToCommutativeRing _≈_ _≤_ + * -_ 0# 1#

  open IsToCommutativeRing isToCommutativeRing public

  isStarCommutativeRing : IsStarCommutativeRing + * ⋆ -_ 0# 1#
  isStarCommutativeRing = record
    { +-isStar = +-isStar
    ; ⋆-anti-homo-* = ⋆-anti-homo-*
    ; isCommutativeRing = isCommutativeRing
    }

  open IsStarCommutativeRing isStarCommutativeRing public
    using
     ( *-isStar
     ; 0⋆≈0
     ; 1⋆≈1
     ; ⋆-cong
     ; ⋆-involutive
     ; ⋆-anti-homo-+
     ; ⋆-homo-+
     ; ⋆-homo-*
     )

record IsToConjugateStarCommutativeRing
    (+ * : Op₂ A) (⋆ : Op₁ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    *-conjugateSymmetric : ConjugateSymmetric * ⋆
    isToStarCommutativeRing : IsToStarCommutativeRing + * ⋆ -_ 0# 1#

  open IsToStarCommutativeRing isToStarCommutativeRing public
