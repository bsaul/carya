{-# OPTIONS --cubical-compatible --safe #-}

module Algebra.Star.Ordered.Total.Bundles where

open import Level using (Level ; _⊔_; suc)
open import Algebra
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Ordered.Total.Bundles
open import Algebra.Star.Bundles
open import Algebra.Star.Ordered.Partial.Bundles
open import Algebra.Star.Ordered.Total.Structures
open import Relation.Binary using (Rel)

record ToStarSemiring (c ℓ₁ ℓ₂ : Level) : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⋆
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_ : Rel Carrier ℓ₁
    _+_ : Op₂ Carrier
    _*_ : Op₂ Carrier
    _⋆  : Op₁ Carrier
    0#  : Carrier
    1#  : Carrier
    _≤_  : Rel Carrier ℓ₂
    isToStarSemiring : IsToStarSemiring _≈_ _≤_ _+_ _*_ _⋆ 0# 1#

  open IsToStarSemiring isToStarSemiring public

  toSemiring : ToSemiring c ℓ₁ ℓ₂
  toSemiring = record { isToSemiring = isToSemiring }

  +-star : Star c ℓ₁
  +-star = record { isStar = +-isStar }

  *-star : Star c ℓ₁
  *-star = record { isStar = *-isStar }

record ToStarCommutativeSemiring (c ℓ₁ ℓ₂ : Level) : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⋆
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_ : Rel Carrier ℓ₁
    _+_ : Op₂ Carrier
    _*_ : Op₂ Carrier
    _⋆  : Op₁ Carrier
    0#  : Carrier
    1#  : Carrier
    _≤_  : Rel Carrier ℓ₂
    isToStarCommutativeSemiring : IsToStarCommutativeSemiring _≈_ _≤_ _+_ _*_ _⋆ 0# 1#

  open IsToStarCommutativeSemiring isToStarCommutativeSemiring public

  toCommutativeSemiring : ToCommutativeSemiring c ℓ₁ ℓ₂
  toCommutativeSemiring = record
    { isToCommutativeSemiring = isToCommutativeSemiring }

  open ToCommutativeSemiring toCommutativeSemiring public
    using (setoid; poset)

  toSemiring : ToSemiring c ℓ₁ ℓ₂
  toSemiring = record { isToSemiring = isToSemiring }

  poSemiring : PoSemiring c ℓ₁ ℓ₂
  poSemiring = record { isPoSemiring = isPoSemiring }

  semiring : Semiring c ℓ₁
  semiring = record { isSemiring = isSemiring }

  commutativeSemiring : CommutativeSemiring c ℓ₁
  commutativeSemiring = record
    { isCommutativeSemiring = isCommutativeSemiring }

  starCommutativeSemiring : StarCommutativeSemiring c ℓ₁
  starCommutativeSemiring = record
    { isStarCommutativeSemiring = isStarCommutativeSemiring }

  +-star : Star c ℓ₁
  +-star = record { isStar = +-isStar }

  *-star : Star c ℓ₁
  *-star = record { isStar = *-isStar }

record ToStarRing (c ℓ₁ ℓ₂ : Level) : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⋆ -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_ : Rel Carrier ℓ₁
    _+_ : Op₂ Carrier
    _*_ : Op₂ Carrier
    _⋆  : Op₁ Carrier
    -_  : Op₁ Carrier
    0#  : Carrier
    1#  : Carrier
    _≤_  : Rel Carrier ℓ₂
    isToStarRing : IsToStarRing _≈_ _≤_ _+_ _*_ _⋆ -_ 0# 1#

  open IsToStarRing isToStarRing public

  toRing : ToRing c ℓ₁ ℓ₂
  toRing = record { isToRing = isToRing }

  poRing : PoRing c ℓ₁ ℓ₂
  poRing = record { isPoRing = isPoRing }

  +-star : Star c ℓ₁
  +-star = record { isStar = +-isStar }

  *-star : Star c ℓ₁
  *-star = record { isStar = *-isStar }

record ToConjugateStarRing (c ℓ₁ ℓ₂ : Level) : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⋆ -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_ : Rel Carrier ℓ₁
    _+_ : Op₂ Carrier
    _*_ : Op₂ Carrier
    _⋆  : Op₁ Carrier
    -_  : Op₁ Carrier
    0#  : Carrier
    1#  : Carrier
    _≤_  : Rel Carrier ℓ₂
    isToConjugateStarRing : IsToConjugateStarRing _≈_ _≤_ _+_ _*_ _⋆ -_ 0# 1#

  open IsToConjugateStarRing isToConjugateStarRing public

  toRing : ToRing c ℓ₁ ℓ₂
  toRing = record { isToRing = isToRing }

  toStarRing : ToStarRing c ℓ₁ ℓ₂
  toStarRing = record { isToStarRing = isToStarRing }

  poStarRing : ToStarRing c ℓ₁ ℓ₂
  poStarRing = record { isToStarRing = isToStarRing }

  poRing : PoRing c ℓ₁ ℓ₂
  poRing = record { isPoRing = isPoRing }

  +-star : Star c ℓ₁
  +-star = record { isStar = +-isStar }

  *-star : Star c ℓ₁
  *-star = record { isStar = *-isStar }

record ToStarCommutativeRing (c ℓ₁ ℓ₂ : Level) : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⋆ -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_ : Rel Carrier ℓ₁
    _+_ : Op₂ Carrier
    _*_ : Op₂ Carrier
    _⋆  : Op₁ Carrier
    -_  : Op₁ Carrier
    0#  : Carrier
    1#  : Carrier
    _≤_  : Rel Carrier ℓ₂
    isToStarCommutativeRing : IsToStarCommutativeRing _≈_ _≤_ _+_ _*_ _⋆ -_ 0# 1#

  open IsToStarCommutativeRing isToStarCommutativeRing public

  toCommutativeRing : ToCommutativeRing c ℓ₁ ℓ₂
  toCommutativeRing = record { isToCommutativeRing = isToCommutativeRing }

  open ToCommutativeRing toCommutativeRing public
    using
     ( toRing
     ; toSemiring
     ; toCommutativeSemiring
     ; poCommutativeRing
     ; poCommutativeSemiring
     ; poSemiring
     ; semiring
     ; commutativeSemiring
     ; poset
     ; +-toAbelianGroup
     ; +-poMonoid
     ; +-commutativeSemigroup
     ; *-commutativeSemigroup
     )

  +-star : Star c ℓ₁
  +-star = record { isStar = +-isStar }

  *-star : Star c ℓ₁
  *-star = record { isStar = *-isStar }

record ToConjugateStarCommutativeRing (c ℓ₁ ℓ₂ : Level)
  : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⋆ -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_ : Rel Carrier ℓ₁
    _+_ : Op₂ Carrier
    _*_ : Op₂ Carrier
    _⋆  : Op₁ Carrier
    -_  : Op₁ Carrier
    0#  : Carrier
    1#  : Carrier
    _≤_  : Rel Carrier ℓ₂
    isToConjugateStarCommutativeRing :
      IsToConjugateStarCommutativeRing _≈_ _≤_ _+_ _*_ _⋆ -_ 0# 1#

  open IsToConjugateStarCommutativeRing isToConjugateStarCommutativeRing public

  toStarCommutativeRing : ToStarCommutativeRing c ℓ₁ ℓ₂
  toStarCommutativeRing = record
     { isToStarCommutativeRing = isToStarCommutativeRing }

  open ToStarCommutativeRing toStarCommutativeRing public
    using
     ( toCommutativeRing
     ; toRing
     ; toSemiring
     ; toCommutativeSemiring
     ; poCommutativeRing
     ; poCommutativeSemiring
     ; poSemiring
     ; semiring
     ; commutativeSemiring
     ; poset
     ; +-toAbelianGroup
     ; +-poMonoid
     ; +-commutativeSemigroup
     ; *-commutativeSemigroup
     )

  +-star : Star c ℓ₁
  +-star = record { isStar = +-isStar }

  -- open Star +-star public


  *-star : Star c ℓ₁
  *-star = record { isStar = *-isStar }
