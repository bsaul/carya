{-# OPTIONS --cubical-compatible --safe #-}

open import Relation.Binary using (Rel; Poset; IsPartialOrder)

module Algebra.Star.Ordered.Partial.Structures
  {a ℓ₁ ℓ₂}
  {A : Set a}
  (_≈_ : Rel A ℓ₁)
  (_≤_ : Rel A ℓ₂)
  where

open import Level using (_⊔_)
open import Algebra
open import Algebra.Star.Definitions _≈_
open import Algebra.Star.Structures _≈_
open import Algebra.Ordered.Partial.Structures

record IsPoStarSemiring
    (+ * : Op₂ A) (⋆ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isStar : IsStar + ⋆
    ⋆-anti-homo-* : AntiHomomorphism * ⋆
    isPoSemiring : IsPoSemiring _≈_ _≤_ + * 0# 1#

  open IsPoSemiring isPoSemiring public

  isStarSemiring : IsStarSemiring + * ⋆ 0# 1#
  isStarSemiring = record
    { +-isStar = +-isStar
    ; ⋆-anti-homo-* = ⋆-anti-homo-*
    ; isSemiring = isSemiring
    }

  open IsStarSemiring isStarSemiring public
    using (*-isStar; 0⋆≈0 ; 1⋆≈1; ⋆-homo-+)

record IsPoStarCommutativeSemiring
    (+ * : Op₂ A) (⋆ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isStar : IsStar + ⋆
    ⋆-anti-homo-* : AntiHomomorphism * ⋆
    isPoCommutativeSemiring : IsPoCommutativeSemiring _≈_ _≤_ + * 0# 1#

  open IsPoCommutativeSemiring isPoCommutativeSemiring public

  isStarCommutativeSemiring : IsStarCommutativeSemiring + * ⋆ 0# 1#
  isStarCommutativeSemiring = record
    { +-isStar = +-isStar
    ; ⋆-anti-homo-* = ⋆-anti-homo-*
    ; isCommutativeSemiring = isCommutativeSemiring
    }

  open IsStarCommutativeSemiring isStarCommutativeSemiring public
    using (*-isStar; 0⋆≈0 ; 1⋆≈1; ⋆-homo-+)

record IsPoStarRing
    (+ * : Op₂ A) (⋆ : Op₁ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isStar : IsStar + ⋆
    ⋆-anti-homo-* : AntiHomomorphism * ⋆
    isPoRing : IsPoRing _≈_ _≤_ + * -_ 0# 1#

  open IsPoRing isPoRing public

  isStarRing : IsStarRing + * ⋆ -_ 0# 1#
  isStarRing = record
    { +-isStar = +-isStar
    ; ⋆-anti-homo-* = ⋆-anti-homo-*
    ; isRing = isRing
    }

  open IsStarRing isStarRing public
    using (*-isStar; 0⋆≈0 ; 1⋆≈1; ⋆-homo-+)

record IsPoConjugateStarRing
    (+ * : Op₂ A) (⋆ : Op₁ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    *-conjugateSymmetric : ConjugateSymmetric * ⋆
    isPoStarRing : IsPoStarRing + * ⋆ -_ 0# 1#

  open IsPoStarRing isPoStarRing public

record IsPoStarCommutativeRing
    (+ * : Op₂ A) (⋆ : Op₁ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    +-isStar : IsStar + ⋆
    ⋆-anti-homo-* : AntiHomomorphism * ⋆
    isPoCommutativeRing : IsPoCommutativeRing _≈_ _≤_ + * -_ 0# 1#

  open IsPoCommutativeRing isPoCommutativeRing public

  isStarCommutativeRing : IsStarCommutativeRing + * ⋆ -_ 0# 1#
  isStarCommutativeRing = record
    { +-isStar = +-isStar
    ; ⋆-anti-homo-* = ⋆-anti-homo-*
    ; isCommutativeRing = isCommutativeRing
    }

  open IsStarCommutativeRing isStarCommutativeRing public
    using (*-isStar; 0⋆≈0 ; 1⋆≈1; ⋆-homo-+)

record IsPoConjugateStarCommutativeRing
    (+ * : Op₂ A) (⋆ : Op₁ A) (-_ : Op₁ A) (0# 1# : A) : Set (a ⊔ ℓ₁ ⊔ ℓ₂) where
  field
    *-conjugateSymmetric : ConjugateSymmetric * ⋆
    isPoStarCommutativeRing : IsPoStarCommutativeRing + * ⋆ -_ 0# 1#

  open IsPoStarCommutativeRing isPoStarCommutativeRing public
