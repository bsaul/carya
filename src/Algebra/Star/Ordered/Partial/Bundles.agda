{-# OPTIONS --cubical-compatible --safe #-}

module Algebra.Star.Ordered.Partial.Bundles where

open import Level using (Level ; _⊔_; suc)
open import Algebra
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Star.Bundles
open import Algebra.Star.Ordered.Partial.Structures
open import Relation.Binary using (Rel)

record PoStarSemiring (c ℓ₁ ℓ₂ : Level) : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⋆
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_ : Rel Carrier ℓ₁
    _+_ : Op₂ Carrier
    _*_ : Op₂ Carrier
    _⋆  : Op₁ Carrier
    0#  : Carrier
    1#  : Carrier
    _≤_  : Rel Carrier ℓ₂
    isPoStarSemiring : IsPoStarSemiring _≈_ _≤_ _+_ _*_ _⋆ 0# 1#

  open IsPoStarSemiring isPoStarSemiring public

  poSemiring : PoSemiring c ℓ₁ ℓ₂
  poSemiring = record { isPoSemiring = isPoSemiring }

  +-star : Star c ℓ₁
  +-star = record {  isStar = +-isStar }

  *-star : Star c ℓ₁
  *-star = record {  isStar = *-isStar }

record PoStarCommutativeSemiring (c ℓ₁ ℓ₂ : Level) : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⋆
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_ : Rel Carrier ℓ₁
    _+_ : Op₂ Carrier
    _*_ : Op₂ Carrier
    _⋆  : Op₁ Carrier
    0#  : Carrier
    1#  : Carrier
    _≤_  : Rel Carrier ℓ₂
    isPoStarCommutativeSemiring : IsPoStarCommutativeSemiring _≈_ _≤_ _+_ _*_ _⋆ 0# 1#

  open IsPoStarCommutativeSemiring isPoStarCommutativeSemiring public

  poCommutativeSemiring : PoCommutativeSemiring c ℓ₁ ℓ₂
  poCommutativeSemiring = record
    { isPoCommutativeSemiring = isPoCommutativeSemiring }

  open PoCommutativeSemiring poCommutativeSemiring public
    using (setoid; poset)

  poSemiring : PoSemiring c ℓ₁ ℓ₂
  poSemiring = record { isPoSemiring = isPoSemiring }

  semiring : Semiring c ℓ₁
  semiring = record { isSemiring = isSemiring }

  commutativeSemiring : CommutativeSemiring c ℓ₁
  commutativeSemiring = record
    { isCommutativeSemiring = isCommutativeSemiring }

  starCommutativeSemiring : StarCommutativeSemiring c ℓ₁
  starCommutativeSemiring = record
    { isStarCommutativeSemiring = isStarCommutativeSemiring }

  +-star : Star c ℓ₁
  +-star = record { isStar = +-isStar }

  *-star : Star c ℓ₁
  *-star = record { isStar = *-isStar }

record PoStarRing (c ℓ₁ ℓ₂ : Level) : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⋆ -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_ : Rel Carrier ℓ₁
    _+_ : Op₂ Carrier
    _*_ : Op₂ Carrier
    _⋆  : Op₁ Carrier
    -_  : Op₁ Carrier
    0#  : Carrier
    1#  : Carrier
    _≤_  : Rel Carrier ℓ₂
    isPoStarRing : IsPoStarRing _≈_ _≤_ _+_ _*_ _⋆ -_ 0# 1#

  open IsPoStarRing isPoStarRing public

  poRing : PoRing c ℓ₁ ℓ₂
  poRing = record { isPoRing = isPoRing }

  +-star : Star c ℓ₁
  +-star = record { isStar = +-isStar }

  *-star : Star c ℓ₁
  *-star = record { isStar = *-isStar }

record PoConjugateStarRing (c ℓ₁ ℓ₂ : Level) : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⋆ -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_ : Rel Carrier ℓ₁
    _+_ : Op₂ Carrier
    _*_ : Op₂ Carrier
    _⋆  : Op₁ Carrier
    -_  : Op₁ Carrier
    0#  : Carrier
    1#  : Carrier
    _≤_  : Rel Carrier ℓ₂
    isPoConjugateStarRing : IsPoConjugateStarRing _≈_ _≤_ _+_ _*_ _⋆ -_ 0# 1#

  open IsPoConjugateStarRing isPoConjugateStarRing public

  poStarRing : PoStarRing c ℓ₁ ℓ₂
  poStarRing = record { isPoStarRing = isPoStarRing }

  poRing : PoRing c ℓ₁ ℓ₂
  poRing = record { isPoRing = isPoRing }

  +-star : Star c ℓ₁
  +-star = record { isStar = +-isStar }

  *-star : Star c ℓ₁
  *-star = record { isStar = *-isStar }

record PoStarCommutativeRing (c ℓ₁ ℓ₂ : Level) : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⋆ -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_ : Rel Carrier ℓ₁
    _+_ : Op₂ Carrier
    _*_ : Op₂ Carrier
    _⋆  : Op₁ Carrier
    -_  : Op₁ Carrier
    0#  : Carrier
    1#  : Carrier
    _≤_  : Rel Carrier ℓ₂
    isPoStarCommutativeRing : IsPoStarCommutativeRing _≈_ _≤_ _+_ _*_ _⋆ -_ 0# 1#

  open IsPoStarCommutativeRing isPoStarCommutativeRing public

  poCommutativeRing : PoCommutativeRing c ℓ₁ ℓ₂
  poCommutativeRing = record { isPoCommutativeRing = isPoCommutativeRing }

  +-star : Star c ℓ₁
  +-star = record { isStar = +-isStar }

  *-star : Star c ℓ₁
  *-star = record { isStar = *-isStar }

record PoConjugateStarCommutativeRing (c ℓ₁ ℓ₂ : Level)
  : Set (suc (c ⊔ ℓ₁ ⊔ ℓ₂)) where
  infix  8 _⋆ -_
  infixl 7 _*_
  infixl 6 _+_
  infix  4 _≈_ _≤_
  field
    Carrier : Set c
    _≈_ : Rel Carrier ℓ₁
    _+_ : Op₂ Carrier
    _*_ : Op₂ Carrier
    _⋆  : Op₁ Carrier
    -_  : Op₁ Carrier
    0#  : Carrier
    1#  : Carrier
    _≤_  : Rel Carrier ℓ₂
    isPoConjugateStarCommutativeRing :
      IsPoConjugateStarCommutativeRing _≈_ _≤_ _+_ _*_ _⋆ -_ 0# 1#

  open IsPoConjugateStarCommutativeRing isPoConjugateStarCommutativeRing public

  poStarCommutativeRing : PoStarCommutativeRing c ℓ₁ ℓ₂
  poStarCommutativeRing = record
    { isPoStarCommutativeRing = isPoStarCommutativeRing }

  poCommutativeRing : PoCommutativeRing c ℓ₁ ℓ₂
  poCommutativeRing = record { isPoCommutativeRing = isPoCommutativeRing }

  +-star : Star c ℓ₁
  +-star = record { isStar = +-isStar }

  *-star : Star c ℓ₁
  *-star = record { isStar = *-isStar }
