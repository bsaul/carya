{-# OPTIONS --safe --cubical-compatible #-}

module Algebra.Star.Ordered.Partial.Construct.DirectProduct where

open import Level using (Level; _⊔_)
open import Algebra.Ordered.Partial.Construct.DirectProduct
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Star.Bundles
open import Algebra.Star.Construct.DirectProduct
open import Algebra.Star.Ordered.Partial.Bundles

open import Data.Product as × using (_,_; _×_)

private
  variable
    a b ℓa₁ ℓa₂ ℓb₁ ℓb₂ : Level

mkPoStarSemiring : PoStarSemiring a ℓa₁ ℓa₂ → PoStarSemiring b ℓb₁ ℓb₂
  → PoStarSemiring (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoStarSemiring A B = record
  { Carrier = A.Carrier × B.Carrier
  ; isPoStarSemiring = record
    { +-isStar = Star.isStar (mkStar A.+-star B.+-star)
    ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar A.*-star B.*-star)
    ; isPoSemiring = A×B.isPoSemiring }
  }
  where module A = PoStarSemiring A
        module B = PoStarSemiring B
        module A×B = PoSemiring (mkPoSemiring A.poSemiring B.poSemiring)

mkPoStarCommutativeSemiring :
    PoStarCommutativeSemiring a ℓa₁ ℓa₂
  → PoStarCommutativeSemiring b ℓb₁ ℓb₂
  → PoStarCommutativeSemiring (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoStarCommutativeSemiring A B = record
  { isPoStarCommutativeSemiring = record
    { +-isStar = Star.isStar (mkStar A.+-star B.+-star)
    ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar A.*-star B.*-star)
    ; isPoCommutativeSemiring = A×B.isPoCommutativeSemiring
    }
  }
  where module A = PoStarCommutativeSemiring A
        module B = PoStarCommutativeSemiring B
        module A×B = PoCommutativeSemiring
                      (mkPoCommutativeSemiring
                        A.poCommutativeSemiring B.poCommutativeSemiring)

mkPoStarRing : PoStarRing a ℓa₁ ℓa₂
             → PoStarRing b ℓb₁ ℓb₂
             → PoStarRing (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoStarRing A B = record
  { Carrier = A.Carrier × B.Carrier
  ; isPoStarRing = record
    { +-isStar = Star.isStar (mkStar A.+-star B.+-star)
    ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar A.*-star B.*-star)
    ; isPoRing = A×B.isPoRing }
  }
  where module A = PoStarRing A
        module B = PoStarRing B
        module A×B = PoRing (mkPoRing A.poRing B.poRing)

mkPoConjugateStarRing : PoConjugateStarRing a ℓa₁ ℓa₂
                      → PoConjugateStarRing b ℓb₁ ℓb₂
                      → PoConjugateStarRing (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoConjugateStarRing A B = record
  { Carrier = A.Carrier × B.Carrier
  ; isPoConjugateStarRing = record
     { *-conjugateSymmetric = A.*-conjugateSymmetric , B.*-conjugateSymmetric
     ; isPoStarRing = A×B.isPoStarRing
     }
  }
  where module A = PoConjugateStarRing A
        module B = PoConjugateStarRing B
        module A×B = PoStarRing (mkPoStarRing A.poStarRing B.poStarRing)

mkPoStarCommutativeRing : PoStarCommutativeRing a ℓa₁ ℓa₂
                        → PoStarCommutativeRing b ℓb₁ ℓb₂
                        → PoStarCommutativeRing (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoStarCommutativeRing A B = record
  { Carrier = A.Carrier × B.Carrier
  ; isPoStarCommutativeRing = record
    { +-isStar = Star.isStar (mkStar A.+-star B.+-star)
    ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar A.*-star B.*-star)
    ; isPoCommutativeRing = A×B.isPoCommutativeRing }
  }
  where module A = PoStarCommutativeRing A
        module B = PoStarCommutativeRing B
        module A×B = PoCommutativeRing
                       (mkPoCommutativeRing
                          A.poCommutativeRing B.poCommutativeRing)

mkPoConjugateStarCommutativeRing :
    PoConjugateStarCommutativeRing a ℓa₁ ℓa₂ →
    PoConjugateStarCommutativeRing b ℓb₁ ℓb₂ →
    PoConjugateStarCommutativeRing (a ⊔ b) (ℓa₁ ⊔ ℓb₁) (ℓa₂ ⊔ ℓb₂)
mkPoConjugateStarCommutativeRing A B = record
  { Carrier = A.Carrier × B.Carrier
  ; isPoConjugateStarCommutativeRing = record
     { *-conjugateSymmetric = A.*-conjugateSymmetric , B.*-conjugateSymmetric
     ; isPoStarCommutativeRing = A×B.isPoStarCommutativeRing
     }
  }
  where module A = PoConjugateStarCommutativeRing A
        module B = PoConjugateStarCommutativeRing B
        module A×B = PoStarCommutativeRing
                       (mkPoStarCommutativeRing
                          A.poStarCommutativeRing B.poStarCommutativeRing)
