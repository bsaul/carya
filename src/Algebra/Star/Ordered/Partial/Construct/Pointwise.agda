{-# OPTIONS --cubical-compatible --safe #-}

open import Level using (Level ; _⊔_)

module Algebra.Star.Ordered.Partial.Construct.Pointwise
   {a : Level}
   (A : Set a)
   where

open import Algebra.Ordered.Partial.Bundles
open import Algebra.Ordered.Partial.Construct.Pointwise A
open import Algebra.Star.Bundles
open import Algebra.Star.Ordered.Partial.Bundles
open import Algebra.Star.Construct.Pointwise

private
  variable
    c ℓ₁ ℓ₂ : Level

mkPoStarSemiring :
     PoStarSemiring c ℓ₁ ℓ₂
   → PoStarSemiring (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoStarSemiring S = record
   { Carrier = A → S.Carrier
   ;  isPoStarSemiring = record
         { +-isStar = Star.isStar (mkStar A S.+-star)
         ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar A S.*-star)
         ; isPoSemiring = SS.isPoSemiring
         }
   }
   where module S = PoStarSemiring S
         module SS = PoSemiring (mkPoSemiring S.poSemiring)

mkPoStarCommutativeSemiring :
     PoStarCommutativeSemiring c ℓ₁ ℓ₂
   → PoStarCommutativeSemiring  (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoStarCommutativeSemiring S = record
   { Carrier = A → S.Carrier
   ; isPoStarCommutativeSemiring = record
      { +-isStar = Star.isStar (mkStar A S.+-star)
      ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar A S.*-star)
      ; isPoCommutativeSemiring = SS.isPoCommutativeSemiring
      }
   }
   where module S = PoStarCommutativeSemiring S
         module SS = PoCommutativeSemiring
            (mkPoCommutativeSemiring S.poCommutativeSemiring)

mkPoStarRing :
     PoStarRing c ℓ₁ ℓ₂
   → PoStarRing (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoStarRing S = record
   { Carrier = A → S.Carrier
   ;  isPoStarRing = record
         { +-isStar = Star.isStar (mkStar A S.+-star)
         ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar A S.*-star)
         ; isPoRing = SS.isPoRing
         }
   }
   where module S = PoStarRing S
         module SS = PoRing (mkPoRing S.poRing)

mkPoConjugateStarRing :
     PoConjugateStarRing c ℓ₁ ℓ₂
   → PoConjugateStarRing (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoConjugateStarRing S = record
   { Carrier = A → S.Carrier
   ;  isPoConjugateStarRing =  record
      { *-conjugateSymmetric = λ {f} {g} {a} → S.*-conjugateSymmetric {f a} {g a}
      ; isPoStarRing = SS.isPoStarRing
      }
   }
   where module S = PoConjugateStarRing S
         module SS = PoStarRing (mkPoStarRing S.poStarRing)

mkPoStarCommutativeRing :
     PoStarCommutativeRing c ℓ₁ ℓ₂
   → PoStarCommutativeRing (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoStarCommutativeRing S = record
   { Carrier = A → S.Carrier
   ;  isPoStarCommutativeRing = record
         { +-isStar = Star.isStar (mkStar A S.+-star)
         ; ⋆-anti-homo-* = Star.⋆-anti-homo-∙ (mkStar A S.*-star)
         ; isPoCommutativeRing = SS.isPoCommutativeRing
         }
   }
   where module S = PoStarCommutativeRing S
         module SS = PoCommutativeRing (mkPoCommutativeRing S.poCommutativeRing)

mkPoConjugateStarCommutativeRing :
     PoConjugateStarCommutativeRing c ℓ₁ ℓ₂
   → PoConjugateStarCommutativeRing (a ⊔ c) (a ⊔ ℓ₁) (a ⊔ ℓ₂)
mkPoConjugateStarCommutativeRing S = record
   { Carrier = A → S.Carrier
   ;  isPoConjugateStarCommutativeRing =  record
      { *-conjugateSymmetric = λ {f} {g} {a} → S.*-conjugateSymmetric {f a} {g a}
      ; isPoStarCommutativeRing = SS.isPoStarCommutativeRing
      }
   }
   where module S = PoConjugateStarCommutativeRing S
         module SS = PoStarCommutativeRing
                           (mkPoStarCommutativeRing S.poStarCommutativeRing)
