---
title: Folding over Vectors valued in a Monoid
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (Monoid)

module Fold.Core {c ℓ} ⦃ M : Monoid c ℓ ⦄ where

open import Data.Fin using (Fin) ; open Fin
open import Data.Nat using (ℕ)
open import Data.Vec.Functional using (foldr)

open Monoid M renaming (Carrier to C)

private
  variable
    n : ℕ
```

</details>

```agda
fold : (Fin n → C) → C
fold = foldr _∙_ ε
```

```agda
⊙ = fold
```
