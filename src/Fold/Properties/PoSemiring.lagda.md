---
title: Fold over `Semiring`s
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Ordered.Partial.Bundles using (PoSemiring)

module Fold.Properties.PoSemiring
  {c ℓ₁ ℓ₂} ⦃ S : PoSemiring c ℓ₁ ℓ₂ ⦄
  where

open import Data.Fin using (Fin ; suc) ; open Fin
open import Data.Nat using (ℕ)
open import Function using (_∘_)

open PoSemiring S renaming (Carrier to C)
  hiding (zero)

private
  variable
    n : ℕ
```

</details>

```agda
open import Fold.Properties.Semiring ⦃ semiring ⦄ public
```

### Monotonicity

Order is preserved by `∑`.

```agda
∑-preserves-≤ :
  ∀ {f g : Fin n → C}
  → (∀ {x} → f x ≤ g x)
  → ∑ f ≤ ∑ g
∑-preserves-≤ {n = ℕ.zero} _ = refl
∑-preserves-≤ {n = ℕ.suc n} fx≤gy = +-mono₂ fx≤gy (∑-preserves-≤ {n = n} fx≤gy)
```
