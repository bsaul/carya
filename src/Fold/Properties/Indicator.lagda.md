---
title: Fold over the index indicator function on a `CommutativeSemiring`
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --without-K #-}

open import Algebra.Bundles using (CommutativeSemiring)

module Fold.Properties.Indicator {c ℓ} ⦃ S : CommutativeSemiring c ℓ ⦄ where

-- stdlib
open import Data.Bool using (if_then_else_)
open CommutativeSemiring S renaming (Carrier to C) hiding (zero)

-- stdlib
open import Data.Fin using (Fin; _≟_; splitAt ; _↑ˡ_ ; _↑ʳ_) ; open Fin
open import Data.Fin.Properties using (≡-decSetoid ; ¬Fin0)
open import Data.Nat as ℕ using (ℕ)
open import Data.Sum as ⊎ using ([_,_]′; inj₁; inj₂)
open import Function using (_∘_; const)
open import Relation.Nullary.Decidable.Core
open import Relation.Binary.PropositionalEquality as ≡ using (_≡_)
open import Relation.Nullary.Negation using (contradiction)

import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

-- carya
open import Fold.Properties.CommutativeSemiring ⦃ S ⦄

private
  variable
    n m : ℕ
```

</details>

## Index indicator function

```agda
eqIndex? : (i i' : Fin n) → Dec (i ≡ i')
eqIndex? i i' = i ≟ i'

𝕀 : Fin n → Fin n → C
𝕀 i j = if does (eqIndex? i j) then 1# else 0#
```

### Properties of the indicator function

The arguments to the indicator function can be swapped.

```agda
𝕀-sym : ∀ (i j : Fin n) → 𝕀 i j ≈ 𝕀 j i
𝕀-sym zero zero = refl
𝕀-sym zero (suc _) = refl
𝕀-sym (suc _) zero = refl
𝕀-sym (suc i) (suc j) = 𝕀-sym i j
```

If multiplying a vector by the indicator function,
the argument to the vector can be swapped.

```agda
𝕀-*-swap : ∀ {f : Fin n → C} → (i i' : Fin n) → 𝕀 i i' * f i' ≈ 𝕀 i i' * f i
𝕀-*-swap {f = f} zero zero = refl
𝕀-*-swap {f = f} zero (suc j) = trans (zeroˡ _) (sym (zeroˡ _))
𝕀-*-swap {f = f} (suc j) zero = trans (zeroˡ _) (sym (zeroˡ _))
𝕀-*-swap {f = f} (suc i) (suc j) = 𝕀-*-swap i j
```

Folding of over the indicator function for a non empty index set
is equal to `1#`.
That is, `𝕀 i i'` is true exactly once.

```agda
∑𝕀≈1# : ∀ (i : Fin (ℕ.suc n)) → ∑ 𝕀 i ≈ 1#
∑𝕀≈1# {n} zero = trans (+-congˡ (∑-preserves-0 {n})) (+-identityʳ 1#)
∑𝕀≈1# {ℕ.suc n} (suc i) = begin
  ∑ 𝕀 (suc i) ≡⟨⟩
  0# + ∑ 𝕀 i  ≈⟨ +-identityˡ _ ⟩
  ∑ 𝕀 i       ≈⟨ ∑𝕀≈1# {n = n} i ⟩
  1#          ∎
  where open ≈-Reasoning setoid
```

An immmediate consquence of `∑𝕀≈1#`:

```agda
∑𝕀*f≈fi : ∀ (i : Fin n) → (f : Fin n → C)
  → ∑ (λ x → 𝕀 i x * (f x)) ≈ f i
∑𝕀*f≈fi {n = ℕ.zero} i _ = contradiction i ¬Fin0
∑𝕀*f≈fi {n = ℕ.suc n} i f = begin
  ∑ (λ x → 𝕀 i x * f x) ≈⟨ ∑-cong {n = ℕ.suc n} (𝕀-*-swap {f = f} i) ⟩
  ∑ (λ x → 𝕀 i x * f i) ≈⟨ ∑-cong {n = ℕ.suc n} (λ x → *-comm (𝕀 i x) (f i)) ⟩
  ∑ (λ x → f i * 𝕀 i x) ≈⟨ c*∑≈∑c* (𝕀 i) (f i) ⟨
  f i * (∑ (𝕀 i))       ≈⟨ *-congˡ (∑𝕀≈1# i) ⟩
  f i * 1#              ≈⟨ *-identityʳ _ ⟩
  f i                   ∎
  where open ≈-Reasoning setoid
```
