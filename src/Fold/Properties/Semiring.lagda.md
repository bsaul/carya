---
title: Fold over `Semiring`s
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (Semiring)

module Fold.Properties.Semiring {c ℓ} ⦃ S : Semiring c ℓ ⦄ where

open Semiring S renaming (Carrier to C) hiding (zero)

open import Algebra.Definitions.RawMonoid +-rawMonoid
open import Data.Fin using (Fin;  _↑ˡ_; _↑ʳ_; splitAt); open Fin
open import Data.Sum using ([_,_]′; inj₁ ; inj₂)
open import Data.Sum.Properties using ([,]-map)
open import Data.Nat as ℕ using (ℕ)
open import Data.Vec.Functional using (_++_; tail)
open import Function using (_∘_; const)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open import Fold.Core
import Fold.Properties.Monoid as MonoidProperties
import Fold.Properties.CommutativeMonoid as CommutativeMonoidProperties

private
  variable
    m n : ℕ
```

</details>

## Syntax for sum and product

```agda
∑_ : (Fin n → C) → C
∑_ = fold ⦃ +-monoid ⦄

∑[_]_ : ∀ n → (Fin n → C) → C
∑[ n ] f = ∑_ {n = n} f

-- double sum
∑∑ : (Fin m → C) → (Fin n → C) → C
∑∑ f g = ∑ λ i → ∑ λ j → f i * g j

∏_ : (Fin n → C) → C
∏_ = fold ⦃ *-monoid ⦄
```

## Properties of `∑`

```agda
open MonoidProperties ⦃ M = +-monoid ⦄ public
  renaming
  ( ⊙-cong         to ∑-cong
  ; ⊙-identityʳ    to ∑-identityʳ
  ; ⊙-identityˡ    to ∑-identityˡ
  ; ⊙-preserves-ε  to ∑-preserves-0
  ; ⊙-preserves-++ to ∑-preserves-++
  ; padʳ-preserves-⊙ to padʳ-preserves-∑
  ; padˡ-preserves-⊙ to padˡ-preserves-∑
  )
  using (
      padˡ
    ; padʳ
  )

open CommutativeMonoidProperties ⦃ M = +-commutativeMonoid ⦄ public
  renaming
  ( ⊙-preserves-∙  to ∑-preserves-+
  ; ⊙-comm         to ∑-comm
  )
  using ()
```

### Corrollary of `∑-preserves-0`

```agda
∑-preserves-0*f : ∀ {n} → (f : Fin n → C) →  (∑ λ x → 0# * f x) ≈ 0#
∑-preserves-0*f {n} f = begin
  (∑ (λ x → 0# * f x))  ≈⟨ ∑-cong (λ i → zeroˡ (f i) ) ⟩
  (∑_ {n} (λ x → 0# ))  ≈⟨ ∑-preserves-0 {n} ⟩
  0#                    ∎
  where open ≈-Reasoning setoid
```

### Distributivity

```agda
c*∑≈∑c* : ∀ (f : Fin n → C) (c : C) → c * ∑ f ≈ ∑ ((c *_) ∘ f)
c*∑≈∑c* {n = ℕ.zero} _ _  = zeroʳ _
c*∑≈∑c* {n = ℕ.suc n}  f c = begin
  c * ∑ f                             ≡⟨⟩
  c * (f zero + ∑ (f ∘ suc))          ≈⟨ distribˡ c _ _ ⟩
  c * f zero + c * ∑ (f ∘ suc)        ≈⟨ +-congˡ (c*∑≈∑c* {n = n} _ _) ⟩
  c * f zero + ∑ ((c *_) ∘ (f ∘ suc)) ≡⟨⟩
  ∑ ((c *_) ∘ f)                      ∎
  where open ≈-Reasoning setoid
```

### Sum of constant `1#` is `n` "*" `1#`

```agda
∑1≈n : ∀ {n} → (f : Fin n → C) → ∑[ n ] (const 1#) ≈ (n × 1#)
∑1≈n {ℕ.zero} _  = refl
∑1≈n {ℕ.suc n} f = +-congˡ (∑1≈n {n = n} (f ∘ suc))
```

## Properties of `∏`

```agda
open MonoidProperties ⦃ M = *-monoid ⦄ public
  renaming
  ( ⊙-cong         to ∏-cong
  ; ⊙-identityʳ    to ∏-identityʳ
  ; ⊙-identityˡ    to ∏-identityˡ
  ; ⊙-preserves-ε  to ∏-preserves-1
  ; ⊙-preserves-++ to ∏-preserves-++
  )
  using ()
```

## Additional lemmas

```agda
padʳ-preserves-∑* : ∀ (o p : ℕ) (x : Fin m) (f : Fin m → Fin o → C)
    → (g : (Fin (o ℕ.+ p) → C))
    → ∑[ (o ℕ.+ p) ] (λ k → padʳ p (f x) k * g k)
    ≈ ∑[ o ] (λ k → f x k * g (k ↑ˡ p))
padʳ-preserves-∑* 0 p _ _ g = begin
  ∑[ p ] (λ k → 0# * g k) ≈⟨ ∑-cong {p} (λ i → zeroˡ (g i)) ⟩
  ∑[ p ] (λ _ → 0#)       ≈⟨ ∑-preserves-0 {p} ⟩
  0#   ∎
  where open ≈-Reasoning setoid
padʳ-preserves-∑*  (ℕ.suc o) p x f g = +-congˡ (begin
  ∑[ o ℕ.+ p ] (λ k → [ f x , (λ _ → 0#) ]′ ([ inj₁ ∘ suc , inj₂ ]′ (splitAt o k)) * g (suc k))
        ≈⟨ ∑-cong {o ℕ.+ p} (λ i → *-congʳ (reflexive ([,]-map (splitAt o i)))) ⟩
  ∑[ o ℕ.+ p ] (λ k → padʳ p (f x ∘ suc ) k * g (suc k)) ≈⟨ padʳ-preserves-∑* o p x (λ i → f i ∘ suc) (g ∘ suc) ⟩
  ∑[ o ] (λ k → f x (suc k) * g (suc (k ↑ˡ p))) ∎)
  where open ≈-Reasoning setoid
```

```agda
padˡ-preserves-∑* : ∀ (o p : ℕ) (x : Fin m) (f : Fin m → Fin p → C)
    → (g : (Fin (o ℕ.+ p) → C))
    → ∑[ (o ℕ.+ p) ] (λ k → padˡ o (f x) k * g k)
    ≈ ∑[ p ] (λ k → f x k * g (o ↑ʳ k))
padˡ-preserves-∑* 0 p _ _ g = refl
padˡ-preserves-∑* (ℕ.suc o) p x f g = begin
  ∑[ ((ℕ.suc o) ℕ.+ p) ] (λ k → padˡ (ℕ.suc o) (f x) k * g k) ≡⟨⟩
  0# * g zero + ∑[ (o ℕ.+ p) ] (λ k → [ (λ _ → 0#) , f x ]′ ([ inj₁ ∘ suc , inj₂ ]′ (splitAt o k)) * g (suc k)) ≈⟨ trans (+-congʳ (zeroˡ (g zero))) (+-identityˡ _) ⟩
  ∑[ (o ℕ.+ p) ] (λ k → [ (λ _ → 0#) , f x ]′ ([ inj₁ ∘ suc , inj₂ ]′ (splitAt o k)) * g (suc k))
    ≈⟨ ∑-cong {o ℕ.+ p}  (λ i → *-congʳ (reflexive ([,]-map (splitAt o i)))) ⟩
  ∑[ (o ℕ.+ p) ] (λ k → [ (λ _ → 0#) , f x ]′ (splitAt o k) * g (suc k))
    ≈⟨ padˡ-preserves-∑* o p x f (g ∘ suc) ⟩
  ∑[ p ] (λ k → f x k * g ((ℕ.suc o) ↑ʳ k)) ∎
  where open ≈-Reasoning setoid
```
