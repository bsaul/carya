---
title: Fold over `CommutativeSemiring`s
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Algebra.Bundles using (CommutativeSemiring)

module Fold.Properties.CommutativeSemiring {c ℓ} ⦃ S : CommutativeSemiring c ℓ ⦄ where

open CommutativeSemiring S renaming (Carrier to C) hiding (zero)

import Algebra.Properties.CommutativeSemigroup as CS
open import Data.Fin using (Fin) ; open Fin
open import Data.Nat using (ℕ)
open import Data.Vec.Functional
open import Function using (_∘_)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

private
  variable
    m n : ℕ
```

</details>

## Inherit Semiring properties

```agda
open import Fold.Properties.Semiring ⦃ semiring ⦄ public
```

## Properties of `∑∑`

### Interchange of indices

```agda
∑∑-swap : {f : Fin m → C} {g : Fin n → C}
    → ∑ (λ i → ∑ λ j → f i * g j) ≈ ∑ (λ j → ∑ λ i → f i * g j)
∑∑-swap {ℕ.zero} {ℕ.zero} = refl
∑∑-swap {ℕ.zero} {ℕ.suc n} =
  trans (sym (+-identityˡ 0#)) (+-congˡ (sym (∑-preserves-0 {n = n})) )
∑∑-swap {ℕ.suc m} {ℕ.zero} = trans (+-identityˡ _) (∑-preserves-0 {n = m})
∑∑-swap {ℕ.suc m} {ℕ.suc n} {f} {g} =
  begin
    ∑ (λ i → ∑ λ j → f i * g j)
  ≡⟨⟩
      f0 * g0 + (∑ λ j → f0 * g (suc j))
    + ∑ (λ i → f (suc i) * g0 + ∑ (λ j → f (suc i) * g (suc j)))
  ≈⟨ +-congˡ (∑∑-swap {f = f ∘ suc} {g = g})  ⟩
      f0 * g0 + ∑ (λ j → f0 * g (suc j))
    + (∑ (λ j → ∑ (λ i → f (suc i) * g j)))
  ≈⟨ +-congˡ
        (+-cong
            (∑-cong (λ i → *-comm (f (suc i)) (g0)))
            (∑-cong λ j → ∑-cong λ i → *-comm (f (suc i)) (g (suc j))))  ⟩
      f0 * g0 + ∑_ {n} (λ j → f0 * g (suc j))
    + (∑ (λ i → g0 * f (suc i) ) + ∑ (λ j → ∑ (λ i → g (suc j) * f (suc i))))
  ≈⟨ +-congˡ (+-congˡ ((∑∑-swap {f = g ∘ suc} {g = f ∘ suc})))  ⟩
      f0 * g0 + ∑ (λ j → f0 * g (suc j))
    + (∑ (λ i → g0 * f (suc i)) + ∑ (λ i → ∑ (λ j →  g (suc j) * f (suc i))))
  ≈⟨ +-congˡ
        (+-cong
            (∑-cong (λ i → *-comm _ (f (suc i))))
            (∑-cong (λ i → ∑-cong λ j → *-comm (g (suc j)) (f (suc i)))))
        ⟩
      f0 * g0 + ∑ (λ j → f0 * g (suc j))
    + (∑ (λ i → f (suc i) * g0) + ∑ (λ i → ∑ (λ j → f (suc i) * g (suc j))))
  ≈⟨ interchange _ _ _ _ ⟩
      f0 * g0 + ∑ (λ i → f (suc i) * g0)
    + (∑ (λ j → f0 * g (suc j)) + ∑ (λ i → ∑ (λ j → f (suc i) * g (suc j))))
  ≈⟨ +-congˡ (∑∑-swap {f = f} {g = g ∘ suc})  ⟩
      f0 * g0 + ∑ (λ i → f (suc i) * g0)
    + ∑ (λ j → f0 * g (suc j) + ∑ λ i → f (suc i) * g (suc j))
  ≡⟨⟩
    ∑ (λ j → ∑ λ i → f i * g j)
  ∎
  where open ≈-Reasoning setoid
        open CS +-commutativeSemigroup
        f0 = f zero
        g0 = g zero
```
