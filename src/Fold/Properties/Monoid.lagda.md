---
title: Properties of `fold` over `Monoid`
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level)
open import Algebra.Bundles using (Monoid)

module Fold.Properties.Monoid {c ℓ} ⦃ M : Monoid c ℓ ⦄ where

open Monoid M renaming (Carrier to C)

-- stdlib
open import Data.Fin using (Fin ; splitAt); open Fin
open import Data.Fin.Properties using (+↔⊎)
open import Data.Nat as ℕ using (ℕ; _+_)
open import Data.Sum using (_⊎_ ; [_,_];  [_,_]′)
open import Data.Sum.Properties using ([,]-map)
open import Data.Vec.Functional using (_++_)
open import Data.Vec.Functional.Relation.Binary.Equality.Setoid setoid
    using (tail⁺ ; head⁺ ; _≋_)
open import Function using (_∘_; Inverse; const)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open import Fold.Core

private
  variable
    n n₁ n₂ : ℕ
```

</details>

## Congruence

`⊙` is congruent with respect to vectors.
That is, one can swap out equivalent vectors.

```agda
⊙-cong : {f g : Fin n → C}
  → (f ≋ g)
  → ⊙ f ≈ ⊙ g
⊙-cong {n = ℕ.zero} _ = refl
⊙-cong {n = ℕ.suc n} {f} {g} f≋g = begin
  ⊙ f                    ≡⟨⟩
  f zero ∙ ⊙ (f ∘ suc)   ≈⟨ ∙-cong (head⁺ _≈_ f≋g) (⊙-cong {f = (f ∘ suc)} {(g ∘ suc)} (tail⁺ _≈_ f≋g)) ⟩
  g zero ∙ (⊙ (g ∘ suc)) ≡⟨⟩
  ⊙ g                    ∎
  where open ≈-Reasoning setoid
```

### Identities

`⊙` preserves the identity of the underlying monoid.

```agda
⊙-identityʳ : (f : Fin n → C) → (⊙ f) ∙ ε ≈ ⊙ f
⊙-identityʳ = identityʳ ∘ ⊙

⊙-identityˡ : (f : Fin n → C) → ε ∙ (⊙ f) ≈ ⊙ f
⊙-identityˡ = identityˡ ∘ ⊙

⊙-preserves-ε : ∀ {n} → (⊙ {n = n} (const ε)) ≈ ε
⊙-preserves-ε {n = ℕ.zero}  = refl
⊙-preserves-ε {n = ℕ.suc n} = begin
  ⊙ {n = ℕ.suc n} (const ε) ≈⟨ ∙-congˡ (⊙-preserves-ε {n = n}) ⟩
  ε ∙ ε                     ≈⟨ identityˡ ε ⟩
  ε                         ∎
  where open ≈-Reasoning setoid
```

##  `⊙` is homomorphic w.r.t. `++`

Folding separately over two vectors
is equivalent to
combining the vectors and then folding.

```agda
⊙-preserves-++ : (f : Fin n₁ → C) (g : Fin n₂ → C)
    → ⊙ f ∙ ⊙ g ≈ ⊙ (f ++ g)
⊙-preserves-++ {n₁ = 0} {n₂ = 0} f g    = identityˡ ε
⊙-preserves-++ {n₁ = 0} _ g             = ⊙-identityˡ g
⊙-preserves-++ {n₁ = ℕ.suc n₁} {n₂} f g = begin
  ⊙ f ∙ ⊙ g                    ≡⟨⟩
  f zero ∙ ⊙ (f ∘ suc) ∙ ⊙ g   ≈⟨ assoc _ _ _ ⟩
  f zero ∙ (⊙ (f ∘ suc) ∙ ⊙ g) ≈⟨ ∙-congˡ (⊙-preserves-++ (f ∘ suc) g) ⟩
  f zero ∙ ⊙ (f ∘ suc ++ g)    ≈⟨ ∙-congˡ (⊙-cong (sym ∘ reflexive ∘ [,]-map ∘ Inverse.to (+↔⊎ {m = n₁})))  ⟩
  ⊙ (f ++ g) ∎
  where open ≈-Reasoning setoid
```

## `pre/appending` identity elements to vectors

```agda
εʳ : (Fin n₁ → C) → (Fin n₁ ⊎ Fin n₂) → C
εʳ = [_, (λ _ → ε) ]

εˡ : (Fin n₂ → C) → (Fin n₁ ⊎ Fin n₂) → C
εˡ = [ (λ _ → ε) ,_]

padʳ : ∀ n₂ → (Fin n₁ → C) → Fin (n₁ + n₂) → C
padʳ _ f = εʳ f ∘ (splitAt _)

padˡ : ∀ n₁ → (Fin n₂ → C) → Fin (n₁ + n₂) → C
padˡ _ f = εˡ f ∘ (splitAt _)

padʳ-preserves-⊙ : ∀ n₂ (f : Fin n₁ → C) → ⊙ f ≈ ⊙ (padʳ n₂ f)
padʳ-preserves-⊙ n₂ f = begin
  ⊙ f                        ≈⟨ identityʳ  _ ⟨
  ⊙ f ∙ ε                    ≈⟨ ∙-congˡ (sym (⊙-preserves-ε {n = n₂})) ⟩
  ⊙ f ∙ ⊙ {n = n₂} (λ _ → ε) ≈⟨ ⊙-preserves-++ f (λ _ → ε) ⟩
  ⊙ (padʳ n₂ f)              ∎
  where open ≈-Reasoning setoid

padˡ-preserves-⊙ : ∀ n₁ (f : Fin n₂ → C) → ⊙ f ≈ ⊙ (padˡ n₁ f)
padˡ-preserves-⊙ n₁ f = begin
  ⊙ f                        ≈⟨ identityˡ _ ⟨
  ε ∙ ⊙ f                    ≈⟨ ∙-congʳ (⊙-preserves-ε {n = n₁}) ⟨
  ⊙ {n = n₁} (λ _ → ε) ∙ ⊙ f ≈⟨ ⊙-preserves-++ {n₁ = n₁} (λ _ → ε) f  ⟩
  ⊙ (padˡ n₁ f)              ∎
  where open ≈-Reasoning setoid
```
