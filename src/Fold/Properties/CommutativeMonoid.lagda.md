---
title: Properties of `fold` over `CommutativeMonoid`
---

<details>
<summary>Options</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level)
open import Algebra.Bundles using (CommutativeMonoid)
```
</details>

```agda
module Fold.Properties.CommutativeMonoid {c ℓ} ⦃ M : CommutativeMonoid c ℓ ⦄ where

open CommutativeMonoid M renaming (Carrier to C)
```

<details>
<summary>Imports</summary>

```agda
-- stdlib
open import Algebra.Properties.CommutativeSemigroup commutativeSemigroup
open import Data.Fin using (Fin); open Fin
open import Data.Nat as ℕ using (ℕ)
open import Data.Vec.Functional.Relation.Binary.Equality.Setoid setoid
    using (tail⁺ ; head⁺ ; _≋_)
open import Function using (_∘_; const)
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open import Fold.Core ⦃ monoid ⦄

private
  variable
    n n₁ n₂ : ℕ
```

</details>

## Inherit Monoid properties

```agda
open import Fold.Properties.Monoid ⦃ monoid ⦄ public
```

##  `⊙` is homomorphic w.r.t. `∙`

Folding separately over two vectors of the same length and then combining
is equivalent to
folding simultaneously the vectors pointwise.

```agda
⊙-preserves-∙ : (f g : Fin n → C)
    → ⊙ f ∙ ⊙ g ≈ ⊙ λ i → f i ∙ g i
⊙-preserves-∙ {n = ℕ.zero} _ _ = identityˡ ε
⊙-preserves-∙ {n = ℕ.suc n} f g = begin
  (⊙ f ∙ ⊙ g)                                   ≡⟨⟩
  f zero ∙ ⊙ (f ∘ suc) ∙ (g zero ∙ ⊙ (g ∘ suc)) ≈⟨ interchange _ _ _ _ ⟩
  f zero ∙ g zero ∙ (⊙ (f ∘ suc) ∙ ⊙ (g ∘ suc)) ≈⟨ ∙-congˡ (⊙-preserves-∙ (f ∘ suc) _) ⟩
  (⊙ λ i → f i ∙ g i)                           ∎
  where open ≈-Reasoning setoid
```

## `⊙` Commutative

`⊙` is commutative with respect to functions `f` and `g`
that map *to* the underlying commutative monoid.

```agda
⊙-comm : (f : Fin n₁ → C) (g : Fin n₂ → C) → (⊙ f ∙ ⊙ g) ≈ (⊙ g ∙ ⊙ f)
⊙-comm f g = comm (⊙ f) (⊙ g)
```
