---
title: Folding over Vectors valued in a Setoid
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Relation.Binary using (Setoid)
open import Algebra.Bundles using (Monoid)

module Fold.Setoid.Core
  {a ℓᵃ} (𝓐 : Setoid a ℓᵃ)
  {c ℓ} ⦃ M : Monoid c ℓ ⦄
  where

open Monoid M renaming (Carrier to C)
open Setoid 𝓐 renaming (Carrier to A)

open import Data.Fin using (Fin)
open import Data.Nat using (ℕ)
open import Function using (_∘_)

open import Fold.Core as Core using ()

private
  variable
    n : ℕ
```

</details>

```agda
fold : (Fin n → A) → (A → C) → C
fold #A g = Core.fold (g ∘ #A)
```

```agda
⊙ = fold
```
