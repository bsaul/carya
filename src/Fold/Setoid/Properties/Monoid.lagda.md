---
title: Properties of `fold` over `Monoid`
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level)
open import Relation.Binary using (Setoid)
open import Algebra.Bundles using (Monoid)

module Fold.Setoid.Properties.Monoid
  {a ℓᵃ} (𝓐 : Setoid a ℓᵃ)
  {c ℓ} ⦃ M : Monoid c ℓ ⦄
  where

open Setoid 𝓐 renaming
  ( Carrier to A
  ; _≈_ to _≈ᴬ_
  ; refl to reflᴬ
  ; sym to symᴬ
  ; reflexive to reflexiveᴬ
  ) using ()
open Monoid M renaming (Carrier to C)

-- stdlib
open import Data.Fin using (Fin; splitAt); open Fin
open import Data.Fin.Properties using (+↔⊎)
open import Data.Nat as ℕ using (ℕ)
open import Data.Sum using (_⊎_ ; [_,_] ; map; inj₁ ; inj₂)
open import Data.Sum.Properties using ([,]-map; [,]-∘)
open import Data.Vec.Functional using (_++_)
open import Data.Vec.Functional.Relation.Binary.Equality.Setoid 𝓐
    using (tail⁺ ; head⁺ ; _≋_)
open import Function using (_∘_; const; Congruent; id; Inverse)

import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open import Fold.Core ⦃ M ⦄ as Core using ()
open import Fold.Setoid.Core 𝓐 ⦃ M ⦄
open import Fold.Properties.Monoid ⦃ M ⦄ as Prop using ()

private
  variable
    n n₁ n₂ : ℕ
```

</details>

### Congruence(s)

`⊙` is congruent with respect to vectors.
That is, one can swap out equivalent vectors.

```agda
⊙-cong# : {f : A → C} {f-cong : Congruent _≈ᴬ_ _≈_ f}
  → (#₁ #₂ : Fin n → A)
  → (#₁ ≋ #₂)
  → ⊙ #₁ f ≈ ⊙ #₂ f
⊙-cong# {n = ℕ.zero} _ _ _ = refl
⊙-cong# {n = ℕ.suc n} {f = f} {f-cong} #₁ #₂ #₁≋#₂ = begin
  ⊙ #₁ f                         ≡⟨⟩
  f (#₁ zero) ∙ ⊙ (#₁ ∘ suc) f   ≈⟨ ∙-congˡ (⊙-cong# {f-cong = f-cong} _ _ (tail⁺ _≈ᴬ_ #₁≋#₂)) ⟩
  f (#₁ zero) ∙ (⊙ (#₂ ∘ suc) f) ≈⟨ ∙-congʳ (f-cong (head⁺ _≈ᴬ_ #₁≋#₂)) ⟩
  f (#₂ zero) ∙ (⊙ (#₂ ∘ suc) f) ≡⟨⟩
  ⊙ #₂ f                         ∎
  where open ≈-Reasoning setoid
```

`⊙` is congruent with respect to equivalent `A → C` maps.

```agda
⊙-cong : {# : Fin n → A}
  → {f g : A → C}
  → {∀ {a} → f a ≈ g a }
  → ⊙ # f ≈ ⊙ # g
⊙-cong {n = ℕ.zero} = refl
⊙-cong {n = ℕ.suc n} {#} {f = f} {g} {ext} = begin
  ⊙ # f                        ≡⟨⟩
  f (# zero) ∙ ⊙ (# ∘ suc) f   ≈⟨ ∙-cong (ext {# zero}) (⊙-cong {n} {# = # ∘ suc} {f} {g} {ext}) ⟩
  g (# zero) ∙ (⊙ (# ∘ suc) g) ≡⟨⟩
  ⊙ # g                      ∎
  where open ≈-Reasoning setoid
```

### Identities

`⊙` preserves the identity of the underlying monoid.

```agda
⊙-identityʳ : (# : Fin n → A) → (f : A → C) → (⊙ # f) ∙ ε ≈ ⊙ # f
⊙-identityʳ # f = Prop.⊙-identityʳ (f ∘ #)

⊙-identityˡ : (# : Fin n → A) → (f : A → C) →  ε ∙ (⊙ # f) ≈ ⊙ # f
⊙-identityˡ # f = Prop.⊙-identityˡ (f ∘ #)

⊙-preserves-ε : (# : Fin n → A) → (⊙ # λ _ → ε) ≈ ε
⊙-preserves-ε {n} _ = Prop.⊙-preserves-ε {n}
```

### Interchange of `∙` and `++`

Folding over two vectors individually is equivalent to
combining the vectors and then folding.

```agda
⊙-preserves-++ : (#₁ : Fin n₁ → A) → (#₂ : Fin n₂ → A)
  → (f : A → C)
  → Congruent _≈ᴬ_ _≈_ f
  → ⊙ #₁ f ∙ ⊙ #₂ f ≈ ⊙ (#₁ ++ #₂) f
⊙-preserves-++ {n} #₁ #₂ f f-cong = begin
  ⊙ #₁ f ∙ ⊙ #₂ f           ≈⟨ Prop.⊙-preserves-++ (f ∘ #₁) (f ∘ #₂) ⟩
  Core.⊙ (f ∘ #₁ ++ f ∘ #₂) ≈⟨ Prop.⊙-cong (sym ∘ reflexive ∘ [,]-∘ f ∘ (splitAt n)) ⟩
  ⊙ (#₁ ++ #₂) f ∎
  where open ≈-Reasoning setoid
```
