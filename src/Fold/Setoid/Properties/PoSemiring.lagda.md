---
title: Fold over `Semiring`s
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Relation.Binary using (Setoid)
open import Algebra.Ordered.Partial.Bundles using (PoSemiring)

module Fold.Setoid.Properties.PoSemiring
  {a ℓᵃ} (𝓐 : Setoid a ℓᵃ)
  {c ℓ₁ ℓ₂} ⦃ S : PoSemiring c ℓ₁ ℓ₂ ⦄
  where

open import Data.Fin using (Fin ; suc) ; open Fin
open import Data.Nat using (ℕ)
open import Function using (_∘_)

open Setoid 𝓐 renaming (Carrier to A) using ()
open PoSemiring S renaming (Carrier to C)
  hiding (zero)

open import Fold.Properties.PoSemiring ⦃ S ⦄ as Prop using ()
-- import Relation.Binary.Reasoning.PartialOrder as <-Reasoning

private
  variable
    n : ℕ
```

</details>

```agda
open import Fold.Setoid.Properties.Semiring 𝓐 ⦃ semiring ⦄ public
```

### Monotonicity

Order is preserved by `∑`.

```agda
∑-preserves-≤ :
  ∀ {# : Fin n → A} {f g : A → C}
  → (∀ {x} → f x ≤ g x)
  → ∑[ # ] f ≤ ∑[ # ] g
∑-preserves-≤ {# = #} {f} {g} fx≤gy = Prop.∑-preserves-≤ {f = f ∘ #} fx≤gy
```
