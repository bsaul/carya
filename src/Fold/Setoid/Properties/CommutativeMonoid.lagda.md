---
title: Properties of `fold` over `CommutativeMonoid`
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level)
open import Relation.Binary using (Setoid)
open import Algebra.Bundles using (CommutativeMonoid)

module Fold.Setoid.Properties.CommutativeMonoid
  {a ℓᵃ} (𝓐 : Setoid a ℓᵃ)
  {c ℓ} ⦃ M : CommutativeMonoid c ℓ ⦄
  where

open Setoid 𝓐 renaming
  ( Carrier to A
  ; _≈_ to _≈ᴬ_
  ; refl to reflᴬ
  ; sym to symᴬ
  ; reflexive to reflexiveᴬ
  ) using ()
open CommutativeMonoid M renaming (Carrier to C)

-- stdlib
open import Data.Fin using (Fin; splitAt); open Fin
open import Data.Fin.Properties using (+↔⊎)
open import Data.Nat as ℕ using (ℕ)
open import Data.Sum using (_⊎_ ; [_,_] ; map; inj₁ ; inj₂)
open import Data.Sum.Properties using ([,]-map; [,]-∘)
open import Data.Vec.Functional using (_++_)
open import Data.Vec.Functional.Relation.Binary.Equality.Setoid 𝓐
    using (tail⁺ ; head⁺ ; _≋_)
open import Function using (_∘_; const; Congruent; id; Inverse)

import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

-- open import Fold.Core ⦃ M ⦄ as Core using ()
open import Fold.Setoid.Core 𝓐 ⦃ monoid ⦄
open import Fold.Properties.CommutativeMonoid ⦃ M ⦄ as Prop using ()

private
  variable
    n n₁ n₂ : ℕ
```

</details>

```agda
open import Fold.Setoid.Properties.Monoid 𝓐 ⦃ monoid ⦄ public
```

### `⊙` preserves `∙`

`⊙` is congruent with respect to the binary operation
of the commutative monoid.
In short,
one can fold and combine by `∙` as you go,
or fold and then combine.

```agda
⊙-preserves-∙ : (# : Fin n → A) → (f g : A → C)
    →  ⊙ # f ∙ ⊙ # g ≈ (⊙ # (λ x → f x ∙ g x))
⊙-preserves-∙ # f g = Prop.⊙-preserves-∙ (f ∘ #) (g ∘ #)
```

###  `⊙` Commutative

`⊙` is commutative with respect to functions `f` and `g`
that map *to* the underlying commutative monoid.

```agda
⊙-comm : (#₁ : Fin n₁ → A) → (#₂ : Fin n₂ → A) → (f g : A → C)
    → (⊙ #₁ f ∙ ⊙ #₂ g) ≈ (⊙ #₂ g ∙ ⊙ #₁ f)
⊙-comm  #₁ #₂ f g = Prop.⊙-comm (f ∘ #₁) (g ∘ #₂)
```
