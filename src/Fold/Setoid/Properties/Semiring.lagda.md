---
title: Fold over `Semiring`s
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Relation.Binary using (Setoid)
open import Algebra.Bundles using (Semiring)

module Fold.Setoid.Properties.Semiring
  {a ℓᵃ} (𝓐 : Setoid a ℓᵃ)
  {c ℓ} ⦃ S : Semiring c ℓ ⦄
  where

open Setoid 𝓐 renaming (Carrier to A) using ()
open Semiring S renaming (Carrier to C)

open import Data.Fin using (Fin ; suc)
open import Data.Nat using (ℕ)
open import Function using (_∘_)


open import Algebra.Definitions.RawMonoid +-rawMonoid

import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

open import Fold.Setoid.Core 𝓐
open import Fold.Properties.Semiring ⦃ S ⦄ as S using ()
import Fold.Setoid.Properties.Monoid 𝓐 as MonoidProperties
import Fold.Setoid.Properties.CommutativeMonoid 𝓐 as CommutativeMonoidProperties


private
  variable
    n : ℕ
```

</details>

## Syntax for sum and product

```agda
∑[_]_ : (Fin n → A) → (A → C) → C
∑[ i ] f = fold ⦃ M = +-monoid ⦄ i f

∏[_]_ : (Fin n → A) → (A → C) → C
∏[ i ] f = fold ⦃ M = *-monoid ⦄ i f
```

## Properties of `∑`

```agda
open MonoidProperties ⦃ M = +-monoid ⦄ public
  renaming
  ( ⊙-cong         to ∑-cong
  ; ⊙-cong#        to ∑-cong#
  ; ⊙-identityʳ    to ∑-identityʳ
  ; ⊙-identityˡ    to ∑-identityˡ
  ; ⊙-preserves-ε  to ∑-preserves-0
  ; ⊙-preserves-++ to ∑-preserves-++
  )
  using ()

open CommutativeMonoidProperties ⦃ M = +-commutativeMonoid ⦄ public
  renaming
  ( ⊙-preserves-∙  to ∑-preserves-+
  ; ⊙-comm         to ∑-comm
  )
  using ()
```

### Distributivity

```agda
c*∑≈∑c* : ∀ (# : Fin n → A) (f : A → C) (c : C)
  → c * ∑[ # ] f ≈ ∑[ # ] λ x → c * f x
c*∑≈∑c* # f = S.c*∑≈∑c* (f ∘ #)
```

### Sum of constant `1#` is `n` "*" `1#`

```agda
∑1≈n : (# : Fin n → A) → ∑[ # ] (λ _ → 1#) ≈ (n × 1#)
∑1≈n # = S.∑1≈n ((λ _ → 1#) ∘ #)
```

## Properties of `∏`

```agda
open MonoidProperties ⦃ M = *-monoid ⦄ public
  renaming
  ( ⊙-cong#        to ∏-cong#
  ; ⊙-cong         to ∏-cong
  ; ⊙-identityʳ    to ∏-identityʳ
  ; ⊙-identityˡ    to ∏-identityˡ
  ; ⊙-preserves-ε  to ∏-preserves-ε
  ; ⊙-preserves-++ to ∏-preserves-++
  )
  using ()
```
