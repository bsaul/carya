---
title: Indicator Functions
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level)
open import Algebra.Ordered.Total.Bundles

module Function.Indicator.Base
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : ToRing r ℓr₁ ℓr₂)
  where

private
  open module 𝓡 = ToRing 𝓡
    renaming (Carrier to R)
    using
    ( module Eq
    ; module Min
    ; module Max
    ; total
    ; 1#
    ; 0#
    ; -_
    ; _-_)
open Min
open Max

open import Algebra.Ordered.Total.Properties.ToRing 𝓡
open import Data.Sum

open import Function.Indicator.Core 𝓡

private
  variable
    a : Level
    A : Set a

private
  infix 3 _≗_
  _≗_ : (A → R) → (A → R) → Set _
  f ≗ g = ∀ {a} → f a 𝓡.≈ g a
```

</details>

Pointwise versions of min/max

```agda
-- min (and)
infixl 7 _⊓ᵖ_
_⊓ᵖ_ : (A → R) → (A → R) → A → R
f ⊓ᵖ g = λ a → (f a) ⊓ (g a)

-- max (or)
infixl 6 _⊔ᵖ_
_⊔ᵖ_ : (A → R) → (A → R) → A → R
f ⊔ᵖ g = λ a → (f a) ⊔ (g a)
```

## Operations on `IsIndicator`

```agda
always : IsIndicator {A = A} λ _ → 1#
always _ = inj₁ Eq.refl

never : IsIndicator {A = A} λ _ → 0#
never _ = inj₂ Eq.refl

and : {f g : A → R} → IsIndicator f → IsIndicator g → IsIndicator (f ⊓ᵖ g)
and {f = f} {g} I J a with I a | J a | total (f a) (g a)
... | (inj₁ f≈1) | (inj₁ _)   | (inj₁ _) = inj₁ f≈1
... | (inj₁ _)   | (inj₁ g≈1) | (inj₂ _) = inj₁ g≈1
... | (inj₁ f≈1) | (inj₂ _)   | (inj₁ _) = inj₁ f≈1
... | (inj₁ _)   | (inj₂ g≈0) | (inj₂ _) = inj₂ g≈0
... | (inj₂ f≈0) | (inj₁ _)   | (inj₁ _) = inj₂ f≈0
... | (inj₂ _)   | (inj₁ g≈1) | (inj₂ _) = inj₁ g≈1
... | (inj₂ f≈0) | (inj₂ _)   | (inj₁ _) = inj₂ f≈0
... | (inj₂ _)   | (inj₂ g≈0) | (inj₂ _) = inj₂ g≈0

or : {f g : A → R} → IsIndicator f → IsIndicator g → IsIndicator (f ⊔ᵖ g)
or {f = f} {g} I J a with I a | J a | total (g a) (f a)
... | (inj₁ f≈1) | (inj₁ _)   | (inj₁ _) = inj₁ f≈1
... | (inj₁ _)   | (inj₁ g≈1) | (inj₂ _) = inj₁ g≈1
... | (inj₁ f≈1) | (inj₂ _)   | (inj₁ _) = inj₁ f≈1
... | (inj₁ _)   | (inj₂ g≈0) | (inj₂ _) = inj₂ g≈0
... | (inj₂ f≈0) | (inj₁ _)   | (inj₁ _) = inj₂ f≈0
... | (inj₂ _)   | (inj₁ g≈1) | (inj₂ _) = inj₁ g≈1
... | (inj₂ f≈0) | (inj₂ _)   | (inj₁ _) = inj₂ f≈0
... | (inj₂ _)   | (inj₂ g≈0) | (inj₂ _) = inj₂ g≈0

not : {f : A → R} → IsIndicator f → IsIndicator λ a → 1# - f a
not {f = f} I a with I a
... | (inj₁ f≈1) = inj₂ (Eq.trans (𝓡.+-congʳ (Eq.sym f≈1)) (𝓡.-‿inverseʳ (f a)))
... | (inj₂ f≈0) = inj₁
          (Eq.trans
              (𝓡.+-congˡ (𝓡.-‿cong f≈0))
              (Eq.trans (𝓡.+-congˡ -0#≈0#) (𝓡.+-identityʳ 1#)))
```

## `Indicator` operations and values

```agda
⊤ : Indicator A
⊤ = mk _ always

private
  ⟦⊤⟧ : ⟦ ⊤ ⟧ ≗ λ (_ : A) → 1#
  ⟦⊤⟧ = Eq.refl

⊥ : Indicator A
⊥ = mk _ never

private
  ⟦⊥⟧ : ⟦ ⊥ ⟧ ≗ λ (_ : A) → 0#
  ⟦⊥⟧ = Eq.refl

_∨_ : Indicator A → Indicator A → Indicator A
(mk _ fi) ∨ (mk _ gi) = mk _ (or fi gi)

private
  ⟦∨⟧ : ∀ {f g : Indicator A} → ⟦ f ∨ g ⟧ ≗ ⟦ f ⟧ ⊔ᵖ ⟦ g ⟧
  ⟦∨⟧ = Eq.refl

_∧_ : Indicator A → Indicator A → Indicator A
(mk _ fi) ∧ (mk _ gi) = mk _ (and fi gi)

private
  ⟦∧⟧ : ∀ {f g : Indicator A} → ⟦ f ∧ g ⟧ ≗ ⟦ f ⟧ ⊓ᵖ ⟦ g ⟧
  ⟦∧⟧ = Eq.refl

∁ : Indicator A → Indicator A
∁ (mk _ fi) = mk _ (not fi)

private
  ⟦∁⟧ : ∀ {f : Indicator A} → ⟦ ∁ f ⟧ ≗ λ x → 1# - ⟦ f ⟧ x
  ⟦∁⟧ = Eq.refl
```
