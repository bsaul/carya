---
title: Properties of Indicator Functions
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level) renaming (_⊔_ to _⊔ℓ_)
open import Algebra.Ordered.Total.Bundles
open import Algebra.Ordered.Total.Construct.NonTrivialToRing
open import Data.Product using (_,_)

module Function.Indicator.Properties
  {r ℓr₁ ℓr₂ : Level}
  ((𝓡 , 1≰0): NonTrivialToRing r ℓr₁ ℓr₂)
  {a : Level}
  (A : Set a)
  where

private
  open module 𝓡 = ToRing 𝓡
    renaming (Carrier to R)
    using
    ( module Eq
    ; module Min
    ; module Max
    ; total
    ; 1#
    ; 0#
    ; -_
    ; _-_)
open Min
open Max

open import Function.Indicator.Core 𝓡
open import Function.Indicator.Base 𝓡

open Indicator

open import Algebra.Definitions (_≈_ {A = A})
open import Algebra.Structures (_≈_ {A = A})
open import Algebra.Bundles
open import Algebra.Lattice.Structures (_≈_ {A = A})
open import Algebra.Lattice.Bundles
open import Algebra.Ordered.Total.Properties.ToRing 𝓡
open import Data.Sum
open import Relation.Binary.Definitions
open import Relation.Nullary.Negation
import Relation.Binary.Reasoning.PartialOrder as ≤-Reasoning
```

</details>

## Properties of `⊤`

```agda
⊤-Max : Maximum (_≤_ {A = A}) ⊤
⊤-Max f {a} with total (⟦ f ⟧ a) (⟦ ⊤ ⟧ a) | Indicator.isIndicator f a
... | (inj₁ f≤1) | _ = f≤1
... | (inj₂ _)   | (inj₁ f≈1) = 𝓡.reflexive f≈1
... | (inj₂ 1≤f) | (inj₂ f≈0) =
  contradiction 1≤f λ x → 1≰0 (𝓡.trans x (𝓡.reflexive f≈0))
```

## Properties of `⊥`

```agda
⊥-Min : Minimum (_≤_ {A = A}) ⊥
⊥-Min f {a} with total (⟦ ⊥ ⟧ a) (⟦ f ⟧ a) | isIndicator f a
... | (inj₁ 0≤f) | _ = 0≤f
... | (inj₂ f≤0) | (inj₁ f≈1) =
  contradiction f≤0 λ x → 1≰0 (𝓡.trans (𝓡.reflexive (Eq.sym f≈1)) x)
... | (inj₂ _)   | (inj₂ f≈0) = 𝓡.reflexive (Eq.sym f≈0)
```

## Properties of `∁`

```agda
∁-cong : Congruent₁ ∁
∁-cong f {a} = 𝓡.+-congˡ (𝓡.-‿cong (f {a}))
```

## Properties of `_∨_`

```agda
∨-cong : Congruent₂ _∨_
∨-cong x y {a} = ⊔-cong (x {a}) (y {a})

∨-assoc : Associative _∨_
∨-assoc x y z {a} = ⊔-assoc (⟦ x ⟧ a) (⟦ y ⟧ a) (⟦ z ⟧ a)

∨-comm : Commutative _∨_
∨-comm x y {a} = ⊔-comm (⟦ x ⟧ a) (⟦ y ⟧ a)

∨-idem : Idempotent _∨_
∨-idem x {a} = ⊔-idem (⟦ x ⟧ a)

∨-inverseˡ : LeftInverse ⊤ ∁ _∨_
∨-inverseˡ f {a}
 with total (⟦ f ⟧ a) (⟦ ∁ f ⟧ a) | isIndicator f a | isIndicator (∁ f) a
... | (inj₁ _) | (inj₁ _)   | (inj₁ ∁f≈1) = ∁f≈1
... | (inj₂ _) | (inj₁ f≈1) | (inj₁ _)    = f≈1
... | (inj₁ _) | (inj₂ _)   | (inj₁ ∁f≈1) = ∁f≈1
... | (inj₂ x) | (inj₂ f≈0) | (inj₁ ∁f≈1) =
  contradiction x λ ¬x → 1≰0
     (begin
        1#                 ≈⟨ ∁f≈1 ⟨
        1# 𝓡.+ - ⟦ f ⟧ a   ≤⟨ ¬x ⟩
        ⟦ f ⟧ a            ≈⟨ f≈0 ⟩
        0#                 ∎)
    where open ≤-Reasoning 𝓡.poset
... | (inj₁ x) | (inj₁ f≈1) | (inj₂ ∁f≈0) =
  contradiction x λ ¬x → 1≰0
     (begin
       1#                ≈⟨ f≈1 ⟨
       ⟦ f ⟧ a           ≤⟨ ¬x ⟩
       1# 𝓡.+ - ⟦ f ⟧ a  ≈⟨ ∁f≈0 ⟩
       0#                ∎)
    where open ≤-Reasoning 𝓡.poset
... | (inj₂ _) | (inj₁ f≈1) | (inj₂ _)    = f≈1
... | _        | (inj₂ f≈0) | (inj₂ ∁f≈0) =
  contradiction f≈0 λ x → 1≰0
    (begin
      1#                ≤⟨ 𝓡.refl ⟩
      1#                ≈⟨ 𝓡.+-identityʳ 1# ⟨
      1# 𝓡.+ 0#         ≈⟨ 𝓡.+-congˡ -0#≈0# ⟨
      1# 𝓡.+ - 0#       ≈⟨ 𝓡.+-congˡ (𝓡.-‿cong x) ⟨
      1# 𝓡.+ - ⟦ f ⟧ a  ≈⟨ ∁f≈0 ⟩
      0# ∎)
    where open ≤-Reasoning 𝓡.poset

-- TODO: this should be derivable from ∨-inverseˡ
∨-inverseʳ : RightInverse ⊤ ∁ _∨_
∨-inverseʳ f {a}
  with total (⟦ ∁ f ⟧ a) (⟦ f ⟧ a) | Indicator.isIndicator f a | isIndicator (∁ f) a
... | (inj₁ _) | (inj₁ f≈1) | (inj₁ _)    = f≈1
... | (inj₂ _) | (inj₁ _)   | (inj₁ ∁f≈1) = ∁f≈1
... | (inj₁ _) | (inj₁ f≈1) | (inj₂ _)    = f≈1
... | (inj₂ x) | (inj₁ f≈1) | (inj₂ ∁f≈0) =
  contradiction x λ ¬x → 1≰0
     (begin
       1#                ≈⟨ f≈1 ⟨
       ⟦ f ⟧ a           ≤⟨ ¬x ⟩
       1# 𝓡.+ - ⟦ f ⟧ a  ≈⟨ ∁f≈0 ⟩
       0#                ∎)
    where open ≤-Reasoning 𝓡.poset
... | (inj₁ x) | (inj₂ f≈0) | (inj₁ ∁f≈1) =
    contradiction x λ ¬x → 1≰0
     (begin
        1#                 ≈⟨ ∁f≈1 ⟨
        1# 𝓡.+ - ⟦ f ⟧ a   ≤⟨ ¬x ⟩
        ⟦ f ⟧ a            ≈⟨ f≈0 ⟩
        0#                 ∎)
    where open ≤-Reasoning 𝓡.poset
... | (inj₂ _) | (inj₂ _)   | (inj₁ ∁f≈1) = ∁f≈1
... | _        | (inj₂ f≈0) | (inj₂ ∁f≈0) =
  contradiction f≈0 λ x → 1≰0
    (begin
      1#                ≤⟨ 𝓡.refl ⟩
      1#                ≈⟨ 𝓡.+-identityʳ 1# ⟨
      1# 𝓡.+ 0#         ≈⟨ 𝓡.+-congˡ -0#≈0# ⟨
      1# 𝓡.+ - 0#       ≈⟨ 𝓡.+-congˡ (𝓡.-‿cong x) ⟨
      1# 𝓡.+ - ⟦ f ⟧ a  ≈⟨ ∁f≈0 ⟩
      0# ∎)
    where open ≤-Reasoning 𝓡.poset

∨-inverse : Inverse ⊤ ∁ _∨_
∨-inverse = ∨-inverseˡ , ∨-inverseʳ

∨-isMagma : IsMagma _∨_
∨-isMagma = record
  { isEquivalence = isEquivalence
  ; ∙-cong = λ {a} {b} {c} {d} → ∨-cong {a} {b} {c} {d}
  }

∨-isSemigroup : IsSemigroup _∨_
∨-isSemigroup = record { isMagma = ∨-isMagma ; assoc = ∨-assoc }

∨-isBand : IsBand _∨_
∨-isBand = record { isSemigroup = ∨-isSemigroup ; idem = ∨-idem }

∨-isSemilattice : IsSemilattice _∨_
∨-isSemilattice = record { isBand = ∨-isBand ; comm = ∨-comm }

∨-magma : Magma (r ⊔ℓ ℓr₁ ⊔ℓ a) (ℓr₁ ⊔ℓ a)
∨-magma = record { isMagma = ∨-isMagma }

∨-semigroup : Semigroup (r ⊔ℓ ℓr₁ ⊔ℓ a) (ℓr₁ ⊔ℓ a)
∨-semigroup = record { isSemigroup = ∨-isSemigroup }

∨-band : Band (r ⊔ℓ ℓr₁ ⊔ℓ a) (ℓr₁ ⊔ℓ a)
∨-band = record { isBand = ∨-isBand }

∨-semilattice : Semilattice (r ⊔ℓ ℓr₁ ⊔ℓ a) (ℓr₁ ⊔ℓ a)
∨-semilattice = record { isSemilattice = ∨-isSemilattice }
```

## Properties of `_∧_`

```agda
∧-cong : Congruent₂ _∧_
∧-cong x y {a} = ⊓-cong (x {a}) (y {a})

∧-assoc : Associative _∧_
∧-assoc x y z {a} = ⊓-assoc (⟦ x ⟧ a) (⟦ y ⟧ a) (⟦ z ⟧ a)

∧-comm : Commutative _∧_
∧-comm x y {a} = ⊓-comm (⟦ x ⟧ a) (⟦ y ⟧ a)

∧-idem : Idempotent _∧_
∧-idem x {a} = ⊓-idem (⟦ x ⟧ a)

∧-inverseˡ : LeftInverse ⊥ ∁ _∧_
∧-inverseˡ f {a}
  with total (⟦ ∁ f ⟧ a) (⟦ f ⟧ a) | Indicator.isIndicator f a | isIndicator (∁ f) a
... | _        | (inj₁ f≈1) | (inj₁ ∁f≈1) =
  contradiction f≈1 λ x → 1≰0
     (begin
      1#                ≤⟨ 𝓡.refl ⟩
      1#                ≈⟨ ∁f≈1 ⟨
      1# 𝓡.+ - ⟦ f ⟧ a  ≈⟨ 𝓡.+-congˡ (𝓡.-‿cong x) ⟩
      1# 𝓡.+ - 1#       ≈⟨ 𝓡.-‿inverseʳ 1# ⟩
      0# ∎)
    where open ≤-Reasoning 𝓡.poset
... | (inj₁ _) | (inj₁ _)   | (inj₂ ∁f≈0) = ∁f≈0
... | (inj₂ x) | (inj₁ f≈1) | (inj₂ ∁f≈0) =
  contradiction x λ ¬x → 1≰0
    (begin
      1#                ≈⟨ f≈1 ⟨
      ⟦ f ⟧ a           ≤⟨ x ⟩
      1# 𝓡.+ - ⟦ f ⟧ a ≈⟨ ∁f≈0 ⟩
      0# ∎)
    where open ≤-Reasoning 𝓡.poset
... | (inj₁ x) | (inj₂ f≈0) | (inj₁ ∁f≈1) =
  contradiction x λ ¬x → 1≰0
    (begin
      1#                ≈⟨ ∁f≈1 ⟨
      1# 𝓡.+ - ⟦ f ⟧ a  ≤⟨ x ⟩
      ⟦ f ⟧ a           ≈⟨ f≈0 ⟩
      0# ∎)
    where open ≤-Reasoning 𝓡.poset
... | (inj₂ _) | (inj₂ f≈0) | (inj₁ _)    = f≈0
... | (inj₁ _) | (inj₂ _  ) | (inj₂ ∁f≈0) = ∁f≈0
... | (inj₂ _) | (inj₂ f≈0) | (inj₂ _   ) = f≈0

-- TODO: this should be derivable from ∧-inverseˡ
∧-inverseʳ : RightInverse ⊥ ∁ _∧_
∧-inverseʳ f {a}
  with total (⟦ f ⟧ a) (⟦ ∁ f ⟧ a) | Indicator.isIndicator f a | isIndicator (∁ f) a
... | _        | (inj₁ f≈1) | (inj₁ ∁f≈1) =
  contradiction f≈1 λ x → 1≰0
     (begin
      1#                ≤⟨ 𝓡.refl ⟩
      1#                ≈⟨ ∁f≈1 ⟨
      1# 𝓡.+ - ⟦ f ⟧ a  ≈⟨ 𝓡.+-congˡ (𝓡.-‿cong x) ⟩
      1# 𝓡.+ - 1#       ≈⟨ 𝓡.-‿inverseʳ 1# ⟩
      0# ∎)
    where open ≤-Reasoning 𝓡.poset
... | (inj₁ x) | (inj₁ f≈1)   | (inj₂ ∁f≈0) =
  contradiction x λ ¬x → 1≰0
    (begin
      1#                ≈⟨ f≈1 ⟨
      ⟦ f ⟧ a           ≤⟨ x ⟩
      1# 𝓡.+ - ⟦ f ⟧ a ≈⟨ ∁f≈0 ⟩
      0# ∎)
    where open ≤-Reasoning 𝓡.poset
... | (inj₂ _) | (inj₁ _)   | (inj₂ ∁f≈0) = ∁f≈0
... | (inj₁ _) | (inj₂ f≈0) | (inj₁ _)    = f≈0
... | (inj₂ x) | (inj₂ f≈0) | (inj₁ ∁f≈1) =
  contradiction x λ ¬x → 1≰0
    (begin
      1#                ≈⟨ ∁f≈1 ⟨
      1# 𝓡.+ - ⟦ f ⟧ a  ≤⟨ x ⟩
      ⟦ f ⟧ a           ≈⟨ f≈0 ⟩
      0# ∎)
    where open ≤-Reasoning 𝓡.poset
... | (inj₁ _) | (inj₂ f≈0) | (inj₂ _)    = f≈0
... | (inj₂ _) | (inj₂ _)   | (inj₂ ∁f≈0) = ∁f≈0

∧-inverse : Inverse ⊥ ∁ _∧_
∧-inverse = ∧-inverseˡ , ∧-inverseʳ

∧-isMagma : IsMagma _∧_
∧-isMagma = record
  { isEquivalence = isEquivalence
  ; ∙-cong = λ {a} {b} {c} {d} → ∧-cong {a} {b} {c} {d}
  }

∧-isSemigroup : IsSemigroup _∧_
∧-isSemigroup = record { isMagma = ∧-isMagma ; assoc = ∧-assoc }

∧-isBand : IsBand _∧_
∧-isBand = record { isSemigroup = ∧-isSemigroup ; idem = ∧-idem }

∧-isSemilattice : IsSemilattice _∧_
∧-isSemilattice = record { isBand = ∧-isBand ; comm = ∧-comm }

∧-magma : Magma (r ⊔ℓ ℓr₁ ⊔ℓ a) (ℓr₁ ⊔ℓ a)
∧-magma = record { isMagma = ∧-isMagma }

∧-semigroup : Semigroup (r ⊔ℓ ℓr₁ ⊔ℓ a) (ℓr₁ ⊔ℓ a)
∧-semigroup = record { isSemigroup = ∧-isSemigroup }

∧-band : Band (r ⊔ℓ ℓr₁ ⊔ℓ a) (ℓr₁ ⊔ℓ a)
∧-band = record { isBand = ∧-isBand }

∧-semilattice : Semilattice (r ⊔ℓ ℓr₁ ⊔ℓ a) (ℓr₁ ⊔ℓ a)
∧-semilattice = record { isSemilattice = ∧-isSemilattice }
```

## Properties of `_∨_` and `_∧_`

```agda
∨-absorbs-∧ : _∨_ Absorbs _∧_
∨-absorbs-∧ x y {a} = ⊔-absorbs-⊓  (⟦ x ⟧ a) (⟦ y ⟧ a)

∧-absorbs-∨ : _∧_ Absorbs _∨_
∧-absorbs-∨ x y {a} = ⊓-absorbs-⊔ (⟦ x ⟧ a) (⟦ y ⟧ a)

∨-∧-absorptive : Absorptive _∨_ _∧_
∨-∧-absorptive = ∨-absorbs-∧ , ∧-absorbs-∨

∨-distribˡ-∧ : _∨_ DistributesOverˡ _∧_
∨-distribˡ-∧ x y z {a} = ⊔-distribˡ-⊓ (⟦ x ⟧ a) (⟦ y ⟧ a) (⟦ z ⟧ a)

∨-distribʳ-∧ : _∨_ DistributesOverʳ _∧_
∨-distribʳ-∧ x y z {a} = ⊔-distribʳ-⊓ (⟦ x ⟧ a) (⟦ y ⟧ a) (⟦ z ⟧ a)

∨-distrib-∧ : _∨_ DistributesOver _∧_
∨-distrib-∧  = ∨-distribˡ-∧ , ∨-distribʳ-∧

∧-distribˡ-∨ : _∧_ DistributesOverˡ _∨_
∧-distribˡ-∨ x y z {a} = ⊓-distribˡ-⊔ (⟦ x ⟧ a) (⟦ y ⟧ a) (⟦ z ⟧ a)

∧-distribʳ-∨ : _∧_ DistributesOverʳ _∨_
∧-distribʳ-∨ x y z {a} = ⊓-distribʳ-⊔ (⟦ x ⟧ a) (⟦ y ⟧ a) (⟦ z ⟧ a)

∧-distrib-∨ : _∧_ DistributesOver _∨_
∧-distrib-∨  = ∧-distribˡ-∨ , ∧-distribʳ-∨

∨-∧-isLattice : IsLattice _∨_ _∧_
∨-∧-isLattice = record
 { isEquivalence = isEquivalence
 ; ∨-comm = ∨-comm
 ; ∨-assoc = ∨-assoc
 ; ∨-cong = λ {a} {b} {c} {d} → ∨-cong {a} {b} {c} {d}
 ; ∧-comm = ∧-comm
 ; ∧-assoc = ∧-assoc
 ; ∧-cong = λ {a} {b} {c} {d} → ∧-cong {a} {b} {c} {d}
 ; absorptive = ∨-∧-absorptive
 }

∨-∧-isDistributiveLattice : IsDistributiveLattice _∨_ _∧_
∨-∧-isDistributiveLattice = record
  { isLattice = ∨-∧-isLattice
  ; ∨-distrib-∧ = ∨-distrib-∧
  ; ∧-distrib-∨ = ∧-distrib-∨
  }

∨-∧-isBooleanAlgebra : IsBooleanAlgebra _∨_ _∧_ ∁ ⊤ ⊥
∨-∧-isBooleanAlgebra = record
 { isDistributiveLattice = ∨-∧-isDistributiveLattice
 ; ∨-complement = ∨-inverse
 ; ∧-complement = ∧-inverse
 ; ¬-cong = λ {f} {g} → ∁-cong {f} {g}
 }
```
