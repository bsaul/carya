---
title: Indicator Functions
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; suc; _⊔_)
open import Algebra.Ordered.Total.Bundles

module Function.Indicator.Core
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : ToRing r ℓr₁ ℓr₂)
  where

private
  open module 𝓡 = ToRing 𝓡
    renaming (Carrier to R)
    using (1# ; 0#; module Eq)

open import Data.Bool using (Bool); open Bool
open import Data.Sum using (_⊎_; inj₁ ; inj₂)
open import Function using (_on_; _∘_)
open import Relation.Binary using
  ( IsEquivalence
  ; IsPartialOrder
  )
open import Relation.Binary.Construct.On as On using ()
open import Relation.Nullary.Decidable
open import Relation.Nullary.Negation using (¬_)
open import Relation.Nullary.Reflects

open import Predicate.Decidable -- {ℓr₁ ⊔ ℓr₂}
  hiding (⟦_⟧; _≈_)

private
  variable
    a ℓ : Level
    A : Set a
```

</details>

## Definition

An `Indicator` function is, simply,
one that always returns `1#` or `0#`.

```agda
IsIndicator : ∀ {a} {A : Set a} → (A → R) → Set (ℓr₁ ⊔ a)
IsIndicator I = ∀ a → (I a 𝓡.≈ 1#) ⊎ (I a 𝓡.≈ 0#)
```

## Type

```agda
record Indicator {a} (A : Set a) : Set (a ⊔ r ⊔ ℓr₁) where
  constructor mk
  field
    I : A → R
    isIndicator : IsIndicator I
```

## Denotation and Equivalence

```agda
⟦_⟧ : Indicator A → A → R
⟦ (mk f _ ) ⟧ = f

private
  infix 3 _≗_ _≤*_
  _≗_ : (A → R) → (A → R) → Set _
  f ≗ g = ∀ {a} → f a 𝓡.≈ g a

  ≗-isEquivalence : ∀ {a} {A : Set a} → IsEquivalence (_≗_ {A = A})
  ≗-isEquivalence = record
    { refl = λ {x} {a} → 𝓡.Eq.refl
    ; sym = λ x {a} → 𝓡.Eq.sym (x {a})
    ; trans = λ x y {a} → 𝓡.Eq.trans (x {a}) (y {a})
    }

  _≤*_ : (A → R) → (A → R) → Set _
  f ≤* g = ∀ {a} → f a 𝓡.≤  g a

  ≤*-isPartialOrder :  ∀ {a} {A : Set a} → IsPartialOrder (_≗_ {A = A}) _≤*_
  ≤*-isPartialOrder = record
    { isPreorder = record
      { isEquivalence = ≗-isEquivalence
      ; reflexive = λ x {a} → 𝓡.reflexive (x {a})
      ; trans = λ x y {a} → 𝓡.trans (x {a}) (y {a})
      }
    ; antisym = λ x y {a} → 𝓡.antisym (x {a}) (y {a})
    }

infix 3 _≈_ _≤_
_≈_ : Indicator A → Indicator A → Set _
_≈_ = _≗_ on ⟦_⟧

_≤_ : Indicator A → Indicator A → Set _
_≤_ = _≤*_ on ⟦_⟧

isEquivalence : ∀ {a} {A : Set a} → IsEquivalence (_≈_ {A = A})
isEquivalence = On.isEquivalence ⟦_⟧ ≗-isEquivalence

isPartialOrder : ∀ {a} {A : Set a} → IsPartialOrder (_≈_ {A = A}) _≤_
isPartialOrder = On.isPartialOrder ⟦_⟧ ≤*-isPartialOrder
```

## Lifting from a Decidable Predicate to an `Indicator`

```agda
private
  to : Pred? A ℓ → A → R
  to P? a with toSum (∣ P? ∣ a)
  ... | (inj₁ _) = 1#
  ... | (inj₂ _) = 0#

  toIs : (P? : Pred? A ℓ) → IsIndicator (to P?)
  toIs P? a  with toSum (∣ P? ∣ a)
  ... | (inj₁ _) = inj₁ Eq.refl
  ... | (inj₂ _) = inj₂ Eq.refl

  lift : Pred? A ℓ → Indicator A
  lift P? = mk _ (toIs P?)

∥_∥  : Pred? A ℓ → A → R
∥_∥  = ⟦_⟧ ∘ lift
```
