{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; suc; _⊔_)
open import Algebra.Ordered.Total.Bundles

module Function.Indicator
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : ToRing r ℓr₁ ℓr₂)
  where

open import Function.Indicator.Core 𝓡 public
open import Function.Indicator.Base 𝓡 public
