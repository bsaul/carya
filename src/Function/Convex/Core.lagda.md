---
title: Convex Functions
---

<details>
<summary>Imports</summary>

```agda
{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_; suc)
open import Algebra.Ordered.Total.Bundles
open import Algebra.Module.Bundles

module Function.Convex.Core
  {r ℓr₁ ℓr₂ : Level}
  (𝓡 : ToSemiring r ℓr₁ ℓr₂)
  {m ℓm : Level}
  (𝓜 : LeftSemimodule (ToSemiring.semiring 𝓡) m ℓm)
  where

open ToSemiring 𝓡 renaming (Carrier to R)
open LeftSemimodule 𝓜 renaming (Carrierᴹ to M)

-- stdlib
open import Data.Product using (_,_; ∃)
open import Function using (Congruent)
```

</details>

```agda
private
  R⁺ : Set _
  R⁺ = ∃ (0# ≤_)

IsConvex : (M → R) → Set (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ m)
IsConvex φ =
    ∀ (p⁺@(p , _) q⁺@(q , _) : R⁺)
  → p + q ≈ 1#
  → ∀ (x y : M)
  → φ (p *ₗ x +ᴹ q *ₗ y) ≤ p * φ x + q * φ y

record Convex : Set (r ⊔ ℓr₁ ⊔ ℓr₂ ⊔ m ⊔ ℓm) where
  constructor mk
  field
    f : M → R
    cong : Congruent _≈ᴹ_ _≈_ f
    isConvex : IsConvex f
```

```agda
⟦_⟧ : Convex → (M → R)
⟦ mk f _ _ ⟧ = f
```
