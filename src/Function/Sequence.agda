{-# OPTIONS --safe --cubical-compatible #-}

open import Level using (Level; _⊔_)

module Function.Sequence where

open import Data.Fin using (toℕ)
open import Data.Nat using (ℕ; _+_); open ℕ
open import Data.Product using (_×_; _,_)
open import Data.Vec.Functional using (Vector ; foldr; [])
open import Data.Fin using (toℕ)
open import Function using (_∘_)
open import Relation.Binary

private
  variable
    a b c : Level
    A : Set a
    B : Set b
    C : Set c

Sequence : Set a → Set _
Sequence A = ℕ → A

scan : (A → A → A) → Sequence A → Sequence A
scan _ f 0 = f 0
scan _∙_ f (suc n) = f (suc n) ∙ scan _∙_ f n

take : Sequence A → (n : ℕ) → Vector A n
take f 0 = []
take f (suc n) = f ∘ toℕ

tail : Sequence A → (n : ℕ) → Sequence A
tail f n = f ∘ (n +_)

splitAt : Sequence A → (n : ℕ) → Vector A n × Sequence A
splitAt f n = take f n , tail f n

lookup : Sequence A → ℕ → A
lookup f = f

map : (A → B) → Sequence A → Sequence B
map f g = f ∘ g

pure : A → Sequence A
pure a _ = a

_⟨*⟩_ : Sequence (A → B) → Sequence A → Sequence B
f ⟨*⟩ g = λ i → f i (g i)

lift₂ : (A → B → C) → Sequence A → Sequence B → Sequence C
lift₂ _∙_ f = map (_∙_) f ⟨*⟩_

private
  open import Relation.Binary.PropositionalEquality using (_≡_ ; refl)

  ex₀ : Sequence ℕ
  ex₀ _ = 0

  ex₁ : Sequence ℕ
  ex₁ _ = 1

  ex₂ : Sequence ℕ
  ex₂ i = i

  ∑ = foldr _+_ 0

  _ : ∑ (take ex₀ 20) ≡ 0 ; _ = refl
  _ : ∑ (take (scan _+_ ex₀) 100) ≡ 0 ; _ = refl
  _ : ∑ (take ex₁ 5) ≡ 5 ; _ = refl
  -- 0 + 1 + 2 + 3 + 4
  _ : ∑ (take (scan _+_ ex₁) 5) ≡ 15 ; _ = refl

  -- 0 + 1 + 2
  _ : ∑ (take ex₂ 3) ≡ 3 ; _ = refl
  -- 0 + 1 + 2 + 3 + 4
  _ : ∑ (take ex₂ 5) ≡ 10 ; _ = refl
  _ : ∑ (take (scan _+_ ex₂) 0) ≡ 0 ; _ = refl
  _ : ∑ (take (scan _+_ ex₂) 1) ≡ 0 ; _ = refl
  _ : ∑ (take (scan _+_ ex₂) 2) ≡ 1 ; _ = refl
  -- 0 + 1 + 3
  _ : ∑ (take (scan _+_ ex₂) 3) ≡ 4 ; _ = refl

  _ : ∑ (take (tail ex₂ 1) 1) ≡ 1 ; _ = refl
  -- orig   : 0, 1 ,2 , 3, ...
  -- tail 2 : 2, 3, ...
  _ : ∑ (take (tail ex₂ 2) 2) ≡ 5 ; _ = refl
  _ : lookup ex₂ 4 ≡ 4 ; _ = refl


module _ {ℓ₁ ℓ₂}
  (_≈_ : Rel A ℓ₁)
  (_≤_ : Rel A ℓ₂)
  where

  open import Relation.Binary.Construct.NonStrictToStrict _≈_ _≤_

  Monotonic↗ : Sequence A → Set ℓ₂
  Monotonic↗ aₙ = ∀ (n : ℕ) → aₙ n ≤ aₙ (suc n)

  StrictMonotonic↗ : Sequence A → Set (ℓ₁ ⊔ ℓ₂)
  StrictMonotonic↗ aₙ = ∀ (n : ℕ) → aₙ n < aₙ (suc n)
