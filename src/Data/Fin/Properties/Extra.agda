
{-# OPTIONS --safe --cubical-compatible #-}

module Data.Fin.Properties.Extra where

open import Data.Fin using (Fin); open Fin
open import Data.Fin.Properties as Fin using ()
open import Level using (0ℓ)
open import Function
open import Function.Construct.Composition
open import Function.Construct.Symmetry
open import Data.Unit.Polymorphic
open import Data.Unit.Polymorphic.Properties using (⊤↔⊤*)

1↔⊤ : Fin 1 ↔ (⊤ {0ℓ})
1↔⊤ = ↔-sym ⊤↔⊤* ↔-∘ Fin.1↔⊤
