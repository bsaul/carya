{-# OPTIONS --safe --cubical-compatible #-}

module Data.Rational.To.Algebra where

open import Algebra.Structures using (IsMagma)
open import Algebra.Ordered.Total.Bundles
open import Algebra.Ordered.Total.Structures
open import Data.Product using (_,_)
open import Data.Rational
open import Data.Rational.Properties
open import Level using (0ℓ)
open import Relation.Binary.PropositionalEquality using (_≡_)

+-isToMagma : IsToMagma _≡_ _≤_ _+_
+-isToMagma = record
  { isTotalOrder = ≤-isTotalOrder
  ; ∙-cong = IsMagma.∙-cong +-isMagma
  ; ∙-compat = λ {_} {_} {r} → +-monoˡ-≤ r
  }

+-isToSemigroup : IsToSemigroup _≡_ _≤_ _+_
+-isToSemigroup = record
   { isToMagma = +-isToMagma
   ; assoc = +-assoc
   }

+-isToMonoid : IsToMonoid _≡_ _≤_ _+_ 0ℚ
+-isToMonoid  = record
   { isToSemigroup = +-isToSemigroup
   ; identity = +-identity
   }

+-isToCommutativeMonoid : IsToCommutativeMonoid _≡_ _≤_ _+_ 0ℚ
+-isToCommutativeMonoid  = record
   { isToMonoid = +-isToMonoid
   ; comm = +-comm
   }

+-*-isToSemiringWithoutAnnihilatingZero :
  IsToSemiringWithoutAnnihilatingZero _≡_ _≤_ _+_ _*_ 0ℚ 1ℚ
+-*-isToSemiringWithoutAnnihilatingZero = record
  { +-isToCommutativeMonoid = +-isToCommutativeMonoid
  ; *-cong = IsMagma.∙-cong *-isMagma
  ; *-assoc = *-assoc
  ; *-identity = *-identity
  ; distrib = *-distrib-+
  ; *-compat = λ {x} {y} {z} x≤y 0≤z
    → *-monoʳ-≤-nonNeg z {{ nonNegative 0≤z }} x≤y
    , *-monoˡ-≤-nonNeg z {{ nonNegative 0≤z }} x≤y
  }

+-*-isToSemiring : IsToSemiring _≡_ _≤_ _+_ _*_ 0ℚ 1ℚ
+-*-isToSemiring = record
  { isToSemiringWithoutAnnihilatingZero = +-*-isToSemiringWithoutAnnihilatingZero
  ; zero = *-zero
  }

+-*-isToCommutativeSemiring : IsToCommutativeSemiring _≡_ _≤_ _+_ _*_ 0ℚ 1ℚ
+-*-isToCommutativeSemiring = record
  { isToSemiring = +-*-isToSemiring
  ; *-comm = *-comm
  }

toSemiring : ToSemiring 0ℓ 0ℓ 0ℓ
toSemiring = record { isToSemiring = +-*-isToSemiring }

toCommutativeSemiring : ToCommutativeSemiring 0ℓ 0ℓ 0ℓ
toCommutativeSemiring = record
 { isToCommutativeSemiring = +-*-isToCommutativeSemiring }
