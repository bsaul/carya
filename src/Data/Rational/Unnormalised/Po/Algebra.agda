{-# OPTIONS --safe --cubical-compatible #-}

module Data.Rational.Unnormalised.Po.Algebra where

open import Algebra.Structures using (IsMagma)
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Ordered.Partial.Structures
open import Data.Integer as ℤ using ()
open import Data.Product using (_,_)
open import Data.Rational.Unnormalised
open import Data.Rational.Unnormalised.Properties
open import Level using (0ℓ)
open import Relation.Binary.PropositionalEquality using (_≡_)

+-isPoMagma : IsPoMagma _≃_ _≤_ _+_
+-isPoMagma = record
  { isPartialOrder =  ≤-isPartialOrder
  ; ∙-cong = IsMagma.∙-cong +-isMagma
  ; ∙-compat = λ {_} {_} {r} → +-monoˡ-≤ r
  }

+-isPoSemigroup : IsPoSemigroup _≃_ _≤_ _+_
+-isPoSemigroup = record
   { isPoMagma = +-isPoMagma
   ; assoc = +-assoc
   }

+-isPoMonoid : IsPoMonoid _≃_ _≤_ _+_ 0ℚᵘ
+-isPoMonoid  = record
   { isPoSemigroup = +-isPoSemigroup
   ; identity = +-identity
   }

+-isPoCommutativeMonoid : IsPoCommutativeMonoid _≃_ _≤_ _+_ 0ℚᵘ
+-isPoCommutativeMonoid  = record
   { isPoMonoid = +-isPoMonoid
   ; comm = +-comm
   }

+-isPoGroup : IsPoGroup _≃_ _≤_ _+_ 0ℚᵘ (-_)
+-isPoGroup = record
  { isPoMonoid = +-isPoMonoid
  ; inverse = +-inverse
  ; ⁻¹-cong = -‿cong
  }

+-isPoAbelianGroup : IsPoAbelianGroup _≃_ _≤_ _+_ 0ℚᵘ (-_)
+-isPoAbelianGroup = record
  { isPoGroup = +-isPoGroup
  ; comm = +-comm
  }

+-*-isPoSemiringWithoutAnnihilatingZero :
  IsPoSemiringWithoutAnnihilatingZero _≃_ _≤_ _+_ _*_ 0ℚᵘ 1ℚᵘ
+-*-isPoSemiringWithoutAnnihilatingZero = record
  { +-isPoCommutativeMonoid = +-isPoCommutativeMonoid
  ; *-cong = IsMagma.∙-cong *-isMagma
  ; *-assoc = *-assoc
  ; *-identity = *-identity
  ; distrib = *-distrib-+
  ; *-compat = λ {x} {y} {z} x≤y 0≤z →
       *-monoˡ-≤-nonNeg z {{ nonNegative 0≤z }} x≤y
     , *-monoʳ-≤-nonNeg z {{ nonNegative 0≤z }} x≤y
  }

+-*-isPoSemiring : IsPoSemiring _≃_ _≤_ _+_ _*_ 0ℚᵘ 1ℚᵘ
+-*-isPoSemiring = record
  { isPoSemiringWithoutAnnihilatingZero = +-*-isPoSemiringWithoutAnnihilatingZero
  ; zero = *-zero
  }

+-*-isPoCommutativeSemiring : IsPoCommutativeSemiring _≃_ _≤_ _+_ _*_ 0ℚᵘ 1ℚᵘ
+-*-isPoCommutativeSemiring = record
  { isPoSemiring = +-*-isPoSemiring
  ; *-comm = *-comm
  }

+-*-isPoRing : IsPoRing _≃_ _≤_ _+_ _*_ -_ 0ℚᵘ 1ℚᵘ
+-*-isPoRing = record
  { +-isPoAbelianGroup = +-isPoAbelianGroup
  ; *-cong = *-cong
  ; *-assoc = *-assoc
  ; *-identity = *-identity
  ; distrib = *-distrib-+
  ; *-compat = λ {x} {y} {z} x≤y 0≤z →
       *-monoˡ-≤-nonNeg z {{ nonNegative 0≤z }} x≤y
     , *-monoʳ-≤-nonNeg z {{ nonNegative 0≤z }} x≤y
  }

+-*-isPoCommutativeRing : IsPoCommutativeRing _≃_ _≤_ _+_ _*_ -_ 0ℚᵘ 1ℚᵘ
+-*-isPoCommutativeRing = record
  { isPoRing = +-*-isPoRing
  ; *-comm = *-comm
  }

poSemiring : PoSemiring 0ℓ 0ℓ 0ℓ
poSemiring = record { isPoSemiring = +-*-isPoSemiring }

poCommutativeSemiring : PoCommutativeSemiring 0ℓ 0ℓ 0ℓ
poCommutativeSemiring = record
 { isPoCommutativeSemiring = +-*-isPoCommutativeSemiring }

poRing : PoRing 0ℓ 0ℓ 0ℓ
poRing = record { isPoRing = +-*-isPoRing }

poCommutativeRing : PoCommutativeRing 0ℓ 0ℓ 0ℓ
poCommutativeRing = record { isPoCommutativeRing = +-*-isPoCommutativeRing }
