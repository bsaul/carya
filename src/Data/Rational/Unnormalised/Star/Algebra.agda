{-# OPTIONS --safe --cubical-compatible #-}

module Data.Rational.Unnormalised.Star.Algebra where

open import Algebra.Structures
open import Algebra.Star.Bundles
open import Algebra.Star.Structures
open import Data.Product using (_,_)
open import Data.Rational.Unnormalised
open import Data.Rational.Unnormalised.Properties
open import Function using (id)
open import Level using (0ℓ)
-- open import Relation.Binary.PropositionalEquality
  -- using (_≃_; refl)

+-isStar : IsStar _≃_ _+_ id
+-isStar = record
  { ⋆-cong = id
  ; ⋆-involutive = λ _ → ≃-refl
  ; ⋆-anti-homo-∙ = +-comm
  }

+-isStarMagma : IsStarMagma _≃_ _+_ id
+-isStarMagma = record
  { isMagma = +-isMagma
  ; isStar = +-isStar
  }

+-isStarSemigroup : IsStarSemigroup _≃_ _+_ id
+-isStarSemigroup = record
  { isSemigroup = +-isSemigroup
  ; isStar = +-isStar
  }

*-isStar : IsStar _≃_ _*_ id
*-isStar = record
  { ⋆-cong = id
  ; ⋆-involutive = λ _ → ≃-refl
  ; ⋆-anti-homo-∙ = *-comm
  }

+-*-isStarSemiring : IsStarSemiring _≃_ _+_ _*_ id 0ℚᵘ 1ℚᵘ
+-*-isStarSemiring = record
  { +-isStar = +-isStar
  ; ⋆-anti-homo-* = IsStar.⋆-anti-homo-∙ *-isStar
  ; isSemiring = IsRing.isSemiring +-*-isRing
  }

+-*-isStarCommutativeSemiring : IsStarCommutativeSemiring _≃_ _+_ _*_ id 0ℚᵘ 1ℚᵘ
+-*-isStarCommutativeSemiring = record
  { +-isStar = +-isStar
  ; ⋆-anti-homo-* = IsStar.⋆-anti-homo-∙ *-isStar
  ; isCommutativeSemiring = IsCommutativeRing.isCommutativeSemiring +-*-isCommutativeRing
  }

+-*-isConjugateStarCommutativeSemiring :
  IsConjugateStarCommutativeSemiring _≃_ _+_ _*_ id 0ℚᵘ 1ℚᵘ
+-*-isConjugateStarCommutativeSemiring = record
  { *-conjugateSymmetric = λ {x} {y} → *-comm x y
  ; isStarCommutativeSemiring = +-*-isStarCommutativeSemiring
  }

+-*-isStarRing : IsStarRing _≃_ _+_ _*_ id -_ 0ℚᵘ 1ℚᵘ
+-*-isStarRing = record
  { +-isStar = +-isStar
  ; ⋆-anti-homo-* = IsStar.⋆-anti-homo-∙ *-isStar
  ; isRing = +-*-isRing
  }

+-*-isStarCommutativeRing : IsStarCommutativeRing _≃_ _+_ _*_ id -_ 0ℚᵘ 1ℚᵘ
+-*-isStarCommutativeRing = record
  { +-isStar = +-isStar
  ; ⋆-anti-homo-* = IsStar.⋆-anti-homo-∙ *-isStar
  ; isCommutativeRing = +-*-isCommutativeRing
  }

+-*-isConjugateStarCommutativeRing :
  IsConjugateStarCommutativeRing _≃_ _+_ _*_ id -_ 0ℚᵘ 1ℚᵘ
+-*-isConjugateStarCommutativeRing = record
  { *-conjugateSymmetric = λ {x} {y} → *-comm x y
  ; isStarCommutativeRing = +-*-isStarCommutativeRing
  }

ℚStarCommutativeSemiring : StarCommutativeSemiring _ _
ℚStarCommutativeSemiring = record
  { isStarCommutativeSemiring = +-*-isStarCommutativeSemiring }

ℚConjugateStarCommutativeSemiring : ConjugateStarCommutativeSemiring _ _
ℚConjugateStarCommutativeSemiring = record
  { isConjugateStarCommutativeSemiring = +-*-isConjugateStarCommutativeSemiring }

ℚConjugateStarCommutativeRing : ConjugateStarCommutativeRing _ _
ℚConjugateStarCommutativeRing = record
  { isConjugateStarCommutativeRing = +-*-isConjugateStarCommutativeRing }
