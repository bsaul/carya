{-# OPTIONS --safe --cubical-compatible #-}

module Data.Rational.Unnormalised.Star.To.Algebra where

open import Algebra.Structures
open import Algebra.Star.Ordered.Total.Bundles
open import Algebra.Star.Ordered.Total.Structures
open import Data.Product using (_,_)
open import Data.Rational.Unnormalised
open import Data.Rational.Unnormalised.Properties
open import Data.Rational.Unnormalised.Star.Algebra
open import Data.Rational.Unnormalised.To.Algebra
open import Function using (id)
open import Level using (0ℓ)


+-*-isToStarSemiring : IsToStarSemiring _≃_ _≤_ _+_ _*_ id 0ℚᵘ 1ℚᵘ
+-*-isToStarSemiring = record
  { +-isStar = +-isStar
  ; ⋆-anti-homo-* = *-comm
  ; isToSemiring = +-*-isToSemiring
  }

+-*-isToStarCommutativeSemiring : IsToStarCommutativeSemiring _≃_ _≤_ _+_ _*_ id 0ℚᵘ 1ℚᵘ
+-*-isToStarCommutativeSemiring = record
  { +-isStar = +-isStar
  ; ⋆-anti-homo-* = *-comm
  ; isToCommutativeSemiring = +-*-isToCommutativeSemiring
  }

+-*-isToStarRing : IsToStarRing _≃_ _≤_ _+_ _*_ id -_ 0ℚᵘ 1ℚᵘ
+-*-isToStarRing = record
  { +-isStar = +-isStar
  ; ⋆-anti-homo-* = *-comm
  ; isToRing = +-*-isToRing
  }

+-*-isToStarCommutativeRing : IsToStarCommutativeRing _≃_ _≤_ _+_ _*_ id -_ 0ℚᵘ 1ℚᵘ
+-*-isToStarCommutativeRing = record
  { +-isStar = +-isStar
  ; ⋆-anti-homo-* = *-comm
  ; isToCommutativeRing = +-*-isToCommutativeRing
  }

+-*-isToConjugateStarCommutativeRing : IsToConjugateStarCommutativeRing _≃_ _≤_ _+_ _*_ id -_ 0ℚᵘ 1ℚᵘ
+-*-isToConjugateStarCommutativeRing = record
  { *-conjugateSymmetric = λ {x} {y} → *-comm x y
  ; isToStarCommutativeRing = +-*-isToStarCommutativeRing
  }

ℚToConjugateStarCommutativeRing : ToConjugateStarCommutativeRing 0ℓ 0ℓ 0ℓ
ℚToConjugateStarCommutativeRing = record
  { isToConjugateStarCommutativeRing = +-*-isToConjugateStarCommutativeRing }
