{-# OPTIONS --safe --cubical-compatible #-}

module Data.Rational.Unnormalised.Extra where

open import Data.Integer.Base as ℤ
  using (ℤ; +_; nonNegative ; nonNeg )
open import Data.Integer.Properties as ℤ using ()
open import Data.Nat as ℕ using ()
open import Data.Nat.Properties as ℕ using ()
open import Data.Rational.Unnormalised as ℚᵘ
open import Data.Rational.Unnormalised.Properties as ℚᵘ

open import Algebra.Bundles using (Ring)
open import Algebra.Properties.Semiring.Mult (Ring.semiring +-*-ring)

open import Relation.Binary.PropositionalEquality
import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

instance

  nonNegFrac : ∀ {n} .⦃  _ : ℕ.NonZero n ⦄ → NonNegative (+ 1 / n)
  nonNegFrac {ℕ.suc n} = ℤ.nonNegative {i = ↥ (+ 1 / ℕ.suc n)} (ℤ.+≤+ ℕ.z≤n)

1/n≈1/[n/1] : ∀ {n} .⦃ _ : ℕ.NonZero n ⦄ → + 1 / n ≃ 1/ (+ n / 1)
1/n≈1/[n/1] {ℕ.suc n} = ℚᵘ.≃-refl

1/n*n≈1 : ∀ {n} .⦃ _ : ℕ.NonZero n ⦄ → (+ 1 / n) * (+ n / 1) ≃ 1ℚᵘ
1/n*n≈1 {n} =
  begin
    (+ 1 / n) * (+ n / 1)
  ≈⟨ *-congʳ 1/n≈1/[n/1] ⟩
    1/ (+ n / 1) * (+ n / 1)
  ≈⟨ *-inverseˡ (+ n / 1) ⟩
    1ℚᵘ
  ∎
  where open ≃-Reasoning

0≤1/n : ∀ {n} .⦃  _ : ℕ.NonZero n ⦄ → 0ℚᵘ ≤ (+ 1 / n)
0≤1/n {n} = ℚᵘ.nonNegative⁻¹ (+ 1 / n)

1/n≃1/n : ∀ {n} → 1ℚᵘ + (+ n / 1) ≃ + ℕ.suc n / 1
1/n≃1/n {ℕ.zero} = ≃-refl
1/n≃1/n {ℕ.suc n} =
  begin
   1ℚᵘ + (+ ℕ.suc n / 1)
  ≈⟨ *≡* (cong +_ (cong (λ x → ℕ.suc (ℕ.suc x)) (ℕ.*-identityʳ _))) ⟩
    + ℕ.suc (ℕ.suc n) / 1
  ∎
  where open ≈-Reasoning ≃-setoid

n≃n/1 : ∀ {n} → n × 1ℚᵘ ≃ (+ n / 1)
n≃n/1 {ℕ.zero} = ≃-refl
n≃n/1 {ℕ.suc n} =
  begin
   ℕ.suc n × 1ℚᵘ
  ≡⟨⟩
    1ℚᵘ + n × 1ℚᵘ
  ≈⟨ +-congʳ 1ℚᵘ (n≃n/1 {n}) ⟩
    1ℚᵘ + (+ n / 1)
  ≈⟨ 1/n≃1/n {n} ⟩
    + ℕ.suc n / 1
  ∎
  where open ≈-Reasoning ≃-setoid
