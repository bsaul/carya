{-# OPTIONS --safe --cubical-compatible #-}

module Data.Rational.Unnormalised.To.Algebra where

open import Algebra.Structures using (IsMagma)
open import Algebra.Ordered.Total.Bundles
open import Algebra.Ordered.Total.Structures
open import Data.Integer as ℤ using ()
open import Data.Product using (_,_)
open import Data.Rational.Unnormalised
open import Data.Rational.Unnormalised.Properties
open import Level using (0ℓ)
open import Relation.Binary.PropositionalEquality using (_≡_)

+-isToMagma : IsToMagma _≃_ _≤_ _+_
+-isToMagma = record
  { isTotalOrder =  ≤-isTotalOrder
  ; ∙-cong = IsMagma.∙-cong +-isMagma
  ; ∙-compat = λ {_} {_} {r} → +-monoˡ-≤ r
  }

+-isToSemigroup : IsToSemigroup _≃_ _≤_ _+_
+-isToSemigroup = record
   { isToMagma = +-isToMagma
   ; assoc = +-assoc
   }

+-isToMonoid : IsToMonoid _≃_ _≤_ _+_ 0ℚᵘ
+-isToMonoid  = record
   { isToSemigroup = +-isToSemigroup
   ; identity = +-identity
   }

+-isToCommutativeMonoid : IsToCommutativeMonoid _≃_ _≤_ _+_ 0ℚᵘ
+-isToCommutativeMonoid  = record
   { isToMonoid = +-isToMonoid
   ; comm = +-comm
   }

+-isToGroup : IsToGroup _≃_ _≤_ _+_ 0ℚᵘ (-_)
+-isToGroup = record
  { isToMonoid = +-isToMonoid
  ; inverse = +-inverse
  ; ⁻¹-cong = -‿cong
  }

+-isToAbelianGroup : IsToAbelianGroup _≃_ _≤_ _+_ 0ℚᵘ (-_)
+-isToAbelianGroup = record
  { isToGroup = +-isToGroup
  ; comm = +-comm
  }

+-*-isToSemiringWithoutAnnihilatingZero :
  IsToSemiringWithoutAnnihilatingZero _≃_ _≤_ _+_ _*_ 0ℚᵘ 1ℚᵘ
+-*-isToSemiringWithoutAnnihilatingZero = record
  { +-isToCommutativeMonoid = +-isToCommutativeMonoid
  ; *-cong = IsMagma.∙-cong *-isMagma
  ; *-assoc = *-assoc
  ; *-identity = *-identity
  ; distrib = *-distrib-+
  ; *-compat = λ {x} {y} {z} x≤y 0≤z →
       *-monoˡ-≤-nonNeg z {{ nonNegative 0≤z }} x≤y
     , *-monoʳ-≤-nonNeg z {{ nonNegative 0≤z }} x≤y
  }

+-*-isToSemiring : IsToSemiring _≃_ _≤_ _+_ _*_ 0ℚᵘ 1ℚᵘ
+-*-isToSemiring = record
  { isToSemiringWithoutAnnihilatingZero = +-*-isToSemiringWithoutAnnihilatingZero
  ; zero = *-zero
  }

+-*-isToCommutativeSemiring : IsToCommutativeSemiring _≃_ _≤_ _+_ _*_ 0ℚᵘ 1ℚᵘ
+-*-isToCommutativeSemiring = record
  { isToSemiring = +-*-isToSemiring
  ; *-comm = *-comm
  }

+-*-isToRing : IsToRing _≃_ _≤_ _+_ _*_ -_ 0ℚᵘ 1ℚᵘ
+-*-isToRing = record
  { +-isToAbelianGroup = +-isToAbelianGroup
  ; *-cong = *-cong
  ; *-assoc = *-assoc
  ; *-identity = *-identity
  ; distrib = *-distrib-+
  ; *-compat = λ {x} {y} {z} x≤y 0≤z →
       *-monoˡ-≤-nonNeg z {{ nonNegative 0≤z }} x≤y
     , *-monoʳ-≤-nonNeg z {{ nonNegative 0≤z }} x≤y
  }

+-*-isToCommutativeRing : IsToCommutativeRing _≃_ _≤_ _+_ _*_ -_ 0ℚᵘ 1ℚᵘ
+-*-isToCommutativeRing = record
  { isToRing = +-*-isToRing
  ; *-comm = *-comm
  }

toSemiring : ToSemiring 0ℓ 0ℓ 0ℓ
toSemiring = record { isToSemiring = +-*-isToSemiring }

toCommutativeSemiring : ToCommutativeSemiring 0ℓ 0ℓ 0ℓ
toCommutativeSemiring = record
 { isToCommutativeSemiring = +-*-isToCommutativeSemiring }

toRing : ToRing 0ℓ 0ℓ 0ℓ
toRing = record { isToRing = +-*-isToRing }

toCommutativeRing : ToCommutativeRing 0ℓ 0ℓ 0ℓ
toCommutativeRing = record { isToCommutativeRing = +-*-isToCommutativeRing }
