{-# OPTIONS --safe --cubical-compatible #-}

module Data.Rational.Po.Algebra where

open import Algebra.Structures using (IsMagma)
open import Algebra.Ordered.Partial.Bundles
open import Algebra.Ordered.Partial.Structures
open import Data.Product using (_,_)
open import Data.Rational
open import Data.Rational.Properties
open import Level using (0ℓ)
open import Relation.Binary.PropositionalEquality using (_≡_)

+-isPoMagma : IsPoMagma _≡_ _≤_ _+_
+-isPoMagma = record
  { isPartialOrder =  ≤-isPartialOrder
  ; ∙-cong = IsMagma.∙-cong +-isMagma
  ; ∙-compat = λ {_} {_} {r} → +-monoˡ-≤ r
  }

+-isPoSemigroup : IsPoSemigroup _≡_ _≤_ _+_
+-isPoSemigroup = record
   { isPoMagma = +-isPoMagma
   ; assoc = +-assoc
   }

+-isPoMonoid : IsPoMonoid _≡_ _≤_ _+_ 0ℚ
+-isPoMonoid  = record
   { isPoSemigroup = +-isPoSemigroup
   ; identity = +-identity
   }

+-isPoCommutativeMonoid : IsPoCommutativeMonoid _≡_ _≤_ _+_ 0ℚ
+-isPoCommutativeMonoid  = record
   { isPoMonoid = +-isPoMonoid
   ; comm = +-comm
   }

+-*-isPoSemiringWithoutAnnihilatingZero :
  IsPoSemiringWithoutAnnihilatingZero _≡_ _≤_ _+_ _*_ 0ℚ 1ℚ
+-*-isPoSemiringWithoutAnnihilatingZero = record
  { +-isPoCommutativeMonoid = +-isPoCommutativeMonoid
  ; *-cong = IsMagma.∙-cong *-isMagma
  ; *-assoc = *-assoc
  ; *-identity = *-identity
  ; distrib = *-distrib-+
  ; *-compat = λ {x} {y} {z} x≤y 0≤z
    → *-monoʳ-≤-nonNeg z {{ nonNegative 0≤z }} x≤y
    , *-monoˡ-≤-nonNeg z {{ nonNegative 0≤z }} x≤y
  }

+-*-isPoSemiring : IsPoSemiring _≡_ _≤_ _+_ _*_ 0ℚ 1ℚ
+-*-isPoSemiring = record
  { isPoSemiringWithoutAnnihilatingZero = +-*-isPoSemiringWithoutAnnihilatingZero
  ; zero = *-zero
  }

+-*-isPoCommutativeSemiring : IsPoCommutativeSemiring _≡_ _≤_ _+_ _*_ 0ℚ 1ℚ
+-*-isPoCommutativeSemiring = record
  { isPoSemiring = +-*-isPoSemiring
  ; *-comm = *-comm
  }

poSemiring : PoSemiring 0ℓ 0ℓ 0ℓ
poSemiring = record { isPoSemiring = +-*-isPoSemiring }

poCommutativeSemiring : PoCommutativeSemiring 0ℓ 0ℓ 0ℓ
poCommutativeSemiring = record
 { isPoCommutativeSemiring = +-*-isPoCommutativeSemiring }
