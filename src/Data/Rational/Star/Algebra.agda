{-# OPTIONS --safe --cubical-compatible #-}

module Data.Rational.Star.Algebra where

open import Algebra.Structures
open import Algebra.Star.Bundles
open import Algebra.Star.Structures
open import Data.Product using (_,_)
open import Data.Rational
open import Data.Rational.Properties
open import Function using (id)
open import Level using (0ℓ)
open import Relation.Binary.PropositionalEquality
  using (_≡_; refl)

+-isStar : IsStar _≡_ _+_ id
+-isStar = record
  { ⋆-cong = id
  ; ⋆-involutive = λ _ → refl
  ; ⋆-anti-homo-∙ = +-comm
  }

+-isStarMagma : IsStarMagma _≡_ _+_ id
+-isStarMagma = record
  { isMagma = +-isMagma
  ; isStar = +-isStar
  }

+-isStarSemigroup : IsStarSemigroup _≡_ _+_ id
+-isStarSemigroup = record
  { isSemigroup = +-isSemigroup
  ; isStar = +-isStar
  }

*-isStar : IsStar _≡_ _*_ id
*-isStar = record
  { ⋆-cong = id
  ; ⋆-involutive = λ _ → refl
  ; ⋆-anti-homo-∙ = *-comm
  }

+-*-isStarSemiring : IsStarSemiring _≡_ _+_ _*_ id 0ℚ 1ℚ
+-*-isStarSemiring = record
  { +-isStar = +-isStar
  ; ⋆-anti-homo-* = IsStar.⋆-anti-homo-∙ *-isStar
  ; isSemiring = IsRing.isSemiring +-*-isRing
  }

+-*-isStarCommutativeSemiring : IsStarCommutativeSemiring _≡_ _+_ _*_ id 0ℚ 1ℚ
+-*-isStarCommutativeSemiring = record
  { +-isStar = +-isStar
  ; ⋆-anti-homo-* = IsStar.⋆-anti-homo-∙ *-isStar
  ; isCommutativeSemiring = IsCommutativeRing.isCommutativeSemiring +-*-isCommutativeRing
  }

+-*-isConjugateStarCommutativeSemiring :
  IsConjugateStarCommutativeSemiring _≡_ _+_ _*_ id 0ℚ 1ℚ
+-*-isConjugateStarCommutativeSemiring = record
  { *-conjugateSymmetric = λ {x} {y} → *-comm x y
  ; isStarCommutativeSemiring = +-*-isStarCommutativeSemiring
  }

ℚStarCommutativeSemiring : StarCommutativeSemiring _ _
ℚStarCommutativeSemiring = record
  { isStarCommutativeSemiring = +-*-isStarCommutativeSemiring }

ℚConjugateStarCommutativeSemiring : ConjugateStarCommutativeSemiring _ _
ℚConjugateStarCommutativeSemiring = record
  { isConjugateStarCommutativeSemiring = +-*-isConjugateStarCommutativeSemiring }
