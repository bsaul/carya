{-# OPTIONS --safe --cubical-compatible #-}

module Data.Rational.Extra where

open import Data.Integer.Base as ℤ
  using (ℤ; +_; nonNegative ; nonNeg )
open import Data.Integer.Properties as ℤ using ()
open import Data.Nat as ℕ using ()
open import Data.Rational as ℚ
open import Data.Rational.Properties as ℚ
open import Data.Rational.Unnormalised.Extra as ℚᵘ using ()
-- open import Data.Rational.Unnormalised as ℚᵘ renaming (_/_ to _/ᵘ_; 1/_ to 1/ᵘ_)
-- open import Data.Rational.Unnormalised.Properties as ℚᵘ
open import Relation.Binary.PropositionalEquality
  using (_≡_; cong ; refl; module ≡-Reasoning)
-- import Relation.Binary.Reasoning.Setoid as ≈-Reasoning

-- instance

--   nonNegFracᵘ : ∀ {n} .⦃  _ : ℕ.NonZero n ⦄ → NonNegative (+ 1 /ᵘ n)
--   nonNegFracᵘ {ℕ.suc n} = ℤ.nonNegative {i = ↥ (+ 1 /ᵘ ℕ.suc n)} (ℤ.+≤+ ℕ.z≤n)

  -- fromℕ : ∀ {n} .⦃  _ : ℕ.NonZero n ⦄ → ℚ.NonZero (+ n / 1)
  -- fromℕ {ℕ.suc n} = >-nonZero {p  = + ℕ.suc n / 1} {!   !}

--   nonNegFrac : ∀ {n} .⦃  _ : ℕ.NonZero n ⦄ → ℚ.NonNegative (+ 1 / n)
--   nonNegFrac {ℕ.suc n} = ?


-- 1/n*n≡1 : ∀ {n} .⦃ _ : ℕ.NonZero n ⦄ → (+ 1 / n) * (+ n / 1) ≡ 1ℚ
-- 1/n*n≡1 {n} =
--   begin
--     (+ 1 / n) * (+ n / 1)
--   ≡⟨ ?  ⟩
--     1/ (+ n / 1) * (+ n / 1)
--   ≡⟨ *-inverseˡ (+ n / 1) ⟩
--     1ℚ
--   ∎
--   where open ≡-Reasoning


0≤1/n : ∀ {n} .⦃ _ : ℕ.NonZero n ⦄ → 0ℚ ≤ (+ 1 / n)
0≤1/n {n} = ℚ.nonNegative⁻¹ (+ 1 / n) ⦃ normalize-nonNeg 1 n ⦄
  -- 1/n≥0 n
