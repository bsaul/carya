{
  description = "Carya: linear algebra in Agda";
  nixConfig = {
    bash-prompt = "λ ";
  };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
    flake-utils.url = "github:numtide/flake-utils";
    nix-filter.url = "github:numtide/nix-filter";
    clethra.url = "sourcehut:~bradleysaul/clethra?ref=0.2.5";
    zizia.url = "sourcehut:~bradleysaul/zizia?ref=0.3.1";
  };

  outputs = { self, nixpkgs, flake-utils, nix-filter, zizia, clethra }:
    flake-utils.lib.eachDefaultSystem (system:
      let

        project = "carya";
        version = "0.1.2";

        pkgs = nixpkgs.legacyPackages.${system};

        filter = nix-filter.lib;

        agda-stdlib = pkgs.agdaPackages.standard-library;
        zizia-lib = zizia.packages.${system}.default;
        clethra-lib = clethra.packages.${system}.default;

        agdaPkgs = [
          agda-stdlib
          pkgs.agdaPackages.agda-categories
          zizia-lib
          clethra-lib
        ];

        agda = pkgs.agda.withPackages
          (p: agdaPkgs);
      in
      {

        formatter = pkgs.nixpkgs-fmt;

        checks.whitespace = pkgs.stdenvNoCC.mkDerivation {
          name = "check-whitespace";
          dontBuild = true;
          src = ./.;
          doCheck = true;
          checkPhase = ''
            ${pkgs.haskellPackages.fix-whitespace}/bin/fix-whitespace --check
          '';
          installPhase = ''mkdir "$out"'';
        };

        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            agda
            pandoc
            pkgs.haskellPackages.fix-whitespace
          ];
        };

        packages.default = pkgs.agdaPackages.mkDerivation {
          pname = project;
          version = version ;
          src = filter {
            root = ./.;
            include = [ "src" "lib" "carya.agda-lib" ];
          };

          everythingFile = "./src/Everything.agda";

          buildInputs = agdaPkgs;

          meta = with pkgs.lib; {
            description = "Linear maps in Agda";
            homepage = "https://gitlab.com/bsaul/carya";
          };
        };
      });
}
