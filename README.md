# `Carya`: Linear Algebra in `Agda`

## Library Organization

The following describes some of the modules found in the `src` directory:

* `Maps` contains code for various flavors of functions
  * `Prelinear`: functions between vector spaces
  * `Linear`: homomorphic `Prelinear` maps (i.e. linear maps)
  * `LinearForm` : `Linear` maps to the ring;
      i.e. the dual of a vector space
  * `Affine` : affine maps
  * `Bilinear`: binary maps that are linear in each argument
  * `PositiveLinear`: positive linear maps
  * each of these directories (generally) contain:
    * `Core`: definition of the map type, denotation and equivalence
    * `Base`: operations on the map type
    * `Properties`: properties of the map type
    * `Algebra`: basic algebra structures and bundles of the map type
    * `Module` : module space(s) of the map type itself.
    * `Category`: categorical structures/bundles of the map type
* `LinearSpace`: Vector (generally Module) space concepts
that involve linear maps in some way.
  * `Dual` : the dual of a vector space (linear forms/linear functionals)
  * `Bidual`: dual of the direct product of two vector spaces
  * `InnerProduct`: inner product spaces
  * `Submodule` : submodule spaces
* `LinearAlgebra`: contains code for concepts from linear algebra
  (which, of course, depend on `Maps`)
  * `Matrix`: matrices

## Inspirations

> Interested in formalizing the generation
of fast running code for linear algebra applications,
the authors show how an index-free,
calculational approach to matrix algebra can be developed
by regarding matrices as morphisms of a category with biproducts. This shifts the traditional view of matrices
as indexed structures to a type-level perspective analogous
to that of the pointfree algebra of programming.
The derivation of fusion, cancellation and abide laws from the biproduct equations makes it easy to calculate algorithms implementing matrix multiplication,
the central operation of matrix algebra,
ranging from
its divide-and-conquer version to its vectorization implementation.

-- from "Typing linear algebra: A biproduct-oriented approach"

> In several branches of science,
"alegbra" means "linear algebra":
the study of vector spaces and linear maps,
that is,
of the category of modules over a ring ${R}$
in the very special case in which $R$ is a *field*.
... we will stress that
*much of what can be done over a field
can in fact be done over less special rings*.

-- from *Algebra: Chapter 0* by Paolo Aluffi (emphasis mine)

> This prompts the question of
whether the various properties of determinants
should not really be developed in a more general setting,
and leads to the wider question of whether the scalars in
the definition of a vector space should not be restricted to lie in a field
but should more generally belong to a ring ...
It turns out that the modest generalisation
so suggested is of enormous importance and
leads to what is arguably the most important structure in the whole of algebra,
namely that of a module.

-- from *Module Theory: An Approach to Linear Algebra*
by T.S. Blyth

> When correctness concerns come as an afterthought
and correctness proofs have to be given once the program is already completed,
the programmer can indeed expect severe troubles.
If, however,
he adheres to the discipline
to produce the correctness proofs as he programs along,
he will produce program and proof with less effort
than programming alone would have taken.

-- from [Concern for Correctness as a Guiding Principle for Program Composition](https://www.cs.utexas.edu/users/EWD/transcriptions/EWD02xx/EWD288.html)
by Edsger W. Dijkstra

* [*Algebra: Chapter 0*](https://bookstore.ams.org/gsm-104/) by Paolo Aluffi
* [*Linear Algebra Done Right*](https://linear.axler.net/) by Sheldon Axler
* [Functional Linear Algebra in Agda](https://github.com/ryanorendorff/functional-linear-algebra)
* [*Typing linear algebra: A biproduct-oriented approach*](https://www.sciencedirect.com/science/article/pii/S0167642312001402)
* [*Picturing Quantum Processes*](https://www.cambridge.org/core/books/picturing-quantum-processes/1119568B3101F3A685BE832FEEC53E52)
by Bob Coecke and Alex Kissinger
* [*Module Theory: An Approach to Linear Algebra*](https://core.ac.uk/download/pdf/148789914.pdf) by T.S. Blyth
* [Compiling to Categories](http://conal.net/papers/compiling-to-categories/)

## Existing/Prior Art

* [LinearMaps Julia package](https://github.com/JuliaLinearAlgebra/LinearMaps.jl)
* [Lean Matrix Cookbook](https://github.com/eric-wieser/lean-matrix-cookbook)
* [COQMatrix](https://github.com/zhengpushi/CoqMatrix)
(see [paper](https://www.sciencedirect.com/science/article/abs/pii/S1383762123001650))
